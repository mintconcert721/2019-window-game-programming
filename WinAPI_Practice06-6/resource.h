//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// WinAPI_Practice06-6.rc에서 사용되고 있습니다.
//
#define IDD_DIALOG6_6                   101
#define IDC_RADIO_CIRCLE                1001
#define IDC_RADIO_RECT                  1002
#define IDC_BUTTON_DRAW                 1003
#define IDC_BUTTON_MOVE                 1004
#define IDC_BUTTON_STOP                 1005
#define IDC_BUTTON_DRAW4                1006
#define IDC_BUTTON_RESTART              1006
#define IDC_RADIO_COLOR1                1007
#define IDC_RADIO_COLOR2                1008
#define IDC_RADIO_COLOR3                1009
#define IDC_EXIT                        1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
