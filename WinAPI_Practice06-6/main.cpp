#include <Windows.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <iterator>
#include "resource.h"

using namespace std;

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

struct Point {
	int x;
	int y;

	Point() {}

	Point(int _x, int _y)
	{
		x = _x;
		y = _y;
	}

	Point(const Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;
	}

	Point& operator=(const Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}
};

struct Path {
	Point start;
	Point end;
	bool isMyTurn = false;

	void Show()
	{
		cout << start.x << ", " << start.y << ", " << end.x << ", " << end.y << endl;
	}

	Path() {}

	Path(int x1, int y1, int x2, int y2)
	{
		start.x = x1;
		start.y = y1;
		end.x = x2;
		end.y = y2;
	}

	Path(int x1, int y1, int x2, int y2, bool turn)
	{
		start.x = x1;
		start.y = y1;
		end.x = x2;
		end.y = y2;
		isMyTurn = turn;
	}

	Path(const Path& rhs)
	{
		start.x = rhs.start.x;
		start.y = rhs.start.y;
		end.x = rhs.end.x;
		end.y = rhs.end.y;
		isMyTurn = rhs.isMyTurn;
	}

	Path& operator=(const Path& rhs)
	{
		start.x = rhs.start.x;
		start.y = rhs.start.y;
		end.x = rhs.end.x;
		end.y = rhs.end.y;
		isMyTurn = rhs.isMyTurn;

		return *this;
	}
};

#define WINDOW_X 1280
#define WINDOW_Y 720

#define FRAME_X 135
#define FRAME_Y 150

#define COLOR1 0
#define COLOR2 1
#define COLOR3 2

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 6-6";

int g_ColorMode = 0;
bool isStop = true;
bool isDrawing = false;
bool isRectangle = false;
bool isBigger = true;

Point mouse;
Point g_CurrentPosition;

vector<Path> container;
vector<Path> recontainer;
vector<Point> moveContainer;

int index = 0;
int move_index = 0;
int shape_size = 20;

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
BOOL Dlg6_6Proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;
	static bool isDrag;

	static HBITMAP hBitmap, hOldBitmap;
	HWND hDlg = NULL;

	static HBRUSH hBrush;
	static HBRUSH hOldBrush;
	static Point length;
	static Point move;

	static int startX, startY, oldX, oldY;
	int endX, endY;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);
		hDlg = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_DIALOG6_6), hWnd, (DLGPROC)Dlg6_6Proc);
		ShowWindow(hDlg, SW_SHOW);

		container.resize(8);
		recontainer.resize(8);

		g_CurrentPosition.x = 0;
		g_CurrentPosition.y = 0;

		isDrag = false;

		SetTimer(hWnd, 0, 100, NULL);
	}
	break;
	case WM_TIMER:
	{
		if (isBigger)
		{
			shape_size += 1;
			if (shape_size > 30)
			{
				isBigger = false;
			}
		}
		else
		{
			shape_size -= 1;
			if (shape_size < 10)
			{
				isBigger = true;
			}
		}

		if (container[move_index].isMyTurn)
		{
			length.x = container[move_index].end.x - container[move_index].start.x;
			length.y = container[move_index].end.y - container[move_index].start.y;

			move.x = length.x / 10;
			move.y = length.y / 10;

			if (isStop == false)
			{
				g_CurrentPosition.x += move.x;
				g_CurrentPosition.y += move.y;
			}

			if ((abs(g_CurrentPosition.x - container[move_index].end.x) < 10) && (abs(g_CurrentPosition.y - container[move_index].end.y) < 10))
			{
				if (move_index < 8)
				{
					container[move_index].isMyTurn = false;
					move_index++;
					if (move_index == 8)
					{
						move_index = 0;
					}
					g_CurrentPosition.x = container[move_index].start.x;
					g_CurrentPosition.y = container[move_index].start.y;
					container[move_index].isMyTurn = true;
				}
			}
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);

		Rectangle(memdc, crt.left, crt.top, crt.right, crt.bottom);

		if (!container.empty())
		{
			for (auto& v : container)
			{
				MoveToEx(memdc, v.start.x, v.start.y, NULL); // 이동하고 선으로 연결
				LineTo(memdc, v.end.x, v.end.y);
			}
		}

		// 도형 색상 바꾸기
		{
			switch (g_ColorMode)
			{
			case COLOR1:
			{
				hBrush = CreateSolidBrush(COLORREF(RGB(255, 0, 0)));
			}
			break;
			case COLOR2:
			{
				hBrush = CreateSolidBrush(COLORREF(RGB(0, 255, 0)));
			}
			break;
			case COLOR3:
			{
				hBrush = CreateSolidBrush(COLORREF(RGB(0, 0, 255)));
			}
			break;
			}
		}

		hOldBrush = (HBRUSH)SelectObject(memdc, hBrush);
		
		// TODO:
		if (isRectangle)
		{
			Rectangle(memdc, g_CurrentPosition.x - shape_size, g_CurrentPosition.y - shape_size, g_CurrentPosition.x + shape_size, g_CurrentPosition.y + shape_size);
		}
		else
		{
			Ellipse(memdc, g_CurrentPosition.x - shape_size, g_CurrentPosition.y - shape_size, g_CurrentPosition.x + shape_size, g_CurrentPosition.y + shape_size);
		}

		SelectObject(memdc, hOldBrush);
		DeleteObject(hBrush);

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		mouse.x = LOWORD(lParam);
		mouse.y = HIWORD(lParam);
		startX = mouse.x;
		startY = mouse.y;

		isDrag = true;

		if (isDrawing)
		{
			if (index == 0)
			{
				container[index].start.x = mouse.x;
				container[index].start.y = mouse.y;
			}
		}
	}
	break;
	case WM_LBUTTONUP:
	{
		mouse.x = LOWORD(lParam);
		mouse.y = HIWORD(lParam);

		isDrag = false;
		if (isDrawing)
		{
			if (index < 7)
			{
				container[index].end.x = mouse.x;
				container[index].end.y = mouse.y;

				index++;

				container[index].start.x = container[index - 1].end.x;
				container[index].start.y = container[index - 1].end.y;
			}
			else
			{
				if (index == 7)
				{
					container[index].end.x = mouse.x;
					container[index].end.y = mouse.y;
					container.begin()->isMyTurn = true;

					g_CurrentPosition.x = container.begin()->start.x;
					g_CurrentPosition.y = container.begin()->start.y;

					isDrawing = false;
				}
			}
		}
	}
	break;
	case WM_MOUSEMOVE:
	{
		memdc = GetDC(hWnd);
		if (isDrag)
		{ // 흰 바탕
			SetROP2(memdc, R2_XORPEN); // 펜의 XOR 연산
			SelectObject(memdc, (HPEN)GetStockObject(WHITE_PEN)); // 흰 펜
			// 흰 바탕 XOR 흰 펜 = 검은색 펜
			endX = LOWORD(lParam);
			endY = HIWORD(lParam);
			MoveToEx(memdc, startX, startY, NULL);
			LineTo(memdc, oldX, oldY); // 지우기 : 흰 바탕 XOR 검은 펜 = 흰 선
			MoveToEx(memdc, startX, startY, NULL);
			LineTo(memdc, endX, endY); // 그리기 : 흰 바탕 XOR 흰 펜 = 검은 선
			oldX = endX; oldY = endY; // 현 지점을 이전 지점으로 설정
		}
		ReleaseDC(hWnd, memdc);

	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

BOOL Dlg6_6Proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_INITDIALOG:
	{
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_RADIO_CIRCLE:
		{
			isRectangle = false;
		}
		break;
		case IDC_RADIO_RECT:
		{
			isRectangle = true;
		}
		break;
		case IDC_BUTTON_MOVE:
		{
			if (isStop)
				isStop = false;
		}
		break;
		case IDC_BUTTON_STOP:
		{
			if (isStop == false)
				isStop = true;
		}
		break;
		case IDC_BUTTON_DRAW:
		{
			isDrawing = true;
		}
		break;
		case IDC_RADIO_COLOR1:
		{
			g_ColorMode = COLOR1;
		}
		break;
		case IDC_RADIO_COLOR2:
		{
			g_ColorMode = COLOR2;
		}
		break;
		case IDC_RADIO_COLOR3:
		{
			g_ColorMode = COLOR3;
		}
		break;
		case IDC_BUTTON_RESTART:
		{
			vector<Path>::iterator it = container.begin();

			while (it != container.end())
			{
				it = container.erase(it);
			}

			container = recontainer;

			move_index = 0;
			isStop = true;
			index = 0;
			isDrawing = true;
		}
		break;
		case IDCLOSE:
		{
			DestroyWindow(hDlg);
			hDlg = NULL;
			break;
		}
		case IDC_EXIT:
		{
			DestroyWindow(hDlg);
			hDlg = NULL;
			break;
		}
		}
	}
	break;
	}

	return 0;
}