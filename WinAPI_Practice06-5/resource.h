//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// WinAPI_Practice06-5.rc에서 사용되고 있습니다.
//
#define IDD_DIALOG6_5                   101
#define IDC_EDIT_NAME                   1001
#define IDC_EDIT_PHONE                  1002
#define IDC_STATIC_NAME                 1003
#define IDC_STATIC_PHONE                1004
#define IDC_STATIC_GENDER               1005
#define IDC_STATIC_BIRTH                1006
#define IDC_FEMALE                      1007
#define IDC_MALE                        1008
#define IDC_COMBO_BIRTH                 1009
#define IDC_STATIC_MEMBERS              1010
#define IDC_MEMBERS_LIST                1011
#define IDC_BUTTON_EXIT                 1012
#define IDC_BUTTON_SORT                 1013
#define IDC_BUTTON_NEW                  1014
#define IDC_BUTTON_JOIN                 1015
#define IDC_BUTTON_LEAVE                1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
