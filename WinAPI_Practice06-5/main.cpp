#include <Windows.h>
#include <stdlib.h>
#include <iostream>
#include <list>
#include <iterator>
#include <string>
#include "resource.h"

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

using namespace std;

#define WINDOW_X 1280
#define WINDOW_Y 720

#define FRAME_X 135
#define FRAME_Y 150

#define FEMALE 0
#define MALE 1

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 6-5";

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK Dlg6_5Proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

class Member
{
private:
	string SName;
	char* Name = nullptr;
	char* PhoneNumber = nullptr;
	char *Gender = nullptr;
	char* BirthYear = nullptr;

public:
	const string GetSName()const { return SName; }
	const char* GetName() const { return Name; }
	const char* GetPhoneNumber() const { return PhoneNumber; }
	const char* GetGender() const { return Gender; }
	const char* GetBirthYear() const { return BirthYear; }

private:
	void DeleteName()
	{
		if (Name != nullptr)
		{
			delete[] Name;
			Name = nullptr;
		}
	}

	void DeleteGender()
	{
		if (Gender != nullptr)
		{
			delete[] Gender;
			Gender = nullptr;
		}
	}

	void DeletePhoneNumber()
	{
		if (PhoneNumber != nullptr)
		{
			delete[] PhoneNumber;
			PhoneNumber = nullptr;
		}
	}

	void DeleteBirthYear()
	{
		if (BirthYear != nullptr)
		{
			delete[] BirthYear;
			BirthYear = nullptr;
		}
	}

	void SetName(const char* name)
	{
		DeleteName();

		int size = strlen(name);
		Name = new char[size + 1];
		strcpy_s(Name, size + 1, name);
	}

	void SetGender(const char* gender)
	{
		DeleteGender();

		int size = strlen(gender);
		Gender = new char[size + 1];
		strcpy_s(Gender, size + 1, gender);
	}

	void SetPhoneNumber(const char* phoneNumber)
	{
		DeletePhoneNumber();

		int size = strlen(phoneNumber);
		PhoneNumber = new char[size + 1];
		strcpy_s(PhoneNumber, size + 1, phoneNumber);
	}

	void SetBirthYear(const char* birthYear)
	{
		DeleteBirthYear();

		int size = strlen(birthYear);
		BirthYear = new char[size + 1];
		strcpy_s(BirthYear, size + 1, birthYear);
	}

public:
	Member()
	{
		SName = "";
		SetName("");
		SetPhoneNumber("");
		SetGender("");
		SetBirthYear("");
	}

	Member(const char* name, const char* phone, const char* gender, const char*birth)
	{
		SName = name;
		SetName(name);
		SetPhoneNumber(phone);
		SetGender(gender);
		SetBirthYear(birth);
	}

	Member(const Member& rhs)
	{
		SName = rhs.SName;
		SetName(rhs.Name);
		SetPhoneNumber(rhs.PhoneNumber);
		SetGender(rhs.Gender);
		SetBirthYear(rhs.BirthYear);
	}

	~Member()
	{
		DeleteName();
		DeletePhoneNumber();
		DeleteGender();
		DeleteBirthYear();
	}
};

std::list<Member> memberContainer;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static int startX, startY;
	static bool Drag;
	static int endX, endY;

	static HBITMAP hBitmap, hOldBitmap;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);
		Drag = false;

		DialogBox(g_hInstance, MAKEINTRESOURCE(IDD_DIALOG6_5), hWnd, (DLGPROC)Dlg6_5Proc);

		SetTimer(hWnd, 0, 150, NULL);
	}
	break;
	case WM_TIMER:
	{
		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
	{
		switch (wParam)
		{
		case 'q': case 'Q':
		{
			PostQuitMessage(0);
		}
		break;
		}
	}
	break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_UP:
		{
		}
		break;
		case VK_DOWN:
		{
		}
		break;
		case VK_LEFT:
		{
		}
		break;
		case VK_RIGHT:
		{
		}
		break;
		}
	}
	break;
	case WM_RBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

BOOL Dlg6_5Proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static int gender;// 성별(남,여)
	static int selection;
	static int ListSelection;
	HDC hdc;
	
	static HWND hComboBox;
	static HWND hList;
	static char str[256];

	static char name[256];
	static char phoneNumber[256];
	static char sex[256];
	static char birthYear[256];

	switch (uMsg)
	{
	case WM_INITDIALOG:
	{
		// 콤보 박스 초기화
		hComboBox = GetDlgItem(hDlg, IDC_COMBO_BIRTH);
		hList = GetDlgItem(hDlg, IDC_MEMBERS_LIST);

		for (int birth = 1994; birth < 2011; birth++)
		{
			sprintf_s(str, sizeof(str), "%d", birth);
			SendMessage(hComboBox, CB_ADDSTRING, 0, (LPARAM)str);
		}
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_BUTTON_JOIN: // 가입버튼 눌렀을 때
		{
			GetDlgItemText(hDlg, IDC_EDIT_NAME, name, 256); // 이름 가져오기
			GetDlgItemText(hDlg, IDC_EDIT_PHONE, phoneNumber, 256); // 전화번호 가져오기

			if (gender) GetDlgItemText(hDlg, IDC_MALE, sex, 256); // 여자성별 가져오기
			else GetDlgItemText(hDlg, IDC_FEMALE, sex, 256); // 남자성별 가져오기

			SendMessage(hComboBox, CB_GETLBTEXT, selection, (LPARAM)birthYear); // 생년 가져오기
			
			sprintf_s(str, sizeof(str), "이름: %s, 전화번호: %s, 성별: %s, 출생연도: %s", name, phoneNumber, sex, birthYear);
			memberContainer.emplace_back(name, phoneNumber, sex, birthYear);
			SendMessage(hList, LB_ADDSTRING, 0, (LPARAM)str);
		}
		break;
		case IDC_BUTTON_LEAVE:
		{
			int entireCount = SendMessage(hList, LB_GETCOUNT, 0, 0);
			
			if (entireCount <= 0)
			{
				MessageBox(hDlg, "가입한 회원이 없는데 탈퇴하려고 했습니다.", "경고", MB_OK);
			}
			else
			{
				SendMessage(hList, LB_DELETESTRING, ListSelection, 0);
				std::list<Member>::iterator it = memberContainer.begin();
				for (int i = 0; i < ListSelection; i++)
				{
					it++;
				}
				memberContainer.erase(it--);
			}
		}
		break;
		case IDC_BUTTON_SORT:
		{
			memberContainer.sort([](const Member& a, const Member& b) {
				return a.GetSName() < b.GetSName();
			});

			SendMessage(hList, LB_RESETCONTENT, 0, 0);

			for (auto& m : memberContainer)
			{
				sprintf_s(str, sizeof(str), "이름: %s, 전화번호: %s, 성별: %s, 출생연도: %s", m.GetName(), m.GetPhoneNumber(), m.GetGender(), m.GetBirthYear());
				SendMessage(hList, LB_ADDSTRING, 0, (LPARAM)str);
				cout << m.GetName() << " ";
			}
		}
		break;
		case IDC_FEMALE: // 여성
		{
			gender = FEMALE;
		}
		break;
		case IDC_MALE: // 남성
		{
			gender = MALE;
		}
		break;
		case IDC_COMBO_BIRTH: // 콤보박스 선택 시
		{
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
			{
				selection = SendMessage(hComboBox, CB_GETCURSEL, 0, 0);
			}
			break;
			}
		}
		break;
		case IDC_BUTTON_NEW: // 새 회원 버튼 눌렀을 때
		{
			SetDlgItemText(hDlg, IDC_EDIT_NAME, "");
			SetDlgItemText(hDlg, IDC_EDIT_PHONE, "");
			gender= FEMALE;
		}
		break;
		case IDC_MEMBERS_LIST:
		{
			switch (HIWORD(wParam))
			{
			case LBN_SELCHANGE:
			{
				ListSelection = SendMessage(hList, LB_GETCURSEL, 0, 0);
			}
			break;
			}
		}
		break;
		case IDC_BUTTON_EXIT:
			EndDialog(hDlg, 0);
			break;
		}
	}
	break;
	}

	return 0;
}
