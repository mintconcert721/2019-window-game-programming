#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <crtdbg.h>
#include <math.h>
#include <iostream>

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 3-3";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 1000
#define WINDOW_Y 600

#define SECOND 1000
#define GROUP_NUMBER 30
#define FPS 60

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);
void CALLBACK TimerProc2(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);

float LengthPts(int x1, int y1, int x2, int y2)
{
	return (sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1)));
}

typedef struct _Circle {
	POINT position; // 중심좌표
	int radius = 40;
	int time = 0;
	bool isVisible = false;
	bool isRotate = false;
	bool RotateOtherDir = false;
	bool isSmallCircleVisible = true;
	bool isCollideWithHero = false;
	POINT point1;
	POINT point2;
	POINT point3;
	COLORREF color = RGB((rand() % 256), (rand() % 256), (rand() % 256));

	void Reset()
	{
		isVisible = false;
		isVisible = false;
		isRotate = false;
		RotateOtherDir = false;
		isCollideWithHero = false;
		time = 0;
	}

	void Update()
	{
		point1.x = position.x - cos(time) * radius;
		point1.y = position.y - sin(time) * radius;

		point2.x = position.x + cos(time) * radius;
		point2.y = position.y + sin(time) * radius;

		point3.x = point1.x - cos(time) * 10;
		point3.y = point1.y - sin(time) * 10;

		if (isRotate)
		{
			if (RotateOtherDir == false)
				time += 1;
			else
				time -= 1;
		}
	}

	bool IsInCircle(int mouseX, int mouseY)
	{
		if (LengthPts(position.x, position.y, mouseX, mouseY) < radius)
			return true;
		else
			return false;
	}

	bool Collide(struct _Circle target)
	{
		int deltaX = this->position.x - target.position.x;
		int deltaY = this->position.y - target.position.y;

		int length = sqrt(deltaX * deltaX + deltaY * deltaY);

		if (length > this->radius + target.radius)
		{
			return false;
		}

		return true;
	}
} Circle;

class CProjectManager
{
private:
	HDC m_Hdc;

	HBRUSH m_Hbrush;
	HBRUSH m_Oldbrush;

	Circle Hero;
	Circle Group[GROUP_NUMBER];
	int groupIndex = 0;
	int m_CollideNumber = 0;
	bool m_IsSelected;

	COLORREF m_RedColor = RGB(255, 0, 0);
	COLORREF m_GreenColor = RGB(0, 255, 0);
	COLORREF m_YellowColor = RGB(255, 255, 0);

public:
	const int GetCollideNumber() const
	{
		return m_CollideNumber;
	}

	void Reset()
	{
		Hero.position.x = 0;
		Hero.position.y = 0;
		Hero.radius = 30;

		for (auto i = 0; i < GROUP_NUMBER; i++)
		{
			Group[i].Reset();
		}

		m_CollideNumber = 0;
	}

	void Spawn()
	{
		srand((unsigned int)time(NULL));

		int x = rand() % WINDOW_X;
		int y = rand() % WINDOW_Y;
		int radius = rand() % 26 + 25;

		Group[groupIndex].position.x = x;
		Group[groupIndex].position.y = y;
		Group[groupIndex].radius = radius;
		Group[groupIndex].isVisible = true;
		
		int  randNumber = rand() % 50;

		if (randNumber < 25)
		{
			Group[groupIndex].isRotate = false;
			// Group[groupIndex].isSmallCircleVisible = false;
		}
		else
		{
			Group[groupIndex].isRotate = true;
			// Group[groupIndex].isSmallCircleVisible = true;
		}
		
		groupIndex++;
	}

	void MoveHero(int mouseX, int mouseY)
	{
		Hero.position.x = mouseX;
		Hero.position.y = mouseY;
	}

	const bool GetSelectValue() const
	{
		return m_IsSelected;
	}

	void SetSelectValue(bool _value)
	{
		m_IsSelected = _value;
	}

	bool IsInGround(int mouseX, int mouseY)
	{
		if (Hero.IsInCircle(mouseX, mouseY) == true)
		{
			return true;
		}
		else
			return false;
	}

	void MouseClick(int mouseX, int mouseY)
	{
	}

	void KeyboardInput(HWND _hWnd, WPARAM _wParam)
	{
		switch (_wParam)
		{
		case 'r': case 'R':
			Reset();
			break;
		case 'q': case 'Q': // 프로그램 종료
			PostQuitMessage(0);
			break;
		}
	}

	void Update(HWND _hWnd)
	{
		if (Hero.position.x >= WINDOW_X - 50 && Hero.position.y >= WINDOW_Y - 50)
		{
			for (auto i = 0; i < GROUP_NUMBER; i++)
			{
				Group[i].RotateOtherDir = false;
				Group[i].isRotate = false;
			}
		}

		for (auto i = 0; i < GROUP_NUMBER; i++)
		{
			if (Group[i].isVisible)
			{
				Group[i].Update();
				if (Hero.Collide((Group[i])))
				{
					Group[i].isCollideWithHero = true;
					if (Group[i].isRotate)
					{
						Group[i].RotateOtherDir = true;
					}
					else
					{
						Group[i].isRotate = true;
					}

					m_CollideNumber++;
				}
			}
		}
	}

	void Draw(HDC hdc, HWND _hWnd)
	{	
		HBRUSH m_Hbrush = CreateSolidBrush(RGB(255, 0, 0));
		HBRUSH m_Oldbrush = (HBRUSH)SelectObject(hdc, m_Hbrush);
		Ellipse(hdc, Hero.position.x - Hero.radius, Hero.position.y - Hero.radius, Hero.position.x + Hero.radius, Hero.position.y + Hero.radius);
		SelectObject(hdc, m_Oldbrush);
		DeleteObject(m_Hbrush);
		
		for (auto i = 0; i < GROUP_NUMBER; i++)
		{
			if (Group[i].isVisible)
			{
				if (Group[i].isCollideWithHero)
				{
					SetROP2(hdc, R2_XORPEN); // 펜의 XOR 연산
					SelectObject(hdc, (HPEN)GetStockObject(WHITE_PEN));

					HPEN m_Hpen = CreatePen(PS_SOLID, 5, Group[i].color);
					HPEN m_Oldpen = (HPEN)SelectObject(hdc, m_Hpen);
					Ellipse(hdc, Group[i].position.x - Group[i].radius, Group[i].position.y - Group[i].radius, Group[i].position.x + Group[i].radius, Group[i].position.y + Group[i].radius);
					SelectObject(hdc, m_Oldpen);
					DeleteObject(m_Hpen);

					MoveToEx(hdc, Group[i].point1.x, Group[i].point1.y, NULL);
					LineTo(hdc, Group[i].point2.x, Group[i].point2.y);

					if (Group[i].isSmallCircleVisible)
					{
						HPEN m_Hpen = CreatePen(PS_SOLID, 5, RGB(0, 0, 0));
						HPEN m_Oldpen = (HPEN)SelectObject(hdc, m_Hpen);
						Ellipse(hdc, Group[i].point3.x - 5, Group[i].point3.y - 5, Group[i].point3.x + 5, Group[i].point3.y + 5);
						SelectObject(hdc, m_Oldpen);
						DeleteObject(m_Hpen);
					}
				}
				else
				{
					SetROP2(hdc, R2_COPYPEN);
					SelectObject(hdc, (HPEN)GetStockObject(WHITE_PEN));
					HPEN m_Hpen = CreatePen(PS_SOLID, 5, Group[i].color);
					HPEN m_Oldpen = (HPEN)SelectObject(hdc, m_Hpen);
					Ellipse(hdc, Group[i].position.x - Group[i].radius, Group[i].position.y - Group[i].radius, Group[i].position.x + Group[i].radius, Group[i].position.y + Group[i].radius);
					SelectObject(hdc, m_Oldpen);
					DeleteObject(m_Hpen);

					MoveToEx(hdc, Group[i].point1.x, Group[i].point1.y, NULL);
					LineTo(hdc, Group[i].point2.x, Group[i].point2.y);

					if (Group[i].isSmallCircleVisible)
					{
						HPEN m_Hpen = CreatePen(PS_SOLID, 5, RGB(0, 0, 0));
						HPEN m_Oldpen = (HPEN)SelectObject(hdc, m_Hpen);
						Ellipse(hdc, Group[i].point3.x - 5, Group[i].point3.y - 5, Group[i].point3.x + 5, Group[i].point3.y + 5);
						SelectObject(hdc, m_Oldpen);
						DeleteObject(m_Hpen);
					}
				}
			}
		}
	}

	CProjectManager()
	{
		Hero.position.x = 0;
		Hero.position.y = 0;
		Hero.radius = 30;
	}

	~CProjectManager()
	{
	}
};

CProjectManager g_Manager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc;
	PAINTSTRUCT ps;

	int mouseX = 0;
	int mouseY = 0;

	static RECT view;
	static HDC  mem1dc, mem2dc;
	static HBITMAP hBit1, hOldBit1;
	static HBITMAP hBit2, hOldBit2;

	static char str[256];
	
	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		GetClientRect(hWnd, &view);
		SetTimer(hWnd, 1, FPS * 100, (TIMERPROC)TimerProc); // 1번 아이디의 타이머가 0.01초 마다 TimerProc 타이머 함수 실행
		SetTimer(hWnd, 2, FPS * 10, (TIMERPROC)TimerProc2); // 1번 아이디의 타이머가 0.01초 마다 TimerProc 타이머 함수 실행
		break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);
		sprintf_s(str, "부딪힌 수 : %d", g_Manager.GetCollideNumber());
		TextOut(hdc, 100, 400, str, strlen(str));
		g_Manager.Draw(hdc, hWnd);
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
		break;
	case WM_KEYDOWN:
		g_Manager.KeyboardInput(hWnd, wParam);
		break;
	case WM_LBUTTONDOWN:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		if (g_Manager.IsInGround(mouseX, mouseY) == true)
		{
			g_Manager.SetSelectValue(true);
		}
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case WM_LBUTTONUP:
		InvalidateRect(hWnd, NULL, TRUE);
		g_Manager.SetSelectValue(false);
		break;
	case WM_MOUSEMOVE:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		if (g_Manager.GetSelectValue())
		{
			g_Manager.MoveHero(mouseX, mouseY);
		}
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case WM_DESTROY:
		if (hBit1) DeleteObject(hBit1);
		DeleteObject(hBit2);
		KillTimer(hWnd, 1);
		KillTimer(hWnd, 2);
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
	{
		g_Manager.Spawn();
	}
	break;
	}
	InvalidateRgn(hWnd, NULL, TRUE);
}

void CALLBACK TimerProc2(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
	{
		g_Manager.Update(hWnd);
	}
	break;
	}
	InvalidateRgn(hWnd, NULL, TRUE);
}