//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// WinAPI_Practice06-1.rc에서 사용되고 있습니다.
//
#define IDD_DIALOG6_1                   101
#define IDC_DIALOG_MOVE                 1003
#define IDC_DIALOG_STOP                 1004
#define IDC_DIALOG_EXIT                 1005
#define IDC_RADIO_FIGURE_1              1006
#define IDC_WINDOW_MOVE                 1012
#define IDC_WINDOW_STOP                 1013
#define IDC_EXIT                        1014
#define IDC_RADIO_FIGURE_2              1015
#define IDC_RADIO_FIGURE_3              1016
#define IDC_RADIO_COLOR_1               1017
#define IDC_RADIO_COLOR_2               1018
#define IDC_RADIO_COLOR_3               1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
