#include <Windows.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include "resource.h"

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

using namespace std;

#define WINDOW_X 1280
#define WINDOW_Y 720

#define FRAME_X 135
#define FRAME_Y 150

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 6-1";

#define CIRCLE 0
#define RECTANGLE 1
#define TRIANGLE 2

#define RIGHT_DOWN 0
#define RIGHT_UP 1
#define LEFT_UP 2
#define LEFT_DOWN 3

RECT crt; // 직사각형모양 범위 선언
RECT DlgCRT; // 직사각형모양 범위 선언

int ShapeCheck;
int ColorCheck;

struct Point {
	int x;
	int y;

	Point& operator=(Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}
};

struct Figure {
	Point position;
	int direction;
	bool isDialog;
	int kind;
	int size;

	HBRUSH hOldBrush;

	void Init(int x, int y, int dir, int ki)
	{
		position.x = x;
		position.y = y;
		direction = dir;
		kind = ki;
		size = 50;
		isDialog = false;
	}

	void Update(Point speed)
	{
		if (isDialog)
		{
			switch (direction)
			{
			case RIGHT_DOWN:
			{
				if (position.y + size >= DlgCRT.bottom)
				{
					direction = RIGHT_UP;
				}
				position.x += speed.x;
				position.y += speed.y;
			}
			break;
			case RIGHT_UP:
			{
				if (position.x + size >= DlgCRT.right)
				{
					direction = LEFT_UP;
				}
				position.x += speed.x;
				position.y -= speed.y;
			}
			break;
			case LEFT_UP:
			{
				if (position.y <= DlgCRT.top)
				{
					direction = LEFT_DOWN;
				}
				position.x -= speed.x;
				position.y -= speed.y;
			}
			break;
			case LEFT_DOWN:
			{
				if (position.x <= DlgCRT.left)
				{
					direction = RIGHT_DOWN;
				}
				position.x -= speed.x;
				position.y += speed.y;
			}
			break;
			}
		}
		else
		{
			switch (direction)
			{
			case RIGHT_DOWN:
			{
				if (position.y + size >= WINDOW_Y)
				{
					direction = RIGHT_UP;
				}
				position.x += speed.x;
				position.y += speed.y;
			}
			break;
			case RIGHT_UP:
			{
				if (position.x + size >= WINDOW_X)
				{
					direction = LEFT_UP;
				}
				position.x += speed.x;
				position.y -= speed.y;
			}
			break;
			case LEFT_UP:
			{
				if (position.y <= 0)
				{
					direction = LEFT_DOWN;
				}
				position.x -= speed.x;
				position.y -= speed.y;
			}
			break;
			case LEFT_DOWN:
			{
				if (position.x <= 0)
				{
					direction = RIGHT_DOWN;
				}
				position.x -= speed.x;
				position.y += speed.y;
			}
			break;
			}
		}
	}

	void Render(HDC hdc)
	{
		HBRUSH hMagentaBrush = CreateSolidBrush(RGB(255, 0, 255));
		HBRUSH hCyanBrush = CreateSolidBrush(RGB(0, 255, 255));
		HBRUSH hYellowBrush = CreateSolidBrush(RGB(255, 255, 0));
		
		switch (ColorCheck)
		{
		case 0:
			hOldBrush = (HBRUSH)SelectObject(hdc, hMagentaBrush);
			break;
		case 1:
			hOldBrush = (HBRUSH)SelectObject(hdc, hCyanBrush);
			break;
		case 2:
			hOldBrush = (HBRUSH)SelectObject(hdc, hYellowBrush);
			break;
		}

		switch (kind)
		{
		case CIRCLE:
		{
			Ellipse(hdc, position.x, position.y, position.x + size, position.y + size);
		}
		break;
		case RECTANGLE:
		{
			Rectangle(hdc, position.x, position.y, position.x + size, position.y + size);
		}
		break;
		case TRIANGLE:
		{
			POINT point[3] = { {position.x + (size / 2), position.y}, {position.x, position.y + size}, {position.x + size, position.y + size} };
			Polygon(hdc, point, 3);
		}
		break;
		}

		DeleteObject(hMagentaBrush);
		DeleteObject(hCyanBrush);
		DeleteObject(hYellowBrush);
	}
};



// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
BOOL Dlg6_1Proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

Figure DialogFigure;
Figure WindowFigure;
Point DialogSpeed;
Point WindowSpeed;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

static HDC hdc, memdc;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;

	static HBITMAP hBitmap, hOldBitmap;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		WindowSpeed.x = 0;
		WindowSpeed.y = 0;

		WindowFigure.Init(WINDOW_X - 50, WINDOW_Y - 50, LEFT_UP, CIRCLE);

		GetClientRect(hWnd, &crt);
	
		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_TIMER:
	{
		WindowFigure.Update(WindowSpeed);
		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);

		Rectangle(memdc, 0, 0, WINDOW_X, WINDOW_Y);

		WindowFigure.Render(memdc);

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);
		DialogBox(g_hInstance, MAKEINTRESOURCE(IDD_DIALOG6_1), hWnd, (DLGPROC)Dlg6_1Proc);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

BOOL Dlg6_1Proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hDlgDC, memDlgDC;
	PAINTSTRUCT DlgPS;
	static HBITMAP hDlgBitmap, hDlgOldBitmap;

	switch (uMsg)
	{
	case WM_INITDIALOG:
	{
		GetClientRect(hDlg, &DlgCRT);

		DialogSpeed.x = 0;
		DialogSpeed.y = 0;

		DialogFigure.Init(0, 0, RIGHT_DOWN, CIRCLE);
		DialogFigure.isDialog = true;

		CheckRadioButton(hDlg, IDC_RADIO_COLOR_1, IDC_RADIO_COLOR_2, IDC_RADIO_COLOR_3);
		CheckRadioButton(hDlg, IDC_RADIO_FIGURE_1, IDC_RADIO_FIGURE_2, IDC_RADIO_FIGURE_3);

		SetTimer(hDlg, 1, 1, NULL);
	}
	break;
	case WM_TIMER:
	{
		DialogFigure.Update(DialogSpeed);
		InvalidateRgn(hDlg, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		memdc = BeginPaint(hDlg, &DlgPS);

		memDlgDC = CreateCompatibleDC(memdc);
		hDlgBitmap = CreateCompatibleBitmap(memdc, DlgCRT.right, DlgCRT.bottom);
		hDlgOldBitmap = (HBITMAP)SelectObject(memDlgDC, hDlgBitmap);

		Rectangle(memDlgDC, 0, 0, DlgCRT.right, DlgCRT.bottom);
		DialogFigure.Render(memDlgDC);

		BitBlt(memdc, 0, 0, DlgCRT.right, DlgCRT.bottom, memDlgDC, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hDlgOldBitmap));
		DeleteDC(memDlgDC);

		EndPaint(hDlg, &DlgPS);
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_RADIO_COLOR_1:
		{
			ColorCheck = 0;
		}
		break;
		case IDC_RADIO_COLOR_2:
		{
			ColorCheck = 1;
		}
		break;
		case IDC_RADIO_COLOR_3:
		{
			ColorCheck = 2;
		}
		break;
		case IDC_RADIO_FIGURE_1:
		{
			ShapeCheck = CIRCLE;
			DialogFigure.kind = CIRCLE;
			WindowFigure.kind = CIRCLE;
		}
		break;
		case IDC_RADIO_FIGURE_2:
		{
			ShapeCheck = RECTANGLE;
			DialogFigure.kind = RECTANGLE;
			WindowFigure.kind = RECTANGLE;
		}
		break;
		case IDC_RADIO_FIGURE_3:
		{
			ShapeCheck = TRIANGLE;
			DialogFigure.kind = TRIANGLE;
			WindowFigure.kind = TRIANGLE;
		}
		break;
		case IDC_DIALOG_MOVE:
		{
			DialogSpeed.x = 1;
			DialogSpeed.y = 1;
		}
		break;
		case IDC_DIALOG_STOP:
		{
			DialogSpeed.x = 0;
			DialogSpeed.y = 0;
		}
		break;
		case IDC_DIALOG_EXIT:
		{
			EndDialog(hDlg, 0);
		}
		break;
		case IDC_WINDOW_MOVE:
		{
			WindowSpeed.x = 1;
			WindowSpeed.y = 1;
		}
		break;
		case IDC_WINDOW_STOP:
		{
			WindowSpeed.x = 0;
			WindowSpeed.y = 0;
		}
		break;
		case IDC_EXIT:
		{
			PostQuitMessage(0);
		}
		break;
		}
	}
	break;
	}

	return 0;
}
