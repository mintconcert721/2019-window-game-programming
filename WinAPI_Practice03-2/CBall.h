#pragma once

class CBall
{
private:
	double m_PositionX;
	double m_PositionY;
	double m_Radius;

	double m_VelocityX;
	double m_VelocityY;
	double m_Speed;

public:
	void SetPositionX(double _PosX);
	void SetPositionY(double _PosY);
	void SetVelocity(double _VelX, double _VelY);
	void SetSpeed(double _Speed);
	void SetRadius(double _Radius);

	const double GetPositionX() const;
	const double GetPositionY() const;
	const double GetVelocityX() const;
	const double GetVelocityY() const;
	const double GetSpeed() const;
	const double GetRadius() const;

	void Update(double _move);

public:
	CBall();
	CBall(double _PosX, double _PosY, double _Radius, double _Speed);
	CBall(double _PosX, double _PosY, double _VelX, double _VelY, double _Speed);
	CBall(const CBall& rhs);
	CBall& operator=(const CBall& rhs);
	~CBall();
};

