#include "CBrick.h"


CBrick::CBrick() : m_PositionX(0), m_PositionY(0), m_HalfX(0), m_HalfY(0), m_Hp(2)
{
}


CBrick::CBrick(int _PosX, int _PosY, int _HalfX, int _HalfY) : m_PositionX(_PosX), m_PositionY(_PosY), m_HalfX(_HalfX), m_HalfY(_HalfY), m_Hp(2)
{
	m_Left = m_PositionX - m_HalfX;
	m_Right = m_PositionX + m_HalfX;
	m_Bottom= m_PositionY - m_HalfY;
	m_Top = m_PositionY + m_HalfY;
}


CBrick::CBrick(const CBrick & rhs)
{
	m_PositionX = rhs.m_PositionX;
	m_PositionY = rhs.m_PositionY;
	m_HalfX = rhs.m_HalfX;
	m_HalfY = rhs.m_HalfY;
	m_Hp = rhs.m_Hp;
	m_Left = rhs.m_Left;
	m_Right = rhs.m_Right;
	m_Bottom = rhs.m_Bottom;
	m_Top = rhs.m_Top;
}


CBrick & CBrick::operator=(const CBrick & rhs)
{
	if (this == &rhs)
		return *this;

	m_PositionX = rhs.m_PositionX;
	m_PositionY = rhs.m_PositionY;
	m_HalfX = rhs.m_HalfX;
	m_HalfY = rhs.m_HalfY;
	m_Hp = rhs.m_Hp;

	return *this;
}


CBrick::~CBrick()
{
}


void CBrick::SetPosition(int _PosX, int _PosY)
{
	m_PositionX = _PosX;
	m_PositionY = _PosY;
}


void CBrick::SetHalf(int _HalfX, int _HalfY)
{
	m_HalfX = _HalfX;
	m_HalfY = _HalfY;
}


void CBrick::Update(int _move)
{
	m_PositionX += _move;

	m_Left = m_PositionX - m_HalfX;
	m_Right = m_PositionX + m_HalfX;
	m_Bottom = m_PositionY - m_HalfY;
	m_Top = m_PositionY + m_HalfY;
}


void CBrick::SetHP(int _Hp)
{
	m_Hp += _Hp;
}


const int CBrick::GetPositionX() const
{
	return m_PositionX;
}


const int CBrick::GetPositionY() const
{
	return m_PositionY;
}


const int CBrick::GetHalfX() const
{
	return m_HalfX;
}


const int CBrick::GetHalfY() const
{
	return m_HalfY;
}


const int CBrick::GetHP() const
{
	return m_Hp;
}


const int CBrick::GetLeft() const
{
	return m_Left;
}


const int CBrick::GetRight() const
{
	return m_Right;
}


const int CBrick::GetTop() const
{
	return m_Top;
}


const int CBrick::GetBottom() const
{
	return m_Bottom;
;
}