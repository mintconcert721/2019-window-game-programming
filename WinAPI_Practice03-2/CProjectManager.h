#pragma once

#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <crtdbg.h>
#include <iostream>

#include "CBall.h"
#include "CBrick.h"

using namespace std;

#define MAX_OBJECTS_NUMBER 20

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 1000
#define WINDOW_Y 600

#define ELAPSED_TIME 1

class CProjectManager
{
private:
	HDC m_Hdc;
	CBrick* m_BrickObjects[MAX_OBJECTS_NUMBER];
	CBall* m_Ball;
	CBrick* m_MovingGround;

	int m_BrickHalfSizeX = 40;
	int m_BrickHalfSizeY = 20;

	int m_BrickSpeed = 1;
	double m_BallSpeed = 5.0;

	bool m_IsSelected = false;

	int m_TouchedBrickNumber = 0;
	int m_DestroyedBrickNumber = 0;

	COLORREF BasicColor = RGB(255, 51, 204); // ��ȫ��
	COLORREF TouchedColor = RGB(0, 255, 255); // �ϴû�
	COLORREF GroundColor = RGB(255, 0, 0); // ������
	COLORREF BallColor = RGB(0, 255, 0); // �ʷϻ�

	HBRUSH m_BasicBrush;
	HBRUSH m_BasicOldBrush;
	HBRUSH m_TouchedBrush;
	HBRUSH m_TouchedOldBrush;
	HBRUSH m_GroundBrush;
	HBRUSH m_GroundOldBrush;
	HBRUSH m_BallBrush;
	HBRUSH m_BallOldBrush;

private:
	void ApplyForce(int x, int y);
	void CheckCollideRectAndWall(CBrick* _target);
	bool CheckCollideRectAndBall(CBall* _ball, CBrick* _target);
	bool IsPointInCircle(CBall* _ball, int _x, int _y);
	void CollectGarbage();
	void SetTouchedBrickNumber();
	void SetDestroyedBrickNumber();

public:
	void MoveGround(int mouseX, int mouseY);
	void Keyboard(HWND _hWnd, WPARAM _wParam);
	void Draw(HWND _hWnd);
	void Update(HWND _hWnd);
	void SetSelectValue(bool _value);
	bool IsInGround(int mouseX, int mouseY);
	const bool GetSelectValue() const;
	const int GetTouchedBrickNumber() const;
	const int GetDestroyedBrickNumber() const;

	CProjectManager();
	~CProjectManager();
};

