#include "CBall.h"


CBall::CBall() : m_PositionX(0), m_PositionY(0), m_VelocityX(0), m_VelocityY(0), m_Speed(1), m_Radius(0)
{
	m_VelocityX = 1.0;
	m_VelocityY = 1.0;
}


CBall::CBall(double _PosX, double _PosY, double _Radius, double _Speed) : m_PositionX(_PosX), m_PositionY(_PosY), m_Speed(_Speed), m_Radius(_Radius)
{
	m_VelocityX = 1.0;
	m_VelocityY = 1.0;
}


CBall::CBall(double _PosX, double _PosY, double _VelX, double _VelY, double _Speed) : m_PositionX(_PosX), m_PositionY(_PosY), m_VelocityX(_VelX), m_VelocityY(_VelY), m_Speed(_Speed), m_Radius(0.0)
{
}


CBall::CBall(const CBall & rhs)
{
	m_PositionX = rhs.m_PositionX;
	m_PositionY = rhs.m_PositionY;
	m_VelocityX = rhs.m_VelocityX;
	m_VelocityY = rhs.m_VelocityY;
	m_Speed = rhs.m_Speed;
	m_Radius = rhs.m_Radius;
}


CBall & CBall::operator=(const CBall & rhs)
{
	if (this == &rhs)
		return *this;

	m_PositionX = rhs.m_PositionX;
	m_PositionY = rhs.m_PositionY;
	m_VelocityX = rhs.m_VelocityX;
	m_VelocityY = rhs.m_VelocityY;
	m_Speed = rhs.m_Speed;
	m_Radius = rhs.m_Radius;

	return *this;
}


CBall::~CBall()
{
}


void CBall::SetPositionX(double _PosX)
{
	m_PositionX = _PosX;
}


void CBall::SetPositionY(double _PosY)
{
	m_PositionY = _PosY;
}


void CBall::SetVelocity(double _VelX, double _VelY)
{
	m_VelocityX = _VelX;
	m_VelocityY = _VelY;
}


void CBall::SetSpeed(double _Speed)
{
	m_Speed = _Speed;
}


void CBall::SetRadius(double _Radius)
{
	m_Radius = _Radius;
}


void CBall::Update(double _move)
{
	m_Speed = _move;

	// Calculate Position
	m_PositionX += m_Speed * m_VelocityX;
	m_PositionY += m_Speed * m_VelocityY;
}


const double CBall::GetPositionX() const
{
	return m_PositionX;
}


const double CBall::GetPositionY() const
{
	return m_PositionY;
}


const double CBall::GetVelocityX() const
{
	return m_VelocityX;
}


const double CBall::GetVelocityY() const
{
	return m_VelocityY;
}


const double CBall::GetSpeed() const
{
	return m_Speed;
}


const double CBall::GetRadius() const
{
	return m_Radius;
}

