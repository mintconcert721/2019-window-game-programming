#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 2-6";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 800
#define WINDOW_Y 800

#define KIND_CIRCLE 1
#define KIND_TRIANGLE 2
#define KIND_RECTANGLE 3

#define ARRAY_SIZE 5

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

typedef struct _Point {
	RECT rect;
	int kind = 0;
	COLORREF Color;
	HPEN Hpen;
	HPEN Oldpen;
	HBRUSH Hbrush;
	HBRUSH Oldbrush;
	int scale;
	_Point& operator=(const _Point& other) // 대입 연산자 오버로딩
	{
		rect = other.rect;
		kind = other.kind;
		Color = other.Color;

		Hbrush = other.Hbrush;
		Oldbrush = other.Oldbrush;

		Hpen = other.Hpen;
		Oldpen = other.Oldpen;
		scale = other.scale;

		return *this;
	}
} Point;

class CProjectManager
{
private:
	HDC m_Hdc;
	COLORREF m_SelectedColor = RGB(255, 0, 0);
	Point m_FigureArray[ARRAY_SIZE];
	int m_FigureArrayIndex = 0;
	int m_AddingArrayIndex = 0;
	int m_BoardWidthNumber = 40;
	int m_BoardHeightNumber = 40;
	int m_Width = 0;
	int m_Height = 0;
	bool IsSelectedAll = false;

private:
	bool CheckOverlap(Point* _array, int _left, int _top)
	{
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			if ((m_FigureArray[i].rect.left == _left) && (m_FigureArray[i].rect.top == _top))
			{
				return false;
			}
		}

		return true;
	}

public:
	void MoveFigure(HWND _hWnd, WPARAM _wParam)
	{
		m_Hdc = GetDC(_hWnd);

		switch (_wParam)
		{
		case VK_UP:
			if (m_FigureArray[m_FigureArrayIndex].rect.top > 0)
			{
				m_FigureArray[m_FigureArrayIndex].rect.top -= 1;
				m_FigureArray[m_FigureArrayIndex].rect.bottom -= 1;
			}
			break;
		case VK_DOWN:
			if (m_FigureArray[m_FigureArrayIndex].rect.bottom < m_BoardHeightNumber - 1)
			{
				m_FigureArray[m_FigureArrayIndex].rect.top += 1;
				m_FigureArray[m_FigureArrayIndex].rect.bottom += 1;
			}
			break;
		case VK_LEFT:
			if (m_FigureArray[m_FigureArrayIndex].rect.left > 0)
			{
				m_FigureArray[m_FigureArrayIndex].rect.left -= 1;
				m_FigureArray[m_FigureArrayIndex].rect.right -= 1;
			}
			break;
		case VK_RIGHT:
			if (m_FigureArray[m_FigureArrayIndex].rect.right < m_BoardWidthNumber - 1)
			{
				m_FigureArray[m_FigureArrayIndex].rect.left += 1;
				m_FigureArray[m_FigureArrayIndex].rect.right += 1;
			}
			break;
		}
		
		ReleaseDC(_hWnd, m_Hdc);
		InvalidateRect(_hWnd, NULL, TRUE);
	}

	void KeyboardInput(HWND _hWnd, WPARAM _wParam)
	{
		m_Hdc = GetDC(_hWnd);

		m_Width = WINDOW_X / m_BoardWidthNumber;
		m_Height = WINDOW_Y / m_BoardHeightNumber;

		switch (_wParam)
		{
		case 's': case 'S': // Small : 작은
			m_BoardWidthNumber = 30;
			m_BoardHeightNumber = 30;
			break;
		case 'm': case 'M': // Medium : 중간
			m_BoardWidthNumber = 40;
			m_BoardHeightNumber = 40;
			break;
		case 'b': case 'B': // Big : 큰
			m_BoardWidthNumber = 50;
			m_BoardHeightNumber = 50;
			break;
		case 'e': case 'E': // 원
		{
			if (m_AddingArrayIndex == 5)
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					m_FigureArray[i] = m_FigureArray[i + 1];
				}

				m_FigureArray[m_AddingArrayIndex - 1].kind = KIND_CIRCLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex - 1].rect.left = left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.top = top;
						m_FigureArray[m_AddingArrayIndex - 1].rect.right = m_FigureArray[m_AddingArrayIndex - 1].rect.left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.bottom = m_FigureArray[m_AddingArrayIndex - 1].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex - 1].Color = RGB(r, g, b);
			}
			else
			{
				m_FigureArray[m_AddingArrayIndex].kind = KIND_CIRCLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex].rect.left = left;
						m_FigureArray[m_AddingArrayIndex].rect.top = top;
						m_FigureArray[m_AddingArrayIndex].rect.right = m_FigureArray[m_AddingArrayIndex].rect.left;
						m_FigureArray[m_AddingArrayIndex].rect.bottom = m_FigureArray[m_AddingArrayIndex].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex].Color = RGB(r, g, b);
				m_AddingArrayIndex++;
			}
		}
		break;
		case 't': case 'T': // 삼각형
		{
			if (m_AddingArrayIndex == 5)
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					m_FigureArray[i] = m_FigureArray[i + 1];
				}

				m_FigureArray[m_AddingArrayIndex - 1].kind = KIND_TRIANGLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex - 1].rect.left = left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.top = top;
						m_FigureArray[m_AddingArrayIndex - 1].rect.right = m_FigureArray[m_AddingArrayIndex - 1].rect.left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.bottom = m_FigureArray[m_AddingArrayIndex - 1].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex - 1].Color = RGB(r, g, b);
			}
			else
			{
				m_FigureArray[m_AddingArrayIndex].kind = KIND_TRIANGLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex].rect.left = left;
						m_FigureArray[m_AddingArrayIndex].rect.top = top;
						m_FigureArray[m_AddingArrayIndex].rect.right = m_FigureArray[m_AddingArrayIndex].rect.left;
						m_FigureArray[m_AddingArrayIndex].rect.bottom = m_FigureArray[m_AddingArrayIndex].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex].Color = RGB(r, g, b);
				m_AddingArrayIndex++;
			}
		}
		break;
		case 'r': case 'R': // 사각형
		{
			if (m_AddingArrayIndex == 5)
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					m_FigureArray[i] = m_FigureArray[i + 1];
				}

				m_FigureArray[m_AddingArrayIndex - 1].kind = KIND_RECTANGLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex - 1].rect.left = left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.top = top;
						m_FigureArray[m_AddingArrayIndex - 1].rect.right = m_FigureArray[m_AddingArrayIndex - 1].rect.left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.bottom = m_FigureArray[m_AddingArrayIndex - 1].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex - 1].Color = RGB(r, g, b);
			}
			else
			{
				m_FigureArray[m_AddingArrayIndex].kind = KIND_RECTANGLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex].rect.left = left;
						m_FigureArray[m_AddingArrayIndex].rect.top = top;
						m_FigureArray[m_AddingArrayIndex].rect.right = m_FigureArray[m_AddingArrayIndex].rect.left;
						m_FigureArray[m_AddingArrayIndex].rect.bottom = m_FigureArray[m_AddingArrayIndex].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex].Color = RGB(r, g, b);
				m_AddingArrayIndex++;
			}
		}
		break;
		case '=': // 크게
		{
			if (IsSelectedAll)
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					m_FigureArray[i].scale++;
				}
			}
			else
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					if (m_FigureArrayIndex == i)
					{
						m_FigureArray[i].scale++;
					}
				}
			}
		}
		break;
		case '-': // 작게
		{
			if (IsSelectedAll)
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					m_FigureArray[i].scale--;
				}
			}
			else
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					if (m_FigureArrayIndex == i)
					{
						m_FigureArray[i].scale--;
					}
				}
			}
		}
		break;
		case '1':
		{
			if (IsSelectedAll) IsSelectedAll = false;
			m_FigureArrayIndex = 0;
		}
		break;
		case '2':
		{
			if (IsSelectedAll) IsSelectedAll = false;
			m_FigureArrayIndex = 1;
		}
		break;
		case '3':
		{
			if (IsSelectedAll) IsSelectedAll = false;
			m_FigureArrayIndex = 2;
		}
		break;
		case '4':
		{
			if (IsSelectedAll) IsSelectedAll = false;
			m_FigureArrayIndex = 3;
		}
		break;
		case '5':
		{
			if (IsSelectedAll) IsSelectedAll = false;
			m_FigureArrayIndex = 4;
		}
		break;
		case '6':
		{
			IsSelectedAll = true;
		}
		break;
		case 'q': case 'Q':
		{
			exit(0);
		}
		break;
		}

		ReleaseDC(_hWnd, m_Hdc);
		InvalidateRect(_hWnd, NULL, TRUE);
	}

	void Delete()
	{
		if (IsSelectedAll)
		{
			for (int i = 0; i < ARRAY_SIZE; i++)
			{
				m_FigureArray[i].kind = 0;
				m_FigureArray[i].rect.left = 0;
				m_FigureArray[i].rect.right = 0;
				m_FigureArray[i].rect.top = 0;
				m_FigureArray[i].rect.bottom = 0;
				m_FigureArray[i].Color = RGB(255, 255, 255);
				m_FigureArray[i].scale = 0;
			}
			m_FigureArrayIndex = 0;
			m_AddingArrayIndex = 0;
		}
		else
		{
			for (int i = 0; i < ARRAY_SIZE; i++)
			{
				if (m_FigureArrayIndex == i)
				{
					if (i != ARRAY_SIZE - 1)
					{
						int count = 0;

						for (int j = i; j < ARRAY_SIZE - 1; j++)
						{
							m_FigureArray[j] = m_FigureArray[j + 1];
						}

						m_AddingArrayIndex = ARRAY_SIZE - 1;
					}
					else
					{
						m_FigureArray[i].kind = 0;
						m_FigureArray[i].rect.left = 0;
						m_FigureArray[i].rect.right = 0;
						m_FigureArray[i].rect.top = 0;
						m_FigureArray[i].rect.bottom = 0;
						m_FigureArray[i].Color = RGB(255, 255, 255);
						m_FigureArray[i].scale = 0;
						m_AddingArrayIndex = ARRAY_SIZE - 1;
					}
				}
			}
		}
		
	}

	void Draw(HWND _hWnd)
	{
		m_Hdc = GetDC(_hWnd);

		m_Width = WINDOW_X / m_BoardWidthNumber;
		m_Height = WINDOW_X / m_BoardHeightNumber;

		for (int width = 0; width < m_BoardWidthNumber; width++)
		{
			for (int height = 0; height < m_BoardHeightNumber; height++)
			{
				Rectangle(m_Hdc, width * m_Width, height * m_Width, width * m_Width + m_Width, height * m_Width + m_Width);
			}
		}

		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			m_FigureArray[i].Hbrush = CreateSolidBrush(m_FigureArray[i].Color);
			m_FigureArray[i].Oldbrush = CreateSolidBrush(m_FigureArray[i].Color);

			m_FigureArray[i].Oldbrush = (HBRUSH)SelectObject(m_Hdc, m_FigureArray[i].Hbrush);
			if (IsSelectedAll)
			{
				m_FigureArray[i].Hpen = CreatePen(PS_SOLID, 3, m_SelectedColor);
				m_FigureArray[i].Oldpen = CreatePen(PS_SOLID, 3, m_SelectedColor);

				m_FigureArray[i].Oldpen = (HPEN)SelectObject(m_Hdc, m_FigureArray[i].Hpen);
			}
			else
			{
				if (m_FigureArrayIndex == i)
				{
					m_FigureArray[i].Hpen = CreatePen(PS_SOLID, 3, m_SelectedColor);
					m_FigureArray[i].Oldpen = CreatePen(PS_SOLID, 3, m_SelectedColor);

					m_FigureArray[i].Oldpen = (HPEN)SelectObject(m_Hdc, m_FigureArray[i].Hpen);
				}
			}

			switch (m_FigureArray[i].kind)
			{
			case KIND_CIRCLE:
			{

				Ellipse(m_Hdc, m_FigureArray[i].rect.left * m_Width, m_FigureArray[i].rect.top * m_Width,
					m_FigureArray[i].rect.right * m_Width + m_Width + m_FigureArray[i].scale, m_FigureArray[i].rect.bottom * m_Width + m_Width + m_FigureArray[i].scale);
			}
			break;
			case KIND_TRIANGLE:
			{
				POINT point[3] = { {m_FigureArray[i].rect.left * m_Width, m_FigureArray[i].rect.bottom * m_Width + m_Width},
				{m_FigureArray[i].rect.right * m_Width + m_Width + m_FigureArray[i].scale, m_FigureArray[i].rect.bottom * m_Width + m_Width},
				{(m_FigureArray[i].rect.left * m_Width + m_FigureArray[i].rect.right * m_Width + m_Width + m_FigureArray[i].scale) / 2, m_FigureArray[i].rect.top * m_Width - m_FigureArray[i].scale} };
				Polygon(m_Hdc, point, 3);
			}
			break;
			case KIND_RECTANGLE:
			{
				Rectangle(m_Hdc, m_FigureArray[i].rect.left * m_Width, m_FigureArray[i].rect.top * m_Width,
					m_FigureArray[i].rect.right * m_Width + m_Width + m_FigureArray[i].scale, m_FigureArray[i].rect.bottom * m_Width + m_Width + m_FigureArray[i].scale);
			}
			break;
			}

			SelectObject(m_Hdc, m_FigureArray[i].Oldbrush);
			SelectObject(m_Hdc, m_FigureArray[i].Oldpen);
			DeleteObject(m_FigureArray[i].Hbrush);
			DeleteObject(m_FigureArray[i].Hpen);
		}
		ReleaseDC(_hWnd, m_Hdc);
	}

	CProjectManager()
	{
	}
};

CProjectManager g_Manager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		g_Manager.Draw(hWnd);
		EndPaint(hWnd, &ps);
		break;
	case WM_CHAR:
		g_Manager.KeyboardInput(hWnd, wParam);
		break;
	case WM_KEYDOWN:
		g_Manager.MoveFigure(hWnd, wParam);
		if (wParam == VK_DELETE)
			g_Manager.Delete();
		break;
	case WM_TIMER:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}