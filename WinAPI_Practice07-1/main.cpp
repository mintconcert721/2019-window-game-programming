#include <Windows.h>
#include <math.h>
#include <iostream>

using namespace std;

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

class CPoint
{
private:
	int x;
	int y;

public:
	const int getX()const { return x; }
	const int getY()const { return y; }

public:
	void setX(int positionX) { x = positionX; }
	void setY(int positionY) { y = positionY; }
	void setXY(int positionX, int positionY)
	{
		x = positionX;
		y = positionY;
	}

public: // operator
	CPoint& operator=(const CPoint& rhs)
	{
		if (this != &rhs)
		{
			x = rhs.x;
			y = rhs.y;
		}

		return *this;
	}

public: // constructor
	CPoint() : x(0), y(0)
	{
	}
	CPoint(int positionX, int positionY) : x(positionX), y(positionY)
	{
	}
	CPoint(const CPoint& rhs) : x(0), y(0)
	{
		x = rhs.x;
		y = rhs.y;
	}
};

struct Point {
	int x;
	int y;

	Point() {}

	Point(int _x, int _y)
	{
		x = _x;
		y = _y;
	}

	Point(const Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;
	}

	Point& operator=(const Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}
};

#define WINDOW_X 1280
#define WINDOW_Y 720

#define CHILD_WINDOW_X WINDOW_X / 2
#define CHILD_WINDOW_Y (WINDOW_Y - 75)

#define FRAME_X 135
#define FRAME_Y 150

#define CIRCLE 0
#define RECTANGLE 1
#define TRIANGLE 2

#define SMALL 0
#define NORMAL 1
#define BIG 2

#define IDC_CHECK1 1001
#define IDC_CHECK2 1002
#define IDC_CHECK3 1003

#define IDC_RADIO1 2001
#define IDC_RADIO2 2002
#define IDC_RADIO3 2003

#define IDC_RADIO2_1 3001
#define IDC_RADIO2_2 3002
#define IDC_RADIO2_3 3003

#define IDC_SCROLL1 3001
#define IDC_SCROLL2 3002

#define IDC_MOVE 4001
#define IDC_EXIT 5001

#define PI 3.1415926535897932
#define DEG2RAD PI / 180
#define RAD2DEG 180 / PI

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "ParentClass";
LPCTSTR lpszChildClass = "ChildClass";
LPCTSTR lpszButtonClass = "button";
LPCTSTR lpszScrollClass = "scrollbar";
LPCTSTR lpszWindowName = "Window Program 7-1 : 공전하는 원 만들기";

Point mouse;
int g_ShapeMode = CIRCLE;
int g_SizeMode = NORMAL;
bool g_Color[3];
bool isClockWise = true; // 시계방향 이동
int g_Radius = 25;
int time = 10;
int divTime = 5;

POINT smallCircleArray[100 * 100 + 1];
POINT normalCircleArray[100 * 100 + 1];
POINT bigCircleArray[100 * 100 + 1];

#define SMALL_RECT 50
#define NORMAL_RECT 150
#define BIG_RECT 250

#define SMALL_TRI 60
#define NORMAL_TRI 120
#define BIG_TRI 180

POINT smallRectangleArray[SMALL_RECT + SMALL_RECT + SMALL_RECT + SMALL_RECT + 1];
POINT normalRectangleArray[NORMAL_RECT + NORMAL_RECT + NORMAL_RECT + NORMAL_RECT + 1];
POINT bigRectangleArray[BIG_RECT + BIG_RECT + BIG_RECT + BIG_RECT + 1];

POINT smallTriangleArray[SMALL_TRI + SMALL_TRI + SMALL_TRI + 1];
POINT normalTriangleArray[NORMAL_TRI + NORMAL_TRI + NORMAL_TRI + 1];
POINT bigTriangleArray[BIG_TRI + BIG_TRI + BIG_TRI + 1];

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	// 차일드 윈도우 클래스 등록
	WndClass.hCursor = LoadCursor(NULL, IDC_HELP);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	WndClass.lpszClassName = lpszChildClass;
	WndClass.lpfnWndProc = (WNDPROC)ChildWndProc;
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;
	static bool isDrag;

	static HBITMAP hBitmap, hOldBitmap;
	HWND hWndChild;

	// 컨트롤 박스
	static HWND hGroup1, hGroup2, hGroup3, hGroup4, hGroup5;
	static HWND hRadio1_1, hRadio1_2, hRadio1_3;
	static HWND hRadio2_1, hRadio2_2, hRadio2_3;
	static HWND hScroll1, hScroll2;
	static HWND hCheck1, hCheck2, hCheck3;
	static HWND hButton1, hButton2;

	static HFONT hFont;

	static int scroll1Pos, scroll2Pos;
	int TempPos;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);

		RECT childrt = { 0, 0, CHILD_WINDOW_X, CHILD_WINDOW_Y };
		AdjustWindowRect(&childrt, WS_OVERLAPPEDWINDOW, false);
		hWndChild = CreateWindow(lpszChildClass, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, 
			20, 20, childrt.right - childrt.left, childrt.bottom - childrt.top, hWnd, NULL, g_hInstance, NULL);

		// TODO:
		hFont = CreateFont(20, 0, 0, 0, 800, 0, 0, 0, HANGEUL_CHARSET, 0, 0, 0, 0, TEXT("맑은 고딕"));

		// 그룹박스로 윈도우 만들기
		hGroup1 = CreateWindow(lpszButtonClass, "공전궤도 모양선택", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
			(WINDOW_X / 2) + 100, 
			40, 
			480, 100, hWnd, NULL, g_hInstance, NULL);
		SendMessage(hGroup1, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hRadio1_1 = CreateWindow(lpszButtonClass, "원", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_GROUP | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20, 40 + 40, 120, 40, hWnd, (HMENU)IDC_RADIO1, g_hInstance, NULL);
		SendMessage(hRadio1_1, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);
		hRadio1_2 = CreateWindow(lpszButtonClass, "사각형", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20 + 120 + 40, 40 + 40, 120, 40, hWnd, (HMENU)IDC_RADIO2, g_hInstance, NULL);
		SendMessage(hRadio1_2, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);
		hRadio1_3 = CreateWindow(lpszButtonClass, "삼각형", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20 + 120 + 40 + 120 + 40, 40 + 40, 120, 40, hWnd, (HMENU)IDC_RADIO3, g_hInstance, NULL);
		SendMessage(hRadio1_3, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		CheckRadioButton(hWnd, IDC_RADIO1, IDC_RADIO2, IDC_RADIO3);

		// 그룹박스로 윈도우 만들기
		hGroup2 = CreateWindow(lpszButtonClass, "공전궤도 크기선택", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
			(WINDOW_X / 2) + 100, 
			40 + 100 + 20,
			480, 100, hWnd, NULL, g_hInstance, NULL);
		SendMessage(hGroup2, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hRadio2_1 = CreateWindow(lpszButtonClass, "작게", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_GROUP | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20, 
			40 + 100 + 20 + 40,
			120, 40, hWnd, (HMENU)IDC_RADIO2_1, g_hInstance, NULL);
		SendMessage(hRadio2_1, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);
		hRadio2_2 = CreateWindow(lpszButtonClass, "보통", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20 + 120 + 40, 
			40 + 100 + 20 + 40,
			120, 40, hWnd, (HMENU)IDC_RADIO2_2, g_hInstance, NULL);
		SendMessage(hRadio2_2, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);
		hRadio2_3 = CreateWindow(lpszButtonClass, "크게", WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20 + 120 + 40 + 120 + 40, 
			40 + 100 + 20 + 40,
			120, 40, hWnd, (HMENU)IDC_RADIO2_3, g_hInstance, NULL);
		SendMessage(hRadio2_3, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		CheckRadioButton(hWnd, IDC_RADIO2_1, IDC_RADIO2_2, IDC_RADIO2_3);

		hGroup3 = CreateWindow(lpszButtonClass, "원 이동속도 조절 (왼쪽 : 느리게, 오른쪽 : 빠르게)", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
			(WINDOW_X / 2) + 100, 
			40 + 100 + 20 + 100 + 20,
			480, 100, hWnd, NULL, g_hInstance, NULL);
		SendMessage(hGroup3, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hScroll1 = CreateWindow(lpszScrollClass, NULL, WS_CHILD | WS_VISIBLE | SBS_HORZ,
			(WINDOW_X / 2) + 100 + 40,
			40 + 100 + 20 + 100 + 20 + 40 + 20,
			400, 20, hWnd, (HMENU)IDC_SCROLL1, g_hInstance, NULL);
		SetScrollRange(hScroll1, SB_CTL, 1, 10, TRUE); SetScrollPos(hScroll1, SB_CTL, 5, TRUE);

		hGroup4 = CreateWindow(lpszButtonClass, "원 크기 조절", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
			(WINDOW_X / 2) + 100,
			40 + 100 + 20 + 100 + 20 + 100 + 20,
			480, 100, hWnd, NULL, g_hInstance, NULL);
		SendMessage(hGroup4, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hScroll2 = CreateWindow(lpszScrollClass, NULL, WS_CHILD | WS_VISIBLE | SBS_HORZ,
			(WINDOW_X / 2) + 100 + 40,
			40 + 100 + 20 + 100 + 20 + 100 + 20 + 40 + 20,
			400, 20, hWnd, (HMENU)IDC_SCROLL2, g_hInstance, NULL);
		SetScrollRange(hScroll2, SB_CTL, 10, 50, TRUE); SetScrollPos(hScroll2, SB_CTL, 25, TRUE);

		scroll1Pos = 5;
		scroll2Pos = 25;

		hGroup5 = CreateWindow(lpszButtonClass, "원 색상 조절", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
			(WINDOW_X / 2) + 100,
			40 + 100 + 20 + 100 + 20 + 100 + 20 + 100 + 20,
			480, 100, hWnd, NULL, g_hInstance, NULL);
		SendMessage(hGroup5, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hCheck1 = CreateWindow(lpszButtonClass, "Cyan", WS_CHILD | WS_VISIBLE | BS_CHECKBOX  | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20,
			40 + 100 + 20 + 100 + 20 + 100 + 20 + 100 + 20 + 40,
			120, 40, hWnd, (HMENU)IDC_CHECK1, g_hInstance, NULL);
		SendMessage(hCheck1, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);
		hCheck2 = CreateWindow(lpszButtonClass, "Magenta", WS_CHILD | WS_VISIBLE | BS_CHECKBOX | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20 + 120 + 40,
			40 + 100 + 20 + 100 + 20 + 100 + 20 + 100 + 20 + 40,
			120, 40, hWnd, (HMENU)IDC_CHECK2, g_hInstance, NULL);
		SendMessage(hCheck2, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);
		hCheck3 = CreateWindow(lpszButtonClass, "Yellow", WS_CHILD | WS_VISIBLE | BS_CHECKBOX | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20 + 120 + 40 + 120 + 40,
			40 + 100 + 20 + 100 + 20 + 100 + 20 + 100 + 20 + 40,
			120, 40, hWnd, (HMENU)IDC_CHECK3, g_hInstance, NULL);
		SendMessage(hCheck3, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButton1 = CreateWindow(lpszButtonClass, "이동 반대로", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20 + 50,
			40 + 100 + 20 + 100 + 20 + 100 + 20 + 100 + 20 + 100 + 20 + 10,
			120, 40, hWnd, (HMENU)IDC_MOVE, g_hInstance, NULL);
		SendMessage(hButton1, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);
		hButton2 = CreateWindow(lpszButtonClass, "종료", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 20 + 100 + 20 + 100 + 20 + 40,
			40 + 100 + 20 + 100 + 20 + 100 + 20 + 100 + 20 + 100 + 20 + 10,
			120, 40, hWnd, (HMENU)IDC_EXIT, g_hInstance, NULL);
		SendMessage(hButton2, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_HSCROLL:
	{
		if ((HWND)lParam == hScroll2)
		{
			TempPos = scroll2Pos;

			switch (LOWORD(wParam))
			{
			case SB_LINELEFT: TempPos = max(10, TempPos - 1); break;
			case SB_LINERIGHT: TempPos = min(50, TempPos + 1); break;
			case SB_PAGELEFT: TempPos = max(10, TempPos - 1); break;
			case SB_PAGERIGHT: TempPos = min(50, TempPos + 1); break;
			case SB_THUMBTRACK: TempPos = HIWORD(wParam); break;
			}

			scroll2Pos = TempPos;
			g_Radius = scroll2Pos;

			SetScrollPos((HWND)lParam, SB_CTL, TempPos, TRUE);
		}

		if ((HWND)lParam == hScroll1)
		{
			TempPos = scroll1Pos;

			switch (LOWORD(wParam))
			{
			case SB_LINELEFT: TempPos = max(1, TempPos - 1); break;
			case SB_LINERIGHT: TempPos = min(10, TempPos + 1); break;
			case SB_PAGELEFT: TempPos = max(1, TempPos - 1); break;
			case SB_PAGERIGHT: TempPos = min(10, TempPos + 1); break;
			case SB_THUMBTRACK: TempPos = HIWORD(wParam); break;
			}

			scroll1Pos = TempPos;
			divTime = scroll1Pos;

			SetScrollPos((HWND)lParam, SB_CTL, TempPos, TRUE);
		}

		SetScrollPos((HWND)lParam, SB_CTL, TempPos, TRUE);
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_RADIO1:
		{
			g_ShapeMode = CIRCLE;
		}
		break;
		case IDC_RADIO2:
		{
			g_ShapeMode = RECTANGLE;
		}
		break;
		case IDC_RADIO3:
		{
			g_ShapeMode = TRIANGLE;
		}
		break;
		case IDC_RADIO2_1:
		{
			g_SizeMode = SMALL;
		}
		break;
		case IDC_RADIO2_2:
		{
			g_SizeMode = NORMAL;
		}
		break;
		case IDC_RADIO2_3:
		{
			g_SizeMode = BIG;
		}
		break;
		case IDC_MOVE:
		{
			if (isClockWise)
				isClockWise = false;
			else
				isClockWise = true;
		}
		break;
		case IDC_EXIT:
		{
			if (SendMessage(hButton2, BN_CLICKED, 0, 0) == STN_CLICKED)
				PostQuitMessage(0);
		}
		break;
		case IDC_CHECK1:
		{
			if (SendMessage(hCheck1, BM_GETCHECK, 0, 0) == BST_UNCHECKED)
			{
				SendMessage(hCheck1, BM_SETCHECK, BST_CHECKED, 0);
				g_Color[0] = true;

				SendMessage(hCheck2, BM_SETCHECK, BST_UNCHECKED, 0);
				g_Color[1] = false;
				SendMessage(hCheck3, BM_SETCHECK, BST_UNCHECKED, 0);
				g_Color[2] = false;
			}
			else
			{
				SendMessage(hCheck1, BM_SETCHECK, BST_UNCHECKED, 0);
				g_Color[0] = false;
			}
		}
		break;
		case IDC_CHECK2:
		{
			if (SendMessage(hCheck2, BM_GETCHECK, 0, 0) == BST_UNCHECKED)
			{
				SendMessage(hCheck2, BM_SETCHECK, BST_CHECKED, 0);
				g_Color[1] = true;

				SendMessage(hCheck1, BM_SETCHECK, BST_UNCHECKED, 0);
				g_Color[0] = false;
				SendMessage(hCheck3, BM_SETCHECK, BST_UNCHECKED, 0);
				g_Color[2] = false;
			}
			else
			{
				SendMessage(hCheck2, BM_SETCHECK, BST_UNCHECKED, 0);
				g_Color[1] = false;
			}
		}
		break;
		case IDC_CHECK3:
		{
			if (SendMessage(hCheck3, BM_GETCHECK, 0, 0) == BST_UNCHECKED)
			{
				SendMessage(hCheck3, BM_SETCHECK, BST_CHECKED, 0);
				g_Color[2] = true;

				SendMessage(hCheck1, BM_SETCHECK, BST_UNCHECKED, 0);
				g_Color[0] = false;
				SendMessage(hCheck2, BM_SETCHECK, BST_UNCHECKED, 0);
				g_Color[1] = false;
			}
			else
			{
				SendMessage(hCheck3, BM_SETCHECK, BST_UNCHECKED, 0);
				g_Color[2] = false;
			}
		}
		break;
		}
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);

		Rectangle(memdc, crt.left, crt.top, crt.right, crt.bottom);

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		mouse.x = LOWORD(lParam);
		mouse.y = HIWORD(lParam);
	}
	break;
	case WM_LBUTTONUP:
	{
	}
	break;
	case WM_MOUSEMOVE:
	{
	}
	break;
	case WM_DESTROY:
		DeleteObject(hFont);
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT ccrt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;
	static int index;

	static HBITMAP hChildBitmap, hChildOldBitmap;

	static CPoint movingCircle;

	static HBRUSH hBrush;
	static HBRUSH hOldBrush;

	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &ccrt);

		time = 10;
		index = 0;

		// Circle
		int i = 0;
		for (float angle = 0.0f; angle <= 1000.0f; angle += 0.1f)
		{
			smallCircleArray[i].x = (ccrt.right - ccrt.left) / 2 + 100 * cos(angle * DEG2RAD);
			smallCircleArray[i].y = (ccrt.bottom - ccrt.top) / 2 + 100 * sin(angle * DEG2RAD);
			i++;
		}

		i = 0;
		for (float angle = 0.0f; angle <= 1000.0f; angle += 0.1f)
		{
			normalCircleArray[i].x = (ccrt.right - ccrt.left) / 2 + 200 * cos(angle * DEG2RAD);
			normalCircleArray[i].y = (ccrt.bottom - ccrt.top) / 2 + 200 * sin(angle * DEG2RAD);
			i++;
		}

		i = 0;
		for (float angle = 0.0f; angle <= 1000.0f; angle += 0.1f)
		{
			bigCircleArray[i].x = (ccrt.right - ccrt.left) / 2 + 300 * cos(angle * DEG2RAD);
			bigCircleArray[i].y = (ccrt.bottom - ccrt.top) / 2 + 300 * sin(angle * DEG2RAD);
			i++;
		}

		// Rectangle
		int newX = (CHILD_WINDOW_X / 2);
		int newY = (CHILD_WINDOW_Y / 2);
		i = 0;

		for (i = 0; i < 4 * SMALL_RECT + 1; i++)
		{
			if (i >= 0 && i < SMALL_RECT)
			{
				smallRectangleArray[i].x = newX + (SMALL_RECT / 2);
				smallRectangleArray[i].y = newY - (SMALL_RECT / 2) + i;
			}
			else if (i >= SMALL_RECT && i < SMALL_RECT * 2)
			{
				smallRectangleArray[i].x = newX + (SMALL_RECT / 2) - (i - SMALL_RECT);
				smallRectangleArray[i].y = newY + (SMALL_RECT / 2);
			}
			else if (i >= 2 * SMALL_RECT && i < SMALL_RECT * 3)
			{
				smallRectangleArray[i].x = newX - (SMALL_RECT / 2);
				smallRectangleArray[i].y = newY + (SMALL_RECT / 2) - (i - 2 * SMALL_RECT);
			}
			else
			{
				smallRectangleArray[i].x = newX - (SMALL_RECT / 2) + (i - 3 * SMALL_RECT);
				smallRectangleArray[i].y = newY - (SMALL_RECT / 2);
			}
		}

		i = 0;

		for (i = 0; i < 4 * NORMAL_RECT + 1; i++)
		{
			if (i >= 0 && i < NORMAL_RECT)
			{
				normalRectangleArray[i].x = newX + (NORMAL_RECT / 2);
				normalRectangleArray[i].y = newY - (NORMAL_RECT / 2) + i;
			}
			else if (i >= NORMAL_RECT && i < NORMAL_RECT * 2)
			{
				normalRectangleArray[i].x = newX + (NORMAL_RECT / 2) - (i - NORMAL_RECT);
				normalRectangleArray[i].y = newY + (NORMAL_RECT / 2);
			}
			else if (i >= 2 * NORMAL_RECT && i < NORMAL_RECT * 3)
			{
				normalRectangleArray[i].x = newX - (NORMAL_RECT / 2);
				normalRectangleArray[i].y = newY + (NORMAL_RECT / 2) - (i - 2 * NORMAL_RECT);
			}
			else
			{
				normalRectangleArray[i].x = newX - (NORMAL_RECT / 2) + (i - 3 * NORMAL_RECT);
				normalRectangleArray[i].y = newY - (NORMAL_RECT / 2);
			}
		}

		i = 0;

		for (i = 0; i < 4 * BIG_RECT + 1; i++)
		{
			if (i >= 0 && i < BIG_RECT)
			{
				bigRectangleArray[i].x = newX + (BIG_RECT / 2);
				bigRectangleArray[i].y = newY - (BIG_RECT / 2) + i;
			}
			else if (i >= BIG_RECT && i < BIG_RECT * 2)
			{
				bigRectangleArray[i].x = newX + (BIG_RECT / 2) - (i - BIG_RECT);
				bigRectangleArray[i].y = newY + (BIG_RECT / 2);
			}
			else if (i >= 2 * BIG_RECT && i < BIG_RECT * 3)
			{
				bigRectangleArray[i].x = newX - (BIG_RECT / 2);
				bigRectangleArray[i].y = newY + (BIG_RECT / 2) - (i - 2 * BIG_RECT);
			}
			else
			{
				bigRectangleArray[i].x = newX - (BIG_RECT / 2) + (i - 3 * BIG_RECT);
				bigRectangleArray[i].y = newY - (BIG_RECT / 2);
			}
		}

		// Triangle
		i = 0;

		newX = 7 + (CHILD_WINDOW_X / 2);
		newY = 10 + (CHILD_WINDOW_Y / 2);

		for (i = 0; i < 3 * SMALL_TRI + 1; i++)
		{
			if (i >= 0 && i < SMALL_TRI)
			{
				smallTriangleArray[i].x = newX + (i / 2);
				smallTriangleArray[i].y = newY - (SMALL_TRI / 2) + i;
			}
			else if (i >= SMALL_TRI && i < SMALL_TRI * 2)
			{
				smallTriangleArray[i].x = newX + (SMALL_TRI / 2) - (i - SMALL_TRI);
				smallTriangleArray[i].y = newY + (SMALL_TRI / 2);
			}
			else
			{
				smallTriangleArray[i].x = newX - (SMALL_TRI / 2) + (i - 2 * SMALL_TRI) / 2;
				smallTriangleArray[i].y = newY + (SMALL_TRI / 2) - (i - 2 * SMALL_TRI);
			}
		}

		i = 0;
		for (i = 0; i < 3 * NORMAL_TRI + 1; i++)
		{
			if (i >= 0 && i < NORMAL_TRI)
			{
				normalTriangleArray[i].x = newX + (i / 2);
				normalTriangleArray[i].y = newY - (NORMAL_TRI / 2) + i;
			}
			else if (i >= NORMAL_TRI && i < NORMAL_TRI * 2)
			{
				normalTriangleArray[i].x = newX + (NORMAL_TRI / 2) - (i - NORMAL_TRI);
				normalTriangleArray[i].y = newY + (NORMAL_TRI / 2);
			}
			else
			{
				normalTriangleArray[i].x = newX - (NORMAL_TRI / 2) + (i - 2 * NORMAL_TRI) / 2;
				normalTriangleArray[i].y = newY + (NORMAL_TRI / 2) - (i - 2 * NORMAL_TRI);
			}
		}

		i = 0;
		for (i = 0; i < 3 * BIG_TRI + 1; i++)
		{
			if (i >= 0 && i < BIG_TRI)
			{
				bigTriangleArray[i].x = newX + (i / 2);
				bigTriangleArray[i].y = newY - (BIG_TRI / 2) + i;
			}
			else if (i >= BIG_TRI && i < BIG_TRI * 2)
			{
				bigTriangleArray[i].x = newX + (BIG_TRI / 2) - (i - BIG_TRI);
				bigTriangleArray[i].y = newY + (BIG_TRI / 2);
			}
			else
			{
				bigTriangleArray[i].x = newX - (BIG_TRI / 2) + (i - 2 * BIG_TRI) / 2;
				bigTriangleArray[i].y = newY + (BIG_TRI / 2) - (i - 2 * BIG_TRI);
			}
		}

		movingCircle.setXY(normalCircleArray[0].x, normalCircleArray[0].y);

		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_TIMER:
	{
		time++;

		if (isClockWise)
		{
			if (time > 10 / divTime)
			{
				index++;
				time = 0;
			}

			switch (g_ShapeMode)
			{
			case CIRCLE:
			{
				if (index >= 3600)
					index = 0;
			}
			break;
			case RECTANGLE:
			{
				switch (g_SizeMode)
				{
				case SMALL:
				{
					if (index > SMALL_RECT * 4)
						index = 0;
				}
				break;
				case NORMAL:
				{
					if (index > NORMAL_RECT * 4)
						index = 0;
				}
				break;
				case BIG:
				{
					if (index > BIG_RECT * 4)
						index = 0;
				}
				break;
				}
			}
			break;
			case TRIANGLE:
			{
				switch (g_SizeMode)
				{
				case SMALL:
				{
					if (index > SMALL_TRI* 3)
						index = 0;
				}
				break;
				case NORMAL:
				{
					if (index > NORMAL_TRI * 3)
						index = 0;
				}
				break;
				case BIG:
				{
					if (index > BIG_TRI * 3)
						index = 0;
				}
				break;
				}
			}
			break;
			}
		}
		else
		{
			if (time > 10 / divTime)
			{
				index--;
				time = 0;
			}

			switch (g_ShapeMode)
			{
			case CIRCLE:
			{
				if (index < 0)
					index = 3600 - 1;
			}
			break;
			case RECTANGLE:
			{
				switch (g_SizeMode)
				{
				case SMALL:
				{
					if (index < 0)
						index = SMALL_RECT * 4;
				}
				break;
				case NORMAL:
				{
					if (index < 0)
						index = NORMAL_RECT * 4;
				}
				break;
				case BIG:
				{
					if (index < 0)
						index = BIG_RECT * 4;
				}
				break;
				}
			}
			break;
			case TRIANGLE:
			{
				switch (g_SizeMode)
				{
				case SMALL:
				{
					if (index < 0)
						index = SMALL_TRI * 3;
				}
				break;
				case NORMAL:
				{
					if (index < 0)
						index = NORMAL_TRI * 3;
				}
				break;
				case BIG:
				{
					if (index < 0)
						index = BIG_TRI * 3;
				}
				break;
				}
			}
			break;
			}
		}

		switch (g_SizeMode)
		{
		case SMALL:
		{
			switch (g_ShapeMode)
			{
			case CIRCLE:
			{
				movingCircle.setXY(smallCircleArray[index].x, smallCircleArray[index].y);
			}
			break;
			case RECTANGLE:
			{
				movingCircle.setXY(smallRectangleArray[index].x, smallRectangleArray[index].y);
			}
			break;
			case TRIANGLE:
			{
				movingCircle.setXY(smallTriangleArray[index].x, smallTriangleArray[index].y);
			}
			break;
			}
		}
		break;
		case NORMAL:
		{
			switch (g_ShapeMode)
			{
			case CIRCLE:
			{
				movingCircle.setXY(normalCircleArray[index].x, normalCircleArray[index].y);
			}
			break;
			case RECTANGLE:
			{
				movingCircle.setXY(normalRectangleArray[index].x, normalRectangleArray[index].y);
			}
			break;
			case TRIANGLE:
			{
				movingCircle.setXY(normalTriangleArray[index].x, normalTriangleArray[index].y);
			}
			break;
			}
		}
		break;
		case BIG:
		{
			switch (g_ShapeMode)
			{
			case CIRCLE:
			{
				movingCircle.setXY(bigCircleArray[index].x, bigCircleArray[index].y);
			}
			break;
			case RECTANGLE:
			{
				movingCircle.setXY(bigRectangleArray[index].x, bigRectangleArray[index].y);
			}
			break;
			case TRIANGLE:
			{
				movingCircle.setXY(bigTriangleArray[index].x, bigTriangleArray[index].y);
			}
			break;
			}
		}
		break;
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hChildBitmap = CreateCompatibleBitmap(hdc, ccrt.right, ccrt.bottom);
		hChildOldBitmap = (HBITMAP)SelectObject(memdc, hChildBitmap);

		Rectangle(memdc, ccrt.left, ccrt.top, ccrt.right, ccrt.bottom);

		switch (g_ShapeMode)
		{
		case CIRCLE:
		{
			switch (g_SizeMode)
			{
			case SMALL:
			{
				Polyline(memdc, smallCircleArray, 10000);     // 선을연속적으로그려준다.
			}
			break;
			case NORMAL:
			{
				Polyline(memdc, normalCircleArray, 10000);     // 선을연속적으로그려준다.
			}
			break;
			case BIG:
			{
				Polyline(memdc, bigCircleArray, 10000);     // 선을연속적으로그려준다.
			}
			break;
			}
		}
		break;
		case RECTANGLE:
		{
			switch (g_SizeMode)
			{
			case SMALL:
			{
				Polyline(memdc, smallRectangleArray, SMALL_RECT + SMALL_RECT + SMALL_RECT + SMALL_RECT + 1);     // 선을연속적으로그려준다.
			}
			break;
			case NORMAL:
			{
				Polyline(memdc, normalRectangleArray, NORMAL_RECT + NORMAL_RECT + NORMAL_RECT + NORMAL_RECT + 1);     // 선을연속적으로그려준다.
			}
			break;
			case BIG:
			{
				Polyline(memdc, bigRectangleArray, BIG_RECT + BIG_RECT + BIG_RECT + BIG_RECT + 1);     // 선을연속적으로그려준다.
			}
			break;
			}
		}
		break;
		case TRIANGLE:
		{
			switch (g_SizeMode)
			{
			case SMALL:
			{
				Polyline(memdc, smallTriangleArray, 3 * SMALL_TRI + 1);     // 선을연속적으로그려준다.
			}
			break;
			case NORMAL:
			{
				Polyline(memdc, normalTriangleArray, 3 * NORMAL_TRI + 1);     // 선을연속적으로그려준다.
			}
			break;
			case BIG:
			{
				Polyline(memdc, bigTriangleArray, 3 * BIG_TRI + 1);     // 선을연속적으로그려준다.
			}
			break;
			}
		}
		break;
		}

		MoveToEx(memdc, ccrt.left, (ccrt.bottom - ccrt.top) / 2, NULL);
		LineTo(memdc, ccrt.right, (ccrt.bottom - ccrt.top) / 2);

		MoveToEx(memdc, (ccrt.right - ccrt.left) / 2, ccrt.top, NULL);
		LineTo(memdc, (ccrt.right - ccrt.left) / 2, ccrt.bottom);

		// TODO : 움직이는 원 그리기
		if (g_Color[0])
		{
			hBrush = CreateSolidBrush(COLORREF(RGB(0, 255, 255)));
		}
		else if (g_Color[1])
		{
			hBrush = CreateSolidBrush(COLORREF(RGB(255, 0, 255)));
		}
		else if(g_Color[2])
		{
			hBrush = CreateSolidBrush(COLORREF(RGB(255, 255, 0)));
		}

		hOldBrush = (HBRUSH)SelectObject(memdc, hBrush);
		Ellipse(memdc, movingCircle.getX() - g_Radius, movingCircle.getY() - g_Radius, movingCircle.getX() + g_Radius, movingCircle.getY() + g_Radius);
		SelectObject(memdc, hOldBrush);
		DeleteObject(hBrush);

		BitBlt(hdc, 0, 0, ccrt.right, ccrt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hChildOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}