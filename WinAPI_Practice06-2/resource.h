//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// WinAPI_Practice06-2.rc에서 사용되고 있습니다.
//
#define IDB_BITMAP1                     101
#define IDB_BITMAP2                     102
#define IDB_BITMAP3                     103
#define IDB_BITMAP4                     104
#define IDB_BITMAP5                     105
#define IDB_BITMAP6                     106
#define IDC_RADIO_10X20                 1001
#define IDC_RADIO_15X25                 1002
#define IDC_BUTTON_LEFT                 1003
#define IDC_CHECK_RED                   1004
#define IDC_BUTTON_RIGHT                1005
#define IDC_BUTTON_CLOCK_ROT            1006
#define IDC_BUTTON_ANTICLOCK_ROT        1007
#define IDC_RADIO_GRID_ON               1008
#define IDC_RADIO_GRID_OFF              1009
#define IDC_BUTTON_SLOW                 1010
#define IDC_BUTTON_NORMAL               1011
#define IDC_BUTTON_FAST                 1012
#define IDC_CHECK_GREEN                 1013
#define IDC_CHECK_BLUE                  1014
#define IDC_STATIC1                     1015
#define IDC_STATIC3                     1016
#define IDD_DIALOG6_2                   1016
#define IDC_STATIC2                     1017
#define ID_EXIT                         1018
#define ID_EXIT2                        1019
#define ID_SETTING                      1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
