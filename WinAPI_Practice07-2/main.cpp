#include <Windows.h>
#include <math.h>
#include <iostream>

using namespace std;

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

#define WINDOW_X 1550
#define WINDOW_Y 900

#define CHILD_WINDOW_X 700
#define CHILD_WINDOW_Y 500

#define FRAME_X 135
#define FRAME_Y 150

#define LEFT 0
#define RIGHT 1
#define DOWN 2
#define UP 3

#define IDC_CHILD1_MOVE_LEFTRIGHT 1001
#define IDC_CHILD1_MOVE_UPDOWN 1002
#define IDC_CHILD1_MOVE_JUMP 1003
#define IDC_CHILD1_SCALEVARIATION 1004

#define IDC_CHILD2_MOVE_LEFTRIGHT 2001
#define IDC_CHILD2_MOVE_UPDOWN 2002
#define IDC_CHILD2_MOVE_JUMP 2003
#define IDC_CHILD2_SCALEVARIATION 2004

#define IDC_STOP_ALL 3001
#define IDC_CHANGE 3002
#define IDC_PROGRAM_EXIT 3003

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "ParentClass";
LPCTSTR lpszChild1Class = "ChildClass1";
LPCTSTR lpszChild2Class = "ChildClass2";
LPCTSTR lpszButtonClass = "button";
LPCTSTR lpszWindowName = "Window Program 7-2 : 2개의 차일드 윈도우 컨트롤하기";

class CPoint
{
private:
	int x;
	int y;
	bool isRectangle;
	int size;
	int speed;
	int direction = RIGHT;
	bool isGaroMode = true;
	bool isScaleVariationMode = false;
	bool isJump = false;

	bool isBigger = true;
	bool isAllStop = false;
	bool isFlyingUp = true;
	int stack = 0;

	HBRUSH hBrush;
	HBRUSH hOldBrush;

public:
	const int getX() const { return x; }
	const int getY() const { return y; }
	const int getSize() const { return size; }
	const int getSpeed() const { return speed; }
	const bool getIsRectangle() const { return isRectangle; }
	const int getDirection() const { return direction; }
	const bool getIsGaroMode() const { return isGaroMode; }
	const bool getIsScaleVariationMode() const { return isScaleVariationMode; }
	const bool getIsAllStop() const { return isAllStop; }
	const bool getIsJump() const { return isJump; }

public:
	void Render(HDC hdc)
	{
		if (isRectangle)
		{
			hBrush = CreateSolidBrush(COLORREF(RGB(255, 0, 255)));
			hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
			Rectangle(hdc, x, y, x + size, y + size);
			SelectObject(hdc, hOldBrush);
			DeleteObject(hBrush);
		}
		else
		{
			hBrush = CreateSolidBrush(COLORREF(RGB(0, 255, 255)));
			hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
			Ellipse(hdc, x, y, x + size, y + size);
			SelectObject(hdc, hOldBrush);
			DeleteObject(hBrush);
		}
	}
	void Update()
	{
		if (isAllStop)
		{
			size = 50;
			speed = 0;
		}
		else
		{
			if (isJump)
			{
				speed = 0;

				if (isFlyingUp)
				{
					if (isGaroMode)
					{
						stack += 1;

						if (stack >= 10)
						{
							isFlyingUp = false;
						}
						else
						{
							y += 1;
						}
					}
					else
					{
						stack += 1;

						if (stack >= 10)
						{
							isFlyingUp = false;
						}
						else
						{
							x += 1;
						}
					}
				}
				else
				{
					if (isGaroMode)
					{
						stack -= 1;
						y -= 1;
						if (stack <= 1)
						{
							isFlyingUp = true;
							isJump = false;
						}
					}
					else
					{
						stack -= 1;
						x -= 1;
						if (stack <= 1)
						{
							isFlyingUp = true;
							isJump = false;
						}
					}
				}
			}
			else
			{
				if (isScaleVariationMode)
				{
					speed = 0;

					if (isBigger)
					{
						size += 1;
						if (size > 75)
							isBigger = false;
					}
					else
					{
						size -= 1;
						if (size < 25)
							isBigger = true;
					}
				}
				else
				{
					size = 50;
					speed = 1;
				}

				if (isGaroMode)
				{
					if ((x == CHILD_WINDOW_X + 17) && (y == CHILD_WINDOW_Y + 37))
					{
						x = 0;
						y = 0;
						direction = RIGHT;
					}

					if (direction == RIGHT) // 오른쪽
					{
						if ((x + size == CHILD_WINDOW_X + 17))
						{
							direction = DOWN;
						}
						else
						{
							x += speed;
						}
					}
					else if (direction == LEFT) // 왼쪽
					{
						if ((x - speed < 0))
						{
							direction = DOWN;
						}
						else
						{
							x -= speed;
						}
					}
					else
					{
						y += speed;
						if ((y % 50) == 0)
						{
							if (x == 0)
							{
								direction = RIGHT;
							}
							else if (x + size == CHILD_WINDOW_X + 17)
							{
								direction = LEFT;
							}
						}
					}
				}
				else
				{
					if ((x == CHILD_WINDOW_X + 17) && (y == 0))
					{
						x = 0;
						y = 0;
						direction = DOWN;
					}
					else if ((x == CHILD_WINDOW_X + 17) && (y == CHILD_WINDOW_Y + 37 - size))
					{
						x = 0;
						y = 0;
						direction = DOWN;
					}

					if (direction == DOWN) // 아래쪽
					{
						if ((y == CHILD_WINDOW_Y + 37 - size))
						{
							direction = RIGHT;
						}
						else
						{
							y += speed;
						}
					}
					else if (direction == UP) // 왼쪽
					{
						if ((y == 0))
						{
							direction = RIGHT;
						}
						else
						{
							y -= speed;
						}
					}
					else
					{
						x += speed;
						if ((x % 50) == 0)
						{
							if (y == 0)
							{
								direction = DOWN;
							}
							else if (y == CHILD_WINDOW_Y + 37 - size)
							{
								direction = UP;
							}
						}
					}
				}
			}
		}
	}

public:
	void setX(int positionX) { x = positionX; }
	void setY(int positionY) { y = positionY; }
	void setXY(int positionX, int positionY)
	{
		x = positionX;
		y = positionY;
	}
	void setIsRectangle(bool value) { isRectangle = value; }
	void setSize(int value) { size = value; }
	void setSpeed(int value) { speed = value; }
	void setDirection(int dir) { direction = dir; }
	void setIsGaroMode(bool value) { isGaroMode = value; }
	void setIsScaleVariationMode(bool value) { isScaleVariationMode = value; }
	void setIsAllStop(bool value) { isAllStop = value; }
	void setIsJump(bool value) { isJump = value; }

public: // operator
	CPoint& operator=(const CPoint& rhs)
	{
		if (this != &rhs)
		{
			x = rhs.x;
			y = rhs.y;
			isRectangle = rhs.isRectangle;
			size = rhs.size;
			speed = rhs.speed;
			direction = rhs.direction;
			isGaroMode = rhs.isGaroMode;
			isScaleVariationMode = rhs.isScaleVariationMode;
			isAllStop = rhs.isAllStop;
			isJump = rhs.isJump;
			isBigger = rhs.isBigger;
			isFlyingUp = rhs.isFlyingUp;
		}

		return *this;
	}

public: // constructor
	CPoint() : x(0), y(0), isRectangle(false), size(0), speed(0)
	{
	}
	CPoint(int positionX, int positionY, bool value, int sizeValue, int speedValue) : x(0), y(0), isRectangle(false), size(0), speed(0)
	{
		x = positionX;
		y = positionY;
		isRectangle = value;
		size = sizeValue;
		speed = speedValue;
	}
	CPoint(const CPoint& rhs) : x(0), y(0), isRectangle(false), size(0), speed(0), direction(-1), isGaroMode(true)
	{
		x = rhs.x;
		y = rhs.y;
		isRectangle = rhs.isRectangle;
		size = rhs.size;
		speed = rhs.speed;
		direction = rhs.direction;
		isGaroMode = rhs.isGaroMode;
		isScaleVariationMode = rhs.isScaleVariationMode;
		isAllStop = rhs.isAllStop;
		isJump = rhs.isJump;
	}
};

CPoint child1Shape;
CPoint child2Shape;
CPoint save;

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK Child1WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK Child2WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	// 차일드 윈도우1 클래스 등록
	WndClass.hCursor = LoadCursor(NULL, IDC_HELP);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	WndClass.lpszClassName = lpszChild1Class;
	WndClass.lpfnWndProc = (WNDPROC)Child1WndProc;
	RegisterClassEx(&WndClass);

	// 차일드 윈도우1 클래스 등록
	WndClass.hCursor = LoadCursor(NULL, IDC_HELP);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	WndClass.lpszClassName = lpszChild2Class;
	WndClass.lpfnWndProc = (WNDPROC)Child2WndProc;
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static HBITMAP hBitmap, hOldBitmap;
	HWND hWndChild1, hWndChild2;

	RECT childrt = { 0, 0, CHILD_WINDOW_X, CHILD_WINDOW_Y };

	static HWND hButton_Child1_LeftRightMove, hButton_Child2_LeftRightMove;
	static HWND hButton_Child1_UpDownMove, hButton_Child2_UpDownMove;
	static HWND hButton_Child1_Jump, hButton_Child2_Jump;
	static HWND hButton_Child1_ScaleVariation, hButton_Child2_ScaleVariation;
	static HWND hButton_StopAll, hButton_Change, hButtonProgramExit;

	static HFONT hFont;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);

		AdjustWindowRect(&childrt, WS_OVERLAPPEDWINDOW, false);
		hWndChild1 = CreateWindow(lpszChild1Class, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER,
			50, 50, childrt.right - childrt.left, childrt.bottom - childrt.top, hWnd, NULL, g_hInstance, NULL);
		hWndChild2 = CreateWindow(lpszChild2Class, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER,
			50 + CHILD_WINDOW_X + 50, 50, childrt.right - childrt.left, childrt.bottom - childrt.top, hWnd, NULL, g_hInstance, NULL);

		// TODO:
		hFont = CreateFont(20, 0, 0, 0, 800, 0, 0, 0, HANGEUL_CHARSET, 0, 0, 0, 0, TEXT("맑은 고딕"));

		hButton_Child1_LeftRightMove = CreateWindow(lpszButtonClass, "좌/우 이동", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			100,
			700,
			100, 50, hWnd, (HMENU)IDC_CHILD1_MOVE_LEFTRIGHT, g_hInstance, NULL);
		SendMessage(hButton_Child1_LeftRightMove, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButton_Child1_UpDownMove = CreateWindow(lpszButtonClass, "상/하 이동", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			100 + 100 + 50,
			700,
			100, 50, hWnd, (HMENU)IDC_CHILD1_MOVE_UPDOWN, g_hInstance, NULL);
		SendMessage(hButton_Child1_UpDownMove, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButton_Child1_Jump = CreateWindow(lpszButtonClass, "제자리 점프", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			100 + 50 + 100 + 50 + 100 + 50,
			700,
			100, 50, hWnd, (HMENU)IDC_CHILD1_MOVE_JUMP, g_hInstance, NULL);
		SendMessage(hButton_Child1_Jump, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButton_Child1_ScaleVariation = CreateWindow(lpszButtonClass, "크게/작게", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			100 + 50 + 100 + 50 + 100 + 50 + 100 + 50,
			700,
			100, 50, hWnd, (HMENU)IDC_CHILD1_SCALEVARIATION, g_hInstance, NULL);
		SendMessage(hButton_Child1_ScaleVariation, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButton_Child2_LeftRightMove = CreateWindow(lpszButtonClass, "좌/우 이동", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100,
			700,
			100, 50, hWnd, (HMENU)IDC_CHILD2_MOVE_LEFTRIGHT, g_hInstance, NULL);
		SendMessage(hButton_Child2_LeftRightMove, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButton_Child2_UpDownMove = CreateWindow(lpszButtonClass, "상/하 이동", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 100 + 50,
			700,
			100, 50, hWnd, (HMENU)IDC_CHILD2_MOVE_UPDOWN, g_hInstance, NULL);
		SendMessage(hButton_Child2_UpDownMove, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButton_Child2_Jump = CreateWindow(lpszButtonClass, "제자리 점프", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 50 + 100 + 50 + 100 + 50,
			700,
			100, 50, hWnd, (HMENU)IDC_CHILD2_MOVE_JUMP, g_hInstance, NULL);
		SendMessage(hButton_Child2_Jump, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButton_Child2_ScaleVariation = CreateWindow(lpszButtonClass, "크게/작게", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) + 100 + 50 + 100 + 50 + 100 + 50 + 100 + 50,
			700,
			100, 50, hWnd, (HMENU)IDC_CHILD2_SCALEVARIATION, g_hInstance, NULL);
		SendMessage(hButton_Child2_ScaleVariation, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButton_StopAll = CreateWindow(lpszButtonClass, "모두 정지", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) - 50 - 100 - 50,
			800,
			100, 50, hWnd, (HMENU)IDC_STOP_ALL, g_hInstance, NULL);
		SendMessage(hButton_StopAll, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButton_Change = CreateWindow(lpszButtonClass, "서로 바꾸기", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) - 50,
			800,
			100, 50, hWnd, (HMENU)IDC_CHANGE, g_hInstance, NULL);
		SendMessage(hButton_Change, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButtonProgramExit = CreateWindow(lpszButtonClass, "프로그램종료", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			(WINDOW_X / 2) - 50 + 100 + 50,
			800,
			100, 50, hWnd, (HMENU)IDC_PROGRAM_EXIT, g_hInstance, NULL);
		SendMessage(hButtonProgramExit, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_CHILD1_MOVE_LEFTRIGHT:
		{
			if (SendMessage(hButton_Child1_LeftRightMove, BN_CLICKED, 0, 0) == STN_CLICKED)
			{
				child1Shape.setIsAllStop(false);
				child1Shape.setSpeed(1);
				child1Shape.setIsGaroMode(true);
				child1Shape.setDirection(RIGHT);
			}
		}
		break;
		case IDC_CHILD1_MOVE_UPDOWN:
		{
			if (SendMessage(hButton_Child1_UpDownMove, BN_CLICKED, 0, 0) == STN_CLICKED)
			{
				child1Shape.setIsAllStop(false);
				child1Shape.setSpeed(1);
				child1Shape.setIsGaroMode(false);
				child1Shape.setDirection(DOWN);
			}
		}
		break;
		case IDC_CHILD1_MOVE_JUMP:
		{
			if (SendMessage(hButton_Child1_Jump, BN_CLICKED, 0, 0) == STN_CLICKED)
			{
				child1Shape.setIsJump(true);
			}
		}
		break;
		case IDC_CHILD1_SCALEVARIATION:
		{
			if (SendMessage(hButton_Child1_ScaleVariation, BN_CLICKED, 0, 0) == STN_CLICKED)
			{
				if(child1Shape.getIsScaleVariationMode())
					child1Shape.setIsScaleVariationMode(false);
				else
					child1Shape.setIsScaleVariationMode(true);
			}
		}
		break;
		case IDC_CHILD2_MOVE_LEFTRIGHT:
		{
			if (SendMessage(hButton_Child2_LeftRightMove, BN_CLICKED, 0, 0) == STN_CLICKED)
			{
				child2Shape.setIsAllStop(false);
				child2Shape.setSpeed(1);
				child2Shape.setIsGaroMode(true);
				child2Shape.setDirection(RIGHT);
			}
		}
		break;
		case IDC_CHILD2_MOVE_UPDOWN:
		{
			if (SendMessage(hButton_Child2_UpDownMove, BN_CLICKED, 0, 0) == STN_CLICKED)
			{
				child2Shape.setIsAllStop(false);
				child2Shape.setSpeed(1);
				child2Shape.setIsGaroMode(false);
				child2Shape.setDirection(DOWN);
			}
		}
		break;
		case IDC_CHILD2_MOVE_JUMP:
		{
			if (SendMessage(hButton_Child2_Jump, BN_CLICKED, 0, 0) == STN_CLICKED)
			{
				child2Shape.setIsJump(true);
			}
		}
		break;
		case IDC_CHILD2_SCALEVARIATION:
		{
			if (SendMessage(hButton_Child2_ScaleVariation, BN_CLICKED, 0, 0) == STN_CLICKED)
			{
				if (child2Shape.getIsScaleVariationMode())
					child2Shape.setIsScaleVariationMode(false);
				else
					child2Shape.setIsScaleVariationMode(true);
			}
		}
		break;
		case IDC_STOP_ALL:
		{
			if (SendMessage(hButton_StopAll, BN_CLICKED, 0, 0) == STN_CLICKED)
			{
				child1Shape.setIsAllStop(true);
				child2Shape.setIsAllStop(true);
			}
		}
		break;
		case IDC_CHANGE:
		{
			if (SendMessage(hButton_Change, BN_CLICKED, 0, 0) == STN_CLICKED)
			{
				save = child1Shape;
				child1Shape = child2Shape;
				child2Shape = save;
			}
		}
		break;
		case IDC_PROGRAM_EXIT:
		{
			if (SendMessage(hButtonProgramExit, BN_CLICKED, 0, 0) == STN_CLICKED)
				PostQuitMessage(0);
		}
		break;
		}
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);

		Rectangle(memdc, crt.left, crt.top, crt.right, crt.bottom);

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

LRESULT CALLBACK Child1WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT c1crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static HBITMAP hChildBitmap, hChildOldBitmap;
	
	static HBRUSH hBrush;
	static HBRUSH hOldBrush;

	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &c1crt);
		child1Shape.setXY(0, 0);
		child1Shape.setSize(50);
		child1Shape.setSpeed(1);
		child1Shape.setIsRectangle(false);
		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_TIMER:
	{
		child1Shape.Update();
		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hChildBitmap = CreateCompatibleBitmap(hdc, c1crt.right, c1crt.bottom);
		hChildOldBitmap = (HBITMAP)SelectObject(memdc, hChildBitmap);

		Rectangle(memdc, c1crt.left, c1crt.top, c1crt.right, c1crt.bottom);

		child1Shape.Render(memdc);

		BitBlt(hdc, 0, 0, c1crt.right, c1crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hChildOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

LRESULT CALLBACK Child2WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT c2crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static HBITMAP hChildBitmap, hChildOldBitmap;

	static HBRUSH hBrush;
	static HBRUSH hOldBrush;

	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &c2crt);
		child2Shape.setXY(0, 0);
		child2Shape.setSize(50);
		child2Shape.setSpeed(1);
		child2Shape.setIsRectangle(true);
		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_TIMER:
	{
		child2Shape.Update();
		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hChildBitmap = CreateCompatibleBitmap(hdc, c2crt.right, c2crt.bottom);
		hChildOldBitmap = (HBITMAP)SelectObject(memdc, hChildBitmap);

		Rectangle(memdc, c2crt.left, c2crt.top, c2crt.right, c2crt.bottom);

		child2Shape.Render(memdc);

		BitBlt(hdc, 0, 0, c2crt.right, c2crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hChildOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}