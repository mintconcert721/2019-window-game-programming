#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <crtdbg.h>
#include <iostream>

//#ifdef UNICODE
//#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
//#else
//#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
//#endif

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 3-1";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 1000
#define WINDOW_Y 1000

#define SECOND 1000
#define BOARD_NUMBER 40
#define MAX_TAIL_NUMBER 30
#define HERO_TAIL_NUMBER 10

#define KIND_BLANK 0
#define KIND_HERO 1
#define KIND_FOLLOW_TAIL 2
#define KIND_SPAWN_TAIL 3

#define DIRECTION_UP 0
#define DIRECTION_DOWN 1
#define DIRECTION_LEFT 2
#define DIRECTION_RIGHT 3

static int tail_index = 0;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);
void CALLBACK FoodTimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);

float LengthPts(int x1, int y1, int x2, int y2)
{
	return (sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1)));
}

typedef struct _Coordi {
	int x;
	int y;
	struct _Coordi() {}
	struct _Coordi(int _x, int _y)
	{
		x = _x;
		y = _y;
	}
	struct _Coordi& operator =(const struct _Coordi& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}
} Coordi;

typedef struct _Point {
	Coordi m_CurrentPosition; // 중심좌표
	RECT m_Rect;
	int m_Direction;
} Point;

typedef struct _Hero {
	Coordi m_CurrentPosition; // 중심좌표
	Coordi m_PreviousPosition; // 중심좌표
	Coordi m_NextPosition;

	RECT m_Rect;
	
	int m_Direction;
	int m_Radius = WINDOW_X / BOARD_NUMBER / 2;

	bool isVisible = false;
	bool isSelected = false;
	bool m_GoToMousePosition = false;
	bool isYMoveFinished = false;
	bool isBigger = false;
	bool isJump = false;
	bool isCollideWithHero = false;
	bool isCollideWithAnotherTail = false;
	bool isVariableFinished = false;
	bool isSnakeTail= false;
	int myIndex = 0;

	void SetDirection(int a, int b, int c)
	{
		srand(unsigned int(time(NULL)));

		int random_number = rand() % 3;

		// 랜덤 좌표 넣어주기(해당 자리에 다른 플레이어가 없을 때까지 랜덤뽑기)
		switch (m_Direction)
		{
		case DIRECTION_UP:
			{
				int numberArray[3] = { a,b,c };
				m_Direction = numberArray[random_number];
			}
			break;
		case DIRECTION_DOWN:
			{
				int numberArray[3] = { a,b,c };
				m_Direction = numberArray[random_number];
			}
			break;
		case DIRECTION_LEFT:
			{
				int numberArray[3] = { a,b,c };
				m_Direction = numberArray[random_number];
			}
			break;
		case DIRECTION_RIGHT:
			{
				int numberArray[3] = { a,b,c };
				m_Direction = numberArray[random_number];
			}
			break;
		}
	}

	void SetEdgeDirection(int a, int b)
	{
		srand(unsigned int(time(NULL)));

		int random_number = rand() % 2;

		// 랜덤 좌표 넣어주기(해당 자리에 다른 플레이어가 없을 때까지 랜덤뽑기)
		switch (m_Direction)
		{
		case DIRECTION_UP:
		{
			int numberArray[2] = { a,b };
			m_Direction = numberArray[random_number];
		}
		break;
		case DIRECTION_DOWN:
		{
			int numberArray[2] = { a,b };
			m_Direction = numberArray[random_number];
		}
		break;
		case DIRECTION_LEFT:
		{
			int numberArray[2] = { a,b };
			m_Direction = numberArray[random_number];
		}
		break;
		case DIRECTION_RIGHT:
		{
			int numberArray[2] = { a,b };
			m_Direction = numberArray[random_number];
		}
		break;
		}
	}

	void UpdateTail(HWND hWnd, struct _Hero* _head, int _speed, struct _Hero* _obj[], int _SnakeTailIndex)
	{
		if (isSnakeTail == true)
		{
			m_PreviousPosition = m_CurrentPosition;

			m_Direction = _head->m_Direction;

			if (myIndex > 1)
			{
				m_CurrentPosition = _obj[_SnakeTailIndex - 2]->m_CurrentPosition;
			}
			else
				m_CurrentPosition = _head->m_PreviousPosition;

			switch (_head->m_Direction)
			{
			case DIRECTION_UP:
				if (m_CurrentPosition.x - _speed > 0)
					m_CurrentPosition.x = m_CurrentPosition.x - _speed + myIndex - 1;
				/*if (myIndex > 1)
				{
					m_CurrentPosition.y = m_CurrentPosition.y - myIndex - 1;
					m_CurrentPosition.x = m_CurrentPosition.x + myIndex - 1;
				}*/
				break;
			case DIRECTION_DOWN:
				if (m_CurrentPosition.x + _speed < BOARD_NUMBER - 1)
					m_CurrentPosition.x = m_CurrentPosition.x + _speed + myIndex - 1;
				/*if (myIndex > 1)
				{
					m_CurrentPosition.y = m_CurrentPosition.y + myIndex - 1;
					m_CurrentPosition.x = m_CurrentPosition.x - myIndex - 1;
				}*/
				break;
			case DIRECTION_LEFT:
				if(m_CurrentPosition.y - _speed > 0)
					m_CurrentPosition.y = m_CurrentPosition.y - _speed + myIndex - 1;
				/*if (myIndex > 1)
					m_CurrentPosition.y = m_CurrentPosition.y + myIndex - 1;*/
				break;
			case DIRECTION_RIGHT:
				if(m_CurrentPosition.y + _speed < BOARD_NUMBER - 1)
					m_CurrentPosition.y = m_CurrentPosition.y + _speed + myIndex - 1;
				break;
			}

			m_Rect.left = m_CurrentPosition.x * m_Radius;
			m_Rect.top = m_CurrentPosition.y * m_Radius;
			m_Rect.right = m_Rect.left + m_Radius;
			m_Rect.bottom = m_Rect.top + m_Radius;
		}
	}

	void Update(HWND hWnd, int _speed)
	{
		m_PreviousPosition = m_CurrentPosition;

		if (m_GoToMousePosition)
		{
			if (isYMoveFinished == false)
			{
				if (m_CurrentPosition.y > m_NextPosition.x) // 마우스 클릭 지점이 주인공보다 왼쪽에 있다면
				{
					m_Direction = DIRECTION_LEFT;
				}
				else if (m_CurrentPosition.y < m_NextPosition.x)
				{
					m_Direction = DIRECTION_RIGHT;
				}

				switch (m_Direction)
				{
				case DIRECTION_UP:
					m_CurrentPosition.x -= _speed;
					break;
				case DIRECTION_DOWN:
					m_CurrentPosition.x += _speed;
					break;
				case DIRECTION_LEFT:
					m_CurrentPosition.y -= _speed;
					break;
				case DIRECTION_RIGHT:
					m_CurrentPosition.y += _speed;
					break;
				}

				if (m_CurrentPosition.y == m_NextPosition.x) isYMoveFinished = true;
			}
			else
			{
				if (m_CurrentPosition.x > m_NextPosition.y)
				{
					m_Direction = DIRECTION_UP;
				}
				else if (m_CurrentPosition.x < m_NextPosition.y)
				{
					m_Direction = DIRECTION_DOWN;
				}

				switch (m_Direction)
				{
				case DIRECTION_UP:
					m_CurrentPosition.x -= _speed;
					break;
				case DIRECTION_DOWN:
					m_CurrentPosition.x += _speed;
					break;
				case DIRECTION_LEFT:
					m_CurrentPosition.y -= _speed;
					break;
				case DIRECTION_RIGHT:
					m_CurrentPosition.y += _speed;
					break;
				}

				if (m_CurrentPosition.x == m_NextPosition.y)
				{
					m_GoToMousePosition = false;
					isYMoveFinished = false;
				}
			}
		}

		if (isSelected == false && m_GoToMousePosition == false && isYMoveFinished == false)
		{
			switch (m_Direction)
			{
			case DIRECTION_UP:
				m_CurrentPosition.x -= _speed;
				break;
			case DIRECTION_DOWN:
				m_CurrentPosition.x += _speed;
				break;
			case DIRECTION_LEFT:
				m_CurrentPosition.y -= _speed;
				break;
			case DIRECTION_RIGHT:
				m_CurrentPosition.y += _speed;
				break;
			}
		}

		m_Rect.left = m_CurrentPosition.x * m_Radius;
		m_Rect.top = m_CurrentPosition.y * m_Radius;
		m_Rect.right = m_Rect.left + m_Radius;
		m_Rect.bottom = m_Rect.top + m_Radius;
	}

	bool IsInCircle(int mouseX, int mouseY)
	{
		if (LengthPts(m_Rect.left + (m_Radius / 2), m_Rect.top + (m_Radius / 2), mouseX, mouseY) < m_Radius / 2)
			return true;
		else
			return false;
	}

	struct _Hero& operator =(const struct _Hero& rhs)
	{
		m_CurrentPosition = rhs.m_CurrentPosition;
		m_PreviousPosition = rhs.m_PreviousPosition;
		m_NextPosition = rhs.m_NextPosition;

		m_Rect = rhs.m_Rect;

		m_Direction = rhs.m_Direction;
		m_Radius = rhs.m_Radius;

		isVisible = rhs.isVisible;
		isSelected = rhs.isSelected;
		m_GoToMousePosition = rhs.m_GoToMousePosition;
		isYMoveFinished = rhs.isYMoveFinished;
		isBigger = rhs.isBigger;
		isJump = rhs.isJump;
		isCollideWithHero = rhs.isCollideWithHero;

		return *this;
	}
} Hero;

class CProjectManager
{
private:
	HDC m_Hdc;

	Point* m_VirtualRect[BOARD_NUMBER][BOARD_NUMBER];
	Hero* m_Hero;
	Hero* m_Tail[MAX_TAIL_NUMBER];
	Hero* m_HeroTail[10];

	Coordi m_ProjectMouse;

	HBRUSH m_Hbrush;
	HBRUSH m_Oldbrush;

	HBRUSH m_HTailbrush;
	HBRUSH m_OldTailbrush;

	HBRUSH m_HHeroTailbrush;
	HBRUSH m_OldHeroTailbrush;

	COLORREF m_RedColor = RGB(255, 0, 0);
	COLORREF m_GreenColor = RGB(0, 255, 0);
	COLORREF m_BlueColor = RGB(0, 0, 255);

	bool isVariable = false;
	int m_HeroSpeed = 1; // WINDOW_X / BOARD_NUMBER;
	int m_TailSpeed = 1;
	int m_Selected_Index = 0;
	int m_SnakeTailIndex = 0;

public:
	void SetDefaultPositonFromJump()
	{
		if (m_Hero->isJump == true)
		{
			switch (m_Hero->m_Direction)
			{
			case DIRECTION_UP:
				m_Hero->m_CurrentPosition.y -= m_HeroSpeed;
				{
					
				}
				break;
			case DIRECTION_DOWN:
				m_Hero->m_CurrentPosition.y += m_HeroSpeed;
				break;
			case DIRECTION_LEFT:
				m_Hero->m_CurrentPosition.x += m_HeroSpeed;
				break;
			case DIRECTION_RIGHT:
				m_Hero->m_CurrentPosition.x += m_HeroSpeed;
				break;
			}
		}

		m_Hero->isJump = false;
	}

	void DisableSelect()
	{
		if (m_Tail[m_Selected_Index]->isSelected)
		{
			m_Tail[m_Selected_Index]->isSelected = false;
		}
	}

	void TailMove(int mouseX, int mouseY)
	{
		for (int i = 0; i < MAX_TAIL_NUMBER; i++)
		{
			if (m_Tail[i]->isSelected == true && m_Tail[i]->isVisible == true) // 보여진 것이고, 선택된 것일 때 드래그 가능하게
			{
				m_Tail[i]->m_CurrentPosition.x = mouseX / (WINDOW_X / BOARD_NUMBER);
				m_Tail[i]->m_CurrentPosition.y = mouseY / (WINDOW_X / BOARD_NUMBER);
			}
		}
	}

	void SetTailVisible(int _index)
	{
		m_Tail[_index]->isVisible = true;

		// 초기설정
		srand(unsigned int(time(NULL)));
		int random_number = rand() % 4;

		m_Tail[_index]->m_Direction = random_number;
		m_Tail[_index]->m_Radius = WINDOW_X / BOARD_NUMBER;

		int random_position[4][2] = { {0,0}, {0, BOARD_NUMBER - 1}, {BOARD_NUMBER - 1, 0}, {BOARD_NUMBER - 1, BOARD_NUMBER - 1} };
		
		m_Tail[_index]->m_CurrentPosition.x = random_position[random_number][0];
		m_Tail[_index]->m_CurrentPosition.y = random_position[random_number][1];

		m_Tail[_index]->m_Rect.left = m_Tail[_index]->m_CurrentPosition.y * m_Tail[_index]->m_Radius;
		m_Tail[_index]->m_Rect.top = m_Tail[_index]->m_CurrentPosition.x * m_Tail[_index]->m_Radius;
		m_Tail[_index]->m_Rect.right = m_Tail[_index]->m_Rect.left + m_Tail[_index]->m_Radius;
		m_Tail[_index]->m_Rect.bottom = m_Tail[_index]->m_Rect.top + m_Tail[_index]->m_Radius;
	}
	
	void IsInCircle(int mouseX, int mouseY)
	{
		//m_ProjectMouse.x = mouseX;
		//m_ProjectMouse.y = mouseY;
		if (m_Hero->IsInCircle(mouseX, mouseY))
		{
			isVariable = true;
		}

		m_Hero->m_GoToMousePosition = true;

		int index1 = mouseX / (WINDOW_X / BOARD_NUMBER);
		int index2 = mouseY / (WINDOW_X / BOARD_NUMBER);

		m_Hero->m_NextPosition.x = index1;
		m_Hero->m_NextPosition.y = index2;

		for (int i = 0; i < MAX_TAIL_NUMBER; i++)
		{
			if (m_Tail[i]->isVisible)
			{
				if (m_Tail[i]->IsInCircle(mouseX, mouseY))
				{
					// 꼬리원 클릭 된 것임.
					m_Tail[i]->isSelected = true;
					m_Selected_Index = i;
				}
			}
		}
	}

	bool RectangleIntersection(Hero* _target1)
	{
		for (int i = 0; i < MAX_TAIL_NUMBER; i++)
		{
			if (m_Tail[i]->isVisible && _target1 != m_Tail[i])
			{
				if (_target1->m_Rect.left	<	m_Tail[i]->m_Rect.right &&
					_target1->m_Rect.top	<	m_Tail[i]->m_Rect.bottom &&
					_target1->m_Rect.right	>	m_Tail[i]->m_Rect.left	 &&
					_target1->m_Rect.bottom	>	m_Tail[i]->m_Rect.top)
				{
					// TODO : 여기서 충돌처리 명령을 실행합니다.
					m_Tail[i]->isVisible = false;
					return true;
				}
			}
		}

		return false;
	}

	bool RectangleIntersectionHeroAndTail(Hero* _target)
	{
		/*if ((m_Hero->m_Rect.left	<=	_target->m_Rect.right) &&
			(m_Hero->m_Rect.top	<=	_target->m_Rect.bottom) &&
			(m_Hero->m_Rect.right	>=	_target->m_Rect.left) &&
			(m_Hero->m_Rect.bottom	>=	_target->m_Rect.top))*/
		if(((m_Hero->m_CurrentPosition.x == _target->m_CurrentPosition.y) && 
			(m_Hero->m_CurrentPosition.y == _target->m_CurrentPosition.x)) ||
			((m_Hero->m_PreviousPosition.x == _target->m_CurrentPosition.y) &&
			(m_Hero->m_PreviousPosition.y == _target->m_CurrentPosition.x)) ||
				((m_Hero->m_CurrentPosition.x == _target->m_PreviousPosition.y) &&
			(m_Hero->m_CurrentPosition.y == _target->m_PreviousPosition.x))
			)
		{
			// TODO : 여기서 충돌처리 명령을 실행합니다.
			_target->isVisible = false;
			_target->isCollideWithHero = true;
			return true;
		}

		return false;
	}

	void Collide(HWND _hWnd, Hero* _target)
	{
		if (_target != NULL)
		{
			switch (_target->m_Direction)
			{
			case DIRECTION_UP:
				if (_target->m_CurrentPosition.x == 0) // 위쪽 벽
				{
					if (_target->m_CurrentPosition.y == 0)
						_target->SetEdgeDirection(DIRECTION_DOWN, DIRECTION_RIGHT);
					else if (_target->m_CurrentPosition.y == BOARD_NUMBER - 1)
						_target->SetEdgeDirection(DIRECTION_DOWN, DIRECTION_LEFT);
					else
						_target->SetDirection(DIRECTION_DOWN, DIRECTION_LEFT, DIRECTION_RIGHT);
				}
				else if (_target->m_CurrentPosition.x - m_HeroSpeed <= 0)
				{
					_target->m_CurrentPosition.x = 0;
					if (_target->m_CurrentPosition.y - m_HeroSpeed <= 0)
						_target->SetEdgeDirection(DIRECTION_DOWN, DIRECTION_RIGHT);
					else if (_target->m_CurrentPosition.y + m_HeroSpeed >= BOARD_NUMBER - 1)
						_target->SetEdgeDirection(DIRECTION_DOWN, DIRECTION_LEFT);
					else
						_target->SetDirection(DIRECTION_DOWN, DIRECTION_LEFT, DIRECTION_RIGHT);
				}
				break;
			case DIRECTION_DOWN:
				if (_target->m_CurrentPosition.x == BOARD_NUMBER - 1) // 아래쪽 벽
				{
					if (_target->m_CurrentPosition.y == 0)
						_target->SetEdgeDirection(DIRECTION_UP, DIRECTION_RIGHT);
					else if (_target->m_CurrentPosition.y == BOARD_NUMBER - 1)
						_target->SetEdgeDirection(DIRECTION_LEFT, DIRECTION_UP);
					else
						_target->SetDirection(DIRECTION_UP, DIRECTION_LEFT, DIRECTION_RIGHT);
				}
				else if (_target->m_CurrentPosition.x + m_HeroSpeed >= BOARD_NUMBER - 1)
				{
					_target->m_CurrentPosition.x = BOARD_NUMBER - 1;
					if (_target->m_CurrentPosition.y - m_HeroSpeed <= 0)
						_target->SetEdgeDirection(DIRECTION_UP, DIRECTION_RIGHT);
					else if (_target->m_CurrentPosition.y + m_HeroSpeed >= BOARD_NUMBER - 1)
						_target->SetEdgeDirection(DIRECTION_LEFT, DIRECTION_UP);
					else
						_target->SetDirection(DIRECTION_UP, DIRECTION_LEFT, DIRECTION_RIGHT);
				}
				break;
			case DIRECTION_LEFT:
				if (_target->m_CurrentPosition.y == 0) // 왼쪽 벽
				{
					if (_target->m_CurrentPosition.x == 0)
						_target->SetEdgeDirection(DIRECTION_DOWN, DIRECTION_RIGHT);
					else if (_target->m_CurrentPosition.x == BOARD_NUMBER - 1)
						_target->SetEdgeDirection(DIRECTION_UP, DIRECTION_RIGHT);
					else
						_target->SetDirection(DIRECTION_UP, DIRECTION_DOWN, DIRECTION_RIGHT);
				}
				else if (_target->m_CurrentPosition.y - m_HeroSpeed <= 0)
				{
					_target->m_CurrentPosition.y = 0;
					if (_target->m_CurrentPosition.x - m_HeroSpeed <= 0)
						_target->SetEdgeDirection(DIRECTION_DOWN, DIRECTION_RIGHT);
					else if (_target->m_CurrentPosition.x + m_HeroSpeed >= BOARD_NUMBER - 1)
						_target->SetEdgeDirection(DIRECTION_UP, DIRECTION_RIGHT);
					else
						_target->SetDirection(DIRECTION_UP, DIRECTION_DOWN, DIRECTION_RIGHT);
				}
				break;
			case DIRECTION_RIGHT:
				if (_target->m_CurrentPosition.y == BOARD_NUMBER - 1) // 오른쪽 벽
				{
					if (_target->m_CurrentPosition.x == 0)
						_target->SetEdgeDirection(DIRECTION_DOWN, DIRECTION_LEFT);
					else if (_target->m_CurrentPosition.x == BOARD_NUMBER - 1)
						_target->SetEdgeDirection(DIRECTION_UP, DIRECTION_LEFT);
					else
						_target->SetDirection(DIRECTION_UP, DIRECTION_DOWN, DIRECTION_LEFT);
				}
				else if (_target->m_CurrentPosition.y + m_HeroSpeed >= BOARD_NUMBER - 1)
				{
					_target->m_CurrentPosition.y = BOARD_NUMBER - 1;
					if (_target->m_CurrentPosition.x - m_HeroSpeed <= 0)
						_target->SetEdgeDirection(DIRECTION_DOWN, DIRECTION_LEFT);
					else if (_target->m_CurrentPosition.x + m_HeroSpeed >= BOARD_NUMBER - 1)
						_target->SetEdgeDirection(DIRECTION_UP, DIRECTION_LEFT);
					else
						_target->SetDirection(DIRECTION_UP, DIRECTION_DOWN, DIRECTION_LEFT);
				}
				break;
			}
		}
	}

	void MoveHero(HWND _hWnd, WPARAM _wParam)
	{
		switch (_wParam)
		{
		case VK_UP:
			m_Hero->m_Direction = DIRECTION_UP;
			break;
		case VK_DOWN:
			m_Hero->m_Direction = DIRECTION_DOWN;
			break;
		case VK_LEFT:
			m_Hero->m_Direction = DIRECTION_LEFT;
			break;
		case VK_RIGHT:
			m_Hero->m_Direction = DIRECTION_RIGHT;
			break;
		}
	}

	void KeyboardInput(HWND _hWnd, WPARAM _wParam)
	{
		switch (_wParam)
		{
		case '=': // 속도 빠르게
			if (m_HeroSpeed < 3)
				m_HeroSpeed++;
			break;
		case '-': // 속도 느리게
			if (m_HeroSpeed > 1)
				m_HeroSpeed--;
			break;
		case VK_SPACE: // 점프
			{
				m_Hero->isJump = true;
				switch (m_Hero->m_Direction)
				{
				case DIRECTION_UP:
					if(m_Hero->m_CurrentPosition.y + m_HeroSpeed < BOARD_NUMBER - 1)
						m_Hero->m_CurrentPosition.y += m_HeroSpeed;
					break;
				case DIRECTION_DOWN:
					if (m_Hero->m_CurrentPosition.y - m_HeroSpeed > 0)
						m_Hero->m_CurrentPosition.y -= m_HeroSpeed;
					break;
				case DIRECTION_LEFT:
					if (m_Hero->m_CurrentPosition.x - m_HeroSpeed > 0)
						m_Hero->m_CurrentPosition.x -= m_HeroSpeed;
					break;
				case DIRECTION_RIGHT:
					if (m_Hero->m_CurrentPosition.x - m_HeroSpeed > 0)
						m_Hero->m_CurrentPosition.x -= m_HeroSpeed;
					break;
				}
			}
			break;
		case 'o': // 애벌레 머리 도형 커졌다 작아졌다 반복
			if (isVariable) isVariable = false;
			else isVariable = true;
			break;
		case 'q': case 'Q': // 프로그램 종료
			PostQuitMessage(0);
			break;
		}
	}

	void Update(HWND _hWnd)
	{
		for (int i = 0; i < MAX_TAIL_NUMBER; i++)
		{
			if (m_Tail[i]->isVisible && m_Tail[i] != NULL)
			{
				RectangleIntersectionHeroAndTail(m_Tail[i]);
				if (m_Tail[i]->isCollideWithHero == true) // 머리와 꼬리가 충돌하여 꼬리가 머리를 따라다녀야 할 때
				{
					m_HeroTail[m_SnakeTailIndex]->isSnakeTail = true;
					m_HeroTail[m_SnakeTailIndex]->m_Radius = m_Hero->m_Radius;
					m_HeroTail[m_SnakeTailIndex]->myIndex = m_SnakeTailIndex + 1;
					m_SnakeTailIndex++;
					m_Tail[i]->isCollideWithHero = false;
				}
			}
		}

		for (int i = 0; i < m_SnakeTailIndex; i++)
		{
			Collide(_hWnd, m_HeroTail[i]);
			m_HeroTail[i]->UpdateTail(_hWnd, m_Hero, m_HeroSpeed, m_HeroTail, m_SnakeTailIndex);
		}

		Collide(_hWnd, m_Hero);

		m_Hero->Update(_hWnd, m_HeroSpeed);

		if (isVariable)
			VariateSize(_hWnd, m_Hero);
		else
			m_Hero->m_Radius = WINDOW_X / BOARD_NUMBER;

		for (int i = 0; i < MAX_TAIL_NUMBER; i++)
		{
			if (m_Tail[i]->isVisible && m_Tail[i] != NULL)
			{
				Collide(_hWnd, m_Tail[i]);

				if (RectangleIntersection(m_Tail[i]))
				{
					m_Tail[i]->isCollideWithAnotherTail = true;
					m_Tail[i]->isBigger = true;
				}
				
				if (m_Tail[i]->isCollideWithAnotherTail == false)
				{
					m_Tail[i]->Update(_hWnd, m_TailSpeed);
				}
			}
		}

		for (int i = 0; i < MAX_TAIL_NUMBER; i++)
		{
			if (m_Tail[i]->isCollideWithAnotherTail == true && m_Tail[i]->isVisible == true && m_Tail[i] != NULL)
			{
				VariateTailSize(_hWnd, m_Tail[i]);
			}
		}
	}

	void VariateSize(HWND _hWnd, Hero* _target)
	{
		if (_target->isBigger)
		{
			_target->m_Radius += 1;
			if (_target->m_Radius > WINDOW_X / BOARD_NUMBER * 2)
				_target->isBigger = false;
		}
		else
		{
			_target->m_Radius -= 1;
			if (_target->m_Radius < WINDOW_X / BOARD_NUMBER)
			{
				_target->isBigger = true;
				_target->isVariableFinished = true;
			}
		}
	}

	void VariateTailSize(HWND _hWnd, Hero* _target)
	{
		if (_target->isBigger)
		{
			_target->m_Radius += 1;
			if (_target->m_Radius > WINDOW_X / BOARD_NUMBER + 10)
				_target->isBigger = false;
		}
		else
		{
			_target->m_Radius -= 1;
			if (_target->m_Radius < WINDOW_X / BOARD_NUMBER + 1)
			{
				_target->isBigger = true;
				_target->isCollideWithAnotherTail = false;
				_target->isVariableFinished = true;
			}
		}
	}

	void Draw(HDC hdc, HWND _hWnd)
	{
		for (int i = 0; i < BOARD_NUMBER; i++)
		{
			for (int j = 0; j < BOARD_NUMBER; j++)
			{
				Rectangle(hdc, m_VirtualRect[i][j]->m_Rect.left, m_VirtualRect[i][j]->m_Rect.top,
					m_VirtualRect[i][j]->m_Rect.right, m_VirtualRect[i][j]->m_Rect.bottom);
				
				m_Hbrush = CreateSolidBrush(m_RedColor);
				m_Oldbrush = (HBRUSH)SelectObject(hdc, m_Hbrush);

				Ellipse(hdc, m_VirtualRect[m_Hero->m_CurrentPosition.x][m_Hero->m_CurrentPosition.y]->m_Rect.left, 
					m_VirtualRect[m_Hero->m_CurrentPosition.x][m_Hero->m_CurrentPosition.y]->m_Rect.top,
					m_VirtualRect[m_Hero->m_CurrentPosition.x][m_Hero->m_CurrentPosition.y]->m_Rect.left + m_Hero->m_Radius,
					m_VirtualRect[m_Hero->m_CurrentPosition.x][m_Hero->m_CurrentPosition.y]->m_Rect.top + m_Hero->m_Radius);

				SelectObject(hdc, m_Oldbrush);
				DeleteObject(m_Hbrush);
			}
		}

		for (int i = 0; i < MAX_TAIL_NUMBER; i++)
		{
			if (m_Tail[i] != NULL && m_Tail[i]->isVisible == true)
			{
				m_HTailbrush = CreateSolidBrush(m_GreenColor);
				m_OldTailbrush = (HBRUSH)SelectObject(hdc, m_HTailbrush);

				// TODO : 여기서 그리기 코드를 추가합니다.
				Ellipse(hdc, m_Tail[i]->m_Rect.left, m_Tail[i]->m_Rect.top, m_Tail[i]->m_Rect.left + m_Tail[i]->m_Radius, m_Tail[i]->m_Rect.top + m_Tail[i]->m_Radius);

				SelectObject(hdc, m_OldTailbrush);
				DeleteObject(m_HTailbrush);
			}
		}

		for (int i = 0; i < m_SnakeTailIndex; i++)
		{
			m_HHeroTailbrush = CreateSolidBrush(m_BlueColor);
			m_OldHeroTailbrush = (HBRUSH)SelectObject(hdc, m_HHeroTailbrush);

			// TODO : 여기서 그리기 코드를 추가합니다.
			if (m_SnakeTailIndex > 0)
			{
				Ellipse(hdc, m_VirtualRect[m_HeroTail[i]->m_CurrentPosition.x][m_HeroTail[i]->m_CurrentPosition.y]->m_Rect.left,
					m_VirtualRect[m_HeroTail[i]->m_CurrentPosition.x][m_HeroTail[i]->m_CurrentPosition.y]->m_Rect.top,
					m_VirtualRect[m_HeroTail[i]->m_CurrentPosition.x][m_HeroTail[i]->m_CurrentPosition.y]->m_Rect.left + m_HeroTail[i]->m_Radius,
					m_VirtualRect[m_HeroTail[i]->m_CurrentPosition.x][m_HeroTail[i]->m_CurrentPosition.y]->m_Rect.top + m_HeroTail[i]->m_Radius);
			}
			

			SelectObject(hdc, m_OldHeroTailbrush);
			DeleteObject(m_HHeroTailbrush);
		}

		/*static char str[256];
		static char str2[256];
		sprintf_s(str, "주인공 : %d / %d", m_Hero->m_CurrentPosition.x, m_Hero->m_CurrentPosition.y);
		sprintf_s(str2, "꼬리 : %d / %d", m_HeroTail[1]->m_CurrentPosition.x, m_HeroTail[1]->m_CurrentPosition.y);
		TextOut(hdc, 500, 450, str, strlen(str));
		TextOut(hdc, 500, 500, str2, strlen(str2));*/
	}

	CProjectManager()
	{
		int width = WINDOW_X / BOARD_NUMBER;

		m_Hero = new Hero;
		m_Hero->m_Direction = DIRECTION_RIGHT;
		m_Hero->m_Radius = width;

		for (int i = 0; i < BOARD_NUMBER; i++)
		{
			for (int j = 0; j < BOARD_NUMBER; j++)
			{
				m_VirtualRect[i][j] = new Point;
			}
		}

		for (int i = 0; i < BOARD_NUMBER; i++)
		{
			for (int j = 0; j < BOARD_NUMBER; j++)
			{
				m_VirtualRect[i][j]->m_Rect.left = j * width;
				m_VirtualRect[i][j]->m_Rect.top = i * width;
				m_VirtualRect[i][j]->m_Rect.right = (j + 1) * width;
				m_VirtualRect[i][j]->m_Rect.bottom = (i + 1) * width;

				if (i == 19 && j == 19)
				{
					m_Hero->m_Rect.left = j * m_Hero->m_Radius;
					m_Hero->m_Rect.top = i * m_Hero->m_Radius;
					m_Hero->m_Rect.right = m_Hero->m_Rect.left + m_Hero->m_Radius;
					m_Hero->m_Rect.bottom = m_Hero->m_Rect.top + m_Hero->m_Radius;

					m_Hero->m_CurrentPosition.x = i;
					m_Hero->m_CurrentPosition.y = j;
				}
			}
		}

		for (int i = 0; i < MAX_TAIL_NUMBER; i++)
		{
			m_Tail[i] = new Hero;
		}

		for (int i = 0; i < HERO_TAIL_NUMBER; i++)
		{
			m_HeroTail[i] = new Hero;
		}
	}

	~CProjectManager()
	{
		delete m_Hero;

		for (int i = 0; i < BOARD_NUMBER; i++)
		{
			for (int j = 0; j < BOARD_NUMBER; j++)
			{
				if (m_VirtualRect[i][j] != NULL)
				{
					delete m_VirtualRect[i][j];
				}
			}
		}

		for (int i = 0; i < MAX_TAIL_NUMBER; i++)
		{
			if (m_Tail[i] != NULL)
			{
				delete m_Tail[i];
			}
		}

		for (int i = 0; i < HERO_TAIL_NUMBER; i++)
		{
			if (m_HeroTail[i] != NULL)
			{
				delete m_HeroTail[i];
			}
		}
	}
};

CProjectManager g_Manager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc;
	PAINTSTRUCT ps;

	int mouseX = 0;
	int mouseY = 0;

	static RECT view;
	static HDC  mem1dc, mem2dc;
	static HBITMAP hBit1, hOldBit1;
	static HBITMAP hBit2, hOldBit2;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		GetClientRect(hWnd, &view);
		SetTimer(hWnd, 1, SECOND / 10, (TIMERPROC)TimerProc); // 1번 아이디의 타이머가 0.01초 마다 TimerProc 타이머 함수 실행
		SetTimer(hWnd, 2, SECOND * 5, (TIMERPROC)FoodTimerProc); // 1번 아이디의 타이머가 0.01초 마다 TimerProc 타이머 함수 실행
		break;
	case WM_PAINT:
		{
			GetClientRect(hWnd, &view);

			hdc = BeginPaint(hWnd, &ps);

			mem1dc = CreateCompatibleDC(hdc);
			mem2dc = CreateCompatibleDC(mem1dc);

			if (hBit1 == NULL)
				hBit1 = CreateCompatibleBitmap(hdc, WINDOW_X, WINDOW_Y);

			if (hBit2 == NULL)
				hBit2 = CreateCompatibleBitmap(hdc, WINDOW_X, WINDOW_Y);
			
			hOldBit1 = (HBITMAP)SelectObject(mem1dc, hBit1);
			hOldBit2 = (HBITMAP)SelectObject(mem2dc, hBit2);

			BitBlt(mem1dc, 0, 0, WINDOW_X, WINDOW_Y, mem2dc, 0, 0, SRCCOPY);

			g_Manager.Draw(mem1dc, hWnd);
			SetBkMode(mem1dc, TRANSPARENT);

			BitBlt(hdc, 0, 0, WINDOW_X, WINDOW_Y, mem1dc, 0, 0, SRCCOPY);

			SelectObject(mem1dc, hOldBit1);
			SelectObject(mem2dc, hOldBit2);

			DeleteDC(mem2dc);
			DeleteDC(mem1dc);

			EndPaint(hWnd, &ps);
		}
		break;
	case WM_CHAR:
		g_Manager.KeyboardInput(hWnd, wParam);
		break;
	case WM_KEYDOWN:
		g_Manager.MoveHero(hWnd, wParam);
		break;
	case WM_KEYUP:
		if (wParam == VK_SPACE)
		{
			g_Manager.SetDefaultPositonFromJump();
		}
		break;
	case WM_LBUTTONDOWN:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		g_Manager.IsInCircle(mouseX, mouseY);
		break;
	case WM_LBUTTONUP:
		g_Manager.DisableSelect();
		break;
	case WM_MOUSEMOVE:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		g_Manager.TailMove(mouseX, mouseY);
		break;
	case WM_DESTROY:
		if (hBit1) DeleteObject(hBit1);
		DeleteObject(hBit2);
		KillTimer(hWnd, 1);
		KillTimer(hWnd, 2);
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
		{
			g_Manager.Update(hWnd);
		}
		break;
	}
	InvalidateRgn(hWnd, NULL, FALSE);
}

void CALLBACK FoodTimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
	{
		g_Manager.SetTailVisible(tail_index);
	}
	break;
	}
	tail_index++;
	InvalidateRgn(hWnd, NULL, FALSE);
}