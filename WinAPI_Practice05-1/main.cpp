#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include "resource.h"

//#ifdef UNICODE
//#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
//#else
//#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
//#endif

template<typename T>
void Swap(T& left, T& right)
{
	T temp = left;
	left = right;
	right = temp;
}

using namespace std;

#define WINDOW_X 900
#define WINDOW_Y 900

#define FULL_IMAGE 2
#define DIV_3X3 3
#define DIV_4X4 4
#define DIV_5X5 5

#define BLANK -1

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 5-1";

typedef struct _Point {
	int x;
	int y;

	_Point& operator=(_Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}
} Point;

class CBmpComponent
{
private:
	Point m_Position;
	Point m_Destination;
	Point m_ImagePosition;
	int m_Size;
	bool m_IsSelected = false;
	bool m_IsMoving = false;

public:
	int m_Kind;
	int m_DestKind;

public:
	void SetIsMoving(bool value)
	{
		m_IsMoving = value;
	}

	const bool GetIsMoving()
	{
		return m_IsMoving;
	}

	void SetIsSelected(bool value)
	{
		m_IsSelected = value;
	}

	const bool GetIsSelected()
	{
		return m_IsSelected;
	}

	void SetImagePosition(int x, int y)
	{
		m_ImagePosition.x = x;
		m_ImagePosition.y = y;
	}

	const Point GetImagePosition() const
	{
		return m_ImagePosition;
	}

	void SetPosition(int x, int y)
	{
		m_Position.x = x;
		m_Position.y = y;
	}

	const Point GetPosition() const
	{
		return m_Position;
	}

	void SetDestination(int x, int y)
	{
		m_Destination.x = x;
		m_Destination.y = y;
	}

	const Point GetDestination() const
	{
		return m_Destination;
	}

	void SetSize(int size)
	{
		m_Size = size;
	}

	const int GetSize() const
	{
		return m_Size;
	}

	void updateMoving()
	{

	}

	CBmpComponent& operator=(const CBmpComponent& rhs)
	{
		if (this != &rhs)
		{
			m_ImagePosition.x = rhs.m_ImagePosition.x;
			m_ImagePosition.y = rhs.m_ImagePosition.y;

			m_Kind = rhs.m_Kind;
			m_DestKind = rhs.m_DestKind;
		}

		return *this;
	}

public:
	CBmpComponent()
	{
	}

	CBmpComponent(const CBmpComponent& rhs)
	{
		m_Position.x = rhs.m_Position.x;
		m_Position.y = rhs.m_Position.y;

		m_ImagePosition.x = rhs.m_ImagePosition.x;
		m_ImagePosition.y = rhs.m_ImagePosition.y;

		m_Size = rhs.m_Size;
		m_IsSelected = rhs.m_IsSelected;

		m_Kind = rhs.m_Kind;
		m_DestKind = rhs.m_DestKind;
	}
};

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
bool IsPointInRect(CBmpComponent target, int x, int y);
bool isCorrect(int a[], int b[], int index);

CBmpComponent g_Puzzle3X3[DIV_3X3][DIV_3X3];
CBmpComponent g_Puzzle4X4[DIV_4X4][DIV_4X4];
CBmpComponent g_Puzzle5X5[DIV_5X5][DIV_5X5];

class CPrjojectManager
{
private:
	int m_GameMode = -1;
	bool m_bIsStage1 = true;

public:
	void SetGameMode(int mode) { m_GameMode = mode; }
	const int GetGameMode() const { return m_GameMode; }
	void SetIsStage1(bool value) { m_bIsStage1 = value; }
	const bool GetIsStage1() { return m_bIsStage1; }

	void Draw(HDC hdc, HDC memdc, HWND hWnd)
	{
		switch (m_GameMode)
		{
		case FULL_IMAGE:
			BitBlt(hdc, 0, 0, 900, 900, memdc, 0, 0, SRCCOPY);
			break;
		case DIV_3X3:
		{
			for (auto i = 0; i < DIV_3X3; i++)
			{
				for (auto j = 0; j < DIV_3X3; j++)
				{
					if (g_Puzzle3X3[i][j].m_Kind != BLANK)
					{
						BitBlt(hdc, g_Puzzle3X3[i][j].GetPosition().x, g_Puzzle3X3[i][j].GetPosition().y, g_Puzzle3X3[i][j].GetSize(), g_Puzzle3X3[i][j].GetSize(),
							memdc, g_Puzzle3X3[i][j].GetImagePosition().x, g_Puzzle3X3[i][j].GetImagePosition().y, SRCCOPY);
					}
					cout << g_Puzzle3X3[i][j].m_Kind << " ";
				}
				cout << endl;
			}
		}
		break;
		case DIV_4X4:
		{
			for (auto i = 0; i < DIV_4X4; i++)
			{
				for (auto j = 0; j < DIV_4X4; j++)
				{
					if (g_Puzzle4X4[i][j].m_Kind != BLANK)
					{
						BitBlt(hdc, g_Puzzle4X4[i][j].GetPosition().x, g_Puzzle4X4[i][j].GetPosition().y, g_Puzzle4X4[i][j].GetSize(), g_Puzzle4X4[i][j].GetSize(),
							memdc, g_Puzzle4X4[i][j].GetImagePosition().x, g_Puzzle4X4[i][j].GetImagePosition().y, SRCCOPY);
					}
					cout << g_Puzzle4X4[i][j].m_Kind << " ";
				}
				cout << endl;
			}
		}
		break;
		case DIV_5X5:
		{
			for (auto i = 0; i < DIV_5X5; i++)
			{
				for (auto j = 0; j < DIV_5X5; j++)
				{
					if (g_Puzzle5X5[i][j].m_Kind != BLANK)
					{
						BitBlt(hdc, g_Puzzle5X5[i][j].GetPosition().x, g_Puzzle5X5[i][j].GetPosition().y, g_Puzzle5X5[i][j].GetSize(), g_Puzzle5X5[i][j].GetSize(),
							memdc, g_Puzzle5X5[i][j].GetImagePosition().x, g_Puzzle5X5[i][j].GetImagePosition().y, SRCCOPY);
					}
					cout << g_Puzzle5X5[i][j].m_Kind << " ";
				}
				cout << endl;
			}
		}
		break;
		}
	}

public:
	CPrjojectManager()
	{
		// TODO : 3X3 퍼즐 초기화
		int index = 0;
		for (auto i = 0; i < DIV_3X3; i++)
		{
			for (auto j = 0; j < DIV_3X3; j++)
			{
				g_Puzzle3X3[i][j].SetPosition(j * WINDOW_X / DIV_3X3, i * WINDOW_Y / DIV_3X3);
				g_Puzzle3X3[i][j].SetImagePosition(j * WINDOW_X / DIV_3X3, i * WINDOW_Y / DIV_3X3);
				g_Puzzle3X3[i][j].SetSize(WINDOW_X / DIV_3X3);

				if ((i == DIV_3X3 - 1) && (j == DIV_3X3 - 1))
				{
					g_Puzzle3X3[i][j].m_Kind = BLANK;
					continue;
				}
				else
				{
					g_Puzzle3X3[i][j].SetPosition(j * WINDOW_X / DIV_3X3, i * WINDOW_Y / DIV_3X3);
					g_Puzzle3X3[i][j].SetImagePosition(j * WINDOW_X / DIV_3X3, i * WINDOW_Y / DIV_3X3);
					g_Puzzle3X3[i][j].SetSize(WINDOW_X / DIV_3X3);
					g_Puzzle3X3[i][j].m_Kind = index++;
				}
			}
		}

		Swap<CBmpComponent>(g_Puzzle3X3[1][0], g_Puzzle3X3[0][2]);
		Swap<CBmpComponent>(g_Puzzle3X3[2][1], g_Puzzle3X3[1][2]);

		// TODO : 4X4 퍼즐 초기화
		index = 0;
		for (auto i = 0; i < DIV_4X4; i++)
		{
			for (auto j = 0; j < DIV_4X4; j++)
			{
				g_Puzzle4X4[i][j].SetPosition(j * WINDOW_X / DIV_4X4, i * WINDOW_Y / DIV_4X4);
				g_Puzzle4X4[i][j].SetImagePosition(j * WINDOW_X / DIV_4X4, i * WINDOW_Y / DIV_4X4);
				g_Puzzle4X4[i][j].SetSize(WINDOW_X / DIV_4X4);

				if ((i == DIV_4X4 - 1) && (j == DIV_4X4 - 1))
				{
					g_Puzzle4X4[i][j].m_Kind = BLANK;
					continue;
				}
				else
				{
					g_Puzzle4X4[i][j].SetPosition(j * WINDOW_X / DIV_4X4, i * WINDOW_Y / DIV_4X4);
					g_Puzzle4X4[i][j].SetImagePosition(j * WINDOW_X / DIV_4X4, i * WINDOW_Y / DIV_4X4);
					g_Puzzle4X4[i][j].SetSize(WINDOW_X / DIV_4X4);
					g_Puzzle4X4[i][j].m_Kind = index++;
				}
			}
		}

		Swap<CBmpComponent>(g_Puzzle4X4[1][0], g_Puzzle4X4[0][3]);
		Swap<CBmpComponent>(g_Puzzle4X4[0][1], g_Puzzle4X4[2][1]);
		Swap<CBmpComponent>(g_Puzzle4X4[1][3], g_Puzzle4X4[3][1]);
		Swap<CBmpComponent>(g_Puzzle4X4[0][2], g_Puzzle4X4[1][2]);

		// TODO : 5X5 퍼즐 초기화
		index = 0;
		for (auto i = 0; i < DIV_5X5; i++)
		{
			for (auto j = 0; j < DIV_5X5; j++)
			{
				g_Puzzle5X5[i][j].SetPosition(j * WINDOW_X / DIV_5X5, i * WINDOW_Y / DIV_5X5);
				g_Puzzle5X5[i][j].SetImagePosition(j * WINDOW_X / DIV_5X5, i * WINDOW_Y / DIV_5X5);
				g_Puzzle5X5[i][j].SetSize(WINDOW_X / DIV_5X5);

				if ((i == DIV_5X5 - 1) && (j == DIV_5X5 - 1))
				{
					g_Puzzle5X5[i][j].m_Kind = BLANK;
					continue;
				}
				else
				{
					g_Puzzle5X5[i][j].SetPosition(j * WINDOW_X / DIV_5X5, i * WINDOW_Y / DIV_5X5);
					g_Puzzle5X5[i][j].SetImagePosition(j * WINDOW_X / DIV_5X5, i * WINDOW_Y / DIV_5X5);
					g_Puzzle5X5[i][j].SetSize(WINDOW_X / DIV_5X5);
					g_Puzzle5X5[i][j].m_Kind = index++;
				}
			}
		}

		Swap<CBmpComponent>(g_Puzzle5X5[0][0], g_Puzzle5X5[4][1]);
		Swap<CBmpComponent>(g_Puzzle5X5[0][3], g_Puzzle5X5[1][0]);
		Swap<CBmpComponent>(g_Puzzle5X5[2][1], g_Puzzle5X5[3][2]);
		Swap<CBmpComponent>(g_Puzzle5X5[3][0], g_Puzzle5X5[2][4]);
		Swap<CBmpComponent>(g_Puzzle5X5[1][4], g_Puzzle5X5[4][3]);
		Swap<CBmpComponent>(g_Puzzle5X5[0][2], g_Puzzle5X5[1][3]);
		Swap<CBmpComponent>(g_Puzzle5X5[1][1], g_Puzzle5X5[3][4]);
	}
};

CPrjojectManager g_ProjectManager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, true);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc1, memdc2;
	PAINTSTRUCT ps;

	static int mouseX = 0;
	static int mouseY = 0;

	static HBITMAP hBitmap1;
	static HBITMAP hBitmap2;
	static int origin_x;
	static int origin_y;

	static int tempMode;
	int speed = 10;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		hBitmap1 = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_Shaymin));
		hBitmap2 = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_Cresselia));
	}
	break;
	case WM_TIMER:
	{
		switch (g_ProjectManager.GetGameMode())
		{
		case DIV_3X3:
		{
			for (auto i = 0; i < DIV_3X3; i++)
			{
				for (auto j = 0; j < DIV_3X3; j++)
				{
					if (g_Puzzle3X3[i][j].GetIsMoving() == true)
					{
						int dest_x = g_Puzzle3X3[i][j].GetDestination().x;
						int dest_y = g_Puzzle3X3[i][j].GetDestination().y;

						int dest_i = g_Puzzle3X3[i][j].GetDestination().x / g_Puzzle3X3[i][j].GetSize();
						int dest_j = g_Puzzle3X3[i][j].GetDestination().y / g_Puzzle3X3[i][j].GetSize();

						if (origin_x < dest_x)
						{
							origin_x += speed;
							if (origin_x >= dest_x)
							{
								g_Puzzle3X3[i][j].SetIsMoving(false);

								g_Puzzle3X3[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle3X3[i][j], g_Puzzle3X3[dest_j][dest_i]);
								g_Puzzle3X3[dest_j][dest_i].m_Kind = g_Puzzle3X3[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
						else if (origin_x > dest_x)
						{
							origin_x -= speed;
							if (origin_x <= dest_x)
							{
								g_Puzzle3X3[i][j].SetIsMoving(false);

								g_Puzzle3X3[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle3X3[i][j], g_Puzzle3X3[dest_j][dest_i]);
								g_Puzzle3X3[dest_j][dest_i].m_Kind = g_Puzzle3X3[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
						else if (origin_y < dest_y)
						{
							origin_y += speed;
							if (origin_y >= dest_y)
							{
								g_Puzzle3X3[i][j].SetIsMoving(false);

								g_Puzzle3X3[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle3X3[i][j], g_Puzzle3X3[dest_j][dest_i]);
								g_Puzzle3X3[dest_j][dest_i].m_Kind = g_Puzzle3X3[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
						else if (origin_y > dest_y)
						{
							origin_y -= speed;
							if (origin_y <= dest_y)
							{
								g_Puzzle3X3[i][j].SetIsMoving(false);

								g_Puzzle3X3[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle3X3[i][j], g_Puzzle3X3[dest_j][dest_i]);
								g_Puzzle3X3[dest_j][dest_i].m_Kind = g_Puzzle3X3[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
					}
				}
			}
		}
		break;
		case DIV_4X4:
		{
			for (auto i = 0; i < DIV_4X4; i++)
			{
				for (auto j = 0; j < DIV_4X4; j++)
				{
					if (g_Puzzle4X4[i][j].GetIsMoving() == true)
					{
						int dest_x = g_Puzzle4X4[i][j].GetDestination().x;
						int dest_y = g_Puzzle4X4[i][j].GetDestination().y;

						int dest_i = g_Puzzle4X4[i][j].GetDestination().x / g_Puzzle4X4[i][j].GetSize();
						int dest_j = g_Puzzle4X4[i][j].GetDestination().y / g_Puzzle4X4[i][j].GetSize();

						if (origin_x < dest_x)
						{
							origin_x += speed;
							if (origin_x >= dest_x)
							{
								g_Puzzle4X4[i][j].SetIsMoving(false);

								g_Puzzle4X4[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle4X4[i][j], g_Puzzle4X4[dest_j][dest_i]);
								g_Puzzle4X4[dest_j][dest_i].m_Kind = g_Puzzle4X4[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
						else if (origin_x > dest_x)
						{
							origin_x -= speed;
							if (origin_x <= dest_x)
							{
								g_Puzzle4X4[i][j].SetIsMoving(false);

								g_Puzzle4X4[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle4X4[i][j], g_Puzzle4X4[dest_j][dest_i]);
								g_Puzzle4X4[dest_j][dest_i].m_Kind = g_Puzzle4X4[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
						else if (origin_y < dest_y)
						{
							origin_y += speed;
							if (origin_y >= dest_y)
							{
								g_Puzzle4X4[i][j].SetIsMoving(false);

								g_Puzzle4X4[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle4X4[i][j], g_Puzzle4X4[dest_j][dest_i]);
								g_Puzzle4X4[dest_j][dest_i].m_Kind = g_Puzzle4X4[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
						else if (origin_y > dest_y)
						{
							origin_y -= speed;
							if (origin_y <= dest_y)
							{
								g_Puzzle4X4[i][j].SetIsMoving(false);

								g_Puzzle4X4[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle4X4[i][j], g_Puzzle4X4[dest_j][dest_i]);
								g_Puzzle4X4[dest_j][dest_i].m_Kind = g_Puzzle4X4[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
					}
				}
			}
		}
		break;
		case DIV_5X5:
		{
			for (auto i = 0; i < DIV_5X5; i++)
			{
				for (auto j = 0; j < DIV_5X5; j++)
				{
					if (g_Puzzle5X5[i][j].GetIsMoving() == true)
					{
						int dest_x = g_Puzzle5X5[i][j].GetDestination().x;
						int dest_y = g_Puzzle5X5[i][j].GetDestination().y;

						int dest_i = g_Puzzle5X5[i][j].GetDestination().x / g_Puzzle5X5[i][j].GetSize();
						int dest_j = g_Puzzle5X5[i][j].GetDestination().y / g_Puzzle5X5[i][j].GetSize();

						if (origin_x < dest_x)
						{
							origin_x += speed;
							if (origin_x >= dest_x)
							{
								g_Puzzle5X5[i][j].SetIsMoving(false);

								g_Puzzle5X5[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle5X5[i][j], g_Puzzle5X5[dest_j][dest_i]);
								g_Puzzle5X5[dest_j][dest_i].m_Kind = g_Puzzle5X5[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
						else if (origin_x > dest_x)
						{
							origin_x -= speed;
							if (origin_x <= dest_x)
							{
								g_Puzzle5X5[i][j].SetIsMoving(false);

								g_Puzzle5X5[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle5X5[i][j], g_Puzzle5X5[dest_j][dest_i]);
								g_Puzzle5X5[dest_j][dest_i].m_Kind = g_Puzzle5X5[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
						else if (origin_y < dest_y)
						{
							origin_y += speed;
							if (origin_y >= dest_y)
							{
								g_Puzzle5X5[i][j].SetIsMoving(false);

								g_Puzzle5X5[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle5X5[i][j], g_Puzzle5X5[dest_j][dest_i]);
								g_Puzzle5X5[dest_j][dest_i].m_Kind = g_Puzzle5X5[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
						else if (origin_y > dest_y)
						{
							origin_y -= speed;
							if (origin_y <= dest_y)
							{
								g_Puzzle5X5[i][j].SetIsMoving(false);

								g_Puzzle5X5[i][j].SetIsSelected(false);
								Swap<CBmpComponent>(g_Puzzle5X5[i][j], g_Puzzle5X5[dest_j][dest_i]);
								g_Puzzle5X5[dest_j][dest_i].m_Kind = g_Puzzle5X5[i][j].m_DestKind;
								KillTimer(hWnd, 1);
							}
						}
					}
				}
			}
		}
		break;
		}
		InvalidateRgn(hWnd, NULL, true);
	}
		break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		switch (g_ProjectManager.GetGameMode())
		{
		case DIV_3X3:
		{
			int index = 0;
			int puzzleArr[DIV_3X3 * DIV_3X3];
			int tempArr[DIV_3X3 * DIV_3X3] = { 0, 1, 2, 3, 4, 5, 6, 7, -1 };
			for (auto i = 0; i < DIV_3X3; i++)
			{
				for (auto j = 0; j < DIV_3X3; j++)
				{
					puzzleArr[index++] = g_Puzzle3X3[i][j].m_Kind;
				}
			}

			if (isCorrect(puzzleArr, tempArr, DIV_3X3 * DIV_3X3))
			{
				MessageBox(hWnd, "짝을 맞췄습니다!", "알림", MB_OK);
				PostQuitMessage(0);
			}
		}
		break;
		case DIV_4X4:
		{
			int index = 0;
			int puzzleArr[DIV_4X4 * DIV_4X4];
			int tempArr[DIV_4X4 * DIV_4X4] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, -1 };
			for (auto i = 0; i < DIV_4X4; i++)
			{
				for (auto j = 0; j < DIV_4X4; j++)
				{
					puzzleArr[index++] = g_Puzzle4X4[i][j].m_Kind;
				}
			}

			if (isCorrect(puzzleArr, tempArr, DIV_4X4 * DIV_4X4))
			{
				MessageBox(hWnd, "짝을 맞췄습니다!", "알림", MB_OK);
				PostQuitMessage(0);
			}
		}
		break;
		case DIV_5X5:
		{
			int index = 0;
			int puzzleArr[DIV_5X5 * DIV_5X5];
			int tempArr[DIV_5X5 * DIV_5X5] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 - 1 };
			for (auto i = 0; i < DIV_5X5; i++)
			{
				for (auto j = 0; j < DIV_5X5; j++)
				{
					puzzleArr[index++] = g_Puzzle5X5[i][j].m_Kind;
				}
			}

			if (isCorrect(puzzleArr, tempArr, DIV_5X5 * DIV_5X5))
			{
				MessageBox(hWnd, "짝을 맞췄습니다!", "알림", MB_OK);
				PostQuitMessage(0);
			}
		}
		break;
		}

		if (g_ProjectManager.GetIsStage1()) // 스테이지 1
		{
			memdc1 = CreateCompatibleDC(hdc);
			SelectObject(memdc1, hBitmap1);

			g_ProjectManager.Draw(hdc, memdc1, hWnd);

			switch (g_ProjectManager.GetGameMode())
			{
			case DIV_3X3:
			{
				for (auto i = 0; i < DIV_3X3; i++)
				{
					for (auto j = 0; j < DIV_3X3; j++)
					{
						if (g_Puzzle3X3[i][j].GetIsMoving() == true)
						{
							BitBlt(hdc, origin_x, origin_y, g_Puzzle3X3[i][j].GetSize(), g_Puzzle3X3[i][j].GetSize(),
								memdc1, g_Puzzle3X3[i][j].GetImagePosition().x, g_Puzzle3X3[i][j].GetImagePosition().y, SRCCOPY);
						}
					}
				}
			}
			break;
			case DIV_4X4:
			{
				for (auto i = 0; i < DIV_4X4; i++)
				{
					for (auto j = 0; j < DIV_4X4; j++)
					{
						if (g_Puzzle4X4[i][j].GetIsMoving() == true)
						{
							BitBlt(hdc, origin_x, origin_y, g_Puzzle4X4[i][j].GetSize(), g_Puzzle4X4[i][j].GetSize(),
								memdc1, g_Puzzle4X4[i][j].GetImagePosition().x, g_Puzzle4X4[i][j].GetImagePosition().y, SRCCOPY);
						}
					}
				}
			}
			break;
			case DIV_5X5:
			{
				for (auto i = 0; i < DIV_5X5; i++)
				{
					for (auto j = 0; j < DIV_5X5; j++)
					{
						if (g_Puzzle5X5[i][j].GetIsMoving() == true)
						{
							BitBlt(hdc, origin_x, origin_y, g_Puzzle5X5[i][j].GetSize(), g_Puzzle5X5[i][j].GetSize(),
								memdc1, g_Puzzle5X5[i][j].GetImagePosition().x, g_Puzzle5X5[i][j].GetImagePosition().y, SRCCOPY);
						}
					}
				}
			}
			break;
			}

			DeleteDC(memdc1);
		}
		else
		{
			memdc2 = CreateCompatibleDC(hdc);
			SelectObject(memdc2, hBitmap2);

			g_ProjectManager.Draw(hdc, memdc2, hWnd);

			switch (g_ProjectManager.GetGameMode())
			{
			case DIV_3X3:
			{
				for (auto i = 0; i < DIV_3X3; i++)
				{
					for (auto j = 0; j < DIV_3X3; j++)
					{
						if (g_Puzzle3X3[i][j].GetIsMoving() == true)
						{
							BitBlt(hdc, origin_x, origin_y, g_Puzzle3X3[i][j].GetSize(), g_Puzzle3X3[i][j].GetSize(),
								memdc2, g_Puzzle3X3[i][j].GetImagePosition().x, g_Puzzle3X3[i][j].GetImagePosition().y, SRCCOPY);
						}
					}
				}
			}
			break;
			case DIV_4X4:
			{
				for (auto i = 0; i < DIV_4X4; i++)
				{
					for (auto j = 0; j < DIV_4X4; j++)
					{
						if (g_Puzzle4X4[i][j].GetIsMoving() == true)
						{
							BitBlt(hdc, origin_x, origin_y, g_Puzzle4X4[i][j].GetSize(), g_Puzzle4X4[i][j].GetSize(),
								memdc2, g_Puzzle4X4[i][j].GetImagePosition().x, g_Puzzle4X4[i][j].GetImagePosition().y, SRCCOPY);
						}
					}
				}
			}
			break;
			case DIV_5X5:
			{
				for (auto i = 0; i < DIV_5X5; i++)
				{
					for (auto j = 0; j < DIV_5X5; j++)
					{
						if (g_Puzzle5X5[i][j].GetIsMoving() == true)
						{
							BitBlt(hdc, origin_x, origin_y, g_Puzzle5X5[i][j].GetSize(), g_Puzzle5X5[i][j].GetSize(),
								memdc2, g_Puzzle5X5[i][j].GetImagePosition().x, g_Puzzle5X5[i][j].GetImagePosition().y, SRCCOPY);
						}
					}
				}
			}
			break;
			}

			DeleteDC(memdc2);
		}
		
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
		break;
	case WM_KEYDOWN:
		break;
	case WM_LBUTTONDOWN:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		{
			switch (g_ProjectManager.GetGameMode())
			{
			case DIV_3X3:
			{
				for (auto i = 0; i < DIV_3X3; i++)
				{
					for (auto j = 0; j < DIV_3X3; j++)
					{
						if (IsPointInRect(g_Puzzle3X3[i][j], mouseX, mouseY))
						{
							g_Puzzle3X3[i][j].SetIsSelected(true);
						}
					}
				}
			}
			break;
			case DIV_4X4:
			{
				for (auto i = 0; i < DIV_4X4; i++)
				{
					for (auto j = 0; j < DIV_4X4; j++)
					{
						if (IsPointInRect(g_Puzzle4X4[i][j], mouseX, mouseY))
						{
							g_Puzzle4X4[i][j].SetIsSelected(true);
						}
					}
				}
			}
			break;
			case DIV_5X5:
			{
				for (auto i = 0; i < DIV_5X5; i++)
				{
					for (auto j = 0; j < DIV_5X5; j++)
					{
						if (IsPointInRect(g_Puzzle5X5[i][j], mouseX, mouseY))
						{
							g_Puzzle5X5[i][j].SetIsSelected(true);
						}
					}
				}
			}
			break;
			}
		}
		break;
	case WM_LBUTTONUP:
	{
		InvalidateRgn(hWnd, NULL, true);

		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);

		switch (g_ProjectManager.GetGameMode())
		{
		case DIV_3X3:
		{
			for (auto i = 0; i < DIV_3X3; i++)
			{
				for (auto j = 0; j < DIV_3X3; j++)
				{
					if (g_Puzzle3X3[i][j].GetIsSelected() == true)
					{
						int dest_i = mouseX / g_Puzzle3X3[i][j].GetSize();
						int dest_j = mouseY / g_Puzzle3X3[i][j].GetSize();

						if (abs((i + j) - (dest_i + dest_j)) == 1)
						{
							cout << g_Puzzle3X3[dest_j][dest_i].GetPosition().x << ", " << g_Puzzle3X3[dest_j][dest_i].GetPosition().y << endl;

							if (i < dest_i)
							{
								if (g_Puzzle3X3[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle3X3[i][j].SetDestination(g_Puzzle3X3[dest_j][dest_i].GetPosition().x, g_Puzzle3X3[dest_j][dest_i].GetPosition().y);
									g_Puzzle3X3[i][j].SetIsMoving(true);
									g_Puzzle3X3[i][j].m_DestKind = g_Puzzle3X3[i][j].m_Kind;
									g_Puzzle3X3[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle3X3[i][j].GetPosition().x;
									origin_y = g_Puzzle3X3[i][j].GetPosition().y;
								}
							}
							else if (i > dest_i)
							{
								if (g_Puzzle3X3[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle3X3[i][j].SetDestination(g_Puzzle3X3[dest_j][dest_i].GetPosition().x, g_Puzzle3X3[dest_j][dest_i].GetPosition().y);
									g_Puzzle3X3[i][j].SetIsMoving(true);
									g_Puzzle3X3[i][j].m_DestKind = g_Puzzle3X3[i][j].m_Kind;
									g_Puzzle3X3[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle3X3[i][j].GetPosition().x;
									origin_y = g_Puzzle3X3[i][j].GetPosition().y;
								}
							}
							else if (j < dest_j)
							{
								if (g_Puzzle3X3[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle3X3[i][j].SetDestination(g_Puzzle3X3[dest_j][dest_i].GetPosition().x, g_Puzzle3X3[dest_j][dest_i].GetPosition().y);
									g_Puzzle3X3[i][j].SetIsMoving(true);
									g_Puzzle3X3[i][j].m_DestKind = g_Puzzle3X3[i][j].m_Kind;
									g_Puzzle3X3[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle3X3[i][j].GetPosition().x;
									origin_y = g_Puzzle3X3[i][j].GetPosition().y;
								}
							}
							else if (j > dest_j)
							{
								if (g_Puzzle3X3[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle3X3[i][j].SetDestination(g_Puzzle3X3[dest_j][dest_i].GetPosition().x, g_Puzzle3X3[dest_j][dest_i].GetPosition().y);
									g_Puzzle3X3[i][j].SetIsMoving(true);
									g_Puzzle3X3[i][j].m_DestKind = g_Puzzle3X3[i][j].m_Kind;
									g_Puzzle3X3[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle3X3[i][j].GetPosition().x;
									origin_y = g_Puzzle3X3[i][j].GetPosition().y;
								}
							}

							g_Puzzle3X3[i][j].SetIsSelected(false);
						}

						SetTimer(hWnd, 1, 1, NULL);
					}
				}
			}
		}
		break;
		case DIV_4X4:
		{
			for (auto i = 0; i < DIV_4X4; i++)
			{
				for (auto j = 0; j < DIV_4X4; j++)
				{
					if (g_Puzzle4X4[i][j].GetIsSelected() == true)
					{
						int dest_i = mouseX / g_Puzzle4X4[i][j].GetSize();
						int dest_j = mouseY / g_Puzzle4X4[i][j].GetSize();

						if (abs((i + j) - (dest_i + dest_j)) == 1)
						{
							cout << g_Puzzle4X4[dest_j][dest_i].GetPosition().x << ", " << g_Puzzle4X4[dest_j][dest_i].GetPosition().y << endl;

							if (i < dest_i)
							{
								if (g_Puzzle4X4[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle4X4[i][j].SetDestination(g_Puzzle4X4[dest_j][dest_i].GetPosition().x, g_Puzzle4X4[dest_j][dest_i].GetPosition().y);
									g_Puzzle4X4[i][j].SetIsMoving(true);
									g_Puzzle4X4[i][j].m_DestKind = g_Puzzle4X4[i][j].m_Kind;
									g_Puzzle4X4[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle4X4[i][j].GetPosition().x;
									origin_y = g_Puzzle4X4[i][j].GetPosition().y;
								}
							}
							else if (i > dest_i)
							{
								if (g_Puzzle4X4[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle4X4[i][j].SetDestination(g_Puzzle4X4[dest_j][dest_i].GetPosition().x, g_Puzzle4X4[dest_j][dest_i].GetPosition().y);
									g_Puzzle4X4[i][j].SetIsMoving(true);
									g_Puzzle4X4[i][j].m_DestKind = g_Puzzle4X4[i][j].m_Kind;
									g_Puzzle4X4[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle4X4[i][j].GetPosition().x;
									origin_y = g_Puzzle4X4[i][j].GetPosition().y;
								}
							}
							else if (j < dest_j)
							{
								if (g_Puzzle4X4[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle4X4[i][j].SetDestination(g_Puzzle4X4[dest_j][dest_i].GetPosition().x, g_Puzzle4X4[dest_j][dest_i].GetPosition().y);
									g_Puzzle4X4[i][j].SetIsMoving(true);
									g_Puzzle4X4[i][j].m_DestKind = g_Puzzle4X4[i][j].m_Kind;
									g_Puzzle4X4[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle4X4[i][j].GetPosition().x;
									origin_y = g_Puzzle4X4[i][j].GetPosition().y;
								}
							}
							else if (j > dest_j)
							{
								if (g_Puzzle4X4[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle4X4[i][j].SetDestination(g_Puzzle4X4[dest_j][dest_i].GetPosition().x, g_Puzzle4X4[dest_j][dest_i].GetPosition().y);
									g_Puzzle4X4[i][j].SetIsMoving(true);
									g_Puzzle4X4[i][j].m_DestKind = g_Puzzle4X4[i][j].m_Kind;
									g_Puzzle4X4[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle4X4[i][j].GetPosition().x;
									origin_y = g_Puzzle4X4[i][j].GetPosition().y;
								}
							}

							g_Puzzle4X4[i][j].SetIsSelected(false);
						}

						SetTimer(hWnd, 1, 1, NULL);
					}
				}
			}
		}
		break;
		case DIV_5X5:
		{
			for (auto i = 0; i < DIV_5X5; i++)
			{
				for (auto j = 0; j < DIV_5X5; j++)
				{
					if (g_Puzzle5X5[i][j].GetIsSelected() == true)
					{
						int dest_i = mouseX / g_Puzzle5X5[i][j].GetSize();
						int dest_j = mouseY / g_Puzzle5X5[i][j].GetSize();

						if (abs((i + j) - (dest_i + dest_j)) == 1)
						{
							cout << g_Puzzle5X5[dest_j][dest_i].GetPosition().x << ", " << g_Puzzle5X5[dest_j][dest_i].GetPosition().y << endl;

							if (i < dest_i)
							{
								if (g_Puzzle4X4[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle5X5[i][j].SetDestination(g_Puzzle5X5[dest_j][dest_i].GetPosition().x, g_Puzzle5X5[dest_j][dest_i].GetPosition().y);
									g_Puzzle5X5[i][j].SetIsMoving(true);
									g_Puzzle5X5[i][j].m_DestKind = g_Puzzle5X5[i][j].m_Kind;
									g_Puzzle5X5[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle5X5[i][j].GetPosition().x;
									origin_y = g_Puzzle5X5[i][j].GetPosition().y;
								}
							}
							else if (i > dest_i)
							{
								if (g_Puzzle5X5[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle5X5[i][j].SetDestination(g_Puzzle5X5[dest_j][dest_i].GetPosition().x, g_Puzzle5X5[dest_j][dest_i].GetPosition().y);
									g_Puzzle5X5[i][j].SetIsMoving(true);
									g_Puzzle5X5[i][j].m_DestKind = g_Puzzle5X5[i][j].m_Kind;
									g_Puzzle5X5[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle5X5[i][j].GetPosition().x;
									origin_y = g_Puzzle5X5[i][j].GetPosition().y;
								}
							}
							else if (j < dest_j)
							{
								if (g_Puzzle5X5[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle5X5[i][j].SetDestination(g_Puzzle5X5[dest_j][dest_i].GetPosition().x, g_Puzzle5X5[dest_j][dest_i].GetPosition().y);
									g_Puzzle5X5[i][j].SetIsMoving(true);
									g_Puzzle5X5[i][j].m_DestKind = g_Puzzle5X5[i][j].m_Kind;
									g_Puzzle5X5[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle5X5[i][j].GetPosition().x;
									origin_y = g_Puzzle5X5[i][j].GetPosition().y;
								}
							}
							else if (j > dest_j)
							{
								if (g_Puzzle5X5[dest_j][dest_i].m_Kind == BLANK)
								{
									g_Puzzle5X5[i][j].SetDestination(g_Puzzle5X5[dest_j][dest_i].GetPosition().x, g_Puzzle5X5[dest_j][dest_i].GetPosition().y);
									g_Puzzle5X5[i][j].SetIsMoving(true);
									g_Puzzle5X5[i][j].m_DestKind = g_Puzzle5X5[i][j].m_Kind;
									g_Puzzle5X5[i][j].m_Kind = BLANK;

									origin_x = g_Puzzle5X5[i][j].GetPosition().x;
									origin_y = g_Puzzle5X5[i][j].GetPosition().y;
								}
							}

							g_Puzzle5X5[i][j].SetIsSelected(false);
						}

						SetTimer(hWnd, 1, 1, NULL);
					}
				}
			}
		}
		break;
		}
	}
	break;
	case WM_MOUSEMOVE:
	{
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case ID_FULL_IMAGE_ON:
			tempMode = g_ProjectManager.GetGameMode();
			g_ProjectManager.SetGameMode(FULL_IMAGE);
			break;
		case ID_FULL_IMAGE_OFF:
			g_ProjectManager.SetGameMode(tempMode);
			break;
		case ID_DIV_3:
			g_ProjectManager.SetGameMode(DIV_3X3);
			break;
		case ID_DIV_4:
			g_ProjectManager.SetGameMode(DIV_4X4);
			break;
		case ID_DIV_5:
			g_ProjectManager.SetGameMode(DIV_5X5);
			break;
		case ID_STAGE1:
			g_ProjectManager.SetIsStage1(true);
			g_ProjectManager.SetGameMode(DIV_3X3);
			break;
		case ID_STAGE2:
			g_ProjectManager.SetIsStage1(false);
			g_ProjectManager.SetGameMode(DIV_3X3);
			break;
		case ID_END:
			PostQuitMessage(0);
			break;
		}

		InvalidateRgn(hWnd, NULL, true);
	}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

bool IsPointInRect(CBmpComponent target, int x, int y)
{
	if ((target.GetPosition().x <= x && x <= target.GetPosition().x + target.GetSize()) &&
		(target.GetPosition().y <= y && y <= target.GetPosition().y + target.GetSize()))
	{
		return true;
	}
	else
		return false;
}

bool isCorrect(int a[], int b[], int index)
{
	int idx = 0;
	while (idx != index)
	{
		if (a[idx] != b[idx])
			return false;
			
		idx++;
	}

	return true;
}