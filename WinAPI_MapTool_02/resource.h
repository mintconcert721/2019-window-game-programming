//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// WinAPI_MapTool_02.rc에서 사용되고 있습니다.
//
#define IDR_MENU1                       101
#define IDB_BITMAP_ITEM                 102
#define IDB_BITMAP_TILE1                103
#define IDB_BITMAP_TILE2                104
#define IDB_BITMAP_TILE3                105
#define IDB_BITMAP_TILE4                106
#define IDB_BITMAP_TILE5                107
#define IDB_BITMAP_TILE6                108
#define IDB_BITMAP_GRASS                     109
#define IDB_BITMAP_PLAYER                     110
#define ID_40001                        40001
#define ID_40002                        40002
#define ID_FILE_LOAD                    40003
#define ID_SAVE                         40004
#define ID_LOAD                         40005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        111
#define _APS_NEXT_COMMAND_VALUE         40006
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
