#include "Library.h"
#include "resource.h"

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	// 차일드 윈도우 클래스 등록
	WndClass.lpfnWndProc = (WNDPROC)ChildWndProc;
	WndClass.hCursor = LoadCursor(NULL, IDC_HELP);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszChildClass;
	WndClass.hIconSm = NULL;
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, TRUE);

	// 윈도우 생성
	hMainhWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 10, 10,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hMainhWnd, nCmdShow);
	UpdateWindow(hMainhWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;
	static HBITMAP hBitmap, hOldBitmap;

	HBITMAP hTile1Bitmap;;
	HBITMAP hTile2Bitmap;
	HBITMAP hTile3Bitmap;
	HBITMAP hTile4Bitmap;
	HBITMAP hTile5Bitmap;
	HBITMAP hTile6Bitmap;
	HBITMAP hItemBitmap;
	HBITMAP hGrassBitmap;
	HBITMAP hPlayerBitmap;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);

		hTile1Bitmap = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_TILE1));
		hTile2Bitmap = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_TILE2));
		hTile3Bitmap = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_TILE3));
		hTile4Bitmap = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_TILE4));
		hTile5Bitmap = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_TILE5));
		hTile6Bitmap = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_TILE6));
		hItemBitmap = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_ITEM));
		hGrassBitmap = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_GRASS));
		hPlayerBitmap = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_PLAYER));

		hChildhWnd = CreateWindow(lpszChildClass, NULL, WS_CHILD | WS_VISIBLE,
			100, 0, // 좌표
			WINDOW_Y, WINDOW_Y, // 크기
			hWnd, (HMENU)CHILD_WINDOW, g_hInstance, NULL);

		hLeft_Button = CreateWindow(lpszButtonClass, "◀", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			0, 0, // 좌표
			100, WINDOW_Y, // 크기
			hWnd, (HMENU)LEFT_BUTTON, g_hInstance, NULL);

		hRight_Button = CreateWindow(lpszButtonClass, "▶", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			900, 0, // 좌표
			100, WINDOW_Y, // 크기
			hWnd, (HMENU)RIGHT_BUTTON, g_hInstance, NULL);

		hTile1_Button = CreateWindow(lpszButtonClass, NULL, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER | BS_BITMAP,
			900 + 200, 50,
			100, 100,
			hWnd, (HMENU)TILE1_BUTTON, g_hInstance, NULL);
		SendMessage(hTile1_Button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hTile1Bitmap);


		hTile2_Button = CreateWindow(lpszButtonClass, NULL, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER | BS_BITMAP,
			900 + 200 + 100 + 50, 50,
			100, 100,
			hWnd, (HMENU)TILE2_BUTTON, g_hInstance, NULL);
		SendMessage(hTile2_Button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hTile2Bitmap);


		hTile3_Button = CreateWindow(lpszButtonClass, NULL, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER | BS_BITMAP,
			900 + 200 + 100 + 50 + 100 + 50, 50,
			100, 100,
			hWnd, (HMENU)TILE3_BUTTON, g_hInstance, NULL);
		SendMessage(hTile3_Button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hTile3Bitmap);


		hTile4_Button = CreateWindow(lpszButtonClass, NULL, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER | BS_BITMAP,
			900 + 200, 50 + 100 + 50,
			100, 100,
			hWnd, (HMENU)TILE4_BUTTON, g_hInstance, NULL);
		SendMessage(hTile4_Button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hTile4Bitmap);


		hTile5_Button = CreateWindow(lpszButtonClass, NULL, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER | BS_BITMAP,
			900 + 200 + 100 + 50, 50 + 100 + 50,
			100, 100,
			hWnd, (HMENU)TILE5_BUTTON, g_hInstance, NULL);
		SendMessage(hTile5_Button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hTile5Bitmap);


		hTile6_Button = CreateWindow(lpszButtonClass, NULL, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER | BS_BITMAP,
			900 + 200 + 100 + 50 + 100 + 50, 50 + 100 + 50,
			100, 100,
			hWnd, (HMENU)TILE6_BUTTON, g_hInstance, NULL);
		SendMessage(hTile6_Button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hTile6Bitmap);


		hGrass_Button = CreateWindow(lpszButtonClass, NULL, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER | BS_BITMAP,
			900 + 200, 50 + 100 + 50 + 100 + 50,
			100, 100,
			hWnd, (HMENU)GRASS_BUTTON, g_hInstance, NULL);
		SendMessage(hGrass_Button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hGrassBitmap);


		hItem_Button = CreateWindow(lpszButtonClass, NULL, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER | BS_BITMAP,
			900 + 200 + 100 + 50, 50 + 100 + 50 + 100 + 50,
			100, 100,
			hWnd, (HMENU)ITEM_BUTTON, g_hInstance, NULL);
		SendMessage(hItem_Button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hItemBitmap);


		hPlayer_Button = CreateWindow(lpszButtonClass, NULL, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER | BS_BITMAP,
			900 + 200 + 100 + 50 + 100 + 50, 50 + 100 + 50 + 100 + 50,
			100, 100,
			hWnd, (HMENU)PLAYER_BUTTON, g_hInstance, NULL);
		SendMessage(hPlayer_Button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hPlayerBitmap);

		hTest_Button = CreateWindow(lpszButtonClass, "테스트", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			900 + 200 + 100 + 50, 50 + 100 + 50 + 100 + 50 + 100 + 50 + 100 + 50,
			100, 100,
			hWnd, (HMENU)TEST_BUTTON, g_hInstance, NULL);

		for (int j = 0; j < TILE_Y; j++)
		{
			for (int i = 0; i < TILE_Y * 3; i++)
			{
				if (i >= 0 && i < TILE_Y)
				{
					TileMap[i][j].position.x = i * TILE_SIZE;
					TileMap[i][j].position.y = j * TILE_SIZE;
					TileMap[i][j].size.x = TILE_SIZE;
					TileMap[i][j].size.y = TILE_SIZE;
				}
				else if (i >= TILE_Y && i < TILE_Y * 2)
				{
					TileMap[i][j].position.x = i * TILE_SIZE;
					TileMap[i][j].position.y = j * TILE_SIZE;
					TileMap[i][j].size.x = TILE_SIZE;
					TileMap[i][j].size.y = TILE_SIZE;
				}
				else if (i >= TILE_Y * 2)
				{
					TileMap[i][j].position.x = i * TILE_SIZE;
					TileMap[i][j].position.y = j * TILE_SIZE;
					TileMap[i][j].size.x = TILE_SIZE;
					TileMap[i][j].size.y = TILE_SIZE;
				}
			}
		}
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case TILE1_BUTTON:
		{
			MouseClickKind = TILE1;
		}
		break;
		case TILE2_BUTTON:
		{
			MouseClickKind = TILE2;
		}
		break;
		case TILE3_BUTTON:
		{
			MouseClickKind = TILE3;
		}
		break;
		case TILE4_BUTTON:
		{
			MouseClickKind = TILE4;
		}
		break;
		case TILE5_BUTTON:
		{
			MouseClickKind = TILE5;
		}
		break;
		case TILE6_BUTTON:
		{
			MouseClickKind = TILE6;
		}
		break;
		case ITEM_BUTTON:
		{
			MouseClickKind = ITEM;
		}
		break;
		case GRASS_BUTTON:
		{
			MouseClickKind = GRASS;
		}
		break;
		case LEFT_BUTTON:
		{
			if (g_NX > 0)
			{
				g_NX -= WINDOW_Y;
			}
		}
		break;
		case RIGHT_BUTTON:
		{
			if (g_NX < WINDOW_Y * 2)
			{
				g_NX += WINDOW_Y;
			}
		}
		break;
		case TEST_BUTTON:
		{
			g_IsTest = true;
		}
		break;
		case ID_SAVE:
		{
			// 파일 저장
		}
		break;
		case ID_LOAD:
		{
			// 파일 불러오기
		}
		break;
		}
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);
		FillRect(memdc, &crt, (HBRUSH)GetStockObject(WHITE_BRUSH)); //도화지 색 변경

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
	{
		PostQuitMessage(0);
	}
	break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}


bool CheckCollisionPlayerAndObejct(Player& p, Object& o)
{
	if (p.position.x > o.position.x + o.size.x)
	{
		return false;
	}
	if (p.position.x + p.size.x < o.position.x)
	{
		return false;
	}
	if (p.position.y > o.position.y + o.size.y)
	{
		return false;
	}
	if (p.position.y + p.size.y < o.position.y)
	{
		return false;
	}

	return true;
}

LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc, bitdc;
	static RECT c1crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static HBITMAP hChildBitmap, hChildOldBitmap;

	switch (uMsg)
	{
	case WM_CREATE:
	{
		BGImage[0].Load("Png/BG1.png");
		BGImage[1].Load("Png/BG2.png");
		BGImage[2].Load("Png/BG3.png");

		PlayerImage.Load("Png/Player.png");
		GrassImage.Load("Png/Grass.png");
		ItemImage.Load("Png/Life.png");

		TileImage[0].Load("Png/Tile1.png");
		TileImage[1].Load("Png/Tile2.png");
		TileImage[2].Load("Png/Tile3.png");
		TileImage[3].Load("Png/Tile4.png");
		TileImage[4].Load("Png/Tile5.png");
		TileImage[5].Load("Png/Tile6.png");

		player.position.x = TILE_SIZE * 2;
		player.position.y = TILE_SIZE * 9;

		GetClientRect(hWnd, &c1crt);
		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_TIMER:
	{
		if (g_IsTest)
		{
			player.position.y += 1;

			// 충돌 처리
			for (auto& v : g_Container1)
			{
				if (CheckCollisionPlayerAndLand(v, player))
				{
					player.position.y -= 1;
				}
			}
			// 충돌 처리
			for (auto& v : g_Container2)
			{
				if (CheckCollisionPlayerAndLand(v, player))
				{
					player.position.y -= 1;
				}
			}
			// 충돌 처리
			for (auto& v : g_Container3)
			{
				if (CheckCollisionPlayerAndLand(v, player))
				{
					player.position.y -= 1;
				}
			}

			cout << player.position.y << endl;
			g_NX += 1; // 게임이 시작되면 왼쪽으로 계속 움직여 주자 

			if ((g_NX % (WINDOW_Y * 2)) == 0)
			{
				g_Value += WINDOW_Y * 3;
				g_DontModify[0] = false;
				g_DontModify[1] = false;
				g_DontModify[2] = false;
			}

			if (g_NX < WINDOW_Y * 3)
			{
				if (g_NX >= 0)
				{
					if (g_DontModify[0] == false)
					{
						for (auto& v : g_Container1)
						{
							v.position.x = v.position.x + g_Value;
						}

						for (int j = 0; j < TILE_Y; j++)
						{
							for (int i = 0; i < TILE_Y; i++)
							{
								TileMap[i][j].position.x = TileMap[i][j].position.x + g_Value;
							}
						}

						g_DontModify[0] = true;
					}

					
				}
				else if (g_NX >= WINDOW_Y && g_NX < WINDOW_Y * 2)
				{
					if (g_DontModify[1] == false)
					{
						for (auto& v : g_Container2)
						{
							v.position.x = v.position.x + g_Value;
						}

						for (int j = 0; j < TILE_Y; j++)
						{
							for (int i = TILE_Y; i < TILE_Y * 2; i++)
							{
								TileMap[i][j].position.x = TileMap[i][j].position.x + g_Value;
							}
						}

						g_DontModify[1] = true;
					}

					
				}
				else if (g_NX >= WINDOW_Y * 2)
				{
					if (g_DontModify[2] == false)
					{
						for (auto& v : g_Container3)
						{
							v.position.x = v.position.x + g_Value;
						}

						for (int j = 0; j < TILE_Y; j++)
						{
							for (int i = TILE_Y * 2; i < TILE_Y * 3; i++)
							{
								TileMap[i][j].position.x = TileMap[i][j].position.x + g_Value;
							}
						}

						g_DontModify[2] = true;
					}

					
				}
			}
		}

		if (GetAsyncKeyState(VK_UP) & 0x8000)
		{
			player.position.y -= 5;

			/*for (int j = 0; j < TILE_Y; j++)
			{
				for (int i = 0; i < TILE_Y * 3; i++)
				{
					if (TileMap[i][j].kind >= TILE1 && TileMap[i][j].kind <= TILE6)
					{
						if (CheckCollisionPlayerAndTiles(player, TileMap[i][j]) == true)
						{
							player.position.y += 5;
						}
					}
				}
			}*/
			/*if (player.position.y - 5 < 0)
				break;

			player.position.y -= 5;*/
		}

		if (GetAsyncKeyState(VK_DOWN) & 0x8000)
		{
			player.position.y += 5;

			


			

			/*for (int j = 0; j < TILE_Y; j++)
			{
				for (int i = 0; i < TILE_Y * 3; i++)
				{
					if (TileMap[i][j].kind >= TILE1 && TileMap[i][j].kind <= TILE6)
					{
						if (CheckCollisionPlayerAndTiles(player, TileMap[i][j]) == true)
						{
							player.position.y -= 5;
						}
					}
				}
			}*/
			/*if (player.position.y + 5 > MapSize.y)
				break;

			player.position.y += 5;*/
		}

		if (GetAsyncKeyState(VK_LEFT) & 0x8000)
		{
			player.position.x -= 5;
		}

		if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
		{
			player.position.x += 5;
		}

		/*for (int i = 0; i < numOfObject; i++)
		{
			if (CheckCollisionPlayerAndObejct(player, objects[i]) == true)
			{
				if (GetAsyncKeyState(VK_UP) & 0x8000)
				{
					player.position.y += 5;
				}

				if (GetAsyncKeyState(VK_DOWN) & 0x8000)
				{
					player.position.y -= 5;
				}

				if (GetAsyncKeyState(VK_LEFT) & 0x8000)
				{
					player.position.x += 5;
				}

				if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
				{
					player.position.x -= 5;
				}
			}
		}*/

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		bitdc = CreateCompatibleDC(memdc);
		hChildBitmap = CreateCompatibleBitmap(hdc, c1crt.right, c1crt.bottom);
		hChildOldBitmap = (HBITMAP)SelectObject(memdc, hChildBitmap);
		FillRect(memdc, &c1crt, (HBRUSH)GetStockObject(GRAY_BRUSH)); //도화지 색 변경

		// TODO:

		BGImage[0].StretchBlt(memdc, 0 - g_NX, 0, WINDOW_Y, WINDOW_Y);
		BGImage[1].StretchBlt(memdc, WINDOW_Y - g_NX, 0, WINDOW_Y, WINDOW_Y);
		BGImage[2].StretchBlt(memdc, WINDOW_Y * 2 - g_NX, 0, WINDOW_Y, WINDOW_Y);

		if (g_NX > WINDOW_Y * 2)
		{
			BGImage[0].StretchBlt(memdc, g_Value - g_NX, 0, WINDOW_Y, WINDOW_Y);
			BGImage[1].StretchBlt(memdc, g_Value + WINDOW_Y - g_NX, 0, WINDOW_Y, WINDOW_Y);
			BGImage[2].StretchBlt(memdc, g_Value + WINDOW_Y * 2 - g_NX, 0, WINDOW_Y, WINDOW_Y);
		}

		if (!g_IsTest)
		{
			for (int j = 0; j < TILE_Y; j++)
			{
				for (int i = 0; i < TILE_Y; i++)
				{
					if (g_NX >= 0 && g_NX < WINDOW_Y)
					{
						switch (TileMap[i][j].kind)
						{
						case TILE1:
						{
							TileImage[0].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE2:
						{
							TileImage[1].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE3:
						{
							TileImage[2].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE4:
						{
							TileImage[3].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE5:
						{
							TileImage[4].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE6:
						{
							TileImage[5].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case ITEM:
						{
							ItemImage.Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case GRASS:
						{
							GrassImage.Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case PLAYER:
						{
							PlayerImage.Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						}
					}
					else if (g_NX >= WINDOW_Y && g_NX < WINDOW_Y * 2)
					{
						switch (TileMap[i + TILE_Y][j].kind)
						{
						case TILE1:
						{
							TileImage[0].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE2:
						{
							TileImage[1].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE3:
						{
							TileImage[2].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE4:
						{
							TileImage[3].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE5:
						{
							TileImage[4].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE6:
						{
							TileImage[5].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case ITEM:
						{
							ItemImage.Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case GRASS:
						{
							GrassImage.Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case PLAYER:
						{
							PlayerImage.Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						}
					}
					else if (g_NX >= WINDOW_Y * 2)
					{
						switch (TileMap[i + TILE_Y * 2][j].kind)
						{
						case TILE1:
						{
							TileImage[0].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE2:
						{
							TileImage[1].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE3:
						{
							TileImage[2].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE4:
						{
							TileImage[3].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE5:
						{
							TileImage[4].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case TILE6:
						{
							TileImage[5].Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case ITEM:
						{
							ItemImage.Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case GRASS:
						{
							GrassImage.Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						case PLAYER:
						{
							PlayerImage.Draw(memdc, TileMap[i][j].position.x, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
						}
						break;
						}
					}
				}
			}

			for (int j = 0; j < TILE_Y; j++)
			{
				for (int i = 0; i < TILE_Y * 3; i++)
				{
					HBRUSH hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
					HBRUSH hOldBrush = (HBRUSH)SelectObject(memdc, hBrush);

					Rectangle(memdc, i * TILE_SIZE, j * TILE_SIZE, (i + 1) * TILE_SIZE, (j + 1) * TILE_SIZE);

					SelectObject(memdc, hOldBrush);
					DeleteObject(hBrush);
				}
			}
		}
		else
		{
			for (auto& v : g_Container1)
			{
				switch (v.kind)
				{
				case TILE1:
				{
					TileImage[0].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE2:
				{
					TileImage[1].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE3:
				{
					TileImage[2].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE4:
				{
					TileImage[3].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE5:
				{
					TileImage[4].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE6:
				{
					TileImage[5].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case ITEM:
				{
					ItemImage.Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case GRASS:
				{
					GrassImage.Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case PLAYER:
				{
					PlayerImage.Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				}
			}
			for (auto& v : g_Container2)
			{
				switch (v.kind)
				{
				case TILE1:
				{
					TileImage[0].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE2:
				{
					TileImage[1].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE3:
				{
					TileImage[2].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE4:
				{
					TileImage[3].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE5:
				{
					TileImage[4].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE6:
				{
					TileImage[5].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case ITEM:
				{
					ItemImage.Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case GRASS:
				{
					GrassImage.Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case PLAYER:
				{
					PlayerImage.Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				}
			}
			for (auto& v : g_Container3)
			{
				switch (v.kind)
				{
				case TILE1:
				{
					TileImage[0].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE2:
				{
					TileImage[1].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE3:
				{
					TileImage[2].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE4:
				{
					TileImage[3].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE5:
				{
					TileImage[4].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case TILE6:
				{
					TileImage[5].Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case ITEM:
				{
					ItemImage.Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case GRASS:
				{
					GrassImage.Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				case PLAYER:
				{
					PlayerImage.Draw(memdc, v.position.x - g_NX, v.position.y, v.size.x, v.size.y);
				}
				break;
				}
			}

			/*for (int j = 0; j < TILE_Y; j++)
			{
				for (int i = 0; i < TILE_Y * 3; i++)
				{
					switch (TileMap[i][j].kind)
					{
					case TILE1:
					{
						TileImage[0].Draw(memdc, TileMap[i][j].position.x - g_NX, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
					}
					break;
					case TILE2:
					{
						TileImage[1].Draw(memdc, TileMap[i][j].position.x - g_NX, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
					}
					break;
					case TILE3:
					{
						TileImage[2].Draw(memdc, TileMap[i][j].position.x - g_NX, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
					}
					break;
					case TILE4:
					{
						TileImage[3].Draw(memdc, TileMap[i][j].position.x - g_NX, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
					}
					break;
					case TILE5:
					{
						TileImage[4].Draw(memdc, TileMap[i][j].position.x - g_NX, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
					}
					break;
					case TILE6:
					{
						TileImage[5].Draw(memdc, TileMap[i][j].position.x - g_NX, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
					}
					break;
					case ITEM:
					{
						ItemImage.Draw(memdc, TileMap[i][j].position.x - g_NX, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
					}
					break;
					case GRASS:
					{
						GrassImage.Draw(memdc, TileMap[i][j].position.x - g_NX, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
					}
					break;
					case PLAYER:
					{
						PlayerImage.Draw(memdc, TileMap[i][j].position.x - g_NX, TileMap[i][j].position.y, TileMap[i][j].size.x, TileMap[i][j].size.y);
					}
					break;
					}
				}
			}*/
		}

		player.Draw(memdc);

		BitBlt(hdc, 0, 0, c1crt.right, c1crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hChildOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		/*case ID_TESTWINDOW_EXIT:
		{
			ShowWindow(hWnd, SW_HIDE);
		}
		break;*/
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		mouse.x = LOWORD(lParam);
		mouse.y = HIWORD(lParam);
		Object o;

		// 배치할 때
		if (g_NX >= 0 && g_NX < WINDOW_Y)
		{
			for (int j = 0; j < TILE_Y; j++)
			{
				for (int i = 0; i < TILE_Y; i++)
				{
					if (IsPointInRectangle(TileMap[i][j], mouse))
					{
						TileMap[i][j].kind = MouseClickKind;
						o.position = TileMap[i][j].position;
						o.kind = TileMap[i][j].kind;
						g_Container1.push_back(o);
					}
				}
			}
		}
		else if (g_NX >= WINDOW_Y && g_NX < WINDOW_Y * 2)
		{
			for (int j = 0; j < TILE_Y; j++)
			{
				for (int i = TILE_Y; i < TILE_Y * 2; i++)
				{
					if (IsPointInRectangle(TileMap[i][j], mouse))
					{
						TileMap[i][j].kind = MouseClickKind;
						o.position = TileMap[i][j].position;
						o.kind = TileMap[i][j].kind;
						g_Container2.push_back(o);
					}
				}
			}
		}
		else if (g_NX >= WINDOW_Y * 2)
		{
			for (int j = 0; j < TILE_Y; j++)
			{
				for (int i = TILE_Y * 2; i < TILE_Y * 3; i++)
				{
					if (IsPointInRectangle(TileMap[i][j], mouse))
					{
						TileMap[i][j].kind = MouseClickKind;
						o.position = TileMap[i][j].position;
						o.kind = TileMap[i][j].kind;
						g_Container3.push_back(o);
					}
				}
			}
		}
	}
	break;
	case WM_DESTROY:
	{
		PostQuitMessage(0);
	}
	break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}