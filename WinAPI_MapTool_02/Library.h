#pragma once

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

#include <Windows.h>
#include <iostream>
#include <atlimage.h>
#include <time.h>
#include <vector>

using namespace std;

#define WINDOW_X 1600
#define WINDOW_Y 800

#define FRAME_X 135
#define FRAME_Y 150

#define TILE1_BUTTON 101
#define TILE2_BUTTON 102
#define TILE3_BUTTON 103
#define TILE4_BUTTON 104
#define TILE5_BUTTON 105
#define TILE6_BUTTON 106
#define GRASS_BUTTON 107
#define ITEM_BUTTON 108
#define TEST_BUTTON 109
#define LEFT_BUTTON 110
#define RIGHT_BUTTON 111
#define CHILD_WINDOW 112
#define PLAYER_BUTTON 113

#define TILE_SIZE 40

#define BLANK 0
#define TILE1 1
#define TILE2 2
#define TILE3 3
#define TILE4 4
#define TILE5 5
#define TILE6 6
#define ITEM 7
#define GRASS 8
#define PLAYER 9

int g_NX = 0;
int MouseClickKind = BLANK;
bool g_IsTest = false;

HINSTANCE g_hInstance;

LPCTSTR lpszClass = "ParentClass";
LPCTSTR lpszChildClass = "ChildClass";
LPCTSTR lpszButtonClass = "button";
LPCTSTR lpszEditClass = "edit";
LPCTSTR lpszWindowName = "Window Program EX-2 : 횡스크롤 러닝 게임을 위한 맵툴 만들기";
TCHAR lpstrFile[100] = "";
char str[256];

OPENFILENAME OFN;
OPENFILENAME SFN;
FILE* fp;

HWND hChildhWnd;
HWND hMainhWnd;

HWND hTile1_Button, hTile2_Button, hTile3_Button, hTile4_Button, hTile5_Button, hTile6_Button;
HWND hGrass_Button, hItem_Button, hPlayer_Button;
HWND hTest_Button, hLeft_Button, hRight_Button;
HWND hWndChild;

HBRUSH hBrush;
HBRUSH hOldBrush;

struct Player
{
	POINT position;
	POINT size;
	CImage image;

	Player()
	{
		position.x = 0;
		position.y = 0;

		size.x = TILE_SIZE;
		size.y = TILE_SIZE;

		image.Load("Png/Player.png");
	}

	void Draw(HDC hdc)
	{
		image.Draw(hdc, position.x, position.y, size.x, size.y);
	}
};

struct Object
{
	POINT position;
	POINT size;
	int kind;

	Object()
	{
		position.x = 0;
		position.y = 0;

		size.x = TILE_SIZE;
		size.y = TILE_SIZE;

		kind = BLANK;
	}

	Object& operator = (const Object& rhs)
	{
		position = rhs.position;
		size = rhs.size;
		kind = rhs.kind;
	}
};

struct TileInfo
{
	POINT position;
	POINT size;
	int kind;

	TileInfo()
	{
		position.x = 0;
		position.y = 0;

		size.x = TILE_SIZE;
		size.y = TILE_SIZE;

		kind = BLANK;
	}

	TileInfo& operator = (const TileInfo& rhs)
	{
		position = rhs.position;
		size = rhs.size;
		kind = rhs.kind;
	}
};

#define TILE_Y 20
TileInfo TileMap[TILE_Y * 3][TILE_Y];

// 전역변수
POINT mouse;
POINT MapSize;
POINT TileNumber;
Player player;

// 함수 선언
char* GetFileName(char file_path[]);
bool IsPointInRectangle(TileInfo rect, POINT mouse);
void DrawGrid(HDC screedDC);

// 함수 정의
char* GetFileName(char file_path[])
{
	char* FileName = nullptr;
	while (*file_path)
	{
		if (*file_path == '\\' && (file_path + 1) != NULL)
		{
			FileName = file_path + 1;
		}

		file_path++;
	}
	return FileName;
}

bool IsPointInRectangle(TileInfo rect, POINT mouse)
{
	if (g_NX >= 0 && g_NX < WINDOW_Y)
	{
		if (((rect.position.x) <= mouse.x) && (mouse.x <= ((rect.position.x) + rect.size.x)) &&
			(rect.position.y <= mouse.y) && (mouse.y <= (rect.position.y + rect.size.y)))
		{
			return true;
		}

		return false;
	}
	else if (g_NX >= WINDOW_Y && g_NX < WINDOW_Y * 2)
	{
		if (((rect.position.x - WINDOW_Y) <= mouse.x) && (mouse.x <= ((rect.position.x - WINDOW_Y) + rect.size.x)) &&
			(rect.position.y <= mouse.y) && (mouse.y <= (rect.position.y + rect.size.y)))
		{
			return true;
		}

		return false;
	}
	else if (g_NX >= WINDOW_Y * 2)
	{
		if (((rect.position.x - WINDOW_Y * 2) <= mouse.x) && (mouse.x <= ((rect.position.x - WINDOW_Y * 2) + rect.size.x)) &&
			(rect.position.y <= mouse.y) && (mouse.y <= (rect.position.y + rect.size.y)))
		{
			return true;
		}

		return false;
	}

	return false;
}

void DrawGrid(HDC screedDC)
{
	for (int j = 0; j < TileNumber.y; j++)
	{
		for (int i = 0; i < TileNumber.x; i++)
		{
			hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
			hOldBrush = (HBRUSH)SelectObject(screedDC, hBrush);

			int x = (MapSize.x / TileNumber.x);
			int y = (MapSize.y / TileNumber.y);

			Rectangle(screedDC, i * x, j * y, (i + 1) * x, (j + 1) * y);

			SelectObject(screedDC, hOldBrush);
			DeleteObject(hBrush);
		}
	}
}

CImage BGImage[3];
CImage PlayerImage;
CImage GrassImage;
CImage ItemImage;
CImage TileImage[6];

bool g_DontModify[3] = { false, false, false };

static int g_Value = 0;

vector<Object> g_Container1;
vector<Object> g_Container2;
vector<Object> g_Container3;

bool CheckCollisionPlayerAndLand(Object& o, Player& p)
{
	if (((o.position.x - g_NX) < p.position.x + p.size.x) && (o.position.y < (p.position.y + p.size.y)) && (((o.position.x - g_NX) > p.position.x) && ((o.position.y + o.size.y) > p.position.y)))
	{
		return true;
	}
	else
		return false;
}