#include <Windows.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <atlimage.h>

#pragma comment(lib, "msimg32.lib")

using namespace std;

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

enum DIRECTION { UP, DOWN, LEFT, RIGHT };

#define WINDOW_X 1280
#define WINDOW_Y 720

#define FRAME_X 135
#define FRAME_Y 150

#define MOVE 0
#define DEATH 1

struct Point {
	// 멤버변수
	int x = 0, y = 0;

	Point& operator=(Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}
};

struct Hero {
	CImage sprite_image; // 이미지
	Point spriteSize;
	int spriteEntireCount;
	int spriteCurrentIndex;

	Point position; // 위치
	int direction; // 방향
	RECT boundingBox; // 충돌처리
	Point realSize; // 실제 게임에서 사용할 크기
	int speed; // 이동속도

	void Init()
	{
		sprite_image.Load(TEXT("./Sprite/mushmom_move.png"));

		spriteSize.x = 135; // 읽을 너비
		spriteSize.y = 150; // 읽을 높이

		spriteEntireCount = 4;
		spriteCurrentIndex = 0;

		srand((unsigned int)time(NULL));

		direction = rand() % 4;

		position.x = WINDOW_X / 2;
		position.y = WINDOW_Y / 2;

		realSize.x = 150;
		realSize.y = 150;

		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;

		speed = 10;
	}

	void move()
	{
		switch (direction)
		{
		case UP:
		{
			if (boundingBox.top > 0)
				position.y -= speed;
			else
				direction = DOWN;
		}
		break;
		case DOWN:
		{
			if (boundingBox.bottom < WINDOW_Y)
				position.y += speed;
			else
				direction = UP;
		}
		break;
		case LEFT:
		{
			if (boundingBox.left > 0)
				position.x -= speed;
			else
				direction = RIGHT;
		}
		break;
		case RIGHT:
		{
			if (boundingBox.right < WINDOW_X)
				position.x += speed;
			else
				direction = LEFT;
		}
		break;
		}
	}

	void update()
	{
		(++spriteCurrentIndex) %= spriteEntireCount;

		// 바운딩박스 업데이트
		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;

		move();
	}

	void render(HDC _hdc, HWND _hWnd)
	{
		int SpriteWidth = sprite_image.GetWidth() / spriteSize.x;
		int SpriteHeight = sprite_image.GetHeight() / spriteSize.y;

		int xCoord = spriteCurrentIndex % SpriteWidth;
		int yCoord = spriteCurrentIndex / SpriteWidth;

		sprite_image.Draw(_hdc, position.x, position.y, realSize.x, realSize.y, xCoord * spriteSize.x, yCoord * spriteSize.y, spriteSize.x, spriteSize.y);
	}
};

Hero g_Hero;

struct Bullet {
	bool isVisible = false;
	int Life = 1;

	CImage sprite_image; // 이미지
	Point spriteSize;
	int spriteEntireCount;
	int spriteCurrentIndex;

	Point position; // 위치
	int direction; // 방향
	RECT boundingBox; // 충돌처리
	Point realSize; // 실제 게임에서 사용할 크기
	int speed; // 이동속도

	void Init()
	{
		sprite_image.Load(TEXT("./Sprite/tear.png"));

		spriteSize.x = 50; // 읽을 너비
		spriteSize.y = 50; // 읽을 높이

		spriteEntireCount = 1;
		spriteCurrentIndex = 0;

		realSize.x = 50;
		realSize.y = 50;

		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;

		speed = 50;
	}

	void move()
	{
		switch (direction)
		{
		case UP:
		{
			if (boundingBox.top > 0)
				position.y -= speed;
			else
				isVisible = false;
		}
		break;
		case DOWN:
		{
			if (boundingBox.bottom < WINDOW_Y)
				position.y += speed;
			else
				isVisible = false;
		}
		break;
		case LEFT:
		{
			if (boundingBox.left > 0)
				position.x -= speed;
			else
				isVisible = false;
		}
		break;
		case RIGHT:
		{
			if (boundingBox.right < WINDOW_X)
				position.x += speed;
			else
				isVisible = false;
		}
		break;
		}
	}

	void update()
	{
		(++spriteCurrentIndex) %= spriteEntireCount;

		// 바운딩박스 업데이트
		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;

		move();
	}

	void render(HDC _hdc, HWND _hWnd)
	{
		int SpriteWidth = sprite_image.GetWidth() / spriteSize.x;
		int SpriteHeight = sprite_image.GetHeight() / spriteSize.y;

		int xCoord = spriteCurrentIndex % SpriteWidth;
		int yCoord = spriteCurrentIndex / SpriteWidth;

		sprite_image.Draw(_hdc, position.x, position.y, realSize.x, realSize.y, xCoord * spriteSize.x, yCoord * spriteSize.y, spriteSize.x, spriteSize.y);
	}
};

Bullet g_Bullet[100];

struct Monster {
	bool isVisible = false;
	int Life = 1;

	CImage sprite_image[2]; // 이미지
	Point spriteSize[2];
	int spriteEntireCount[2];
	int spriteCurrentIndex;

	Point position; // 위치
	int direction; // 방향
	RECT boundingBox; // 충돌처리
	Point realSize; // 실제 게임에서 사용할 크기
	int speed; // 이동속도

	void Init()
	{
		sprite_image[MOVE].Load(TEXT("./Sprite/monster_move.png"));
		sprite_image[DEATH].Load(TEXT("./Sprite/monster_death.png"));

		spriteSize[MOVE].x = 196; // 읽을 너비
		spriteSize[MOVE].y = 195; // 읽을 높이

		spriteSize[DEATH].x = 239; // 읽을 너비
		spriteSize[DEATH].y = 195; // 읽을 높이

		spriteEntireCount[MOVE] = 4;
		spriteEntireCount[DEATH] = 8;
		spriteCurrentIndex = 0;

		realSize.x = 100;
		realSize.y = 100;

		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;

		speed = 20;
	}

	void move()
	{
		switch (direction)
		{
		case UP:
		{
			if (boundingBox.top > 0)
				position.y -= speed;
			else
				direction = DOWN;
		}
		break;
		case DOWN:
		{
			if (boundingBox.bottom < WINDOW_Y)
				position.y += speed;
			else
				direction = UP;
		}
		break;
		case LEFT:
		{
			if (boundingBox.left > 0)
				position.x -= speed;
			else
				direction = RIGHT;
		}
		break;
		case RIGHT:
		{
			if (boundingBox.right < WINDOW_X)
				position.x += speed;
			else
				direction = LEFT;
		}
		break;
		}
	}

	bool CheckCollision(Bullet* bullet)
	{
		for (int i = 0; i < 100; i++)
		{
			if (bullet[i].isVisible)
			{
				for (int i = 0; i < 100; i++)
				{
					if (bullet[i].isVisible)
					{
						if (bullet[i].boundingBox.right < boundingBox.left)
						{
							return false;
						}
						if (bullet[i].boundingBox.bottom < boundingBox.top)
						{
							return false;
						}
						if (bullet[i].boundingBox.left > boundingBox.right)
						{
							return false;
						}
						if (bullet[i].boundingBox.top > boundingBox.bottom)
						{
							return false;
						}

						bullet[i].Life = 0;
						bullet[i].isVisible = false;
						return true;
					}
				}
			}
		}
	}

	void update()
	{
		if (Life <= 0 && spriteCurrentIndex == 7)
		{
			isVisible = false;
		}

		if (CheckCollision(g_Bullet) == true)
		{
			Life = 0;
			speed = 0;
		}

		if (Life > 0)
			(++spriteCurrentIndex) %= spriteEntireCount[MOVE];
		else
			(++spriteCurrentIndex) %= spriteEntireCount[DEATH];

		// 바운딩박스 업데이트
		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;

		move();
	}

	void render(HDC _hdc, HWND _hWnd, int mode)
	{
		if (mode == MOVE)
		{
			int SpriteWidth = sprite_image[MOVE].GetWidth() / spriteSize[MOVE].x;
			int SpriteHeight = sprite_image[MOVE].GetHeight() / spriteSize[MOVE].y;

			int xCoord = spriteCurrentIndex % SpriteWidth;
			int yCoord = spriteCurrentIndex / SpriteWidth;

			sprite_image[MOVE].Draw(_hdc, position.x, position.y, realSize.x, realSize.y, xCoord * spriteSize[MOVE].x, yCoord * spriteSize[MOVE].y, spriteSize[MOVE].x, spriteSize[MOVE].y);
		}
		else
		{
			int SpriteWidth = sprite_image[DEATH].GetWidth() / spriteSize[DEATH].x;
			int SpriteHeight = sprite_image[DEATH].GetHeight() / spriteSize[DEATH].y;

			int xCoord = spriteCurrentIndex % SpriteWidth;
			int yCoord = spriteCurrentIndex / SpriteWidth;

			sprite_image[DEATH].Draw(_hdc, position.x, position.y, realSize.x, realSize.y, xCoord * spriteSize[DEATH].x, yCoord * spriteSize[DEATH].y, spriteSize[DEATH].x, spriteSize[DEATH].y);
		}
	}
};

Monster g_Monster[50];

struct Obstacle {
	bool isVisible = true;
	int Life = 3;

	CImage sprite_image; // 이미지
	Point spriteSize;
	int spriteEntireCount;
	int spriteCurrentIndex;

	Point position; // 위치
	int direction; // 방향
	RECT boundingBox; // 충돌처리
	Point realSize; // 실제 게임에서 사용할 크기

	void Init()
	{
		sprite_image.Load(TEXT("./Sprite/obstacle.png"));

		spriteSize.x = 445; // 읽을 너비
		spriteSize.y = 425; // 읽을 높이

		spriteEntireCount = 1;
		spriteCurrentIndex = 0;

		realSize.x = 200;
		realSize.y = 200;

		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;
	}

	bool CheckCollision(Bullet* bullet)
	{
		for (int i = 0; i < 100; i++)
		{
			if (bullet[i].isVisible)
			{
				if (bullet[i].boundingBox.right < boundingBox.left)
				{
					return false;
				}
				if (bullet[i].boundingBox.bottom < boundingBox.top)
				{
					return false;
				}
				if (bullet[i].boundingBox.left > boundingBox.right)
				{
					return false;
				}
				if (bullet[i].boundingBox.top > boundingBox.bottom)
				{
					return false;
				}

				bullet[i].Life = 0;
				bullet[i].isVisible = false;
				return true;

				/*if ((bullet[i].boundingBox.left >= boundingBox.left) &&
					(bullet[i].boundingBox.right <= boundingBox.right) &&
					(bullet[i].boundingBox.bottom <= boundingBox.bottom) &&
					(bullet[i].boundingBox.top >= boundingBox.top))
				{
					bullet[i].Life = 0;
					bullet[i].isVisible = false;

					return true;
				}*/
			}
		}

		// return false;
	}

	void update()
	{
		if (CheckCollision(g_Bullet) == true)
		{
			Life -= 1;
		}

		if (Life <= 0)
		{
			isVisible = false;
		}

		(++spriteCurrentIndex) %= spriteEntireCount;

		// 바운딩박스 업데이트
		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;
	}

	void render(HDC _hdc, HWND _hWnd)
	{
		int SpriteWidth = sprite_image.GetWidth() / spriteSize.x;
		int SpriteHeight = sprite_image.GetHeight() / spriteSize.y;

		int xCoord = spriteCurrentIndex % SpriteWidth;
		int yCoord = spriteCurrentIndex / SpriteWidth;

		sprite_image.Draw(_hdc, position.x, position.y, realSize.x, realSize.y, xCoord * spriteSize.x, yCoord * spriteSize.y, spriteSize.x, spriteSize.y);
	}
};

Obstacle g_Obstacle[3];

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 5-6";

int BulletIndex = 0;
int MonsterIndex = 0;
float g_Time = 0.0f;

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static int startX, startY;
	static bool Drag;
	static int endX, endY;

	static HBITMAP hBitmap, hOldBitmap;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);

		Drag = false;
		BulletIndex = 0;
		MonsterIndex = 0;
		g_Time = 0.0f;

		g_Hero.Init();

		for (int i = 0; i < 50; i++)
		{
			g_Monster[i].Init();
		}

		for (int i = 0; i < 100; i++)
		{
			g_Bullet[i].Init();
		}

		for (int i = 0; i < 3; i++)
		{
			g_Obstacle[i].Init();
		}

		g_Obstacle[0].position.x = 50;
		g_Obstacle[0].position.y = 50;

		g_Obstacle[1].position.x = 400;
		g_Obstacle[1].position.y = 300;

		g_Obstacle[2].position.x = 800;
		g_Obstacle[2].position.y = 0;

		SetTimer(hWnd, 0, 150, NULL);
		SetTimer(hWnd, 1, 150, NULL);
	}
	break;
	case WM_TIMER:
	{
		switch (wParam)
		{
		case 0:
		{
			g_Hero.update();

			for (int i = 0; i < BulletIndex; i++)
			{
				if (g_Bullet[i].isVisible)
				{
					g_Bullet[i].update();
				}
			}

			for (int i = 0; i < 3; i++)
			{
				if (g_Obstacle[i].isVisible)
				{
					g_Obstacle[i].update();
				}
			}

			for (int i = 0; i < MonsterIndex; i++)
			{
				if (g_Monster[i].isVisible)
				{
					g_Monster[i].update();
				}
			}
		}
		break;
		case 1:
		{
			g_Time += 0.01f;

			if (g_Time > 0.1f)
			{
				g_Monster[MonsterIndex].isVisible = true;

				Point randomPos[3] = { {50, 50}, {400, 300}, {800, 0} };
				int radomIndex = rand() % 3;
				g_Monster[MonsterIndex].position = randomPos[radomIndex];
				g_Monster[MonsterIndex].direction = rand() % 4;

				MonsterIndex++;

				g_Time = 0.0f;
			}
		}
		break;
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);

		Rectangle(memdc, 0, 0, WINDOW_X, WINDOW_Y);














		for (int i = 0; i < 3; i++)
		{
			if (g_Obstacle[i].isVisible)
			{
				g_Obstacle[i].render(memdc, hWnd);
			}
		}

		for (int i = 0; i < BulletIndex; i++)
		{
			if (g_Bullet[i].isVisible)
			{
				g_Bullet[i].render(memdc, hWnd);
			}
		}

		for (int i = 0; i < MonsterIndex; i++)
		{
			if (g_Monster[i].isVisible)
			{
				if (g_Monster[i].Life)
				{
					g_Monster[i].render(memdc, hWnd, MOVE);
				}
				else
				{
					g_Monster[i].render(memdc, hWnd, DEATH);
				}
			}
		}

		g_Hero.render(memdc, hWnd);

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
	{
		switch (wParam)
		{
		case 'q': case 'Q':
		{
			PostQuitMessage(0);
		}
		break;
		}
	}
	break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_UP:
		{
			g_Hero.direction = UP;
		}
		break;
		case VK_DOWN:
		{
			g_Hero.direction = DOWN;
		}
		break;
		case VK_LEFT:
		{
			g_Hero.direction = LEFT;
		}
		break;
		case VK_RIGHT:
		{
			g_Hero.direction = RIGHT;
		}
		break;
		case VK_SPACE:
		{
			// 여기서 총을 발사
			g_Bullet[BulletIndex].isVisible = true;
			g_Bullet[BulletIndex].position.x = g_Hero.position.x + (g_Hero.realSize.x / 2);
			g_Bullet[BulletIndex].position.y = g_Hero.position.y + (g_Hero.realSize.y / 2);
			g_Bullet[BulletIndex].direction = g_Hero.direction;

			if (BulletIndex < 100)
				BulletIndex++;

			SetTimer(hWnd, 1, 150, NULL);
		}
		break;
		}
	}
	break;
	case WM_RBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}