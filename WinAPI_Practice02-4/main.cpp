#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 2-4";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 800
#define WINDOW_Y 800

#define RECT_X 20
#define RECT_Y 20

#define KIND_BLANK 0
#define KIND_PLAYER1 1
#define KIND_PLAYER2 2
#define KIND_DANGER_PLAYER1 3
#define KIND_DANGER_PLAYER2 4
#define KIND_DANGERZONE -1

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

typedef struct _Coordi {
	int x = 0;
	int y = 0;
} Coordi;

typedef struct _Point {
	RECT rect;
	int center_x = (rect.left + rect.right) / 2;
	int center_y = (rect.top + rect.bottom) / 2;

	int kind;
	int block_kind;

	HBRUSH player_brush;
	COLORREF m_Color;
	Coordi coordinate;

	_Point(int _kind) : kind(_kind) {}
	_Point() {}
	_Point& operator=(const _Point& other)
	{
		coordinate.x = other.coordinate.x;
		coordinate.y = other.coordinate.y;

		return *this;
	}
} Point;

class CProjectManager
{
private:
	HDC m_Hdc;
	HBRUSH m_Hbrush;
	HBRUSH m_Oldbrush;

	COLORREF m_PinkColor = RGB(255, 51, 153); // 가운데 다크존

	COLORREF m_RedColor = RGB(255, 0, 0); // 플레이어1
	COLORREF m_BlueColor = RGB(0, 0, 255); // 플레이어2

	Point m_VirtualRect[RECT_X][RECT_Y];
	Point m_Player1;
	Point m_Player2;
	Point numArray[4];
	int player_sequence = 1;

public:
	void MovePlayer(HWND _hwnd, WPARAM _wParam)
	{
		m_Hdc = GetDC(_hwnd);
		switch (_wParam)
		{
		///////////////////////////////////////////////////////////////////////////// 플레이어1 /////////////////////////////////////////////////////////////////////////////
		case 'w': case 'W':
			if (m_Player1.coordinate.x > 0)
			{
				if (player_sequence == 1)
				{
					if (m_VirtualRect[m_Player1.coordinate.x - 1][m_Player1.coordinate.y].kind == KIND_PLAYER2)
						return;

					m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind = KIND_BLANK;
					m_Player1.coordinate.x--;

					if (m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind == KIND_DANGERZONE || 
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind == KIND_DANGER_PLAYER1 || 
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind == KIND_DANGER_PLAYER2)
					{
						MessageBox(_hwnd, "표시구역에 들어왔기 때문에 랜덤위치로 재설정됩니다", "경고", MB_OK);
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind = KIND_DANGER_PLAYER1;
						// 새로운 위치로 이동
						SetPlayerLocation(m_Player1, KIND_DANGER_PLAYER1);
					}
					else
					{
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind = KIND_PLAYER1;
						player_sequence = 2;
					}
				}
				else
				{
					// 자기차례가 아닌데 움직이려고 했던 것임
					MessageBox(_hwnd, "플레이어2의 순서입니다!", "경고", MB_OK);
				}
			}
			break;
		case 'a': case 'A':
			if (m_Player1.coordinate.y > 0)
			{
				if (player_sequence == 1)
				{
					if (m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y - 1].kind == KIND_PLAYER2)
						return;

					m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind = KIND_BLANK;
					m_Player1.coordinate.y--;

					if (m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind == KIND_DANGERZONE ||
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind == KIND_DANGER_PLAYER1 ||
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind == KIND_DANGER_PLAYER2)
					{
						MessageBox(_hwnd, "표시구역에 들어왔기 때문에 랜덤위치로 재설정됩니다", "경고", MB_OK);
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind = KIND_DANGER_PLAYER1;
						// 새로운 위치로 이동
						SetPlayerLocation(m_Player1, KIND_DANGER_PLAYER1);
					}
					else
					{
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind = KIND_PLAYER1;
						player_sequence = 2;
					}
				}
				else
				{
					// 자기차례가 아닌데 움직이려고 했던 것임
					MessageBox(_hwnd, "플레이어2의 순서입니다!", "경고", MB_OK);
				}
			}
			break;
		case 's': case 'S':
			if (m_Player1.coordinate.x < RECT_X - 1)
			{
				if (player_sequence == 1)
				{
					if (m_VirtualRect[m_Player1.coordinate.x + 1][m_Player1.coordinate.y].kind == KIND_PLAYER2)
						return;

					m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind = KIND_BLANK;
					m_Player1.coordinate.x++;

					if (m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind == KIND_DANGERZONE ||
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind == KIND_DANGER_PLAYER1 ||
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind == KIND_DANGER_PLAYER2)
					{
						MessageBox(_hwnd, "표시구역에 들어왔기 때문에 랜덤위치로 재설정됩니다", "경고", MB_OK);
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind = KIND_DANGER_PLAYER1;
						// 새로운 위치로 이동
						SetPlayerLocation(m_Player1, KIND_DANGER_PLAYER1);
					}
					else
					{
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind = KIND_PLAYER1;
						player_sequence = 2;
					}
				}
				else
				{
					// 자기차례가 아닌데 움직이려고 했던 것임
					MessageBox(_hwnd, "플레이어2의 순서입니다!", "경고", MB_OK);
				}
			}
			break;
		case 'd': case 'D':
			if (m_Player1.coordinate.y < RECT_Y - 1)
			{
				if (player_sequence == 1)
				{
					if (m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y + 1].kind == KIND_PLAYER2)
						return;

					m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind = KIND_BLANK;
					m_Player1.coordinate.y++;

					if (m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind == KIND_DANGERZONE ||
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind == KIND_DANGER_PLAYER1 ||
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind == KIND_DANGER_PLAYER2)
					{
						MessageBox(_hwnd, "표시구역에 들어왔기 때문에 랜덤위치로 재설정됩니다", "경고", MB_OK);
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].block_kind = KIND_DANGER_PLAYER1;
						// 새로운 위치로 이동
						SetPlayerLocation(m_Player1, KIND_DANGER_PLAYER1);
					}
					else
					{
						m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].kind = KIND_PLAYER1;
						player_sequence = 2;
					}
				}
				else
				{
					// 자기차례가 아닌데 움직이려고 했던 것임
					MessageBox(_hwnd, "플레이어2의 순서입니다!", "경고", MB_OK);
				}
			}
			break;
		///////////////////////////////////////////////////////////////////////////// 플레이어2 /////////////////////////////////////////////////////////////////////////////
		case 'i': case 'I':
			if (m_Player2.coordinate.x > 0)
			{
				if (player_sequence == 2)
				{
					if (m_VirtualRect[m_Player2.coordinate.x - 1][m_Player2.coordinate.y].kind == KIND_PLAYER1)
						return;

					m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind = KIND_BLANK;
					m_Player2.coordinate.x--;

					if (m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind == KIND_DANGERZONE ||
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind == KIND_DANGER_PLAYER1 || 
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind == KIND_DANGER_PLAYER2)
					{
						MessageBox(_hwnd, "표시구역에 들어왔기 때문에 랜덤위치로 재설정됩니다", "경고", MB_OK);
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind = KIND_DANGER_PLAYER2;
						// 새로운 위치로 이동
						SetPlayerLocation(m_Player2, KIND_DANGER_PLAYER2);
					}
					else
					{
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind = KIND_PLAYER2;
						player_sequence = 1;
					}	
				}
				else
				{
					// 자기차례가 아닌데 움직이려고 했던 것임
					MessageBox(_hwnd, "플레이어1의 순서입니다!", "경고", MB_OK);
				}
			}
			break;
		case 'j': case 'J':
			if (m_Player2.coordinate.y > 0)
			{
				if (player_sequence == 2)
				{
					if (m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y - 1].kind == KIND_PLAYER1)
						return;

					m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind = KIND_BLANK;
					m_Player2.coordinate.y--;

					if (m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind == KIND_DANGERZONE ||
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind == KIND_DANGER_PLAYER1 ||
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind == KIND_DANGER_PLAYER2)
					{
						MessageBox(_hwnd, "표시구역에 들어왔기 때문에 랜덤위치로 재설정됩니다", "경고", MB_OK);
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind = KIND_DANGER_PLAYER2;
						// 새로운 위치로 이동
						SetPlayerLocation(m_Player2, KIND_DANGER_PLAYER2);
					}
					else
					{
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind = KIND_PLAYER2;
						player_sequence = 1;
					}
				}
				else
				{
					// 자기차례가 아닌데 움직이려고 했던 것임
					MessageBox(_hwnd, "플레이어1의 순서입니다!", "경고", MB_OK);
				}
			}
			break;
		case 'k': case 'K':
			if (m_Player2.coordinate.x < RECT_X - 1)
			{
				if (player_sequence == 2)
				{
					if (m_VirtualRect[m_Player2.coordinate.x + 1][m_Player2.coordinate.y].kind == KIND_PLAYER1)
						return;

					m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind = KIND_BLANK;
					m_Player2.coordinate.x++;

					if (m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind == KIND_DANGERZONE ||
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind == KIND_DANGER_PLAYER1 ||
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind == KIND_DANGER_PLAYER2)
					{
						MessageBox(_hwnd, "표시구역에 들어왔기 때문에 랜덤위치로 재설정됩니다", "경고", MB_OK);
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind = KIND_DANGER_PLAYER2;
						// 새로운 위치로 이동
						SetPlayerLocation(m_Player2, KIND_DANGER_PLAYER2);
					}
					else
					{
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind = KIND_PLAYER2;
						player_sequence = 1;
					}
				}
				else
				{
					// 자기차례가 아닌데 움직이려고 했던 것임
					MessageBox(_hwnd, "플레이어1의 순서입니다!", "경고", MB_OK);
				}
			}
			break;
		case 'l': case 'L':
			if (m_Player2.coordinate.y < RECT_Y - 1)
			{
				if (player_sequence == 2)
				{
					if (m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y + 1].kind == KIND_PLAYER1)
						return;

					m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind = KIND_BLANK;
					m_Player2.coordinate.y++;

					if (m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind == KIND_DANGERZONE ||
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind == KIND_DANGER_PLAYER1 ||
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind == KIND_DANGER_PLAYER2)
					{
						MessageBox(_hwnd, "표시구역에 들어왔기 때문에 랜덤위치로 재설정됩니다", "경고", MB_OK);
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].block_kind = KIND_DANGER_PLAYER2;
						// 새로운 위치로 이동
						SetPlayerLocation(m_Player2, KIND_DANGER_PLAYER2);
					}
					else
					{
						m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].kind = KIND_PLAYER2;
						player_sequence = 1;
					}
				}
				else
				{
					// 자기차례가 아닌데 움직이려고 했던 것임
					MessageBox(_hwnd, "플레이어1의 순서입니다!", "경고", MB_OK);
				}
			}
			break;
		}
		ReleaseDC(_hwnd, m_Hdc);
		InvalidateRect(_hwnd, NULL, TRUE);
	}

	void SetPlayerLocation(Point& _player, int _kind)
	{
		m_VirtualRect[_player.coordinate.x][_player.coordinate.y].kind = _kind;

		// 랜덤 좌표 넣어주기(해당 자리에 다른 플레이어가 없을 때까지 랜덤뽑기)

		srand(unsigned int(time(NULL)));

		int random_number = 0;
		Point temp = 0;

		for (int i = 0; i < 4; i++) // 4자리 섞기
		{
			random_number = rand() % 4;
			temp = numArray[i];
			numArray[i] = numArray[random_number];
			numArray[random_number] = temp;
		}

		for (int i = 0; i < 4; i++)
		{
			if (m_VirtualRect[numArray[i].coordinate.x][numArray[i].coordinate.y].kind != KIND_PLAYER1 &&
				m_VirtualRect[numArray[i].coordinate.x][numArray[i].coordinate.y].kind != KIND_PLAYER2)
			{
				_player.coordinate.x = numArray[i].coordinate.x;
				_player.coordinate.y = numArray[i].coordinate.y;
			}
		}

		if(_player.kind == KIND_PLAYER1)
			m_VirtualRect[_player.coordinate.x][_player.coordinate.y].kind = KIND_PLAYER1;
		else if(_player.kind == KIND_PLAYER2)
			m_VirtualRect[_player.coordinate.x][_player.coordinate.y].kind = KIND_PLAYER2;
	}

	void ReStart()
	{
		int width = 30;

		m_Hbrush = CreateSolidBrush(RGB(0, 0, 0));

		m_Player1.kind = KIND_PLAYER1;
		m_Player2.kind = KIND_PLAYER2;

		m_Player1.m_Color = m_RedColor;
		m_Player1.coordinate.x = 0;
		m_Player1.coordinate.y = 0;

		m_Player2.m_Color = m_BlueColor;
		m_Player2.coordinate.x = 19;
		m_Player2.coordinate.y = 19;

		for (int i = 0; i < RECT_X; i++)
		{
			for (int j = 0; j < RECT_Y; j++)
			{
				m_VirtualRect[i][j].rect.left = j * width;
				m_VirtualRect[i][j].rect.top = i * width;
				m_VirtualRect[i][j].rect.right = (j + 1) * width;
				m_VirtualRect[i][j].rect.bottom = (i + 1) * width;

				if ((i > 7 && i < 12) && (j > 7 && j < 12))
				{
					m_VirtualRect[i][j].kind = KIND_DANGERZONE;
					m_VirtualRect[i][j].block_kind = KIND_DANGERZONE;
				}
				else if (i == 0 && j == 0)
					m_VirtualRect[i][j].kind = KIND_PLAYER1;
				else if (i == 19 && j == 19)
					m_VirtualRect[i][j].kind = KIND_PLAYER2;
				else
					m_VirtualRect[i][j].kind = KIND_BLANK;
			}
		}

		SetPlayerLocation(m_Player1, KIND_BLANK);
		SetPlayerLocation(m_Player2, KIND_BLANK);
	}

	void DrawRectangles(HWND _hwnd)
	{
		m_Hdc = GetDC(_hwnd);

		for (int i = 0; i < RECT_X; i++)
		{
			for (int j = 0; j < RECT_Y; j++)
			{
				Rectangle(m_Hdc, m_VirtualRect[i][j].rect.left, m_VirtualRect[i][j].rect.top,
					m_VirtualRect[i][j].rect.right, m_VirtualRect[i][j].rect.bottom);

				switch (m_VirtualRect[i][j].kind)
				{
					case KIND_DANGERZONE:
					{
						m_Hbrush = CreateSolidBrush(m_PinkColor);
						m_Oldbrush = (HBRUSH)SelectObject(m_Hdc, m_Hbrush);
						Rectangle(m_Hdc, m_VirtualRect[i][j].rect.left, m_VirtualRect[i][j].rect.top,
							m_VirtualRect[i][j].rect.right, m_VirtualRect[i][j].rect.bottom);
						SelectObject(m_Hdc, m_Oldbrush);
						DeleteObject(m_Hbrush);
					}
					break;
					case KIND_PLAYER1:
					{
						m_Hbrush = CreateSolidBrush(m_Player1.m_Color);
						m_Oldbrush = (HBRUSH)SelectObject(m_Hdc, m_Hbrush);
						Ellipse(m_Hdc, m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].rect.left, m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].rect.top,
							m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].rect.right, m_VirtualRect[m_Player1.coordinate.x][m_Player1.coordinate.y].rect.bottom);
						SelectObject(m_Hdc, m_Oldbrush);
						DeleteObject(m_Hbrush);
					}
					break;
					case KIND_PLAYER2:
					{
						m_Hbrush = CreateSolidBrush(m_Player2.m_Color);
						m_Oldbrush = (HBRUSH)SelectObject(m_Hdc, m_Hbrush);
						Ellipse(m_Hdc, m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].rect.left, m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].rect.top,
							m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].rect.right, m_VirtualRect[m_Player2.coordinate.x][m_Player2.coordinate.y].rect.bottom);
						SelectObject(m_Hdc, m_Oldbrush);
						DeleteObject(m_Hbrush);
					}
					break;
				}

				switch (m_VirtualRect[i][j].block_kind)
				{
				case KIND_DANGER_PLAYER1:
					m_Hbrush = CreateSolidBrush(m_PinkColor);
					m_Oldbrush = (HBRUSH)SelectObject(m_Hdc, m_Hbrush);
					Rectangle(m_Hdc, m_VirtualRect[i][j].rect.left, m_VirtualRect[i][j].rect.top,
						m_VirtualRect[i][j].rect.right, m_VirtualRect[i][j].rect.bottom);
					SelectObject(m_Hdc, m_Oldbrush);
					DeleteObject(m_Hbrush);
					m_Hbrush = CreateSolidBrush(m_Player1.m_Color);
					m_Oldbrush = (HBRUSH)SelectObject(m_Hdc, m_Hbrush);
					Ellipse(m_Hdc, m_VirtualRect[i][j].rect.left, m_VirtualRect[i][j].rect.top,
						m_VirtualRect[i][j].rect.right, m_VirtualRect[i][j].rect.bottom);
					SelectObject(m_Hdc, m_Oldbrush);
					DeleteObject(m_Hbrush);
					break;
				case KIND_DANGER_PLAYER2:
					m_Hbrush = CreateSolidBrush(m_PinkColor);
					m_Oldbrush = (HBRUSH)SelectObject(m_Hdc, m_Hbrush);
					Rectangle(m_Hdc, m_VirtualRect[i][j].rect.left, m_VirtualRect[i][j].rect.top,
						m_VirtualRect[i][j].rect.right, m_VirtualRect[i][j].rect.bottom);
					SelectObject(m_Hdc, m_Oldbrush);
					DeleteObject(m_Hbrush);
					m_Hbrush = CreateSolidBrush(m_Player2.m_Color);
					m_Oldbrush = (HBRUSH)SelectObject(m_Hdc, m_Hbrush);
					Ellipse(m_Hdc, m_VirtualRect[i][j].rect.left, m_VirtualRect[i][j].rect.top,
						m_VirtualRect[i][j].rect.right, m_VirtualRect[i][j].rect.bottom);
					SelectObject(m_Hdc, m_Oldbrush);
					DeleteObject(m_Hbrush);
					break;
				}
			}
		}
		ReleaseDC(_hwnd, m_Hdc);
	}

	CProjectManager() : m_Player1(KIND_PLAYER1), m_Player2(KIND_PLAYER2)
	{
		int width = 30;

		m_Hbrush = CreateSolidBrush(RGB(0, 0, 0));

		m_Player1.m_Color = m_RedColor;
		m_Player1.coordinate.x = 0;
		m_Player1.coordinate.y = 0;

		m_Player2.m_Color = m_BlueColor;
		m_Player2.coordinate.x = 19;
		m_Player2.coordinate.y = 19;

		for (int i = 0; i < RECT_X; i++)
		{
			for (int j = 0; j < RECT_Y; j++)
			{
				m_VirtualRect[i][j].rect.left = j * width;
				m_VirtualRect[i][j].rect.top = i * width;
				m_VirtualRect[i][j].rect.right = (j + 1) * width;
				m_VirtualRect[i][j].rect.bottom = (i + 1) * width;

				if ((i > 7 && i < 12) && (j > 7 && j < 12))
					m_VirtualRect[i][j].kind = KIND_DANGERZONE;
				else if (i == 0 && j == 0)
					m_VirtualRect[i][j].kind = KIND_PLAYER1;
				else if (i == 19 && j == 19)
					m_VirtualRect[i][j].kind = KIND_PLAYER2;
				else
					m_VirtualRect[i][j].kind = KIND_BLANK;
			}
		}

		numArray[0].coordinate.x = 0;
		numArray[0].coordinate.y = 0;

		numArray[1].coordinate.x = 0;
		numArray[1].coordinate.y = RECT_Y - 1;

		numArray[2].coordinate.x = RECT_X - 1;
		numArray[2].coordinate.y = 0;

		numArray[3].coordinate.x = RECT_X - 1;
		numArray[3].coordinate.y = RECT_Y - 1;
	}
};

CProjectManager g_Manager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		g_Manager.DrawRectangles(hWnd);
		EndPaint(hWnd, &ps);
		break;
	case WM_CHAR:
		if (wParam == 'q' || wParam == 'Q')
			exit(0);
		else if (wParam == 'r' || wParam == 'R')
			g_Manager.ReStart();
		else
			g_Manager.MovePlayer(hWnd, wParam);
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case WM_KEYDOWN:
		hdc = GetDC(hWnd);
		ReleaseDC(hWnd, hdc);
		break;
	case WM_TIMER:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}