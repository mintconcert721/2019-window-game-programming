#include <Windows.h>
#include <math.h>
#include <iostream>
#include "resource.h"

using namespace std;

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

#define WINDOW_X 1600
#define WINDOW_Y 800

#define CHILD_WINDOW_X (500 - 6)
#define CHILD_WINDOW_Y (600 - 6)

#define FRAME_X 135
#define FRAME_Y 150

#define IDC_LEFT 1001
#define IDC_RIGHT 1002
#define IDC_EDIT 1003
#define IDC_SELECT 1004
#define IDC_MOVE 1005
#define IDC_COMPLETE 1006
#define IDC_STOP 1007
#define IDC_PAINTPAN 1008
#define IDC_LIST 1009

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "ParentClass";
LPCTSTR lpszChildClass = "ChildClass";
LPCTSTR lpszButtonClass = "button";
LPCTSTR lpszEditClass = "edit";
LPCTSTR lpszListBoxClass = "listbox";
LPCTSTR lpszWindowName = "Window Program 7-3 : 차일드 윈도우와 컨트롤을 이용하여 연결된 그림 만들기";

struct INFO {
	HBITMAP hBitmap;
	POINT position;
};

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

INFO hImageGroup[10];
HBITMAP hBitmapSource[10];
int ImageIndex = 0;
int EditIndex = 0;
int moveX = 0;
int moveY = 0;
int speed = 0;
char str[256];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	// 차일드 윈도우1 클래스 등록
	WndClass.hCursor = LoadCursor(NULL, IDC_HELP);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	WndClass.lpszClassName = lpszChildClass;
	WndClass.lpfnWndProc = (WNDPROC)ChildWndProc;
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc, memdc2;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static HBITMAP hBitmap, hOldBitmap;
	HWND hWndChild;

	RECT childrt = { 8, 31, CHILD_WINDOW_X, CHILD_WINDOW_Y };

	static HWND hButtonLeft, hButtonRight;
	static HWND hEdit, hButtonPaintpan, hListBox;
	static HWND hButtonMove, hButtonSelect, hButtonComplete, hButtonStop;

	static HFONT hFont;

	static char Items[][256] = { "고양이", "롤-유미", "롤-조이", "롤-진", "마영전-이비", "마영전-카이", "사이퍼즈-마틴", "사이퍼즈-캐럴", "연예인-정채연", "정해인정채연" };
	// 고양이 롤-유미 롤-조이 롤-진 마영전-이비 마영전-카이 사이퍼즈-마틴 사이퍼즈-캐럴 연예인-정채연 정해인정채연

	static BITMAP bmp;
	int width, height;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);
		
		hButtonPaintpan = CreateWindow(lpszChildClass, NULL, WS_CHILD | WS_VISIBLE,
			200, 150, 500, 600, hWnd, (HMENU)IDC_PAINTPAN, g_hInstance, NULL);

		// TODO:
		hFont = CreateFont(45, 0, 0, 0, 800, 0, 0, 0, HANGEUL_CHARSET, 0, 0, 0, 0, TEXT("맑은 고딕"));

		hButtonLeft = CreateWindow(lpszButtonClass, "◀", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			50,
			WINDOW_Y / 2,
			100, 50, hWnd, (HMENU)IDC_LEFT, g_hInstance, NULL);
		SendMessage(hButtonLeft, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButtonRight = CreateWindow(lpszButtonClass, "▶", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			200 + (childrt.right - childrt.left) + 50,
			WINDOW_Y / 2,
			100, 50, hWnd, (HMENU)IDC_RIGHT, g_hInstance, NULL);
		SendMessage(hButtonRight, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hEdit = CreateWindow(lpszEditClass, "", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | ES_CENTER | ES_READONLY,
			200 + 5,
			50,
			(childrt.right - childrt.left), 50, hWnd, (HMENU)IDC_EDIT, g_hInstance, NULL);
		SendMessage(hEdit, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);
		
		hListBox = CreateWindow(lpszListBoxClass, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | LBS_STANDARD | LBS_SORT,
			200 + (childrt.right - childrt.left) + 200 + 100,
			50,
			400, 400, hWnd, (HMENU)IDC_LIST, g_hInstance, NULL);
		SendMessage(hEdit, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButtonMove = CreateWindow(lpszButtonClass, "이동", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			200 + (childrt.right - childrt.left) + 200 + 100 + 50,
			WINDOW_Y / 2 + 100,
			100, 50, hWnd, (HMENU)IDC_MOVE, g_hInstance, NULL);
		SendMessage(hButtonMove, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButtonSelect = CreateWindow(lpszButtonClass, "선택", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			200 + (childrt.right - childrt.left) + 200 + 100 + 50 + 100 + 50 + 50,
			WINDOW_Y / 2 + 100,
			100, 50, hWnd, (HMENU)IDC_SELECT, g_hInstance, NULL);
		SendMessage(hButtonSelect, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButtonComplete = CreateWindow(lpszButtonClass, "완성", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			200 + (childrt.right - childrt.left) + 200 + 100 + 50,
			WINDOW_Y / 2 + 200,
			100, 50, hWnd, (HMENU)IDC_COMPLETE, g_hInstance, NULL);
		SendMessage(hButtonComplete, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hButtonStop = CreateWindow(lpszButtonClass, "멈춤", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER | BS_CENTER | BS_VCENTER,
			200 + (childrt.right - childrt.left) + 200 + 100 + 50 + 100 + 50 + 50,
			WINDOW_Y / 2 + 200,
			100, 50, hWnd, (HMENU)IDC_STOP, g_hInstance, NULL);
		SendMessage(hButtonStop, WM_SETFONT, (WPARAM)hFont, (LPARAM)FALSE);

		hBitmapSource[0] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP1));
		hBitmapSource[1] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP2));
		hBitmapSource[2] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP3));
		hBitmapSource[3] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP4));
		hBitmapSource[4] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP5));
		hBitmapSource[5] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP6));
		hBitmapSource[6] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP7));
		hBitmapSource[7] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP8));
		hBitmapSource[8] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP9));
		hBitmapSource[9] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP10));

		for (int i = 0; i < 10; i++)
		{
			SendMessage(hListBox, LB_ADDSTRING, 0, (LPARAM)Items[i]);
		}
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_LEFT:
		{
			switch (HIWORD(wParam))
			{
			case BN_CLICKED:
			{
				if (EditIndex > 0)
				{
					EditIndex--;
					sprintf_s(str, "%d", EditIndex);
					SendMessage(hEdit, WM_SETTEXT, 0, (LPARAM)str);
					/*GetDlgItem(hButtonPaintpan, IDC_PAINTPAN);
					SendMessage(hButtonPaintpan, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hImageGroup[ImageIndex]);*/
				}
			}
			break;
			}
		}
		break;
		case IDC_RIGHT:
		{
			switch (HIWORD(wParam))
			{
			case BN_CLICKED:
			{
				if (EditIndex < 9)
				{
					EditIndex++;
					sprintf_s(str, "%d", EditIndex);
					SendMessage(hEdit, WM_SETTEXT, 0, (LPARAM)str);
					/*GetDlgItem(hButtonPaintpan, IDC_PAINTPAN);
					SendMessage(hButtonPaintpan, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hImageGroup[9]);*/
				}
			}
			break;
			}
		}
		break;
		case IDC_LIST:
		{

			switch (HIWORD(wParam))
			{
			case LBN_SELCHANGE:
			{
				ImageIndex = SendMessage(hListBox, LB_GETCURSEL, 0, 0);
			}
			break;
			}
		}
		break;
		case IDC_SELECT:
		{
			switch (HIWORD(wParam))
			{
			case BN_CLICKED:
			{
				hImageGroup[EditIndex].hBitmap = hBitmapSource[ImageIndex];
				hImageGroup[EditIndex].position.x = 500 * EditIndex;
				/*GetDlgItem(hButtonPaintpan, IDC_PAINTPAN);
				SendMessage(hButtonPaintpan, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hImageGroup[ImageIndex]);*/
			}
			break;
			}
		}
		break;
		case IDC_COMPLETE:
		{
			EnableWindow(hButtonSelect, false);
		}
		break;
		case IDC_MOVE:
		{
			speed = 1;
		}
		break;
		case IDC_STOP:
		{
			speed = 0;
		}
		break;
		}
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);
		FillRect(memdc, &crt, (HBRUSH)GetStockObject(WHITE_BRUSH)); //도화지 색 변경

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc, bitdc;
	static RECT c1crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static HBITMAP hChildBitmap, hChildOldBitmap;

	static HBRUSH hBrush;
	static HBRUSH hOldBrush;

	static int ImageHeight;
	static int ImageWidth;

	static int Image2Height;
	static int Image2Width;

	switch (uMsg)
	{
	case WM_CREATE:
	{
		moveX = 0;
		moveY = 0;
		GetClientRect(hWnd, &c1crt);
		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_TIMER:
	{
		moveX -= speed;

		/*for (int i = 0; i < 10; i++)
		{
			hImageGroup[i].position.x -= speed;
		}*/

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		bitdc = CreateCompatibleDC(memdc);
		hChildBitmap = CreateCompatibleBitmap(hdc, c1crt.right, c1crt.bottom);
		hChildOldBitmap = (HBITMAP)SelectObject(memdc, hChildBitmap);
		FillRect(memdc, &c1crt, (HBRUSH)GetStockObject(GRAY_BRUSH)); //도화지 색 변경

		BITMAP bmp;
		GetObject(hImageGroup[EditIndex].hBitmap, sizeof(BITMAP), &bmp);
		ImageWidth = bmp.bmWidth;
		ImageHeight = bmp.bmHeight;

		hChildOldBitmap = (HBITMAP)SelectObject(bitdc, hImageGroup[EditIndex].hBitmap);

		StretchBlt(memdc, moveX, moveY, c1crt.right, c1crt.bottom, bitdc, 0, 0, ImageWidth, ImageHeight, SRCCOPY);
		SelectObject(bitdc, hChildOldBitmap);

		/*if (EditIndex > 1)
		{
			for (int i = 0; i < 10; i++)
			{
				if (hImageGroup[i].position.x < 500)
				{
					BITMAP bmp2;
					GetObject(hImageGroup[i].hBitmap, sizeof(BITMAP), &bmp2);
					Image2Width = bmp2.bmWidth;
					Image2Height = bmp2.bmHeight;

					StretchBlt(memdc, hImageGroup[i].position.x, moveY, c1crt.right - hImageGroup[i].position.x, c1crt.bottom, bitdc, 0, 0, Image2Width, Image2Height, SRCCOPY);
					SelectObject(bitdc, hChildOldBitmap);
				}
			}
		}*/
		

		BitBlt(hdc, 0, 0, c1crt.right, c1crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hChildOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}