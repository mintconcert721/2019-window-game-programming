#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include "resource.h"

TCHAR str[100], lpstrFile[100] = "";

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 8-3 : 그래픽 데이터 파일에 저장하기";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 800
#define WINDOW_Y 800

#define KIND_CIRCLE 1
#define KIND_TRIANGLE 2
#define KIND_RECTANGLE 3

#define ARRAY_SIZE 5

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

OPENFILENAME OFN;

struct Point {
	RECT rect;
	int kind = 0;
	COLORREF Color;
	HPEN Hpen;
	HPEN Oldpen;
	HBRUSH Hbrush;
	HBRUSH Oldbrush;
	int scale;

	Point& operator=(const Point& other) // 대입 연산자 오버로딩
	{
		rect = other.rect;
		kind = other.kind;
		Color = other.Color;

		Hbrush = other.Hbrush;
		Oldbrush = other.Oldbrush;

		Hpen = other.Hpen;
		Oldpen = other.Oldpen;
		scale = other.scale;

		return *this;
	}
};

class CProjectManager
{
private:
	// OPENFILENAME SFN;
	COLORREF m_SelectedColor = RGB(255, 0, 0);
	Point m_FigureArray[ARRAY_SIZE];
	int m_FigureArrayIndex = 0;
	int m_AddingArrayIndex = 0;
	int m_BoardWidthNumber = 40;
	int m_BoardHeightNumber = 40;
	int m_Width = 0;
	int m_Height = 0;
	bool IsSelectedAll = false;
	FILE* fp;

private:
	bool CheckOverlap(Point* _array, int _left, int _top)
	{
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			if ((m_FigureArray[i].rect.left == _left) && (m_FigureArray[i].rect.top == _top))
			{
				return false;
			}
		}

		return true;
	}

public:
	void FileSave(HWND _hWnd)
	{
		fopen_s(&fp, "data.txt", "w");
		
		if (fp == NULL)
		{
			cout << "FILE OPEN ERROR!" << endl;
			exit(0);
		}

		int count = 0;

		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			if (m_FigureArray[i].kind == KIND_CIRCLE || m_FigureArray[i].kind == KIND_TRIANGLE || m_FigureArray[i].kind == KIND_RECTANGLE)
			{
				count++;
			}
		}

		fprintf_s(fp, "%d\n", count);

		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			fprintf_s(fp, "%d %d %d %d %d %d %d\n",
				m_FigureArray[i].kind, m_FigureArray[i].rect.top, m_FigureArray[i].rect.left, m_FigureArray[i].rect.bottom, m_FigureArray[i].rect.right, m_FigureArray[i].scale, m_FigureArray[i].Color);
		}

		fclose(fp);

		MessageBox(_hWnd, "파일을 저장하였습니다.", "저장하기", MB_OK);
		/*memset(&OFN, 0, sizeof(OPENFILENAME));
		SFN.lStructSize = sizeof(OPENFILENAME);
		SFN.hwndOwner = _hWnd;
		SFN.lpstrFilter = TEXT("텍스트 파일(*.txt)\0*.txt\0모든 파일(*.*)\0*.*\0");
		SFN.lpstrFile = lpstrFile;
		SFN.nMaxFile = 256;
		SFN.lpstrInitialDir = ".";
		if (GetSaveFileName(&SFN) != 0) {
			wsprintf(str, "%s 파일에 저장하시겠습니까?", SFN.lpstrFile);
			MessageBox(_hWnd, str, "저장하기 선택", MB_OK);
		}
		else
		{
			MessageBox(_hWnd, "저장하기를 취소하셨습니다.", "실패", MB_OK);
		}*/
	}

	void FileLoad(HWND _hWnd, LPTSTR name)
	{
		fopen_s(&fp, name, "r");

		if (fp == NULL)
		{
			cout << "FILE OPEN ERROR!" << endl;
			exit(0);
		}

		int count = 0;

		fscanf_s(fp, "%d\n", &count);

		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			fscanf_s(fp, "%d %d %d %d %d %d %d\n",
				&m_FigureArray[i].kind, &m_FigureArray[i].rect.top, &m_FigureArray[i].rect.left, &m_FigureArray[i].rect.bottom, &m_FigureArray[i].rect.right, &m_FigureArray[i].scale, &m_FigureArray[i].Color);
			/*fprintf_s(fp, "종류 : %d 좌표 : %d %d %d %d 크기 : %d 색상 : %d\n",
				m_FigureArray[i].kind, m_FigureArray[i].rect.top, m_FigureArray[i].rect.left, m_FigureArray[i].rect.bottom, m_FigureArray[i].rect.right, m_FigureArray[i].scale, m_FigureArray[i].Color);*/
		}

		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			printf_s("%d %d %d %d %d %d %d\n",
				m_FigureArray[i].kind, m_FigureArray[i].rect.top, m_FigureArray[i].rect.left, m_FigureArray[i].rect.bottom, m_FigureArray[i].rect.right, m_FigureArray[i].scale, m_FigureArray[i].Color);
		}

		fclose(fp);
	}

	void MoveFigure(HWND _hWnd, WPARAM _wParam)
	{
		switch (_wParam)
		{
		case VK_UP:
			if (m_FigureArray[m_FigureArrayIndex].rect.top > 0)
			{
				m_FigureArray[m_FigureArrayIndex].rect.top -= 1;
				m_FigureArray[m_FigureArrayIndex].rect.bottom -= 1;
			}
			break;
		case VK_DOWN:
			if (m_FigureArray[m_FigureArrayIndex].rect.bottom < m_BoardHeightNumber - 1)
			{
				m_FigureArray[m_FigureArrayIndex].rect.top += 1;
				m_FigureArray[m_FigureArrayIndex].rect.bottom += 1;
			}
			break;
		case VK_LEFT:
			if (m_FigureArray[m_FigureArrayIndex].rect.left > 0)
			{
				m_FigureArray[m_FigureArrayIndex].rect.left -= 1;
				m_FigureArray[m_FigureArrayIndex].rect.right -= 1;
			}
			break;
		case VK_RIGHT:
			if (m_FigureArray[m_FigureArrayIndex].rect.right < m_BoardWidthNumber - 1)
			{
				m_FigureArray[m_FigureArrayIndex].rect.left += 1;
				m_FigureArray[m_FigureArrayIndex].rect.right += 1;
			}
			break;
		}

		InvalidateRect(_hWnd, NULL, FALSE);
	}

	void KeyboardInput(HWND _hWnd, WPARAM _wParam)
	{
		m_Width = WINDOW_X / m_BoardWidthNumber;
		m_Height = WINDOW_Y / m_BoardHeightNumber;

		switch (_wParam)
		{
		case 's': case 'S': // Small : 작은
			m_BoardWidthNumber = 30;
			m_BoardHeightNumber = 30;
			break;
		case 'm': case 'M': // Medium : 중간
			m_BoardWidthNumber = 40;
			m_BoardHeightNumber = 40;
			break;
		case 'b': case 'B': // Big : 큰
			m_BoardWidthNumber = 50;
			m_BoardHeightNumber = 50;
			break;
		case 'e': case 'E': // 원
		{
			if (m_AddingArrayIndex == 5)
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					m_FigureArray[i] = m_FigureArray[i + 1];
				}

				m_FigureArray[m_AddingArrayIndex - 1].kind = KIND_CIRCLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex - 1].rect.left = left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.top = top;
						m_FigureArray[m_AddingArrayIndex - 1].rect.right = m_FigureArray[m_AddingArrayIndex - 1].rect.left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.bottom = m_FigureArray[m_AddingArrayIndex - 1].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex - 1].Color = RGB(r, g, b);
			}
			else
			{
				m_FigureArray[m_AddingArrayIndex].kind = KIND_CIRCLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex].rect.left = left;
						m_FigureArray[m_AddingArrayIndex].rect.top = top;
						m_FigureArray[m_AddingArrayIndex].rect.right = m_FigureArray[m_AddingArrayIndex].rect.left;
						m_FigureArray[m_AddingArrayIndex].rect.bottom = m_FigureArray[m_AddingArrayIndex].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex].Color = RGB(r, g, b);
				m_AddingArrayIndex++;
			}
		}
		break;
		case 't': case 'T': // 삼각형
		{
			if (m_AddingArrayIndex == 5)
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					m_FigureArray[i] = m_FigureArray[i + 1];
				}

				m_FigureArray[m_AddingArrayIndex - 1].kind = KIND_TRIANGLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex - 1].rect.left = left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.top = top;
						m_FigureArray[m_AddingArrayIndex - 1].rect.right = m_FigureArray[m_AddingArrayIndex - 1].rect.left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.bottom = m_FigureArray[m_AddingArrayIndex - 1].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex - 1].Color = RGB(r, g, b);
			}
			else
			{
				m_FigureArray[m_AddingArrayIndex].kind = KIND_TRIANGLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex].rect.left = left;
						m_FigureArray[m_AddingArrayIndex].rect.top = top;
						m_FigureArray[m_AddingArrayIndex].rect.right = m_FigureArray[m_AddingArrayIndex].rect.left;
						m_FigureArray[m_AddingArrayIndex].rect.bottom = m_FigureArray[m_AddingArrayIndex].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex].Color = RGB(r, g, b);
				m_AddingArrayIndex++;
			}
		}
		break;
		case 'r': case 'R': // 사각형
		{
			if (m_AddingArrayIndex == 5)
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					m_FigureArray[i] = m_FigureArray[i + 1];
				}

				m_FigureArray[m_AddingArrayIndex - 1].kind = KIND_RECTANGLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex - 1].rect.left = left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.top = top;
						m_FigureArray[m_AddingArrayIndex - 1].rect.right = m_FigureArray[m_AddingArrayIndex - 1].rect.left;
						m_FigureArray[m_AddingArrayIndex - 1].rect.bottom = m_FigureArray[m_AddingArrayIndex - 1].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex - 1].Color = RGB(r, g, b);
			}
			else
			{
				m_FigureArray[m_AddingArrayIndex].kind = KIND_RECTANGLE;

				while (1)
				{
					srand((unsigned int)time(NULL));

					int left = rand() % m_BoardWidthNumber;
					int top = rand() % m_BoardHeightNumber;

					if (CheckOverlap(m_FigureArray, left, top) == true)
					{
						m_FigureArray[m_AddingArrayIndex].rect.left = left;
						m_FigureArray[m_AddingArrayIndex].rect.top = top;
						m_FigureArray[m_AddingArrayIndex].rect.right = m_FigureArray[m_AddingArrayIndex].rect.left;
						m_FigureArray[m_AddingArrayIndex].rect.bottom = m_FigureArray[m_AddingArrayIndex].rect.top;

						break;
					}
				}

				int r, g, b;

				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;

				m_FigureArray[m_AddingArrayIndex].Color = RGB(r, g, b);
				m_AddingArrayIndex++;
			}
		}
		break;
		case '=': // 크게
		{
			if (IsSelectedAll)
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					m_FigureArray[i].scale++;
				}
			}
			else
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					if (m_FigureArrayIndex == i)
					{
						m_FigureArray[i].scale++;
					}
				}
			}
		}
		break;
		case '-': // 작게
		{
			if (IsSelectedAll)
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					m_FigureArray[i].scale--;
				}
			}
			else
			{
				for (int i = 0; i < ARRAY_SIZE; i++)
				{
					if (m_FigureArrayIndex == i)
					{
						m_FigureArray[i].scale--;
					}
				}
			}
		}
		break;
		case '1':
		{
			if (IsSelectedAll) IsSelectedAll = false;
			m_FigureArrayIndex = 0;
		}
		break;
		case '2':
		{
			if (IsSelectedAll) IsSelectedAll = false;
			m_FigureArrayIndex = 1;
		}
		break;
		case '3':
		{
			if (IsSelectedAll) IsSelectedAll = false;
			m_FigureArrayIndex = 2;
		}
		break;
		case '4':
		{
			if (IsSelectedAll) IsSelectedAll = false;
			m_FigureArrayIndex = 3;
		}
		break;
		case '5':
		{
			if (IsSelectedAll) IsSelectedAll = false;
			m_FigureArrayIndex = 4;
		}
		break;
		case '6':
		{
			IsSelectedAll = true;
		}
		break;
		case 'q': case 'Q':
		{
			exit(0);
		}
		break;
		}

		InvalidateRect(_hWnd, NULL, FALSE);
	}

	void Delete()
	{
		if (IsSelectedAll)
		{
			for (int i = 0; i < ARRAY_SIZE; i++)
			{
				m_FigureArray[i].kind = 0;
				m_FigureArray[i].rect.left = 0;
				m_FigureArray[i].rect.right = 0;
				m_FigureArray[i].rect.top = 0;
				m_FigureArray[i].rect.bottom = 0;
				m_FigureArray[i].Color = RGB(255, 255, 255);
				m_FigureArray[i].scale = 0;
			}
			m_FigureArrayIndex = 0;
			m_AddingArrayIndex = 0;
		}
		else
		{
			for (int i = 0; i < ARRAY_SIZE; i++)
			{
				if (m_FigureArrayIndex == i)
				{
					if (i != ARRAY_SIZE - 1)
					{
						int count = 0;

						for (int j = i; j < ARRAY_SIZE - 1; j++)
						{
							m_FigureArray[j] = m_FigureArray[j + 1];
						}

						m_AddingArrayIndex = ARRAY_SIZE - 1;
					}
					else
					{
						m_FigureArray[i].kind = 0;
						m_FigureArray[i].rect.left = 0;
						m_FigureArray[i].rect.right = 0;
						m_FigureArray[i].rect.top = 0;
						m_FigureArray[i].rect.bottom = 0;
						m_FigureArray[i].Color = RGB(255, 255, 255);
						m_FigureArray[i].scale = 0;
						m_AddingArrayIndex = ARRAY_SIZE - 1;
					}
				}
			}
		}

	}

	void Draw(HWND _hWnd, HDC _hdc)
	{
		m_Width = WINDOW_X / m_BoardWidthNumber;
		m_Height = WINDOW_X / m_BoardHeightNumber;

		for (int width = 0; width < m_BoardWidthNumber; width++)
		{
			for (int height = 0; height < m_BoardHeightNumber; height++)
			{
				Rectangle(_hdc, width * m_Width, height * m_Width, width * m_Width + m_Width, height * m_Width + m_Width);
			}
		}

		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			m_FigureArray[i].Hbrush = CreateSolidBrush(m_FigureArray[i].Color);
			m_FigureArray[i].Oldbrush = CreateSolidBrush(m_FigureArray[i].Color);

			m_FigureArray[i].Oldbrush = (HBRUSH)SelectObject(_hdc, m_FigureArray[i].Hbrush);
			if (IsSelectedAll)
			{
				m_FigureArray[i].Hpen = CreatePen(PS_SOLID, 3, m_SelectedColor);
				m_FigureArray[i].Oldpen = CreatePen(PS_SOLID, 3, m_SelectedColor);

				m_FigureArray[i].Oldpen = (HPEN)SelectObject(_hdc, m_FigureArray[i].Hpen);
			}
			else
			{
				if (m_FigureArrayIndex == i)
				{
					m_FigureArray[i].Hpen = CreatePen(PS_SOLID, 3, m_SelectedColor);
					m_FigureArray[i].Oldpen = CreatePen(PS_SOLID, 3, m_SelectedColor);

					m_FigureArray[i].Oldpen = (HPEN)SelectObject(_hdc, m_FigureArray[i].Hpen);
				}
			}

			switch (m_FigureArray[i].kind)
			{
			case KIND_CIRCLE:
			{

				Ellipse(_hdc, m_FigureArray[i].rect.left * m_Width, m_FigureArray[i].rect.top * m_Width,
					m_FigureArray[i].rect.right * m_Width + m_Width + m_FigureArray[i].scale, m_FigureArray[i].rect.bottom * m_Width + m_Width + m_FigureArray[i].scale);
			}
			break;
			case KIND_TRIANGLE:
			{
				POINT point[3] = { {m_FigureArray[i].rect.left * m_Width, m_FigureArray[i].rect.bottom * m_Width + m_Width},
				{m_FigureArray[i].rect.right * m_Width + m_Width + m_FigureArray[i].scale, m_FigureArray[i].rect.bottom * m_Width + m_Width},
				{(m_FigureArray[i].rect.left * m_Width + m_FigureArray[i].rect.right * m_Width + m_Width + m_FigureArray[i].scale) / 2, m_FigureArray[i].rect.top * m_Width - m_FigureArray[i].scale} };
				Polygon(_hdc, point, 3);
			}
			break;
			case KIND_RECTANGLE:
			{
				Rectangle(_hdc, m_FigureArray[i].rect.left * m_Width, m_FigureArray[i].rect.top * m_Width,
					m_FigureArray[i].rect.right * m_Width + m_Width + m_FigureArray[i].scale, m_FigureArray[i].rect.bottom * m_Width + m_Width + m_FigureArray[i].scale);
			}
			break;
			}

			SelectObject(_hdc, m_FigureArray[i].Oldbrush);
			SelectObject(_hdc, m_FigureArray[i].Oldpen);
			DeleteObject(m_FigureArray[i].Hbrush);
			DeleteObject(m_FigureArray[i].Hpen);
		}
	}

	CProjectManager()
	{
	}
};

CProjectManager g_Manager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT ccrt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static HBITMAP hBitmap, hOldBitmap;

	static HBRUSH hBrush;
	static HBRUSH hOldBrush;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		GetClientRect(hWnd, &ccrt);
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, ccrt.right, ccrt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);
		FillRect(memdc, &ccrt, (HBRUSH)GetStockObject(WHITE_BRUSH)); //도화지 색 변경

		g_Manager.Draw(hWnd, memdc);

		BitBlt(hdc, 0, 0, ccrt.right, ccrt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
		break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case ID_FILE_SAVE:
			g_Manager.FileSave(hWnd);
			break;
		case ID_FILE_LOAD:
		{
			memset(&OFN, 0, sizeof(OPENFILENAME)); // 초기화
			OFN.lStructSize = sizeof(OPENFILENAME);
			OFN.hwndOwner = hWnd;
			OFN.lpstrFilter = TEXT("텍스트 파일(*.txt)\0*.txt\0모든 파일(*.*)\0*.*\0");
			OFN.lpstrFile = lpstrFile;
			OFN.nMaxFile = 256;
			OFN.lpstrInitialDir = "."; // 초기 디렉토리
			if (GetOpenFileName(&OFN) != 0)
			{
				g_Manager.FileLoad(hWnd, (OFN.lpstrFile));
			}
			else
			{
				MessageBox(hWnd, "파일을 선택하지 않았습니다.", "실패", MB_OK);
			}

			InvalidateRgn(hWnd, NULL, false);
		}
		break;
		}
	}
	break;
	case WM_CHAR:
		g_Manager.KeyboardInput(hWnd, wParam);
		break;
	case WM_KEYDOWN:
		g_Manager.MoveFigure(hWnd, wParam);
		if (wParam == VK_DELETE)
			g_Manager.Delete();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}