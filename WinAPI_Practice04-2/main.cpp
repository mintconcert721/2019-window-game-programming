#include "CProjectManager.h"

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 4-2";

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);

CProjectManager* g_Manager;
double g_time;
bool isPlaying = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	int mouseX = 0;
	int mouseY = 0;
	static char str[256];

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		g_Manager = new CProjectManager();
		SetTimer(hWnd, 1, 1000 / 60, (TIMERPROC)TimerProc); // 1번 아이디의 타이머가 4초 마다 TimerProc 타이머 함수 실행
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		sprintf_s(str, "색변한 벽돌수 : %d / 파괴된 벽돌수 : %d", g_Manager->GetTouchedBrickNumber(), g_Manager->GetDestroyedBrickNumber());
		TextOut(hdc, 100, 400, str, strlen(str));
		g_Manager->Draw(hWnd);
		EndPaint(hWnd, &ps);
		break;
	case WM_CHAR:
		g_Manager->Keyboard(hWnd, wParam);
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case WM_LBUTTONDOWN:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		if (g_Manager->IsInGround(mouseX, mouseY) == true)
		{
			g_Manager->SetSelectValue(true);
		}
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case WM_LBUTTONUP:
		InvalidateRect(hWnd, NULL, TRUE);
		g_Manager->SetSelectValue(false);
		break;
	case WM_MOUSEMOVE:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		if (g_Manager->GetSelectValue())
		{
			g_Manager->MoveGround(mouseX, mouseY);
			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
	case WM_KEYDOWN:
		break;
	case WM_COMMAND:
		{
			switch (LOWORD(wParam))
			{
			case ID_GAME_START:
				isPlaying = true;
				break;
			case ID_BALL_CIRCLE:
				g_Manager->SetBallKind(false);
				break;
			case ID_2ND_FLOOR:
				g_Manager->SetHeight(2);
				break;
			case ID_3RD_FLOOR:
				g_Manager->SetHeight(3);
				break;
			case ID_4TH_FLOOR:
				g_Manager->SetHeight(4);
				break;
			case ID_BALL_RECT:
				g_Manager->SetBallKind(true);
				break;
			case ID_GROUND_BIG:
				g_Manager->SetBrickSize(150);
				break;
			case ID_GROUND_MEDIUM:
				g_Manager->SetBrickSize(100);
				break;
			case ID_GROUND_SMALL:
				g_Manager->SetBrickSize(50);
				break;
			case ID_SPEED_FAST:
				g_Manager->SetGameSpeed(10);
				break;
			case ID_SPEED_MEDIUM:
				g_Manager->SetGameSpeed(5);
				break;
			case ID_SPEED_SLOW:
				g_Manager->SetGameSpeed(1);
				break;
			case ID_GAME_END:
				isPlaying = false;
				MessageBox(hWnd, "게임이 종료됩니다...", "경고!", MB_OK);
				PostQuitMessage(0);
				break;
			}
			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
	case WM_DESTROY:
		delete g_Manager;
		KillTimer(hWnd, 1);
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
		{
			if(isPlaying)
				g_Manager->Update(hWnd);
		}
		break;
	}

	InvalidateRect(hWnd, NULL, TRUE);
}