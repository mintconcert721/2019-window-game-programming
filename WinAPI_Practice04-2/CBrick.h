#pragma once

#include <math.h>

class CBrick
{
private:
	int m_PositionX;
	int m_PositionY;
	int m_HalfX;
	int m_HalfY;
	int m_Hp;

	int m_Left;
	int m_Right;
	int m_Top;
	int m_Bottom;

	bool m_IsVisible;

public:
	void SetPosition(int _PosX, int _PosY);
	void SetHalf(int _HalfX, int _HalfY);
	void SetHP(int _Hp);
	void SetVisible(bool _value);

	const int GetPositionX() const;
	const int GetPositionY() const;
	const int GetHalfX() const;
	const int GetHalfY() const;
	const int GetHP() const;

	const int GetLeft() const;
	const int GetRight() const;
	const int GetTop() const;
	const int GetBottom() const;
	const bool GetVisible() const;

	void Update(int _move, int _halfx);

public:
	CBrick();
	CBrick(int _PosX, int _PosY, int _HalfX, int _HalfY);
	CBrick(const CBrick& rhs);
	CBrick& operator=(const CBrick& rhs);
	~CBrick();
};

