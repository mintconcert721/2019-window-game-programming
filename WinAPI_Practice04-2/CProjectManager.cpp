#include "CProjectManager.h"


CProjectManager::CProjectManager()
{
	for (int i = 0; i < MAX_OBJECTS_NUMBER; i++) // 초기화
	{
		if (i < 10)
		{
			m_BrickObjects[i].SetPosition(m_BrickHalfSizeX + i * m_BrickHalfSizeX * 2, m_BrickHalfSizeY);
			m_BrickObjects[i].SetHalf(m_BrickHalfSizeX, m_BrickHalfSizeY);
			m_BrickObjects[i].SetVisible(true);
		}
		else if(i >= 10 && i < 20)
		{
			m_BrickObjects[i].SetPosition(m_BrickHalfSizeX + (i - 10) * m_BrickHalfSizeX * 2, m_BrickHalfSizeY + m_BrickHalfSizeY * 2);
			m_BrickObjects[i].SetHalf(m_BrickHalfSizeX, m_BrickHalfSizeY);
			m_BrickObjects[i].SetVisible(true);
		}
		else if (i >= 20 && i < 30)
		{
			m_BrickObjects[i].SetPosition(m_BrickHalfSizeX + (i - 20) * m_BrickHalfSizeX * 2, m_BrickHalfSizeY + m_BrickHalfSizeY * 4);
			m_BrickObjects[i].SetHalf(m_BrickHalfSizeX, m_BrickHalfSizeY);
			m_BrickObjects[i].SetVisible(false);
		}
		else
		{
			m_BrickObjects[i].SetPosition(m_BrickHalfSizeX + (i - 30) * m_BrickHalfSizeX * 2, m_BrickHalfSizeY + m_BrickHalfSizeY * 6);
			m_BrickObjects[i].SetHalf(m_BrickHalfSizeX, m_BrickHalfSizeY);
			m_BrickObjects[i].SetVisible(false);
		}
	}

	m_Ball.SetPositionX(300);
	m_Ball.SetPositionY(300);
	m_Ball.SetRadius(10);
	m_Ball.SetSpeed(m_BallSpeed);

	m_MovingGround.SetPosition(500, 500);
	m_MovingGround.SetHalf(m_MovingGroundHalfSizeX, 20);
}


CProjectManager::~CProjectManager()
{
}


void CProjectManager::MoveGround(int mouseX, int mouseY)
{
	m_MovingGround.SetPosition(mouseX, m_MovingGround.GetPositionY());
}


void CProjectManager::Keyboard(HWND _hWnd, WPARAM _wParam)
{
	m_Hdc = GetDC(_hWnd);

	switch (_wParam)
	{
	case '=': // 공의 이동속도 빠르게
		m_BallSpeed += 1;
		m_Ball.Update(m_BallSpeed);
		break;
	case '-': // 공의 이동속도 느리게
		if (m_BallSpeed > 1)
		{
			m_BallSpeed -= 1;
			m_Ball.Update(m_BallSpeed);
		}
		break;
	}

	ReleaseDC(_hWnd, m_Hdc);
}


void CProjectManager::Draw(HWND _hWnd)
{
	m_Hdc = GetDC(_hWnd);

	m_TouchedBrush = CreateSolidBrush(TouchedColor);
	m_BasicBrush = CreateSolidBrush(BasicColor);
	m_GroundBrush = CreateSolidBrush(GroundColor);
	m_BallBrush = CreateSolidBrush(BallColor);

	for (int i = 0; i < MAX_OBJECTS_NUMBER; i++) // 초기화
	{
		if (m_BrickObjects[i].GetHP() != 0 && m_BrickObjects[i].GetVisible() == true)
		{
			CheckCollideRectAndWall(m_BrickObjects[i]);

			switch (m_BrickObjects[i].GetHP())
			{
			case 1:
				m_TouchedOldBrush = (HBRUSH)SelectObject(m_Hdc, m_TouchedBrush);
				Rectangle(m_Hdc, m_BrickObjects[i].GetLeft(), m_BrickObjects[i].GetTop(), m_BrickObjects[i].GetRight(), m_BrickObjects[i].GetBottom());
				SelectObject(m_Hdc, m_TouchedOldBrush);

				break;
			case 2:
				m_BasicOldBrush = (HBRUSH)SelectObject(m_Hdc, m_BasicBrush);
				Rectangle(m_Hdc, m_BrickObjects[i].GetPositionX() - m_BrickObjects[i].GetHalfX(), m_BrickObjects[i].GetPositionY() - m_BrickObjects[i].GetHalfY(),
					m_BrickObjects[i].GetPositionX() + m_BrickObjects[i].GetHalfX(), m_BrickObjects[i].GetPositionY() + m_BrickObjects[i].GetHalfY());
				SelectObject(m_Hdc, m_BasicOldBrush);

				break;
			}
		}
	}

	m_GroundOldBrush = (HBRUSH)SelectObject(m_Hdc, m_GroundBrush);
	Rectangle(m_Hdc, m_MovingGround.GetPositionX() - m_MovingGround.GetHalfX(), m_MovingGround.GetPositionY() - m_MovingGround.GetHalfY(),
		m_MovingGround.GetPositionX() + m_MovingGround.GetHalfX(), m_MovingGround.GetPositionY() + m_MovingGround.GetHalfY());
	SelectObject(m_Hdc, m_GroundOldBrush);

	m_BallOldBrush = (HBRUSH)SelectObject(m_Hdc, m_BallBrush);
	if (m_IsBallRectangle)
	{
		Rectangle(m_Hdc, m_Ball.GetPositionX() - m_Ball.GetRadius(), m_Ball.GetPositionY() - m_Ball.GetRadius(),
			m_Ball.GetPositionX() + m_Ball.GetRadius(), m_Ball.GetPositionY() + m_Ball.GetRadius());
	}
	else
	{
		Ellipse(m_Hdc, m_Ball.GetPositionX() - m_Ball.GetRadius(), m_Ball.GetPositionY() - m_Ball.GetRadius(),
			m_Ball.GetPositionX() + m_Ball.GetRadius(), m_Ball.GetPositionY() + m_Ball.GetRadius());
	}
	SelectObject(m_Hdc, m_BallOldBrush);

	DeleteObject(m_TouchedBrush);
	DeleteObject(m_BasicBrush);
	DeleteObject(m_GroundBrush);
	DeleteObject(m_BallBrush);

	ReleaseDC(_hWnd, m_Hdc);
}


void CProjectManager::Update(HWND hWnd)
{
	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	for (int i = 0; i < MAX_OBJECTS_NUMBER; i++)
	{
		m_BrickObjects[i].Update(m_BrickSpeed, m_BrickHalfSizeX);
	}

	m_MovingGround.Update(0, m_MovingGroundHalfSizeX);
		
	if ((m_Ball.GetPositionY() - m_Ball.GetRadius() > m_MovingGround.GetPositionY() + m_MovingGround.GetHalfY()))
	{
		MessageBox(hWnd, "공이 판 밑으로 내려갔습니다. 게임을 종료합니다!", "알림", MB_OK);
		PostQuitMessage(0);
	}

	if (m_Ball.GetPositionY() - m_Ball.GetRadius() < 0)
	{
		MessageBox(hWnd, "공이 범위를 벗어났습니다. 게임을 종료합니다!", "알림", MB_OK);
		PostQuitMessage(0);
	}

	if (m_Ball.GetPositionX() + m_Ball.GetRadius() < 0)
	{
		float wallNormalX = 1.0f, wallNormalY = 0.0f, wallNormalZ = 0.0f;

		float dotProductResult = -(m_Ball.GetVelocityX() * wallNormalX + m_Ball.GetVelocityY() * wallNormalY);

		float reflectionX, reflectionY;
		reflectionX = m_Ball.GetVelocityX() + 2 * wallNormalX * dotProductResult;
		reflectionY = m_Ball.GetVelocityY() + 2 * wallNormalY * dotProductResult;

		int newPositionX = 0 + m_Ball.GetRadius();

		m_Ball.SetVelocity(reflectionX, reflectionY);
		m_Ball.SetPositionX(newPositionX);
		m_Ball.SetPositionY(m_Ball.GetPositionY());
	}

	if (m_Ball.GetPositionX() + m_Ball.GetRadius() > WINDOW_X - (FrameX << 1))
	{
		float wallNormalX = -1.0f, wallNormalY = 0.0f, wallNormalZ = 0.0f;

		float dotProductResult = -(m_Ball.GetVelocityX() * wallNormalX + m_Ball.GetVelocityY() * wallNormalY);

		float reflectionX, reflectionY;
		reflectionX = m_Ball.GetVelocityX() + 2 * wallNormalX * dotProductResult;
		reflectionY = m_Ball.GetVelocityY() + 2 * wallNormalY * dotProductResult;

		int newPositionX = WINDOW_X - (FrameX << 1) - m_Ball.GetRadius();

		m_Ball.SetVelocity(reflectionX, reflectionY);
		m_Ball.SetPositionX(newPositionX);
		m_Ball.SetPositionY(m_Ball.GetPositionY());
	}

	if ((m_Ball.GetPositionY() + m_Ball.GetRadius() > m_MovingGround.GetPositionY() - m_MovingGround.GetHalfY()) &&
		(m_Ball.GetPositionX() - m_Ball.GetRadius() >= m_MovingGround.GetPositionX() - m_MovingGround.GetHalfX()) &&
		(m_Ball.GetPositionX() + m_Ball.GetRadius() <= m_MovingGround.GetPositionX() + m_MovingGround.GetHalfX()))
	{
		float wallNormalX = 0.0f, wallNormalY = 1.0f, wallNormalZ = 0.0f;

		float dotProductResult = -(m_Ball.GetVelocityX() * wallNormalX + m_Ball.GetVelocityY() * wallNormalY);

		float reflectionX, reflectionY;
		reflectionX = m_Ball.GetVelocityX() + 2 * wallNormalX * dotProductResult;
		reflectionY = m_Ball.GetVelocityY() + 2 * wallNormalY * dotProductResult;

		int newPositionY = m_MovingGround.GetPositionY() - m_MovingGround.GetHalfY() - m_Ball.GetRadius();

		m_Ball.SetVelocity(reflectionX, reflectionY);
		m_Ball.SetPositionX(m_Ball.GetPositionX());
		m_Ball.SetPositionY(newPositionY);
	}

	for (int i = 0; i < MAX_OBJECTS_NUMBER; i++)
	{
		if (m_BrickObjects[i].GetHP() != 0 && m_BrickObjects[i].GetVisible() == true)
		{
			/*if (m_Ball.GetPositionY() - m_Ball.GetRadius() < m_BrickObjects[i].GetPositionY() + m_BrickObjects[i].GetHalfY() &&
				(m_Ball.GetPositionX() - m_Ball.GetRadius() >= m_BrickObjects[i].GetPositionX() - m_BrickObjects[i].GetHalfX()) &&
				(m_Ball.GetPositionX() + m_Ball.GetRadius() <= m_BrickObjects[i].GetPositionX() + m_BrickObjects[i].GetHalfX()))*/
			if (CheckCollideRectAndBall(m_Ball, m_BrickObjects[i]) == true)
			{
				float wallNormalX = 0.0f, wallNormalY = -1.0f, wallNormalZ = 0.0f;

				float dotProductResult = -(m_Ball.GetVelocityX() * wallNormalX + m_Ball.GetVelocityY() * wallNormalY);

				float reflectionX, reflectionY;
				reflectionX = m_Ball.GetVelocityX() + 2 * wallNormalX * dotProductResult;
				reflectionY = m_Ball.GetVelocityY() + 2 * wallNormalY * dotProductResult;

				m_Ball.SetVelocity(reflectionX, reflectionY);

				m_BrickObjects[i].SetHP(-1);
				m_BrickObjects[i].SetPosition(m_BrickObjects[i].GetPositionX(), m_BrickObjects[i].GetPositionY() + 2 * m_BrickObjects[i].GetHalfY());

				int newPositionY = m_BrickObjects[i].GetPositionY() + m_BrickObjects[i].GetHalfY() + m_Ball.GetRadius();

				m_Ball.SetPositionX(m_Ball.GetPositionX());
				m_Ball.SetPositionY(newPositionY);

				if (m_BrickObjects[i].GetHP() == 1)
				{
					m_TouchedBrickNumber++;
				}
				if (m_BrickObjects[i].GetHP() == 0)
				{
					m_TouchedBrickNumber--;
					m_DestroyedBrickNumber++;
				}
			}
		}
	}

	m_Ball.Update(m_BallSpeed);

	if (m_DestroyedBrickNumber == 19)
		PostQuitMessage(0);

	SetTouchedBrickNumber();
	SetDestroyedBrickNumber();
}


void CProjectManager::CheckCollideRectAndWall(CBrick _target)
{
	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	if (_target.GetRight() + m_BrickSpeed > WINDOW_X - (FrameX << 1))
	{
		m_BrickSpeed = -1;
	}
	else if (_target.GetLeft() + m_BrickSpeed < 0)
	{
		m_BrickSpeed = 1;
	}
}


bool CProjectManager::IsPointInCircle(CBall _ball, int _x, int _y)
{
	int deltaX = _ball.GetPositionX() - _x;
	int deltaY = _ball.GetPositionY() - _y;
	int length = (int)sqrt(pow((double)deltaX, 2.0) + pow((double)deltaY, 2.0));

	if (length > _ball.GetRadius())
		return false;

	return true;
}


bool CProjectManager::CheckCollideRectAndBall(CBall _ball, CBrick _target) // 벽돌과 공의 충돌처리
{
	if ((_target.GetLeft() <= _ball.GetPositionX() && _ball.GetPositionX() <= _target.GetRight()) ||
		(_target.GetBottom() <= _ball.GetPositionY() && _ball.GetPositionY() <= _target.GetTop()))
	{
		if (_target.GetLeft() - _ball.GetRadius() < _ball.GetPositionX() && _ball.GetPositionX() < _target.GetRight() + _ball.GetRadius() &&
			_target.GetBottom() - _ball.GetRadius() < _ball.GetPositionY() && _ball.GetPositionY() < _target.GetTop() + _ball.GetRadius())
		{
			return true;
		}
	}
	else
	{
		if (IsPointInCircle(_ball, _target.GetLeft(), _target.GetTop())) return true;
		if (IsPointInCircle(_ball, _target.GetLeft(), _target.GetBottom())) return true;
		if (IsPointInCircle(_ball, _target.GetRight(), _target.GetTop())) return true;
		if (IsPointInCircle(_ball, _target.GetRight(), _target.GetBottom())) return true;
	}

	return false;
}


void CProjectManager::CollectGarbage()
{
}


void CProjectManager::SetSelectValue(bool _value)
{
	m_IsSelected = _value;
}


bool CProjectManager::IsInGround(int mouseX, int mouseY)
{
	if ((mouseX >= m_MovingGround.GetLeft() && mouseX <= m_MovingGround.GetRight()) &&
		(mouseY >= m_MovingGround.GetBottom() && mouseY <= m_MovingGround.GetTop()) == true)
	{
		return true;
	}
	else
		return false;
}


void CProjectManager::Start()
{
}


void CProjectManager::Stop()
{
}


const bool CProjectManager::GetSelectValue() const
{
	return m_IsSelected;
}


void CProjectManager::SetTouchedBrickNumber()
{
	/*for (int i = 0; i < MAX_OBJECTS_NUMBER; i++)
	{
		if (m_BrickObjects[i].GetHP() == 1)
			m_TouchedBrickNumber++;
	}*/
}


void CProjectManager::SetDestroyedBrickNumber()
{
	/*for (int i = 0; i < MAX_OBJECTS_NUMBER; i++)
	{
		if (m_BrickObjects[i] == NULL)
			m_DestroyedBrickNumber++;
	}*/
}


void CProjectManager::SetGameSpeed(int _speed)
{
	m_BallSpeed = _speed;
}


void CProjectManager::SetBrickSize(int _halfx)
{
	m_MovingGroundHalfSizeX = _halfx;
}


void CProjectManager::SetBallKind(bool _kind)
{
	m_IsBallRectangle = _kind;
}


void CProjectManager::SetHeight(int _height)
{
	switch (_height)
	{
	case 2:
		for (int i = 0; i < MAX_OBJECTS_NUMBER; i++) // 초기화
		{
			if (i >= 20 && i < 30)
			{
				m_BrickObjects[i].SetVisible(false);
				// m_BrickObjects[i].SetHP(2);
			}
			else if (i >= 30 && i < MAX_OBJECTS_NUMBER)
			{
				m_BrickObjects[i].SetVisible(false);
				// m_BrickObjects[i].SetHP(2);
			}
		}
		break;
	case 3:
		for (int i = 0; i < MAX_OBJECTS_NUMBER; i++) // 초기화
		{
			if (i >= 20 && i < 30)
			{
				m_BrickObjects[i].SetVisible(true);
				// m_BrickObjects[i].SetHP(2);
			}
			else if (i >= 30 && i < MAX_OBJECTS_NUMBER)
			{
				m_BrickObjects[i].SetVisible(false);
				// m_BrickObjects[i].SetHP(2);
			}
		}
		break;
	case 4:
		for (int i = 0; i < MAX_OBJECTS_NUMBER; i++) // 초기화
		{
			if (i >= 20 && i < 30)
			{
				m_BrickObjects[i].SetVisible(true);
				// m_BrickObjects[i].SetHP(2);
			}
			else if (i >= 30 && i < MAX_OBJECTS_NUMBER)
			{
				m_BrickObjects[i].SetVisible(true);
				// m_BrickObjects[i].SetHP(2);
			}
		}
		break;
	}
}


const int CProjectManager::GetTouchedBrickNumber() const
{
	return m_TouchedBrickNumber;
}


const int CProjectManager::GetDestroyedBrickNumber() const
{
	return m_DestroyedBrickNumber;
}