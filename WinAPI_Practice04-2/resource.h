//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// Resource.rc에서 사용되고 있습니다.
//
#define IDR_MENU1                       101
#define ID_40001                        40001
#define ID_40002                        40002
#define ID_40003                        40003
#define ID_40004                        40004
#define ID_40005                        40005
#define ID_40006                        40006
#define ID_40007                        40007
#define ID_40008                        40008
#define ID_40009                        40009
#define ID_40010                        40010
#define ID_40011                        40011
#define ID_40012                        40012
#define ID_40013                        40013
#define ID_40014                        40014
#define ID_40015                        40015
#define ID_40016                        40016
#define ID_GAME_START                   40017
#define ID_GAME_END                     40018
#define ID_SPEED_FAST                   40019
#define ID_SPEED_MEDIUM                 40020
#define ID_SPEED_SLOW                   40021
#define ID_GROUND_SMALL                 40022
#define ID_GROUND_MEDIUM                40023
#define ID_GROUND_BIG                   40024
#define ID_BALL_CIRCLE                  40025
#define ID_BALL_RECT                    40026
#define ID_2ND_FLOOR                    40027
#define ID_3RD_FLOOR                    40028
#define ID_4TH_FLOOR                    40029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40030
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
