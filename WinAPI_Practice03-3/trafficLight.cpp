#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <crtdbg.h>
#include <iostream>

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 3-3";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 1000
#define WINDOW_Y 600

#define SECOND 1000
#define VEHICLE_NUMBER 4

#define DOWN 0
#define UP 1
#define RIGHT 2
#define LEFT 3

#define GREEN 0
#define YELLOW 1
#define RED 2

static int AddLength[VEHICLE_NUMBER];

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);
void CALLBACK TrafficLightTimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);

float LengthPts(int x1, int y1, int x2, int y2)
{
	return (sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1)));
}

typedef struct _Circle {
	POINT current_position; // 중심좌표
	RECT rect;
	int radius = 20;
	int kind = 0;

	bool IsInCircle(int mouseX, int mouseY)
	{
		if (LengthPts(current_position.x, current_position.y, mouseX, mouseY) < radius)
			return true;
		else
			return false;
	}
} Circle;

typedef struct _Vehicle {
	POINT CurrentPosition;
	RECT Rect;
	int Width = 40;
	int Height = 20;
	int Direction;
	int Speed;
	POINT StopLine; // 사거리 도착
	POINT FinishLine; // 사거리 건넘
	POINT RedrawLine; // 화면 밖
	bool isMyOrder = false; // 자기 차례인가?
	bool goStaright = true; // 무작정 가기

	void SetSpeed(int _speed)
	{
		Speed += _speed;
	}

	void Check()
	{
		switch (Direction)
		{
		case UP:
		{
			if (Rect.top < 0)
			{
				AddLength[UP] += Speed;
				if (Rect.bottom <= 0)
				{
					// { {550, 50}, {450, 550}, {50, 250}, {950, 350} };
					CurrentPosition.y = WINDOW_Y;
					AddLength[UP] = 0;
				}
			}
		}
		break;
		case DOWN:
		{
			if (Rect.bottom > WINDOW_Y)
			{
				AddLength[DOWN] += Speed;
				if (Rect.top >= WINDOW_Y)
				{
					// { {550, 50}, {450, 550}, {50, 250}, {950, 350} };
					CurrentPosition.y = 0;
					AddLength[DOWN] = 0;
				}
			}
		}
		break;
		case LEFT:
		{
			if (Rect.left < 0)
			{
				AddLength[LEFT] += Speed;
				if (Rect.right <= 0)
				{
					// { {550, 50}, {450, 550}, {50, 250}, {950, 350} };
					CurrentPosition.x = WINDOW_X;
					AddLength[LEFT] = 0;
				}
			}
		}
		break;
		case RIGHT:
		{
			if (Rect.right > WINDOW_X)
			{
				AddLength[RIGHT] += Speed;
				if (Rect.left >= WINDOW_X)
				{
					// { {550, 50}, {450, 550}, {50, 250}, {950, 350} };
					CurrentPosition.x = AddLength[RIGHT] - 50;
					AddLength[RIGHT] = 0;
				}
			}
		}
		break;
		}
	}

	void Update()
	{
		///////////////////////////////////////////////////// TODO : 움직임 설정 /////////////////////////////////////////////////////
		if (goStaright == true)
		{
			if (isMyOrder == true)
			{
				switch (Direction)
				{
				case UP:
					CurrentPosition.y -= Speed;
					break;
				case DOWN:
					CurrentPosition.y += Speed;
					break;
				case LEFT:
					CurrentPosition.x -= Speed;
					break;
				case RIGHT:
					CurrentPosition.x += Speed;
					break;
				}
			}
			else
			{
				switch (Direction)
				{
				case UP:
					if (Rect.top >= StopLine.y)
						CurrentPosition.y -= Speed;
					break;
				case DOWN:
					if (Rect.bottom <= StopLine.y)
						CurrentPosition.y += Speed;
					break;
				case LEFT:
					if (Rect.left >= StopLine.x)
						CurrentPosition.x -= Speed;
					break;
				case RIGHT:
					if (Rect.right <= StopLine.x)
						CurrentPosition.x += Speed;
					break;
				}
			}
		}

		///////////////////////////////////////////////////// TODO : 바운딩박스 설정 /////////////////////////////////////////////////////
		switch (Direction)
		{
		case UP: case DOWN:
		{
			Rect.left = CurrentPosition.x - Height;
			Rect.top = CurrentPosition.y - Width;
			Rect.right = CurrentPosition.x + Height;
			Rect.bottom = CurrentPosition.y + Width;
		}
		break;
		case LEFT: case RIGHT:
		{
			Rect.left = CurrentPosition.x - Width;
			Rect.top = CurrentPosition.y - Height;
			Rect.right = CurrentPosition.x + Width;
			Rect.bottom = CurrentPosition.y + Height;
		}
		break;
		}
	}
} Vehicle;

class CProjectManager
{
private:
	HDC m_Hdc;
	Circle m_TrafficLightArray[3];
	Vehicle m_Vehicles[VEHICLE_NUMBER];

	HBRUSH m_Hbrush;
	HBRUSH m_Oldbrush;

	COLORREF m_RedColor = RGB(255, 0, 0);
	COLORREF m_GreenColor = RGB(0, 255, 0);
	COLORREF m_YellowColor = RGB(255, 255, 0);
	COLORREF m_HumanColor = RGB(0, 255, 255);

	int m_NowTrafficLight = GREEN;
	int m_VehicleSpeed = 1;
	int m_HumanSpeed = 5;

public:
	void MouseClick(int mouseX, int mouseY)
	{
		for (auto& traffic : m_TrafficLightArray)
		{
			if (traffic.IsInCircle(mouseX, mouseY))
			{
				m_NowTrafficLight = traffic.kind;
			}
		}
	}

	void KeyboardInput(HWND _hWnd, WPARAM _wParam)
	{
		switch (_wParam)
		{
		case '=': // 속도 증가
			if (m_VehicleSpeed < 10)
				m_VehicleSpeed++;
			break;
		case '-': // 속도 감소
			if (m_VehicleSpeed > 1)
				m_VehicleSpeed--;
			break;
		case 'q': case 'Q': // 프로그램 종료
			PostQuitMessage(0);
			break;
		}
	}

	void Update(HWND _hWnd)
	{
		for (auto i = 0; i < VEHICLE_NUMBER; i++)
		{
			if (m_NowTrafficLight == GREEN || m_NowTrafficLight == YELLOW)
				m_Vehicles[i].Update();

			m_Vehicles[i].Check();
		}
	}

	void Draw(HDC hdc, HWND _hWnd)
	{
		Rectangle(hdc, 0, 0, WINDOW_X, WINDOW_Y);
		Rectangle(hdc, 0, 0, 400, 200); // 왼상단
		Rectangle(hdc, 0, 400, 400, WINDOW_Y); // 왼하단
		Rectangle(hdc, 600, 0, WINDOW_X, 200); // 오른상단
		Rectangle(hdc, 750, 50, 950, 100); // 신호등

		Ellipse(hdc, m_TrafficLightArray[0].current_position.x - m_TrafficLightArray[0].radius, m_TrafficLightArray[0].current_position.y - m_TrafficLightArray[0].radius,
			m_TrafficLightArray[0].current_position.x + m_TrafficLightArray[0].radius, m_TrafficLightArray[0].current_position.y + m_TrafficLightArray[0].radius);
		Ellipse(hdc, m_TrafficLightArray[1].current_position.x - m_TrafficLightArray[1].radius, m_TrafficLightArray[1].current_position.y - m_TrafficLightArray[1].radius,
			m_TrafficLightArray[1].current_position.x + m_TrafficLightArray[1].radius, m_TrafficLightArray[1].current_position.y + m_TrafficLightArray[1].radius);
		Ellipse(hdc, m_TrafficLightArray[2].current_position.x - m_TrafficLightArray[2].radius, m_TrafficLightArray[2].current_position.y - m_TrafficLightArray[2].radius,
			m_TrafficLightArray[2].current_position.x + m_TrafficLightArray[2].radius, m_TrafficLightArray[2].current_position.y + m_TrafficLightArray[2].radius);

		switch (m_NowTrafficLight)
		{
		case GREEN:
			m_Hbrush = CreateSolidBrush(m_GreenColor);
			m_Oldbrush = (HBRUSH)SelectObject(hdc, m_Hbrush);
			Ellipse(hdc, m_TrafficLightArray[0].current_position.x - m_TrafficLightArray[0].radius, m_TrafficLightArray[0].current_position.y - m_TrafficLightArray[0].radius,
				m_TrafficLightArray[0].current_position.x + m_TrafficLightArray[0].radius, m_TrafficLightArray[0].current_position.y + m_TrafficLightArray[0].radius);
			SelectObject(hdc, m_Oldbrush);
			DeleteObject(m_Hbrush);
			break;
		case YELLOW:
			m_Hbrush = CreateSolidBrush(m_YellowColor);
			m_Oldbrush = (HBRUSH)SelectObject(hdc, m_Hbrush);
			Ellipse(hdc, m_TrafficLightArray[1].current_position.x - m_TrafficLightArray[1].radius, m_TrafficLightArray[1].current_position.y - m_TrafficLightArray[1].radius,
				m_TrafficLightArray[1].current_position.x + m_TrafficLightArray[1].radius, m_TrafficLightArray[1].current_position.y + m_TrafficLightArray[1].radius);
			SelectObject(hdc, m_Oldbrush);
			DeleteObject(m_Hbrush);
			break;
		case RED:
			m_Hbrush = CreateSolidBrush(m_RedColor);
			m_Oldbrush = (HBRUSH)SelectObject(hdc, m_Hbrush);
			Ellipse(hdc, m_TrafficLightArray[2].current_position.x - m_TrafficLightArray[2].radius, m_TrafficLightArray[2].current_position.y - m_TrafficLightArray[2].radius,
				m_TrafficLightArray[2].current_position.x + m_TrafficLightArray[2].radius, m_TrafficLightArray[2].current_position.y + m_TrafficLightArray[2].radius);
			SelectObject(hdc, m_Oldbrush);
			DeleteObject(m_Hbrush);
			break;
		}

		Rectangle(hdc, 600, 400, WINDOW_X, WINDOW_Y); // 오른하단

		for (int index = 0; index < VEHICLE_NUMBER; index++)
		{
			switch (m_Vehicles[index].Direction)
			{
			case UP:
				Rectangle(hdc, m_Vehicles[index].Rect.left, m_Vehicles[index].Rect.top, m_Vehicles[index].Rect.right, m_Vehicles[index].Rect.bottom);
				if (m_Vehicles[index].Rect.top - m_Vehicles[index].Width * 2 <= 0)
				{
					Rectangle(hdc, m_Vehicles[index].Rect.left, WINDOW_Y, m_Vehicles[index].Rect.right, WINDOW_Y - AddLength[UP]);
				}
				break;
			case DOWN:
				Rectangle(hdc, m_Vehicles[index].Rect.left, m_Vehicles[index].Rect.top, m_Vehicles[index].Rect.right, m_Vehicles[index].Rect.bottom);
				if (m_Vehicles[index].Rect.bottom + m_Vehicles[index].Width * 2 >= WINDOW_Y)
				{
					Rectangle(hdc, m_Vehicles[index].Rect.left, 0, m_Vehicles[index].Rect.right, AddLength[DOWN]);
				}
				break;
			case LEFT:
				Rectangle(hdc, m_Vehicles[index].Rect.left, m_Vehicles[index].Rect.top, m_Vehicles[index].Rect.right, m_Vehicles[index].Rect.bottom);
				if (m_Vehicles[index].Rect.left <= 0)
				{
					Rectangle(hdc, WINDOW_X, m_Vehicles[index].Rect.top, WINDOW_X - AddLength[LEFT], m_Vehicles[index].Rect.bottom);
				}
				break;
			case RIGHT:
				Rectangle(hdc, m_Vehicles[index].Rect.left, m_Vehicles[index].Rect.top, m_Vehicles[index].Rect.right, m_Vehicles[index].Rect.bottom);
				if (m_Vehicles[index].Rect.right >= WINDOW_X)
				{
					Rectangle(hdc, 0, m_Vehicles[index].Rect.top, AddLength[RIGHT], m_Vehicles[index].Rect.bottom);
				}
				break;
			}
		}

		/*m_Hbrush = CreateSolidBrush(m_HumanColor);
		m_Oldbrush = (HBRUSH)SelectObject(hdc, m_Hbrush);
		Ellipse(hdc, m_Human->current_position.x - m_Human->radius, m_Human->current_position.y - m_Human->radius,
			m_Human->current_position.x + m_Human->radius, m_Human->current_position.y + m_Human->radius);
		SelectObject(hdc, m_Oldbrush);
		DeleteObject(m_Hbrush);*/
	}

	CProjectManager()
	{
		srand((unsigned int)time(NULL));

		int direction = 0;
		int position[VEHICLE_NUMBER][2] = { {550, 50}, {450, 550}, {50, 250}, {950, 350} };
		for (int index = 0; index < VEHICLE_NUMBER; index++)
		{
			m_Vehicles[index].Direction = direction++;
			m_Vehicles[index].CurrentPosition.x = position[index][0];
			m_Vehicles[index].CurrentPosition.y = position[index][1];
		}

		m_Vehicles[DOWN].StopLine = { 550, 200 };
		m_Vehicles[UP].StopLine = { 450, 400 };
		m_Vehicles[RIGHT].StopLine = { 400, 250 };
		m_Vehicles[LEFT].StopLine = { 600, 350 };

		m_Vehicles[DOWN].FinishLine = { 550, 400 };
		m_Vehicles[UP].FinishLine = { 450, 200 };
		m_Vehicles[RIGHT].FinishLine = { 600, 250 };
		m_Vehicles[LEFT].FinishLine = { 400, 350 };

		m_Vehicles[DOWN].RedrawLine = { 550, 600 };
		m_Vehicles[UP].RedrawLine = { 450, 000 };
		m_Vehicles[RIGHT].RedrawLine = { 1000, 250 };
		m_Vehicles[LEFT].RedrawLine = { 0, 350 };

		m_Vehicles[DOWN].Speed = rand() % 4 + 1;
		m_Vehicles[UP].Speed = rand() % 4 + 1;
		m_Vehicles[RIGHT].Speed = rand() % 4 + 1;
		m_Vehicles[LEFT].Speed = rand() % 4 + 1;

		for (auto i = 0; i < VEHICLE_NUMBER; i++)
		{
			switch (m_Vehicles[i].Direction)
			{
			case UP: case DOWN:
			{
				m_Vehicles[i].Rect.left = m_Vehicles[i].CurrentPosition.x - m_Vehicles[i].Height;
				m_Vehicles[i].Rect.top = m_Vehicles[i].CurrentPosition.y - m_Vehicles[i].Width;
				m_Vehicles[i].Rect.right = m_Vehicles[i].CurrentPosition.x + m_Vehicles[i].Height;
				m_Vehicles[i].Rect.bottom = m_Vehicles[i].CurrentPosition.y + m_Vehicles[i].Width;
			}
			break;
			case LEFT: case RIGHT:
			{
				m_Vehicles[i].Rect.left = m_Vehicles[i].CurrentPosition.x - m_Vehicles[i].Width;
				m_Vehicles[i].Rect.top = m_Vehicles[i].CurrentPosition.y - m_Vehicles[i].Height;
				m_Vehicles[i].Rect.right = m_Vehicles[i].CurrentPosition.x + m_Vehicles[i].Width;
				m_Vehicles[i].Rect.bottom = m_Vehicles[i].CurrentPosition.y + m_Vehicles[i].Height;
			}
			break;
			}
		}

		m_TrafficLightArray[0].current_position.x = 790;
		m_TrafficLightArray[0].current_position.y = 75;
		m_TrafficLightArray[0].kind = GREEN;

		m_TrafficLightArray[1].current_position.x = 850;
		m_TrafficLightArray[1].current_position.y = 75;
		m_TrafficLightArray[1].kind = YELLOW;

		m_TrafficLightArray[2].current_position.x = 910;
		m_TrafficLightArray[2].current_position.y = 75;
		m_TrafficLightArray[2].kind = RED;
	}

	~CProjectManager()
	{
	}
};

CProjectManager g_Manager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc;
	PAINTSTRUCT ps;

	int mouseX = 0;
	int mouseY = 0;

	static RECT view;
	static HDC  mem1dc, mem2dc;
	static HBITMAP hBit1, hOldBit1;
	static HBITMAP hBit2, hOldBit2;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		GetClientRect(hWnd, &view);
		SetTimer(hWnd, 1, 1, (TIMERPROC)TimerProc); // 1번 아이디의 타이머가 0.01초 마다 TimerProc 타이머 함수 실행
		SetTimer(hWnd, 2, SECOND * 5, (TIMERPROC)TrafficLightTimerProc); // 1번 아이디의 타이머가 0.01초 마다 TimerProc 타이머 함수 실행
		break;
	case WM_PAINT:
	{
		GetClientRect(hWnd, &view);

		hdc = BeginPaint(hWnd, &ps);

		mem1dc = CreateCompatibleDC(hdc);
		mem2dc = CreateCompatibleDC(mem1dc);

		if (hBit1 == NULL)
			hBit1 = CreateCompatibleBitmap(hdc, WINDOW_X, WINDOW_Y);

		if (hBit2 == NULL)
			hBit2 = CreateCompatibleBitmap(hdc, WINDOW_X, WINDOW_Y);

		hOldBit1 = (HBITMAP)SelectObject(mem1dc, hBit1);
		hOldBit2 = (HBITMAP)SelectObject(mem2dc, hBit2);

		BitBlt(mem1dc, 0, 0, WINDOW_X, WINDOW_Y, mem2dc, 0, 0, SRCCOPY);

		g_Manager.Draw(mem1dc, hWnd);
		SetBkMode(mem1dc, TRANSPARENT);

		BitBlt(hdc, 0, 0, WINDOW_X, WINDOW_Y, mem1dc, 0, 0, SRCCOPY);

		SelectObject(mem1dc, hOldBit1);
		SelectObject(mem2dc, hOldBit2);

		DeleteDC(mem2dc);
		DeleteDC(mem1dc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
		g_Manager.KeyboardInput(hWnd, wParam);
		break;
	case WM_KEYDOWN:
		break;
	case WM_LBUTTONDOWN:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		g_Manager.MouseClick(mouseX, mouseY);
		break;
	case WM_LBUTTONUP:
		break;
	case WM_DESTROY:
		if (hBit1) DeleteObject(hBit1);
		DeleteObject(hBit2);
		KillTimer(hWnd, 1);
		KillTimer(hWnd, 2);
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
	{
		g_Manager.Update(hWnd);
	}
	break;
	}
	InvalidateRgn(hWnd, NULL, FALSE);
}

void CALLBACK TrafficLightTimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
	{

	}
	break;
	}
	InvalidateRgn(hWnd, NULL, FALSE);
}