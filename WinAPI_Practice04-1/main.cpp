#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <crtdbg.h>
#include <iostream>
#include "resource.h"

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 4-1";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 1000
#define WINDOW_Y 600

#define SECOND 1000
#define VEHICLE_NUMBER 4

#define DOWN 0
#define UP 1
#define RIGHT 2
#define LEFT 3

#define GREEN 0
#define YELLOW 1
#define RED 2

static int AddLength[VEHICLE_NUMBER];

float LengthPts(int x1, int y1, int x2, int y2)
{
	return (sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1)));
}

typedef struct _Circle {
	POINT current_position; // 중심좌표
	RECT rect;
	int radius = 20;
	int kind = 0;

	bool IsInCircle(int mouseX, int mouseY)
	{
		if (LengthPts(current_position.x, current_position.y, mouseX, mouseY) < radius)
			return true;
		else
			return false;
	}
} Circle;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);

bool g_IsPlaying = false;
int now_traffic_color = GREEN;
int g_VehicleSpeed = 5;
int* g_TempVehicleSpeed;
Circle g_TrafficLights[3];

COLORREF g_RedColor = RGB(255, 0, 0);
COLORREF g_GreenColor = RGB(0, 255, 0);
COLORREF g_YellowColor = RGB(255, 255, 0);

typedef struct _Vehicle {
	POINT CurrentPosition;
	RECT Rect;
	int Width = 40;
	int Height = 20;
	int Direction;
	int Speed;
	POINT StopLine; // 사거리 도착
	POINT FinishLine; // 사거리 건넘
	POINT RedrawLine; // 화면 밖
	bool isMyOrder = true; // 자기 차례인가?
	bool goStaright = true; // 무작정 가기

	void SetSpeed(int _speed)
	{
		Speed += _speed;
	}

	void Check()
	{
		switch (Direction)
		{
		case UP:
		{
			if (Rect.top < 0)
			{
				AddLength[UP] += Speed;
				if (Rect.bottom <= 0)
				{
					// { {550, 50}, {450, 550}, {50, 250}, {950, 350} };
					CurrentPosition.y = WINDOW_Y;
					AddLength[UP] = 0;
				}
			}
		}
		break;
		case DOWN:
		{
			if (Rect.bottom > WINDOW_Y)
			{
				AddLength[DOWN] += Speed;
				if (Rect.top >= WINDOW_Y)
				{
					// { {550, 50}, {450, 550}, {50, 250}, {950, 350} };
					CurrentPosition.y = 0;
					AddLength[DOWN] = 0;
				}
			}
		}
		break;
		case LEFT:
		{
			if (Rect.left < 0)
			{
				AddLength[LEFT] += Speed;
				if (Rect.right <= 0)
				{
					// { {550, 50}, {450, 550}, {50, 250}, {950, 350} };
					CurrentPosition.x = WINDOW_X;
					AddLength[LEFT] = 0;
				}
			}
		}
		break;
		case RIGHT:
		{
			if (Rect.right > WINDOW_X)
			{
				AddLength[RIGHT] += Speed;
				if (Rect.left >= WINDOW_X)
				{
					// { {550, 50}, {450, 550}, {50, 250}, {950, 350} };
					CurrentPosition.x = AddLength[RIGHT] - 50;
					AddLength[RIGHT] = 0;
				}
			}
		}
		break;
		}
	}

	void Update()
	{
		///////////////////////////////////////////////////// TODO : 움직임 설정 /////////////////////////////////////////////////////
		if (goStaright == true)
		{
			if (isMyOrder == true)
			{
				switch (Direction)
				{
				case UP:
					CurrentPosition.y -= Speed;
					break;
				case DOWN:
					CurrentPosition.y += Speed;
					break;
				case LEFT:
					CurrentPosition.x -= Speed;
					break;
				case RIGHT:
					CurrentPosition.x += Speed;
					break;
				}
			}
			else
			{
				switch (Direction)
				{
				case UP:
					if (Rect.top >= StopLine.y)
						CurrentPosition.y -= Speed;
					break;
				case DOWN:
					if (Rect.bottom <= StopLine.y)
						CurrentPosition.y += Speed;
					break;
				case LEFT:
					if (Rect.left >= StopLine.x)
						CurrentPosition.x -= Speed;
					break;
				case RIGHT:
					if (Rect.right <= StopLine.x)
						CurrentPosition.x += Speed;
					break;
				}
			}
		}

		///////////////////////////////////////////////////// TODO : 바운딩박스 설정 /////////////////////////////////////////////////////
		switch (Direction)
		{
		case UP: case DOWN:
		{
			Rect.left = CurrentPosition.x - Height;
			Rect.top = CurrentPosition.y - Width;
			Rect.right = CurrentPosition.x + Height;
			Rect.bottom = CurrentPosition.y + Width;
		}
		break;
		case LEFT: case RIGHT:
		{
			Rect.left = CurrentPosition.x - Width;
			Rect.top = CurrentPosition.y - Height;
			Rect.right = CurrentPosition.x + Width;
			Rect.bottom = CurrentPosition.y + Height;
		}
		break;
		}
	}
} Vehicle;

Vehicle g_Vehicles[VEHICLE_NUMBER];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = MAKEINTRESOURCE(ID_MENU);
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc;
	PAINTSTRUCT ps;

	int mouseX = 0;
	int mouseY = 0;

	static HBRUSH hbrush;
	static HBRUSH oldbrush;
	static HPEN hPen;
	static HPEN oldPen;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		SetTimer(hWnd, 1, 1000, (TIMERPROC)TimerProc); // 1번 아이디의 타이머가 0.01초 마다 TimerProc 타이머 함수 실행

		g_TrafficLights[0].current_position.x = 790;
		g_TrafficLights[0].current_position.y = 75;
		g_TrafficLights[0].kind = GREEN;

		g_TrafficLights[1].current_position.x = 850;
		g_TrafficLights[1].current_position.y = 75;
		g_TrafficLights[1].kind = YELLOW;

		g_TrafficLights[2].current_position.x = 910;
		g_TrafficLights[2].current_position.y = 75;
		g_TrafficLights[2].kind = RED;

		srand((unsigned int)time(NULL));

		int direction = 0;
		int position[VEHICLE_NUMBER][2] = { {550, 50}, {450, 550}, {50, 250}, {950, 350} };
		for (int index = 0; index < VEHICLE_NUMBER; index++)
		{
			g_Vehicles[index].Direction = direction++;
			g_Vehicles[index].CurrentPosition.x = position[index][0];
			g_Vehicles[index].CurrentPosition.y = position[index][1];
		}

		g_Vehicles[DOWN].StopLine = { 550, 200 };
		g_Vehicles[UP].StopLine = { 450, 400 };
		g_Vehicles[RIGHT].StopLine = { 400, 250 };
		g_Vehicles[LEFT].StopLine = { 600, 350 };

		g_Vehicles[DOWN].FinishLine = { 550, 400 };
		g_Vehicles[UP].FinishLine = { 450, 200 };
		g_Vehicles[RIGHT].FinishLine = { 600, 250 };
		g_Vehicles[LEFT].FinishLine = { 400, 350 };

		g_Vehicles[DOWN].RedrawLine = { 550, 600 };
		g_Vehicles[UP].RedrawLine = { 450, 000 };
		g_Vehicles[RIGHT].RedrawLine = { 1000, 250 };
		g_Vehicles[LEFT].RedrawLine = { 0, 350 };

		g_Vehicles[DOWN].Speed = rand() % 4 + 1;
		g_Vehicles[UP].Speed = rand() % 4 + 1;
		g_Vehicles[RIGHT].Speed = rand() % 4 + 1;
		g_Vehicles[LEFT].Speed = rand() % 4 + 1;

		for (auto i = 0; i < VEHICLE_NUMBER; i++)
		{
			switch (g_Vehicles[i].Direction)
			{
			case UP: case DOWN:
			{
				g_Vehicles[i].Rect.left = g_Vehicles[i].CurrentPosition.x - g_Vehicles[i].Height;
				g_Vehicles[i].Rect.top = g_Vehicles[i].CurrentPosition.y - g_Vehicles[i].Width;
				g_Vehicles[i].Rect.right = g_Vehicles[i].CurrentPosition.x + g_Vehicles[i].Height;
				g_Vehicles[i].Rect.bottom = g_Vehicles[i].CurrentPosition.y + g_Vehicles[i].Width;
			}
			break;
			case LEFT: case RIGHT:
			{
				g_Vehicles[i].Rect.left = g_Vehicles[i].CurrentPosition.x - g_Vehicles[i].Width;
				g_Vehicles[i].Rect.top = g_Vehicles[i].CurrentPosition.y - g_Vehicles[i].Height;
				g_Vehicles[i].Rect.right = g_Vehicles[i].CurrentPosition.x + g_Vehicles[i].Width;
				g_Vehicles[i].Rect.bottom = g_Vehicles[i].CurrentPosition.y + g_Vehicles[i].Height;
			}
			break;
			}
		}
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		// TODO : 그림 그리기
		Rectangle(hdc, 0, 0, 400, 200); // 왼상단
		Rectangle(hdc, 0, 400, 400, WINDOW_Y); // 왼하단
		Rectangle(hdc, 600, 0, WINDOW_X, 200); // 오른상단
		Rectangle(hdc, 600, 400, WINDOW_X, WINDOW_Y); // 오른하단

		Rectangle(hdc, 750, 50, 950, 100); // 신호등

		Ellipse(hdc, g_TrafficLights[0].current_position.x - g_TrafficLights[0].radius, g_TrafficLights[0].current_position.y - g_TrafficLights[0].radius,
			g_TrafficLights[0].current_position.x + g_TrafficLights[0].radius, g_TrafficLights[0].current_position.y + g_TrafficLights[0].radius);
		Ellipse(hdc, g_TrafficLights[1].current_position.x - g_TrafficLights[1].radius, g_TrafficLights[1].current_position.y - g_TrafficLights[1].radius,
			g_TrafficLights[1].current_position.x + g_TrafficLights[1].radius, g_TrafficLights[1].current_position.y + g_TrafficLights[1].radius);
		Ellipse(hdc, g_TrafficLights[2].current_position.x - g_TrafficLights[2].radius, g_TrafficLights[2].current_position.y - g_TrafficLights[2].radius,
			g_TrafficLights[2].current_position.x + g_TrafficLights[2].radius, g_TrafficLights[2].current_position.y + g_TrafficLights[2].radius);

		// TODO : 신호등 테두리 체크
		switch (now_traffic_color)
		{
		case GREEN:
			hPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 0));
			oldPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 0));
			Ellipse(hdc, g_TrafficLights[0].current_position.x - g_TrafficLights[0].radius, g_TrafficLights[0].current_position.y - g_TrafficLights[0].radius,
				g_TrafficLights[0].current_position.x + g_TrafficLights[0].radius, g_TrafficLights[0].current_position.y + g_TrafficLights[0].radius);
			SelectObject(hdc, oldPen);
			DeleteObject(hPen);
			break;
		case YELLOW:
			hPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 0));
			oldPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 0));
			Ellipse(hdc, g_TrafficLights[1].current_position.x - g_TrafficLights[1].radius, g_TrafficLights[1].current_position.y - g_TrafficLights[1].radius,
				g_TrafficLights[1].current_position.x + g_TrafficLights[1].radius, g_TrafficLights[1].current_position.y + g_TrafficLights[1].radius);
			SelectObject(hdc, oldPen);
			DeleteObject(hPen);
			break;
		case RED:
			hPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 0));
			oldPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 0));
			Ellipse(hdc, g_TrafficLights[2].current_position.x - g_TrafficLights[2].radius, g_TrafficLights[2].current_position.y - g_TrafficLights[2].radius,
				g_TrafficLights[2].current_position.x + g_TrafficLights[2].radius, g_TrafficLights[2].current_position.y + g_TrafficLights[2].radius);
			SelectObject(hdc, oldPen);
			DeleteObject(hPen);
			break;
		}

		// TODO : 신호등 현재 색깔 체크
		switch (now_traffic_color)
		{
		case GREEN:
			hbrush = CreateSolidBrush(g_GreenColor);
			oldbrush = (HBRUSH)SelectObject(hdc, hbrush);
			Ellipse(hdc, g_TrafficLights[0].current_position.x - g_TrafficLights[0].radius, g_TrafficLights[0].current_position.y - g_TrafficLights[0].radius,
				g_TrafficLights[0].current_position.x + g_TrafficLights[0].radius, g_TrafficLights[0].current_position.y + g_TrafficLights[0].radius);
			SelectObject(hdc, oldbrush);
			DeleteObject(hbrush);
			break;
		case YELLOW:
			hbrush = CreateSolidBrush(g_YellowColor);
			oldbrush = (HBRUSH)SelectObject(hdc, hbrush);
			Ellipse(hdc, g_TrafficLights[1].current_position.x - g_TrafficLights[1].radius, g_TrafficLights[1].current_position.y - g_TrafficLights[1].radius,
				g_TrafficLights[1].current_position.x + g_TrafficLights[1].radius, g_TrafficLights[1].current_position.y + g_TrafficLights[1].radius);
			SelectObject(hdc, oldbrush);
			DeleteObject(hbrush);
			break;
		case RED:
			hbrush = CreateSolidBrush(g_RedColor);
			oldbrush = (HBRUSH)SelectObject(hdc, hbrush);
			Ellipse(hdc, g_TrafficLights[2].current_position.x - g_TrafficLights[2].radius, g_TrafficLights[2].current_position.y - g_TrafficLights[2].radius,
				g_TrafficLights[2].current_position.x + g_TrafficLights[2].radius, g_TrafficLights[2].current_position.y + g_TrafficLights[2].radius);
			SelectObject(hdc, oldbrush);
			DeleteObject(hbrush);
			break;
		}

		for (int index = 0; index < VEHICLE_NUMBER; index++)
		{
			switch (g_Vehicles[index].Direction)
			{
			case UP:
				Rectangle(hdc, g_Vehicles[index].Rect.left, g_Vehicles[index].Rect.top, g_Vehicles[index].Rect.right, g_Vehicles[index].Rect.bottom);
				if (g_Vehicles[index].Rect.top - g_Vehicles[index].Width * 2 <= 0)
				{
					Rectangle(hdc, g_Vehicles[index].Rect.left, WINDOW_Y, g_Vehicles[index].Rect.right, WINDOW_Y - AddLength[UP]);
				}
				break;
			case DOWN:
				Rectangle(hdc, g_Vehicles[index].Rect.left, g_Vehicles[index].Rect.top, g_Vehicles[index].Rect.right, g_Vehicles[index].Rect.bottom);
				if (g_Vehicles[index].Rect.bottom + g_Vehicles[index].Width * 2 >= WINDOW_Y)
				{
					Rectangle(hdc, g_Vehicles[index].Rect.left, 0, g_Vehicles[index].Rect.right, AddLength[DOWN]);
				}
				break;
			case LEFT:
				Rectangle(hdc, g_Vehicles[index].Rect.left, g_Vehicles[index].Rect.top, g_Vehicles[index].Rect.right, g_Vehicles[index].Rect.bottom);
				if (g_Vehicles[index].Rect.left <= 0)
				{
					Rectangle(hdc, WINDOW_X, g_Vehicles[index].Rect.top, WINDOW_X - AddLength[LEFT], g_Vehicles[index].Rect.bottom);
				}
				break;
			case RIGHT:
				Rectangle(hdc, g_Vehicles[index].Rect.left, g_Vehicles[index].Rect.top, g_Vehicles[index].Rect.right, g_Vehicles[index].Rect.bottom);
				if (g_Vehicles[index].Rect.right >= WINDOW_X)
				{
					Rectangle(hdc, 0, g_Vehicles[index].Rect.top, AddLength[RIGHT], g_Vehicles[index].Rect.bottom);
				}
				break;
			}

			EndPaint(hWnd, &ps);
		}
	}
	break;
	case WM_CHAR:
		break;
	case WM_KEYDOWN:
		break;
	case WM_LBUTTONDOWN:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);

		// TODO : 신호등 체크
		for (auto& traffic : g_TrafficLights)
		{
			if (traffic.IsInCircle(mouseX, mouseY))
			{
				now_traffic_color = traffic.kind;
			}
		}

		InvalidateRgn(hWnd, NULL, TRUE);
		break;
	case WM_LBUTTONUP:
		InvalidateRgn(hWnd, NULL, TRUE);

		break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case ID_GAME_START:
			g_VehicleSpeed = 5;
			cout << g_VehicleSpeed << endl;
			break;
		case ID_GAME_END:
			g_VehicleSpeed = 0;
			cout << g_VehicleSpeed << endl;
			break;
		case ID_FAST:
			if (g_VehicleSpeed < 10)
				g_VehicleSpeed++;
			break;
		case ID_SLOW:
			if (g_VehicleSpeed > 1)
				g_VehicleSpeed--;
			cout << g_VehicleSpeed;
			break;
		case ID_RED:
			now_traffic_color = RED;
			break;
		case ID_BLUE:
			now_traffic_color = GREEN;
			break;
		case ID_YELLOW:
			now_traffic_color = YELLOW;
			break;
		}
	}
	break;
	case WM_DESTROY:
		KillTimer(hWnd, 1);
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
	{
		for (auto i = 0; i < VEHICLE_NUMBER; i++)
		{
			g_Vehicles[i].SetSpeed(g_VehicleSpeed);
			// g_Vehicles[i].Update();
		}
	}
	break;
	}
	InvalidateRgn(hWnd, NULL, TRUE);
}