//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// WinAPI_Practice06-3.rc에서 사용되고 있습니다.
//
#define IDD_DIALOG6_3                   101
#define IDC_RADIO_SIN                   1001
#define IDC_RADIO_ZIGZAG                1002
#define IDC_RADIO_SPRING                1003
#define IDC_CHECK_REDLINE               1004
#define IDC_CHECK_GREENLINE             1005
#define IDC_CHECK_BLUELINE              1006
#define IDC_CHECK_REVERSEDCOLOR         1007
#define IDC_EXIT                        1008
#define IDC_BUTTON1                     1009
#define IDC_BUTTON2                     1010
#define IDC_BUTTON3                     1011
#define IDC_BUTTON4                     1012
#define IDC_BUTTON5                     1013

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
