#include <Windows.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include "resource.h"

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

struct Point {
	int x;
	int y;

	Point& operator=(Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}
};

using namespace std;

#define WINDOW_X 1280
#define WINDOW_Y 720

#define FRAME_X 135
#define FRAME_Y 150

#define POINT_NUMBER 100
#define PI 3.141592

#define SIN 0
#define ZIGZAG 1
#define SPRING 2

int Shape = -1;

POINT pointArray[POINT_NUMBER * 100];
POINT zigzagArray[POINT_NUMBER * 100];
POINT springArray[POINT_NUMBER * 100 + 1];

Point g_CurvePosition;
Point g_CirclePosition;

bool color[4] = { false, false , false, false };
bool isLeftMove = false;
bool isUpDownMove = false;
bool isUp = true;
bool isCircleGoAlongShpae = false;
bool isCircleMove = true;

int speed = 1;
int circleSpeed = 0;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 6-3";

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
BOOL Dlg6_3Proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static int startX, startY;
	static int endX, endY;

	static HBITMAP hBitmap, hOldBitmap;

	static HPEN hPen;
	static HPEN hOldPen;
	static HBRUSH hBrush;
	static HBRUSH hOldBrush;

	static int index;
	static int time;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);
		index = 0;
		time = 0;

		// TODO : 사인곡선
		for (int i = 0; i < POINT_NUMBER * 100; i++)
		{
			pointArray[i].x = i * WINDOW_X / POINT_NUMBER;
			pointArray[i].y = (int)(WINDOW_Y / 2 * (1 - sin(6 * PI * i / POINT_NUMBER)));
		}

		// TODO : 지그재그곡선
		for (int i = 0; i < POINT_NUMBER * 100; i++)
		{
			zigzagArray[i].x = i * WINDOW_X / (POINT_NUMBER / 5);

			if (i % 2 == 0)
				zigzagArray[i].y = WINDOW_Y / 2 + 50;
			else
				zigzagArray[i].y = WINDOW_Y / 2 - 50;
		}

		// TODO : 스프링곡선
		int i = 0;
		for (float angle = 0.0f; angle <= 1000.0f; angle += 0.1f)
		{
			springArray[i].x = 100 +  i + 60.0f * cos(angle);
			springArray[i].y = WINDOW_Y / 2 + 60.0f * sin(angle);
			i++;
		}

		g_CurvePosition.x = 0;
		g_CurvePosition.y = 0;

		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_TIMER:
	{
		if (isLeftMove)
		{
			g_CurvePosition.x -= speed;
		}

		if (isUpDownMove)
		{
			if (isUp)
			{
				g_CurvePosition.y -= speed;
			}
			else
			{
				g_CurvePosition.y += speed;
			}
		}

		switch (Shape)
		{
		case SIN:
		{
			for (int i = 0; i < POINT_NUMBER * 100; i++)
			{
				pointArray[i].x = g_CurvePosition.x + i * WINDOW_X / POINT_NUMBER;
				pointArray[i].y = g_CurvePosition.y + (int)(WINDOW_Y / 2 * (1 - sin(6 * PI * i / POINT_NUMBER)));
			}
		}
		break;
		case ZIGZAG:
		{
			for (int i = 0; i < POINT_NUMBER * 100; i++)
			{
				zigzagArray[i].x = g_CurvePosition.x + i * WINDOW_X / (POINT_NUMBER / 5);

				if (i % 2 == 0)
					zigzagArray[i].y = g_CurvePosition.y + WINDOW_Y / 2 + 50;
				else
					zigzagArray[i].y = g_CurvePosition.y + WINDOW_Y / 2 - 50;
			}
		}
		break;
		case SPRING:
		{
			int i = 0;
			for (float angle = 0.0f; angle <= 1000.0f; angle += 0.1f)
			{
				springArray[i].x = g_CurvePosition.x + 100 + i + 60.0f * cos(angle);
				springArray[i].y = g_CurvePosition.y + WINDOW_Y / 2 + 60.0f * sin(angle);
				i++;
			}
		}
		break;
		}

		if (isCircleGoAlongShpae)
		{
			if (Shape == SIN)
			{
				g_CirclePosition.x = pointArray[index].x;
				g_CirclePosition.y = pointArray[index].y;
			}
			else if (Shape == ZIGZAG)
			{
				g_CirclePosition.x = zigzagArray[index].x;
				g_CirclePosition.y = zigzagArray[index].y;
			}
			else
			{
				g_CirclePosition.x = springArray[index].x;
				g_CirclePosition.y = springArray[index].y;
			}

			if (isCircleMove)
			{
				if (time > 10)
				{
					index++;
					time = 0;
				}
				time++;
			}
		}
		else
		{
			index = 0;
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);
		Rectangle(memdc, crt.left, crt.top, crt.right, crt.bottom);

		{
			if (color[0] == true && color[1] == false && color[2] == false)
			{
				if (color[3]) // 반전이라면
					hPen = CreatePen(PS_SOLID, 1, RGB(0, 255, 255));
				else
					hPen = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
			}
			else if (color[0] == false && color[1] == true && color[2] == false)
			{
				if (color[3]) // 반전이라면
					hPen = CreatePen(PS_SOLID, 1, RGB(255, 0, 255));
				else
					hPen = CreatePen(PS_SOLID, 1, RGB(0, 255, 0));
			}
			else if (color[0] == false && color[1] == false && color[2] == true)
			{
				if (color[3]) // 반전이라면
					hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 0));
				else
					hPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
			}
			else if (color[0] == true && color[1] == true && color[2] == false)
			{
				if (color[3]) // 반전이라면
					hPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
				else
					hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 0));
			}
			else if (color[0] == false && color[1] == true && color[2] == true)
			{
				if (color[3]) // 반전이라면
					hPen = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
				else
					hPen = CreatePen(PS_SOLID, 1, RGB(0, 255, 255));
			}
			else if (color[0] == true && color[1] == false && color[2] == true)
			{
				if (color[3]) // 반전이라면
					hPen = CreatePen(PS_SOLID, 1, RGB(0, 255, 0));
				else
					hPen = CreatePen(PS_SOLID, 1, RGB(255, 0, 255));
			}
			else if (color[0] == true && color[1] == true && color[2] == true)
			{
				if (color[3]) // 반전이라면
					hPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
				else
					hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
			}
		}

		hOldPen = (HPEN)SelectObject(memdc, hPen);

		// TODO:
		switch (Shape)
		{
		case SIN:
		{
			//MoveToEx(memdc, 0, WINDOW_Y / 2, NULL);    // 시작점
			//LineTo(memdc, WINDOW_X, WINDOW_Y / 2);     // 끝점
			Polyline(memdc, pointArray, POINT_NUMBER * 100);     // 선을연속적으로그려준다.
		}
		break;
		case ZIGZAG:
		{
			Polyline(memdc, zigzagArray, POINT_NUMBER * 100);     // 선을연속적으로그려준다.
		}
		break;
		case SPRING:
		{
			Polyline(memdc, springArray, POINT_NUMBER * 100);     // 선을연속적으로그려준다.
		}
		break;
		}

		SelectObject(memdc, hOldPen);
		DeleteObject(hPen);

		hBrush = CreateSolidBrush(COLORREF(RGB(255, 0, 0)));
		hOldBrush = (HBRUSH)SelectObject(memdc, hBrush);
		if (isCircleGoAlongShpae)
		{
			Ellipse(memdc, g_CirclePosition.x - 10, g_CirclePosition.y - 10, g_CirclePosition.x + 10, g_CirclePosition.y + 10);
		}
		SelectObject(memdc, hOldBrush);
		DeleteObject(hBrush);

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
	{
		switch (wParam)
		{
		case 'j': case 'J':
		{
		}
		break;
		case 'e': case 'E':
		{
		}
		break;
		case 's': case 'S':
		{
		}
		break;
		case 't': case 'T':
		{
		}
		break;
		}
	}
	break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_UP:
		{
		}
		break;
		case VK_DOWN:
		{
		}
		break;
		case VK_LEFT:
		{
		}
		break;
		case VK_RIGHT:
		{
		}
		break;
		}
	}
	break;
	case WM_RBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);

		DialogBox(g_hInstance, MAKEINTRESOURCE(IDD_DIALOG6_3), hWnd, (DLGPROC)Dlg6_3Proc);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

BOOL Dlg6_3Proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_INITDIALOG:
	{
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_CHECK_REDLINE:
		{
			if (color[0])
				color[0] = false;
			else
				color[0] = true;
		}
		break;
		case IDC_CHECK_GREENLINE:
		{
			if (color[1])
				color[1] = false;
			else
				color[1] = true;
		}
		break;
		case IDC_CHECK_BLUELINE:
		{
			if (color[2])
				color[2] = false;
			else
				color[2] = true;
		}
		break;
		case IDC_CHECK_REVERSEDCOLOR:
		{
			if (color[3])
				color[3] = false;
			else
				color[3] = true;
		}
		break;
		case IDC_BUTTON1: // 좌측으로 이동하기
		{
			speed = 1;
			isUpDownMove = false;
			g_CurvePosition.y = 0;

			isCircleGoAlongShpae = false;
			circleSpeed = 0;
			isCircleMove = false;

			if (isLeftMove)
				isLeftMove = false;
			else
				isLeftMove = true;
		}
		break;
		case IDC_BUTTON2: // 위 아래로 이동하기
		{
			speed = 1;
			isLeftMove = false;
			g_CurvePosition.x = 0;

			isCircleGoAlongShpae = false;
			circleSpeed = 0;
			isCircleMove = false;

			if (isUpDownMove)
			{
				if (isUp)
					isUp = false;
				else
					isUp = true;
			}
			else
				isUpDownMove = true;
		}
		break;
		case IDC_BUTTON3: // 멈추기
		{
			speed = 0;
			circleSpeed = 0;
			isCircleMove = false;
		}
		break;
		case IDC_BUTTON4: // 리셋
		{
			speed = 1;

			g_CurvePosition.x = 0;
			g_CurvePosition.y = 0;

			for (auto& n : color)
				n = false;

			isLeftMove = false;
			isUpDownMove = false;
			isUp = true;
			isCircleGoAlongShpae = false;

			isCircleMove = true;
			circleSpeed = 0;
		}
		break;
		case IDC_BUTTON5: // 경로를 따라 이동
		{
			if (isCircleGoAlongShpae == false)
				isCircleGoAlongShpae = true;

			isCircleMove = true;
			circleSpeed = 1;

			switch (Shape)
			{
			case SIN:
			{
				g_CirclePosition.x = pointArray[0].x;
				g_CirclePosition.y = pointArray[0].y;
			}
			break;
			case ZIGZAG:
			{
				g_CirclePosition.x = zigzagArray[0].x;
				g_CirclePosition.y = zigzagArray[0].y;
			}
			break;
			case SPRING:
			{
				g_CirclePosition.x = springArray[0].x;
				g_CirclePosition.y = springArray[0].y;
			}
			break;
			}
		}
		break;
		case IDC_RADIO_SIN:
		{
			Shape = SIN;
		}
		break;
		case IDC_RADIO_ZIGZAG:
		{
			Shape = ZIGZAG;
		}
		break;
		case IDC_RADIO_SPRING:
		{
			Shape = SPRING;
		}
		break;
		case IDC_EXIT:
			PostQuitMessage(0);
			break;
		}
	}
	break;
	}

	return 0;
}
