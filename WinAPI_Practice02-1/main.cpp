#include <Windows.h>
#include <time.h>
#include <iostream>

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = L"Window Class Name";
LPCTSTR lpszWindowName = L"Window Program 2-1";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	540
#define WINDOW_X 800
#define WINDOW_Y 600
#define FRAGMENT_NUMBER 4
#define CHAR_HEIGHT 16

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

typedef struct _Point
{
	int center_x;
	int center_y;
} Point;

class CProjectManager
{
private:
	RECT m_Rect[FRAGMENT_NUMBER];
	Point m_Quadrant1;
	Point m_Quadrant2;
	Point m_Quadrant3;
	Point m_Quadrant4;
	COLORREF m_Color[4];
	int numArray[4] = { 0,1,2,3 };

public:
	void Suffle()
	{
		srand(unsigned int(time(NULL)));

		int random_number = 0;
		int temp = 0;

		for (int i = 0; i < 4; i++)
		{
			random_number = rand() % 4;
			temp = numArray[i];
			numArray[i] = numArray[random_number];
			numArray[random_number] = temp;
		}
	}

	void DrawPaint(HDC _hdc)
	{
		DrawX(_hdc, numArray[0]);
		DrawTriangle(_hdc, numArray[1]);
		DrawRhombus(_hdc, numArray[2]);
		DrawButterfly(_hdc, numArray[3]);
	}

	void DrawLine(HDC _hdc)
	{
		for (int idx = 0; idx < FRAGMENT_NUMBER; idx++)
			Rectangle(_hdc, m_Rect[idx].left, m_Rect[idx].top, m_Rect[idx].right, m_Rect[idx].bottom);
	}

	void DrawX(HDC _hdc, int _number) // X
	{
		int startIndex = -7;
		int endIndex = 8;
		Point _Quadrant;

		switch (_number)
		{
		case 0:
			_Quadrant = m_Quadrant1;
			break;
		case 1:
			_Quadrant = m_Quadrant2;
			break;
		case 2:
			_Quadrant = m_Quadrant3;
			break;
		case 3:
			_Quadrant = m_Quadrant4;
			break;
		}

		m_Color[0] = RGB(255, 0, 0);
		SetTextColor(_hdc, RGB(0, 0, 0));
		SetBkColor(_hdc, m_Color[0]);

		for (int idx = startIndex; idx < endIndex; idx++) // ↗
		{
			TextOut(_hdc, _Quadrant.center_x + CHAR_HEIGHT * idx, _Quadrant.center_y - CHAR_HEIGHT * idx, L"□", lstrlen(L"□"));
		}

		for (int idx = startIndex; idx < endIndex; idx++) // ↘
		{
			TextOut(_hdc, _Quadrant.center_x - CHAR_HEIGHT * idx, _Quadrant.center_y - CHAR_HEIGHT * idx, L"□", lstrlen(L"□"));
		}
	}

	void DrawTriangle(HDC _hdc, int _number) // 삼각형
	{
		int startIndex = -7;
		int endIndex = 6;
		Point _Quadrant;

		switch (_number)
		{
		case 0:
			_Quadrant = m_Quadrant1;
			break;
		case 1:
			_Quadrant = m_Quadrant2;
			break;
		case 2:
			_Quadrant = m_Quadrant3;
			break;
		case 3:
			_Quadrant = m_Quadrant4;
			break;
		}

		m_Color[1] = RGB(0, 255, 0);
		SetTextColor(_hdc, RGB(0, 0, 0));
		SetBkColor(_hdc, m_Color[1]);

		for (int height_idx = startIndex; height_idx < endIndex; height_idx++)
		{
			for (int width_idx = 0; width_idx < endIndex + height_idx; width_idx++)
			{
				TextOut(_hdc, _Quadrant.center_x - 96 - CHAR_HEIGHT * height_idx + CHAR_HEIGHT * width_idx, _Quadrant.center_y - CHAR_HEIGHT * height_idx, L"□", lstrlen(L"□"));
				TextOut(_hdc, _Quadrant.center_x + 80 + CHAR_HEIGHT * height_idx - CHAR_HEIGHT * width_idx, _Quadrant.center_y - CHAR_HEIGHT * height_idx, L"□", lstrlen(L"□"));
			}
		}
	}

	void DrawRhombus(HDC _hdc, int _number) // 마름모
	{
		int startIndex = -5;
		int endIndex = 4;
		Point _Quadrant;

		switch (_number)
		{
		case 0:
			_Quadrant = m_Quadrant1;
			break;
		case 1:
			_Quadrant = m_Quadrant2;
			break;
		case 2:
			_Quadrant = m_Quadrant3;
			break;
		case 3:
			_Quadrant = m_Quadrant4;
			break;
		}

		m_Color[2] = RGB(0, 0, 255);
		SetTextColor(_hdc, RGB(0, 0, 0));
		SetBkColor(_hdc, m_Color[2]);

		for (int height_idx = startIndex; height_idx < endIndex; height_idx++)
		{
			for (int width_idx = 0; width_idx < endIndex + height_idx; width_idx++)
			{
				TextOut(_hdc, _Quadrant.center_x - 62 - CHAR_HEIGHT * height_idx + CHAR_HEIGHT * width_idx, _Quadrant.center_y + 60 - CHAR_HEIGHT * height_idx, L"□", lstrlen(L"□"));
				TextOut(_hdc, _Quadrant.center_x + 50 + CHAR_HEIGHT * height_idx - CHAR_HEIGHT * width_idx, _Quadrant.center_y + 60 - CHAR_HEIGHT * height_idx, L"□", lstrlen(L"□"));
			}
		}

		for (int height_idx = startIndex; height_idx < endIndex; height_idx++)
		{
			for (int width_idx = 0; width_idx < endIndex + height_idx; width_idx++)
			{
				TextOut(_hdc, _Quadrant.center_x - 62 - CHAR_HEIGHT * height_idx + CHAR_HEIGHT * width_idx, _Quadrant.center_y - 52 +CHAR_HEIGHT * height_idx, L"□", lstrlen(L"□"));
				TextOut(_hdc, _Quadrant.center_x + 50 + CHAR_HEIGHT * height_idx - CHAR_HEIGHT * width_idx, _Quadrant.center_y - 52 + CHAR_HEIGHT * height_idx, L"□", lstrlen(L"□"));
			}
		}
	}

	void DrawButterfly(HDC _hdc, int _number) // 나비
	{
		int LeftIndex = 5;
		int LeftIndex2 = 0;
		Point _Quadrant;

		switch (_number)
		{
		case 0:
			_Quadrant = m_Quadrant1;
			break;
		case 1:
			_Quadrant = m_Quadrant2;
			break;
		case 2:
			_Quadrant = m_Quadrant3;
			break;
		case 3:
			_Quadrant = m_Quadrant4;
			break;
		}

		m_Color[3] = RGB(0, 0, 0);
		SetTextColor(_hdc, RGB(255, 255, 255));
		SetBkColor(_hdc, m_Color[3]);

		for (int width_idx = LeftIndex * (-1); width_idx < LeftIndex; width_idx++)
		{
			for (int height_idx = (LeftIndex - LeftIndex2) * (-1); height_idx < (LeftIndex - LeftIndex2); height_idx++)
			{
				TextOut(_hdc, _Quadrant.center_x + CHAR_HEIGHT * width_idx, _Quadrant.center_y + CHAR_HEIGHT * height_idx, L"□", lstrlen(L"□"));
			}
			LeftIndex2++;
		}

		int rightIndex = 6;
		int rightIndex2 = 5;

		for (int width_idx = rightIndex; width_idx < rightIndex + 5; width_idx++)
		{
			for (int height_idx = (rightIndex - rightIndex2) * (-1); height_idx < (rightIndex - rightIndex2); height_idx++)
			{
				TextOut(_hdc, _Quadrant.center_x + CHAR_HEIGHT * width_idx - 96, _Quadrant.center_y + CHAR_HEIGHT * height_idx, L"□", lstrlen(L"□"));
			}
			rightIndex2--;
		}
	}

	CProjectManager() // 생성자
	{
		m_Rect[0].left = 0;
		m_Rect[0].top = 0;
		m_Rect[0].right = WINDOW_X / 2;
		m_Rect[0].bottom = WINDOW_Y / 2;

		m_Rect[1].left = WINDOW_X / 2;
		m_Rect[1].top = 0;
		m_Rect[1].right = WINDOW_X;
		m_Rect[1].bottom = WINDOW_Y / 2;

		m_Rect[2].left = 0;
		m_Rect[2].top = WINDOW_Y / 2;
		m_Rect[2].right = WINDOW_X / 2;
		m_Rect[2].bottom = WINDOW_Y;

		m_Rect[3].left = WINDOW_X / 2;
		m_Rect[3].top = WINDOW_Y / 2;
		m_Rect[3].right = WINDOW_X;
		m_Rect[3].bottom = WINDOW_Y;

		m_Quadrant1.center_x = 200;
		m_Quadrant1.center_y = 150;

		m_Quadrant2.center_x = 600;
		m_Quadrant2.center_y = 150;

		m_Quadrant3.center_x = 200;
		m_Quadrant3.center_y = 450;

		m_Quadrant4.center_x = 600;
		m_Quadrant4.center_y = 450;
	}
};

CProjectManager g_Manager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;
	case WM_CHAR:
		hdc = GetDC(hWnd);
		if (wParam == 'q' || wParam == 'Q')
			exit(0);
		if (wParam == 'n' || wParam == 'N')
		{
			g_Manager.Suffle();
			g_Manager.DrawLine(hdc);
			g_Manager.DrawPaint(hdc);
		}
		ReleaseDC(hWnd, hdc);
		break;
	case WM_TIMER:
		hdc = GetDC(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}