//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// WinAPI_Practice06-4.rc에서 사용되고 있습니다.
//
#define IDD_DIALOG6_4                   101
#define IDC_EDIT_BOX                    1001
#define IDC_BUTTON_7                    1002
#define IDC_BUTTON_8                    1003
#define IDC_BUTTON_9                    1004
#define IDC_BUTTON_DIV                  1005
#define IDC_STATIC_TEXT                 1006
#define IDC_BUTTON_4                    1018
#define IDC_BUTTON_5                    1019
#define IDC_BUTTON_6                    1020
#define IDC_BUTTON_MUL                  1021
#define IDC_BUTTON_1                    1022
#define IDC_BUTTON_2                    1023
#define IDC_BUTTON_3                    1024
#define IDC_BUTTON_SUB                  1025
#define IDC_BUTTON_0                    1026
#define IDC_BUTTON_POINT                1027
#define IDC_BUTTON_RESULT               1028
#define IDC_BUTTON_PLUS                 1029
#define IDC_BUTTON_REVERSE              1030
#define IDC_BUTTON_CE                   1031
#define IDC_BUTTON_CLEAR                1032
#define IDC_BUTTON_OCTAL                1033
#define IDC_BUTTON_DECIMAL              1034
#define IDC_BUTTON_DELETE               1035
#define IDC_BUTTON_EXPONENT             1036
#define IDC_BUTTON_DELETE3              1037
#define IDC_BUTTON_END                  1037

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
