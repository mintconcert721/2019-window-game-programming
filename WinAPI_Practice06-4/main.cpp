#include <Windows.h>
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include "resource.h"

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

using namespace std;

#define WINDOW_X 1280
#define WINDOW_Y 720

#define FRAME_X 135
#define FRAME_Y 150

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 6-4";

struct StringInfo {
	string content;
	bool isInteger;
};

#define EXPONENT_MODE 0
#define DECIMAL_MODE 1
#define RESULT_MODE 2

string g_sentence;
vector<StringInfo> container;
char g_Str[256];
int result;
int mode = -1;

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
BOOL Dlg6_4Proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
void InitialSentence();
int MakeFomula(vector<StringInfo>* stl);
long decTooctal(long number);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	// ShowWindow(hWnd, nCmdShow);
	// UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static int startX, startY;
	static bool Drag;
	static int endX, endY;
	HWND hDlg = NULL;

	static HBITMAP hBitmap, hOldBitmap;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		hDlg = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_DIALOG6_4), hWnd, (DLGPROC)Dlg6_4Proc);
		ShowWindow(hDlg, SW_SHOW);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

BOOL Dlg6_4Proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HWND hEdit;

	switch (uMsg)
	{
	case WM_INITDIALOG:
	{
		hEdit = GetDlgItem(hDlg, IDC_EDIT_BOX);
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_BUTTON_1:
		{
			InitialSentence();
			g_sentence += "1";
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
			cout << g_sentence << endl;
		}
		break;
		case IDC_BUTTON_2:
		{
			InitialSentence();
			g_sentence += "2";
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
			cout << g_sentence << endl;
		}
		break;
		case IDC_BUTTON_3:
		{
			InitialSentence();
			g_sentence += "3";
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
			cout << g_sentence << endl;
		}
		break;
		case IDC_BUTTON_4:
		{
			InitialSentence();
			g_sentence += "4";
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
			cout << g_sentence << endl;
		}
		break;
		case IDC_BUTTON_5:
		{
			InitialSentence();
			g_sentence += "5";
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
			cout << g_sentence << endl;
		}
		break;
		case IDC_BUTTON_6:
		{
			InitialSentence();
			g_sentence += "6";
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
			cout << g_sentence << endl;
		}
		break;
		case IDC_BUTTON_7:
		{
			InitialSentence();
			g_sentence += "7";
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
			cout << g_sentence << endl;
		}
		break;
		case IDC_BUTTON_8:
		{
			InitialSentence();
			g_sentence += "8";
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
			cout << g_sentence << endl;
		}
		break;
		case IDC_BUTTON_9:
		{
			InitialSentence();
			g_sentence += "9";
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
			cout << g_sentence << endl;
		}
		break;
		case IDC_BUTTON_0:
		{
			InitialSentence();
			g_sentence += "0";
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
			cout << g_sentence << endl;
		}
		break;
		case IDC_BUTTON_CLEAR:
		{
			g_sentence.clear();
			container.clear();
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
		}
		break;
		case IDC_BUTTON_REVERSE:
		{
			reverse(g_sentence.begin(), g_sentence.end());
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
		}
		break;
		case IDC_BUTTON_DECIMAL: // 10 곱하기
		{
			GetDlgItemText(hDlg, IDC_EDIT_BOX, g_Str, 256);
			int digit = atoi(g_Str);
			result = digit * 10;

			g_sentence.clear();
			_itoa_s((long)result, (char*)g_Str, sizeof(g_Str), 10);
			g_sentence += g_Str;

			mode = EXPONENT_MODE;
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
		}
		break;
		case IDC_BUTTON_PLUS: // 덧셈
		{
			GetDlgItemText(hDlg, IDC_EDIT_BOX, g_Str, 256);
			container.push_back(StringInfo{ g_Str, true });
			container.push_back(StringInfo{ "+", false });
			g_sentence.clear();
			SetDlgItemText(hDlg, IDC_EDIT_BOX, "");
		}
		break;
		case IDC_BUTTON_SUB: // 뺄셈
		{
			GetDlgItemText(hDlg, IDC_EDIT_BOX, g_Str, 256);
			container.push_back(StringInfo{ g_Str, true });
			container.push_back(StringInfo{ "-", false });
			g_sentence.clear();
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
		}
		break;
		case IDC_BUTTON_MUL: // 곱하기
		{
			GetDlgItemText(hDlg, IDC_EDIT_BOX, g_Str, 256);
			container.push_back(StringInfo{ g_Str, true });
			container.push_back(StringInfo{ "*", false });
			g_sentence.clear();
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
		}
		break;
		case IDC_BUTTON_DIV: // 나누기
		{
			GetDlgItemText(hDlg, IDC_EDIT_BOX, g_Str, 256);
			container.push_back(StringInfo{ g_Str, true });
			container.push_back(StringInfo{ "/", false });
			g_sentence.clear();
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
		}
		break;
		case IDC_BUTTON_RESULT: // = (결과)
		{
			GetDlgItemText(hDlg, IDC_EDIT_BOX, g_Str, 256);
			container.push_back(StringInfo{ g_Str, true });
			container.push_back(StringInfo{ "=", false });
			g_sentence.clear();
			result = MakeFomula(&container);
			_itoa_s((long)result, (char*)g_Str, sizeof(g_Str), 10);
			g_sentence += g_Str;
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
		}
		break;
		case IDC_BUTTON_OCTAL:
		{
			GetDlgItemText(hDlg, IDC_EDIT_BOX, g_Str, 256);
			int digit = atoi(g_Str);
			g_sentence.clear();
			decTooctal((long)digit);
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
		}
		break;
		case IDC_BUTTON_CE:
		{
			if (!container.empty())
			{
				vector<StringInfo>::iterator iter = container.end() - 1;
				if (iter != container.begin())
				{
					container.erase(iter--);
				}
				else
				{
					g_sentence.clear();
					SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
				}
			}
		}
		break;
		case IDC_BUTTON_DELETE:
		{
			if (!g_sentence.empty())
			{
				string::iterator iter = g_sentence.end() - 1;
				if (iter != g_sentence.begin())
				{
					g_sentence.erase(iter--);
					SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
				}
				else
				{
					g_sentence.clear();
					SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
				}
			}
		}
		break;
		case IDC_BUTTON_EXPONENT:
		{
			GetDlgItemText(hDlg, IDC_EDIT_BOX, g_Str, 256);
			int digit = atoi(g_Str);
			result = (int)pow(10.0, double(digit));

			g_sentence.clear();
			_itoa_s((long)result, (char*)g_Str, sizeof(g_Str), 10);
			g_sentence += g_Str;

			mode = EXPONENT_MODE;
			SetDlgItemText(hDlg, IDC_EDIT_BOX, g_sentence.c_str());
		}
		break;
		case IDC_BUTTON_END:
		{
			DestroyWindow(hDlg);
			hDlg = NULL;
		}
		break;
		}
	}
	break;
	}
	return 0;
}

void InitialSentence()
{
	if (mode == EXPONENT_MODE || mode == DECIMAL_MODE || mode == RESULT_MODE)
		g_sentence.clear();

	mode = -1;
}

int MakeFomula(vector<StringInfo>* stl)
{
	int digital = 0;
	int left = 0;
	int right = 0;
	int i = 0;

	for (auto iter = stl->begin(); iter != stl->end(); iter++)
	{
		if ((*iter).isInteger)
		{
			digital = atoi((*iter).content.c_str());

			if (i >= 2 && (i % 2 == 0))
			{
				right = digital;
				vector<StringInfo>::iterator prevIter = iter - 1;

				if (strcmp((*prevIter).content.c_str(), "*") == 0)
				{
					left = left * right;
					digital = left;
				}
				else if (strcmp((*prevIter).content.c_str(), "+") == 0)
				{
					left = left + right;
					digital = left;
				}
				else if (strcmp((*prevIter).content.c_str(), "-") == 0)
				{
					left = left - right;
					digital = left;
				}
				else if (strcmp((*prevIter).content.c_str(), "/") == 0)
				{
					left = left / right;
					digital = left;
				}
			}
			else
			{
				left = digital;
			}
		}
		else
		{
			if (strcmp((*iter).content.c_str(), "=") == 0)
			{
				stl->clear();
				mode = RESULT_MODE;
				return digital;
			}
		}

		i++;
	}
}

long decTooctal(long number)
{
	g_sentence.clear();

	if (number < 1)
		return number;
	else
	{
		decTooctal(number / 8);
		_itoa_s((long)(number % 8), (char*)g_Str, sizeof(g_Str), 8);
		g_sentence += g_Str;
	}
}
