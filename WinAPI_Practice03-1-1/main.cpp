#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <crtdbg.h>
#include <iostream>

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 3-1-1";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 1000
#define WINDOW_Y 1000

#define FPS 60
#define BOARD_NUMBER 40

#define RADIUS 20
#define BODY_NUM 20
#define FOOD_NUM 100

typedef struct _Worm {
	POINT m_PrevPosition; // 이전좌표
	POINT m_CurrentPosition; // 중심좌표
	RECT m_Rect;
	int m_Radius = RADIUS;
	int m_Speed = 1;
	bool m_Visible = false;
	int m_Direction;

	void Update();
	void ChangeMove(int _direction);
	_Worm& operator=(const _Worm& rhs);
} Worm;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);
void CALLBACK FoodTimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);

void Init();
float LengthPts(int x1, int y1, int x2, int y2);
void Move(WPARAM wParam, Worm& target);
bool Collide(Worm& _head, Worm& _food);
void TailMove();

enum { UP, DOWN, LEFT, RIGHT };

int g_Length = 0;
int g_Direction = RIGHT;
int g_FoodIndex = 0;

bool isVariationOn = false;
bool isBigger = false;
bool isJump = false;

Worm g_Head;
Worm g_Body[BODY_NUM];
Worm g_Food[FOOD_NUM];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	Init();

	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc;
	PAINTSTRUCT ps;

	int mouseX = 0;
	int mouseY = 0;
	
	static RECT view;
	static HDC  mem1dc, mem2dc;
	static HBITMAP hBit1, hOldBit1;
	static HBITMAP hBit2, hOldBit2;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &view);
		SetTimer(hWnd, 1, FPS, (TIMERPROC)TimerProc); // 1번 아이디의 타이머가 0.01초 마다 TimerProc 타이머 함수 실행
		SetTimer(hWnd, 2, FPS * 100, (TIMERPROC)FoodTimerProc); // 1번 아이디의 타이머가 0.01초 마다 TimerProc 타이머 함수 실행
	}
	break;
	case WM_PAINT:
	{
		GetClientRect(hWnd, &view);
		hdc = BeginPaint(hWnd, &ps);

		mem1dc = CreateCompatibleDC(hdc);
		mem2dc = CreateCompatibleDC(mem1dc);

		if (hBit1 == NULL)
			hBit1 = CreateCompatibleBitmap(hdc, WINDOW_X, WINDOW_Y);

		if (hBit2 == NULL)
			hBit2 = CreateCompatibleBitmap(hdc, WINDOW_X, WINDOW_Y);

		hOldBit1 = (HBITMAP)SelectObject(mem1dc, hBit1);
		hOldBit2 = (HBITMAP)SelectObject(mem2dc, hBit2);

		BitBlt(mem1dc, 0, 0, WINDOW_X, WINDOW_Y, mem2dc, 0, 0, SRCCOPY);

		////////////////////////////////////////////////////// TODO //////////////////////////////////////////////////////
		Rectangle(mem1dc, 0, 0, WINDOW_X, WINDOW_Y); // 뒤의 흰 배경을 그립니다.

		// 보드판을 그립니다.
		for (int j = 0; j < BOARD_NUMBER; j++)
		{
			for (int i = 0; i < BOARD_NUMBER; i++)
			{
				Rectangle(mem1dc, j * RADIUS, i * RADIUS, j * RADIUS + RADIUS, i * RADIUS + RADIUS);
			}
		}

		// 먹이를 그립니다.
		for (auto i = 0; i < FOOD_NUM; i++)
		{
			if (g_Food[i].m_Visible)
			{
				HBRUSH m_Hbrush = CreateSolidBrush(RGB(0, 255, 0));
				HBRUSH m_Oldbrush = (HBRUSH)SelectObject(mem1dc, m_Hbrush);
				Ellipse(mem1dc, g_Food[i].m_CurrentPosition.x * RADIUS, g_Food[i].m_CurrentPosition.y * RADIUS,
					g_Food[i].m_CurrentPosition.x * RADIUS + g_Food[i].m_Radius, g_Food[i].m_CurrentPosition.y * RADIUS + g_Food[i].m_Radius);
				SelectObject(mem1dc, m_Oldbrush);
				DeleteObject(m_Hbrush);
			}
		}

		for (auto i = 0; i < g_Length; i++)
		{
			HBRUSH m_Hbrush = CreateSolidBrush(RGB(0, 0, 255));
			HBRUSH m_Oldbrush = (HBRUSH)SelectObject(mem1dc, m_Hbrush);
			Ellipse(mem1dc, g_Body[i].m_CurrentPosition.x * RADIUS, g_Body[i].m_CurrentPosition.y * RADIUS,
				g_Body[i].m_CurrentPosition.x * RADIUS + g_Body[i].m_Radius, g_Body[i].m_CurrentPosition.y * RADIUS + g_Body[i].m_Radius);
			SelectObject(mem1dc, m_Oldbrush);
			DeleteObject(m_Hbrush);
		}

		// 주인공 머리를 그립니다.
		HBRUSH m_Hbrush = CreateSolidBrush(RGB(255, 0, 0));
		HBRUSH m_Oldbrush = (HBRUSH)SelectObject(mem1dc, m_Hbrush);
		/*Ellipse(mem1dc, g_Head.m_CurrentPosition.x * RADIUS, g_Head.m_CurrentPosition.y * RADIUS,
			g_Head.m_CurrentPosition.x * RADIUS + g_Head.m_Radius, g_Head.m_CurrentPosition.y * RADIUS + g_Head.m_Radius);*/
		Ellipse(mem1dc, g_Head.m_CurrentPosition.x * RADIUS, g_Head.m_CurrentPosition.y * RADIUS,
			g_Head.m_CurrentPosition.x * RADIUS + g_Head.m_Radius, g_Head.m_CurrentPosition.y * RADIUS + g_Head.m_Radius);
		SelectObject(mem1dc, m_Oldbrush);
		DeleteObject(m_Hbrush);

		SetBkMode(mem1dc, TRANSPARENT);

		BitBlt(hdc, 0, 0, WINDOW_X, WINDOW_Y, mem1dc, 0, 0, SRCCOPY);

		SelectObject(mem1dc, hOldBit1);
		SelectObject(mem2dc, hOldBit2);

		DeleteDC(mem2dc);
		DeleteDC(mem1dc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
		if (wParam == 'q' || wParam == 'Q') PostQuitMessage(0);
		else if (wParam == 'o' || wParam == 'O')
		{
			if (isVariationOn) isVariationOn = false;
			else isVariationOn = true;
		}
		else if (wParam == VK_SPACE)
		{
			isJump = true;
			switch (g_Direction)
			{
			case UP:
				if (g_Head.m_CurrentPosition.x < BOARD_NUMBER - 1)
				{
					g_Head.m_CurrentPosition.x += g_Head.m_Speed;
				}
				break;
			case DOWN:
				if (g_Head.m_CurrentPosition.x > 0)
				{
					g_Head.m_CurrentPosition.x -= g_Head.m_Speed;
				}
				break;
			case LEFT:
				if (g_Head.m_CurrentPosition.y > 0)
				{
					g_Head.m_CurrentPosition.y -= g_Head.m_Speed;
				}
				break;
			case RIGHT:
				if (g_Head.m_CurrentPosition.y < BOARD_NUMBER - 1)
				{
					g_Head.m_CurrentPosition.y += g_Head.m_Speed;
				}
				break;
			}
		}
		else if (wParam == '=')
		{
			if(g_Head.m_Speed < 3)
				g_Head.m_Speed += 1;
		}
		else if (wParam == '-')
		{
			if (g_Head.m_Speed > 1)
				g_Head.m_Speed -= 1;
		}
		break;
	case WM_KEYDOWN:
		Move(wParam, g_Head);
		break;
	case WM_KEYUP:
		if (wParam == VK_SPACE)
		{
		}
		break;
	case WM_LBUTTONDOWN:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		break;
	case WM_LBUTTONUP:
		break;
	case WM_MOUSEMOVE:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		break;
	case WM_DESTROY:
		if (hBit1) DeleteObject(hBit1);
		DeleteObject(hBit2);
		KillTimer(hWnd, 1);
		KillTimer(hWnd, 2);
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void Init()
{
	g_Head.m_CurrentPosition.x = BOARD_NUMBER / 2;
	g_Head.m_CurrentPosition.y = BOARD_NUMBER / 2;
	g_Head.m_Radius = RADIUS;

	g_Head.m_Rect.left = g_Head.m_CurrentPosition.x * RADIUS;
	g_Head.m_Rect.top = g_Head.m_CurrentPosition.y * RADIUS;
	g_Head.m_Rect.right = g_Head.m_CurrentPosition.x * RADIUS + g_Head.m_Radius;
	g_Head.m_Rect.bottom = g_Head.m_CurrentPosition.y * RADIUS + g_Head.m_Radius;

	g_Head.m_Speed = 1;
	g_Head.m_Visible = true;
	g_Head.m_Direction = RIGHT;
}

void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
	{
		if (isVariationOn)
		{
			if (isBigger)
			{
				g_Head.m_Radius += 5;
				if (g_Head.m_Radius + 5 > RADIUS * 2)
					isBigger = false;
			}
			else
			{
				g_Head.m_Radius -= 5;
				if (g_Head.m_Radius - 5 < RADIUS)
					isBigger = true;
			}
		}

		for (auto i = 0; i < FOOD_NUM; i++) // 머리와 먹이 충돌
		{
			if (Collide(g_Head, g_Food[i]))
			{
				g_Length++;
			}
		}

		g_Head.Update();

		for (auto i = 0; i < FOOD_NUM; i++)
		{
			if (g_Food[i].m_Visible)
			{
				g_Food[i].Update();
			}
		}



		for (auto i = 0; i < g_Length; i++)
		{
			g_Body[i].Update();
		}
	}
	break;
	}
	InvalidateRgn(hWnd, NULL, FALSE);
}

void CALLBACK FoodTimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
	{
		if (g_FoodIndex < FOOD_NUM)
		{
			srand((unsigned int)time(NULL));

			int x = rand() % BOARD_NUMBER;
			int y = rand() % BOARD_NUMBER;

			g_Food[g_FoodIndex].m_CurrentPosition.x = x;
			g_Food[g_FoodIndex].m_CurrentPosition.y = y;
			g_Food[g_FoodIndex].m_Direction = rand() % 4;
			g_Food[g_FoodIndex].m_Visible = true;

			g_FoodIndex++;
		}
	}
	break;
	}
	InvalidateRgn(hWnd, NULL, FALSE);
}

float LengthPts(int x1, int y1, int x2, int y2)
{
	return (sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1)));
}

void Move(WPARAM wParam, Worm& target)
{
	TailMove();

	switch (wParam)
	{
	case VK_UP:
		if (target.m_CurrentPosition.y - target.m_Speed < 0)
		{
			target.ChangeMove(UP);
		}
		else
		{
			target.m_Direction = UP;
		}
		break;
	case VK_DOWN:
		if (target.m_CurrentPosition.y + target.m_Speed > BOARD_NUMBER - 1)
		{
			target.ChangeMove(DOWN);
		}
		else
		{
			target.m_Direction = DOWN;
		}
		break;
	case VK_LEFT:
		if (target.m_CurrentPosition.x - target.m_Speed < 0)
		{
			target.ChangeMove(LEFT);
		}
		else
		{
			target.m_Direction = LEFT;
		}
		break;
	case VK_RIGHT:
		if (target.m_CurrentPosition.x + target.m_Speed > BOARD_NUMBER - 1)
		{
			target.ChangeMove(RIGHT);
		}
		else
		{
			target.m_Direction = RIGHT;
		}
		break;
	}
}

void _Worm::ChangeMove(int _direction)
{
	srand((unsigned int)time(NULL));

	if (m_CurrentPosition.x <= 0 && m_CurrentPosition.y <= 0)
	{
		m_CurrentPosition.x = 0;
		m_CurrentPosition.y = 0;

		int arr[2] = { DOWN, RIGHT };
		int rand_num = rand() % 2;

		m_Direction = arr[rand_num];

		return;
	}
	else if (m_CurrentPosition.x >= (BOARD_NUMBER - 1) && m_CurrentPosition.y <= 0)
	{
		m_CurrentPosition.x = BOARD_NUMBER - 1;
		m_CurrentPosition.y = 0;

		int arr[2] = { LEFT, DOWN };
		int rand_num = rand() % 2;
		
		m_Direction = arr[rand_num];
		
		return;
	}
	else if (m_CurrentPosition.x <= 0 && m_CurrentPosition.y >= (BOARD_NUMBER - 1))
	{
		m_CurrentPosition.x = 0;
		m_CurrentPosition.y = BOARD_NUMBER - 1;

		int arr[2] = { UP, RIGHT };
		int rand_num = rand() % 2;

		m_Direction = arr[rand_num];

		return;
	}
	else if (m_CurrentPosition.x >= (BOARD_NUMBER - 1) && m_CurrentPosition.y >= (BOARD_NUMBER - 1))
	{
		m_CurrentPosition.x = BOARD_NUMBER - 1;
		m_CurrentPosition.y = BOARD_NUMBER - 1;

		int arr[2] = { UP, LEFT };
		int rand_num = rand() % 2;

		m_Direction = arr[rand_num];

		return;
	}
	else
	{
		switch (m_Direction)
		{
		case UP:
		{
			int arr[3] = { DOWN, LEFT, RIGHT };
			int rand_num = rand() % 3;
			m_Direction = arr[rand_num];
		}
		break;
		case DOWN:
		{
			int arr[3] = { UP, LEFT, RIGHT };
			int rand_num = rand() % 3;
			m_Direction = arr[rand_num];
		}
		break;
		case LEFT:
		{
			int arr[3] = { UP, DOWN, RIGHT };
			int rand_num = rand() % 3;
			m_Direction = arr[rand_num];
		}
		break;
		case RIGHT:
		{
			int arr[3] = { UP, DOWN, LEFT };
			int rand_num = rand() % 3;
			m_Direction = arr[rand_num];
		}
		break;
		}
	}
}

void TailMove()
{
	g_Head.m_PrevPosition = g_Head.m_CurrentPosition;

	for (auto i = 0; i < g_Length; i++)
	{
		g_Body[i].m_PrevPosition = g_Body[i].m_CurrentPosition;
	}

	g_Body[0].m_CurrentPosition = g_Head.m_PrevPosition;

	for (auto i = g_Length - 1; i >= 1; i--)
	{
		g_Body[i].m_CurrentPosition = g_Body[i - 1].m_PrevPosition;
	}
}

_Worm & _Worm::operator=(const _Worm & rhs)
{
	if (this != &rhs)
	{
		m_PrevPosition = rhs.m_PrevPosition;
		m_CurrentPosition = rhs.m_CurrentPosition; // 중심좌표
		m_Rect = rhs.m_Rect;
		m_Radius = rhs.m_Radius;
		m_Speed = rhs.m_Speed;
		m_Visible = rhs.m_Visible;
		m_Direction = rhs.m_Direction;
	}

	return *this;
}

void _Worm::Update()
{
	TailMove();

	switch (m_Direction)
	{
	case UP:
		if (m_CurrentPosition.y - m_Speed < 0)
		{
			ChangeMove(UP);
		}
		else
		{
			m_CurrentPosition.y -= m_Speed;
		}
		break;
	case DOWN:
		if (m_CurrentPosition.y + m_Speed > BOARD_NUMBER - 1)
		{
			ChangeMove(DOWN);
		}
		else
		{
			m_CurrentPosition.y += m_Speed;
		}
		break;
	case LEFT:
		if (m_CurrentPosition.x - m_Speed < 0)
		{
			ChangeMove(LEFT);
		}
		else
		{
			m_CurrentPosition.x -= m_Speed;
		}
		break;
	case RIGHT:
		if (m_CurrentPosition.x + m_Speed > BOARD_NUMBER - 1)
		{
			ChangeMove(RIGHT);
		}
		else
		{
			m_CurrentPosition.x += m_Speed;
		}
		break;
	}

	m_Rect.left = m_CurrentPosition.x * RADIUS;
	m_Rect.top = m_CurrentPosition.y * RADIUS;
	m_Rect.right = m_CurrentPosition.x * RADIUS + m_Radius;
	m_Rect.bottom = m_CurrentPosition.y * RADIUS + m_Radius;
}

bool Collide(Worm& _target1, Worm& _target2)
{
	if (_target1.m_Visible && _target2.m_Visible)
	{
		if ((_target1.m_CurrentPosition.x == _target2.m_CurrentPosition.x) &&
			(_target1.m_CurrentPosition.y == _target2.m_CurrentPosition.y))
		{
			_target2.m_Visible = false;
			return true;
		}
	}

	return false;
}