//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// WinAPI_MapTool_01.rc에서 사용되고 있습니다.
//
#define IDD_DIALOG1                     101
#define IDB_BITMAP_OBJECT1              103
#define IDB_BITMAP_BG1                  104
#define IDB_BITMAP_BG2                  105
#define IDB_BITMAP_BG3                  106
#define IDB_BITMAP_OBJECT2              107
#define IDB_BITMAP_OBJECT3              108
#define IDB_BITMAP_MONSTER1             109
#define IDB_BITMAP_MONSTER2             111
#define IDB_BITMAP_MONSTER3             112
#define IDR_MENU1                       113
#define IDR_MENU2                       114
#define IDC_RADIO_8x8                   1001
#define IDC_RADIO_10x10                 1002
#define IDC_RADIO_15x15                 1003
#define IDC_RADIO_8x9                   1004
#define IDC_RADIO_10x11                 1005
#define IDC_IMAGE_LIST                  1006
#define IDC_EDIT_MAP_GARO               1007
#define IDC_EDIT_MAP_SERO               1008
#define IDC_EDIT_TILE_GARO              1009
#define IDC_EDIT_TILE_SERO              1010
#define IDC_BUTTON_MAP_DRAW             1011
#define IDC_BUTTON_TILE_DRAW            1012
#define IDC_BUTTON_OBJECT1              1013
#define IDC_BUTTON_OBJECT2              1014
#define IDC_BUTTON_OBJECT3              1015
#define IDC_BUTTON_ENEMY1               1016
#define IDC_BUTTON_ENEMY2               1017
#define IDC_BUTTON_ENEMY3               1018
#define IDC_BUTTON_MAP_LOAD             1019
#define IDC_BUTTON_MAP_SAVE             1020
#define IDC_BUTTON_GAME_TEST            1021
#define IDC_BUTTON_EXIT                 1022
#define IDC_GRID_ON                     1023
#define IDC_RADIO2                      1024
#define IDC_GRID_OFF                    1024
#define IDC_BUTTON_IMAGE_INSERT         1025
#define ID_40001                        40001
#define ID_OPEN_DIALOG                  40002
#define ID_40003                        40003
#define ID_TESTWINDOW_EXIT              40004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        115
#define _APS_NEXT_COMMAND_VALUE         40005
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
