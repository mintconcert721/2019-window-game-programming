#include <Windows.h>
#include <iostream>
#include <atlimage.h>
#include <time.h>
#include "resource.h"

using namespace std;

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

#define WINDOW_X 1600
#define WINDOW_Y 1000

#define FRAME_X 135
#define FRAME_Y 150

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "ParentClass";
LPCTSTR lpszChildClass = "ChildClass";

LPCTSTR lpszWindowName = "Window Program EX-1 : 타일 기반의 게임을 위한 맵툴 만들기";

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK DlgMapTool_1Proc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
char* GetFileName(char file_path[]);

TCHAR lpstrFile[100] = "";
char str[256];

OPENFILENAME OFN;
OPENFILENAME SFN;
FILE* fp;

POINT MapSize; // 맵의 크기
POINT GridSize; // 그리드 크기
POINT SIZE_PER_TILE; // 타일 하나의 크기
bool isGridOn; // 그리드 On/Off 유무
int selectIndex = 0;

POINT mouse;

#define BLANK -1

#define BG1 0
#define BG2 1
#define BG3 2

#define OBJ1 3
#define OBJ2 4
#define OBJ3 5

#define MS1 6
#define MS2 7
#define MS3 8

#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3
#define PLAYER_ALONG 4

int MouseClickKind = BLANK;

struct BackgroundInfo
{
	POINT position;
	POINT size;
	int kind;
	CImage image[3];

	BackgroundInfo()
	{
		position.x = 0;
		position.y = 0;
		size.x = 800;
		size.y = 800;
		kind = BG3;

		image[0].Load("BG1.bmp");
		image[1].Load("BG2.bmp");
		image[2].Load("BG3.bmp");
	}

	void Draw(HDC hdc)
	{
		SetStretchBltMode(hdc, COLORONCOLOR);

		switch (kind)
		{
		case BG1:
		{
			image[0].StretchBlt(hdc, position.x, position.y, size.x, size.y, SRCCOPY);
		}
		break;
		case BG2:
		{
			image[1].StretchBlt(hdc, position.x, position.y, size.x, size.y, SRCCOPY);
		}
		break;
		case BG3:
		{
			image[2].StretchBlt(hdc, position.x, position.y, size.x, size.y, SRCCOPY);
		}
		break;
		}
	}
};

struct TileInfo
{
	POINT position;
	POINT size;
	int kind;

	TileInfo()
	{
		position.x = 0;
		position.y = 0;

		size.x = 0;
		size.y = 0;

		kind = BLANK;
	}

	TileInfo& operator = (const TileInfo& rhs)
	{
		position = rhs.position;
		size = rhs.size;
		kind = rhs.kind;
	}
};

BackgroundInfo Background;
TileInfo** Tiles;
CImage image[6];

HWND hChildhWnd;
HWND hMainhWnd;

HBRUSH hBrush;
HBRUSH hOldBrush;

bool IsPointInRectangle(TileInfo rect, POINT mouse);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	// 차일드 윈도우 클래스 등록
	WndClass.lpfnWndProc = (WNDPROC)ChildWndProc;
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	WndClass.lpszMenuName = MAKEINTRESOURCE(IDR_MENU2);
	WndClass.lpszClassName = lpszChildClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hMainhWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 10, 10,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hMainhWnd, nCmdShow);
	UpdateWindow(hMainhWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static HBITMAP hBitmap, hOldBitmap;
	HWND hWndChild;

	HWND hDlg;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);

		MapSize.x = 500;
		MapSize.y = 500;
		GridSize.x = 10;
		GridSize.y = 10;

		image[0].Load("Object1.png");
		image[1].Load("Object2.png");
		image[2].Load("Object3.png");
		image[3].Load("Monster1.png");
		image[4].Load("Monster2.png");
		image[5].Load("Monster3.png");

		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		mouse.x = LOWORD(lParam);
		mouse.y = HIWORD(lParam);

		if (Tiles)
		{
			for (int j = 0; j < GridSize.y; j++)
			{
				for (int i = 0; i < GridSize.x; i++)
				{
					if (Tiles[j])
					{
						if (IsPointInRectangle(Tiles[i][j], mouse))
						{
							// cout << i << ", " << j << endl;
							Tiles[i][j].kind = MouseClickKind;
						}
					}
				}
			}
		}
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case ID_OPEN_DIALOG:
		{
			hDlg = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, (DLGPROC)DlgMapTool_1Proc);
			ShowWindow(hDlg, SW_SHOW);
		}
		break;
		}
	}
	break;
	case WM_TIMER:
	{
		InvalidateRgn(hWnd, NULL, FALSE);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);
		FillRect(memdc, &crt, (HBRUSH)GetStockObject(WHITE_BRUSH)); //도화지 색 변경

		Background.Draw(memdc);
		
		SetStretchBltMode(memdc, COLORONCOLOR);

		if (Tiles)
		{
			for (int j = 0; j < GridSize.y; j++)
			{
				for (int i = 0; i < GridSize.x; i++)
				{
					if (Tiles[j])
					{
						switch (Tiles[i][j].kind)
						{
						case OBJ1:
						{
							image[0].Draw(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y);
							// image[0].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
						}
						break;
						case OBJ2:
						{
							image[1].Draw(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y);
							// image[1].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
						}
						break;
						case OBJ3:
						{
							image[2].Draw(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y);
							// image[2].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
						}
						break;
						case MS1:
						{
							image[3].Draw(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y);
							// image[3].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
						}
						break;
						case MS2:
						{
							image[4].Draw(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y);
							// image[4].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
						}
						break;
						case MS3:
						{
							image[5].Draw(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y);
							// image[5].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
						}
						break;
						}
					}
				}
			}
		}

		if (isGridOn)
		{
			for (int j = 0; j < GridSize.y; j++)
			{
				for (int i = 0; i < GridSize.x; i++)
				{
					hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
					hOldBrush = (HBRUSH)SelectObject(memdc, hBrush);

					int x = (MapSize.x / GridSize.x);
					int y = (MapSize.y / GridSize.y);

					Rectangle(memdc, i * x, j * y, (i + 1) * x, (j + 1) * y);

					SelectObject(memdc, hOldBrush);
					DeleteObject(hBrush);
				}
			}
		}

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
	{
		// 동적할당
		if (Tiles)
		{
			for (int i = 0; i < GridSize.y; i++)
			{
				if (Tiles[i] != nullptr)
				{
					delete[] Tiles[i];
					Tiles[i] = nullptr;
				}
			}

			if (Tiles != nullptr)
			{
				delete[] Tiles;
				Tiles = nullptr;
			}
		}
		
		PostQuitMessage(0);
	}
	break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

BOOL CALLBACK DlgMapTool_1Proc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static HWND hList;
	static HWND hMapGaroButton, hMapSeroButton;
	static HWND hTileGaroButton, hTileSeroButton;

	static HWND hMonsterButton[3];
	static HWND hObjectButton[3];

	static HBITMAP hMonsterButtonImage[3];
	static HBITMAP hObjectButtonImage[3];

	static char Items[][256] = { "벡터", "용암", "종이"};

	switch (iMsg)
	{
	case WM_INITDIALOG:
	{
		hList = GetDlgItem(hDlg, IDC_IMAGE_LIST);

		hMonsterButtonImage[0] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_MONSTER1));
		hMonsterButtonImage[1] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_MONSTER2));
		hMonsterButtonImage[2] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_MONSTER3));
		
		hObjectButtonImage[0] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_OBJECT1));
		hObjectButtonImage[1] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_OBJECT2));
		hObjectButtonImage[2] = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP_OBJECT3));

		hMonsterButton[0] = GetDlgItem(hDlg, IDC_BUTTON_ENEMY1);
		hMonsterButton[1] = GetDlgItem(hDlg, IDC_BUTTON_ENEMY2);
		hMonsterButton[2] = GetDlgItem(hDlg, IDC_BUTTON_ENEMY3);

		hObjectButton[0] = GetDlgItem(hDlg, IDC_BUTTON_OBJECT1);
		hObjectButton[1] = GetDlgItem(hDlg, IDC_BUTTON_OBJECT2);
		hObjectButton[2] = GetDlgItem(hDlg, IDC_BUTTON_OBJECT3);

		for (int idx = 0; idx < 3; idx++)
		{
			SendMessage(hMonsterButton[idx], BM_SETIMAGE, 0, (LPARAM)hMonsterButtonImage[idx]);
			SendMessage(hObjectButton[idx], BM_SETIMAGE, 0, (LPARAM)hObjectButtonImage[idx]);
		}

		for (int i = 0; i < 3; i++)
		{
			SendMessage(hList, LB_ADDSTRING, 0, (LPARAM)Items[i]);
		}
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_BUTTON_GAME_TEST:
		{
			RECT childrt = { 0, 0, MapSize.x, MapSize.y };
			AdjustWindowRect(&childrt, WS_OVERLAPPEDWINDOW, TRUE);
			hChildhWnd = CreateWindow(lpszChildClass, NULL, WS_POPUP | WS_VISIBLE | WS_BORDER,
				100, 10, childrt.right - childrt.left, childrt.bottom - childrt.top, hMainhWnd, NULL, g_hInstance, NULL);
		}
		break;
		case IDC_BUTTON_MAP_SAVE:
		{
			memset(&SFN, 0, sizeof(OPENFILENAME));
			SFN.lStructSize = sizeof(OPENFILENAME);
			SFN.hwndOwner = hDlg;
			SFN.lpstrFilter = TEXT("텍스트 파일(*.txt)\0*.txt\0모든 파일(*.*)\0*.*\0");
			SFN.lpstrFile = lpstrFile;
			SFN.nMaxFile = 256;
			SFN.lpstrInitialDir = ".";
			if (GetSaveFileName(&SFN) != 0)
			{
				strcpy_s(str, sizeof(str), (SFN.lpstrFile));
				char* name = GetFileName(str);

				fopen_s(&fp, name, "w");

				if (fp == NULL)
				{
					cout << "FILE OPEN ERROR!" << endl;
					exit(0);
				}

				// 파일 저장하기
				fprintf_s(fp, "%d %d %d %d %d\n", Background.kind, Background.position.x, Background.position.y, Background.size.x, Background.size.y);
				fprintf_s(fp, "%d %d %d %d\n", MapSize.x, MapSize.y, GridSize.x, GridSize.y);

				if (Tiles)
				{
					for (int j = 0; j < GridSize.y; j++)
					{
						for (int i = 0; i < GridSize.x; i++)
						{
							if (Tiles[j])
							{
								fprintf_s(fp, "%d %d %d %d %d\n", Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, Tiles[i][j].kind);
							}
						}
					}
				}

				fclose(fp);

				MessageBox(hDlg, "파일을 저장하였습니다.", "저장하기", MB_OK);
			}
			else
			{
				MessageBox(hDlg, "저장하기를 취소하셨습니다.", "실패", MB_OK);
			}
		}
		break;
		case IDC_BUTTON_MAP_LOAD:
		{
			memset(&OFN, 0, sizeof(OPENFILENAME));
			OFN.lStructSize = sizeof(OPENFILENAME);
			OFN.hwndOwner = hDlg;
			OFN.lpstrFilter = TEXT("텍스트 파일(*.txt)\0*.txt\0모든 파일(*.*)\0*.*\0");
			OFN.lpstrFile = lpstrFile;
			OFN.nMaxFile = 256;
			OFN.lpstrInitialDir = ".";
			if (GetOpenFileName(&OFN) != 0)
			{
				strcpy_s(str, sizeof(str), (OFN.lpstrFile));
				char* name = GetFileName(str);

				fopen_s(&fp, name, "r");

				if (fp == NULL)
				{
					cout << "FILE OPEN ERROR!" << endl;
					exit(0);
				}

				// 파일 불러오기
				fscanf_s(fp, "%d %d %d %d %d\n", &Background.kind, &Background.position.x, &Background.position.y, &Background.size.x, &Background.size.y);
				fscanf_s(fp, "%d %d %d %d\n", &MapSize.x, &MapSize.y, &GridSize.x, &GridSize.y);

				if (Tiles)
				{
					for (int i = 0; i < GridSize.y; i++)
					{
						if (Tiles[i] != nullptr)
						{
							delete[] Tiles[i];
							Tiles[i] = nullptr;
						}
					}

					if (Tiles != nullptr)
					{
						delete[] Tiles;
						Tiles = nullptr;
					}
				}

				// 동적할당
				if (Tiles == nullptr)
				{
					Tiles = new TileInfo*[GridSize.y];
					for (int i = 0; i < GridSize.y; i++)
					{
						Tiles[i] = new TileInfo[GridSize.x];
					}
				}

				if (Tiles)
				{
					for (int j = 0; j < GridSize.y; j++)
					{
						for (int i = 0; i < GridSize.x; i++)
						{
							if (Tiles[j])
							{
								fscanf_s(fp, "%d %d %d %d %d\n", &Tiles[i][j].position.x, &Tiles[i][j].position.y, &Tiles[i][j].size.x, &Tiles[i][j].size.y, &Tiles[i][j].kind);
							}
						}
					}
				}

				fclose(fp);
			}
			else
			{
				MessageBox(hDlg, "불러오기를 취소하셨습니다.", "실패", MB_OK);
			}
		}
		break;
		case IDC_BUTTON_MAP_DRAW:
		{
			MapSize.x = GetDlgItemInt(hDlg, IDC_EDIT_MAP_GARO, NULL, FALSE);
			MapSize.y = GetDlgItemInt(hDlg, IDC_EDIT_MAP_SERO, NULL, FALSE);

			Background.size.x = MapSize.x;
			Background.size.y = MapSize.y;

			Background.position.x = 0;
			Background.position.y =0;

			// cout << "맵 크기 : " << MapSize.x << ", " << MapSize.y << endl;
		}
		break;
		case IDC_BUTTON_IMAGE_INSERT:
		{
			Background.kind = selectIndex;
		}
		break;
		case IDC_BUTTON_TILE_DRAW:
		{
			if (Tiles)
			{
				for (int i = 0; i < GridSize.y; i++)
				{
					if (Tiles[i] != nullptr)
					{
						delete[] Tiles[i];
						Tiles[i] = nullptr;
					}
				}

				if (Tiles != nullptr)
				{
					delete[] Tiles;
					Tiles = nullptr;
				}
			}

			GridSize.x = GetDlgItemInt(hDlg, IDC_EDIT_TILE_GARO, NULL, FALSE);
			GridSize.y = GetDlgItemInt(hDlg, IDC_EDIT_TILE_SERO, NULL, FALSE);

			// 동적할당
			if (Tiles == nullptr)
			{
				Tiles = new TileInfo*[GridSize.y];
				for (int i = 0; i < GridSize.y; i++)
				{
					Tiles[i] = new TileInfo[GridSize.x];
				}
			}

			int x = (MapSize.x / GridSize.x);
			int y = (MapSize.y / GridSize.y);

			for (int j = 0; j < GridSize.y; j++)
			{
				for (int i = 0; i < GridSize.x; i++)
				{
					Tiles[i][j].position.x = i * x;
					Tiles[i][j].position.y = j * y;
					Tiles[i][j].size.x = x;
					Tiles[i][j].size.y = y;
				}
			}

			// cout << "타일 크기 : " << GridSize.x << ", " << GridSize.y << endl;
		}
		break;
		case IDC_GRID_ON:
		{
			isGridOn = true;
		}
		break;
		case IDC_GRID_OFF:
		{
			isGridOn = false;
		}
		break;
		case IDC_BUTTON_OBJECT1:
		{
			MouseClickKind = OBJ1;
		}
		break;
		case IDC_BUTTON_OBJECT2:
		{
			MouseClickKind = OBJ2;
		}
		break;
		case IDC_BUTTON_OBJECT3:
		{
			MouseClickKind = OBJ3;
		}
		break;
		case IDC_BUTTON_ENEMY1:
		{
			MouseClickKind = MS1;
		}
		break;
		case IDC_BUTTON_ENEMY2:
		{
			MouseClickKind = MS2;
		}
		break;
		case IDC_BUTTON_ENEMY3:
		{
			MouseClickKind = MS3;
		}
		break;
		case IDC_IMAGE_LIST:
		{
			if (HIWORD(wParam) == LBN_SELCHANGE)
			{
				selectIndex = SendMessage(hList, LB_GETCURSEL, 0, 0);
			}
		}
		break;
		case IDC_BUTTON_EXIT:
		{
			PostQuitMessage(0);
			// DestroyWindow(hDlg);
		}
		break;
		case IDCLOSE:
		{
			DestroyWindow(hDlg);
			hDlg = NULL;
		}
		break;
		case IDCANCEL:
		{
			DestroyWindow(hDlg);
			hDlg = NULL;
		}
		break;
		}

	}
	break;
	}
	return 0;
}

struct Player
{
	POINT position;
	POINT size;
	CImage image;

	Player()
	{
		position.x = 0;
		position.y = 0;

		size.x = 0;
		size.y = 0;

		image.Load("Player.png");
	}

	void Draw(HDC hdc)
	{
		image.Draw(hdc, position.x, position.y, size.x, size.y);
	}
};

struct Object
{
	POINT position;
	POINT size;
	int kind;

	Object()
	{
		position.x = 0;
		position.y = 0;

		size.x = 0;
		size.y = 0;

		kind = BLANK;
	}

	Object& operator = (const Object& rhs)
	{
		position = rhs.position;
		size = rhs.size;
		kind = rhs.kind;
	}
};

Player player;

struct Enemy
{
	POINT position;
	POINT size;
	int kind;
	int direction;

	Enemy()
	{
		position.x = 0;
		position.y = 0;

		size.x = 0;
		size.y = 0;

		kind = BLANK;
	}

	void move()
	{
		switch (direction)
		{
		case UP:
		{
			if (position.y > 0)
				position.y -= 1;
			else
				direction = DOWN;
		}
		break;
		case DOWN:
		{
			if (position.y + size.y < MapSize.y)
				position.y += 1;
			else
				direction = UP;
		}
		break;
		case LEFT:
		{
			if (position.x > 0)
				position.x -= 1;
			else
				direction = RIGHT;
		}
		break;
		case RIGHT:
		{
			if (position.x + size.x < MapSize.x)
				position.x += 1;
			else
				direction = LEFT;
		}
		break;
		case PLAYER_ALONG:
		{
			if (position.x < player.position.x)
				position.x += 1;
			else
				position.x -= 1;

			if (position.y < player.position.y)
				position.y += 1;
			else
				position.y -= 1;
		}
		break;
		}
	}

	void Update()
	{
		move();
	}

	Enemy& operator = (const Enemy& rhs)
	{
		position = rhs.position;
		size = rhs.size;
		kind = rhs.kind;
	}
};

bool CheckCollisionPlayerAndEnemy(Player& p, Enemy& e)
{
	if (p.position.x > e.position.x + e.size.x)
	{
		return false;
	}
	if (p.position.x + p.size.x < e.position.x)
	{
		return false;
	}
	if (p.position.y > e.position.y + e.size.y)
	{
		return false;
	}
	if (p.position.y + p.size.y < e.position.y)
	{
		return false;
	}

	return true;
}

bool CheckCollisionPlayerAndObejct(Player& p, Object& o)
{
	if (p.position.x > o.position.x + o.size.x)
	{
		return false;
	}
	if (p.position.x + p.size.x < o.position.x)
	{
		return false;
	}
	if (p.position.y > o.position.y + o.size.y)
	{
		return false;
	}
	if (p.position.y + p.size.y < o.position.y)
	{
		return false;
	}

	return true;
}

int numOfEnemy;
int numOfObject;
int enemyIndex;
int objectIndex;

Enemy* enemies;
Object* objects;

LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc, bitdc;
	static RECT c1crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static HBITMAP hChildBitmap, hChildOldBitmap;

	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &c1crt);

		numOfEnemy = 0;
		numOfObject = 0;
		enemyIndex = 0;
		objectIndex = 0;

		if (Tiles)
		{
			for (int j = 0; j < GridSize.y; j++)
			{
				for (int i = 0; i < GridSize.x; i++)
				{
					if (Tiles[j])
					{
						if (Tiles[j][i].kind == MS1 || Tiles[j][i].kind == MS2 || Tiles[j][i].kind == MS3)
						{
							numOfEnemy += 1;
						}
						else if (Tiles[j][i].kind == OBJ1 || Tiles[j][i].kind == OBJ2 || Tiles[j][i].kind == OBJ3)
						{
							numOfObject += 1;
						}
					}
				}
			}
		}

		player.position.x = 0;
		player.position.y = 0;
		player.size.x = (MapSize.x / GridSize.x) / 2;
		player.size.y = (MapSize.y / GridSize.y) / 2;

		enemies = new Enemy[numOfEnemy];
		objects = new Object[numOfObject];

		if (Tiles)
		{
			for (int j = 0; j < GridSize.y; j++)
			{
				for (int i = 0; i < GridSize.x; i++)
				{
					if (Tiles[j])
					{
						if (Tiles[j][i].kind == MS1 || Tiles[j][i].kind == MS2 || Tiles[j][i].kind == MS3)
						{
							srand((unsigned int)time(NULL));

							enemies[enemyIndex].kind = Tiles[j][i].kind;
							enemies[enemyIndex].position.x = Tiles[j][i].position.x;
							enemies[enemyIndex].position.y = Tiles[j][i].position.y;
							enemies[enemyIndex].size.x = Tiles[j][i].size.x;
							enemies[enemyIndex].size.y = Tiles[j][i].size.y;

							if (enemyIndex % 2 == 0)
								enemies[enemyIndex].direction = PLAYER_ALONG;
							else
								enemies[enemyIndex].direction = rand() % 4;

							enemyIndex++;
						}
						else if (Tiles[j][i].kind == OBJ1 || Tiles[j][i].kind == OBJ2 || Tiles[j][i].kind == OBJ3)
						{
							objects[objectIndex].kind = Tiles[j][i].kind;
							objects[objectIndex].position.x = Tiles[j][i].position.x;
							objects[objectIndex].position.y = Tiles[j][i].position.y;
							objects[objectIndex].size.x = Tiles[j][i].size.x;
							objects[objectIndex].size.y = Tiles[j][i].size.y;
							objectIndex++;
						}
					}
				}
			}
		}

		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_TIMER:
	{
		if (GetAsyncKeyState(VK_UP) & 0x8000)
		{
			if (player.position.y - 5 < 0)
				break;

			player.position.y -= 5;
		}

		if (GetAsyncKeyState(VK_DOWN) & 0x8000)
		{
			if (player.position.y + 5 > MapSize.y)
				break;
			
			player.position.y += 5;
		}
		
		if (GetAsyncKeyState(VK_LEFT) & 0x8000)
		{
			if (player.position.x - 5 < 0)
				break;
			
			player.position.x -= 5;
		}
		
		if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
		{
			if (player.position.x + 5 > MapSize.x)
				break;
			
			player.position.x += 5;
		}

		for (int i = 0; i < numOfEnemy; i++)
		{
			enemies[i].Update();
		}

		for (int i = 0; i < numOfEnemy; i++)
		{
			if (CheckCollisionPlayerAndEnemy(player, enemies[i]) == true)
			{
				if (GetAsyncKeyState(VK_UP) & 0x8000)
				{
					player.position.y += 5;
				}

				if (GetAsyncKeyState(VK_DOWN) & 0x8000)
				{
					player.position.y -= 5;
				}

				if (GetAsyncKeyState(VK_LEFT) & 0x8000)
				{
					player.position.x += 5;
				}

				if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
				{
					player.position.x -= 5;
				}
			}
		}
		cout << "맵 크기 : " << MapSize.x << ", " << MapSize.y << endl;
		for (int i = 0; i < numOfObject; i++)
		{
			if (CheckCollisionPlayerAndObejct(player, objects[i]) == true)
			{
				if (GetAsyncKeyState(VK_UP) & 0x8000)
				{
					player.position.y += 5;
				}

				if (GetAsyncKeyState(VK_DOWN) & 0x8000)
				{
					player.position.y -= 5;
				}

				if (GetAsyncKeyState(VK_LEFT) & 0x8000)
				{
					player.position.x += 5;
				}

				if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
				{
					player.position.x -= 5;
				}
			}
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		bitdc = CreateCompatibleDC(memdc);
		hChildBitmap = CreateCompatibleBitmap(hdc, c1crt.right, c1crt.bottom);
		hChildOldBitmap = (HBITMAP)SelectObject(memdc, hChildBitmap);
		FillRect(memdc, &c1crt, (HBRUSH)GetStockObject(WHITE_BRUSH)); //도화지 색 변경

		// TODO:
		Background.Draw(memdc);

		player.Draw(memdc);

		for (int i = 0; i < numOfEnemy; i++)
		{
			switch(enemies[i].kind)
			{
			case MS1:
			{
				image[3].Draw(memdc, enemies[i].position.x, enemies[i].position.y, enemies[i].size.x, enemies[i].size.y);
				// image[3].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
			}
			break;
			case MS2:
			{
				image[4].Draw(memdc, enemies[i].position.x, enemies[i].position.y, enemies[i].size.x, enemies[i].size.y);
				// image[4].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
			}
			break;
			case MS3:
			{
				image[5].Draw(memdc, enemies[i].position.x, enemies[i].position.y, enemies[i].size.x, enemies[i].size.y);
				// image[5].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
			}
			break;
			}
		}

		if (Tiles)
		{
			for (int j = 0; j < GridSize.y; j++)
			{
				for (int i = 0; i < GridSize.x; i++)
				{
					if (Tiles[j])
					{
						switch (Tiles[i][j].kind)
						{
						case OBJ1:
						{
							image[0].Draw(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y);
							// image[0].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
						}
						break;
						case OBJ2:
						{
							image[1].Draw(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y);
							// image[1].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
						}
						break;
						case OBJ3:
						{
							image[2].Draw(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y);
							// image[2].StretchBlt(memdc, Tiles[i][j].position.x, Tiles[i][j].position.y, Tiles[i][j].size.x, Tiles[i][j].size.y, SRCCOPY);
						}
						break;
						}
					}
				}
			}
		}

		if (isGridOn)
		{
			for (int j = 0; j < GridSize.y; j++)
			{
				for (int i = 0; i < GridSize.x; i++)
				{
					hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
					hOldBrush = (HBRUSH)SelectObject(memdc, hBrush);

					int x = (MapSize.x / GridSize.x);
					int y = (MapSize.y / GridSize.y);

					Rectangle(memdc, i * x, j * y, (i + 1) * x, (j + 1) * y);

					SelectObject(memdc, hOldBrush);
					DeleteObject(hBrush);
				}
			}
		}

		BitBlt(hdc, 0, 0, c1crt.right, c1crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hChildOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case ID_TESTWINDOW_EXIT:
		{
			ShowWindow(hWnd, SW_HIDE);
		}
		break;
		}
	}
	break;
	case WM_DESTROY:
	{
		if (enemies)
			delete[] enemies;

		if (objects)
			delete[] objects;

		PostQuitMessage(0);
	}
	break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

char* GetFileName(char file_path[])
{
	char* FileName = nullptr;
	while (*file_path)
	{
		if (*file_path == '\\' && (file_path + 1) != NULL)
		{
			FileName = file_path + 1;
		}

		file_path++;
	}
	return FileName;
}

bool IsPointInRectangle(TileInfo rect, POINT mouse)
{
	if ((rect.position.x <= mouse.x) && (mouse.x <= (rect.position.x + rect.size.x)) &&
		(rect.position.y <= mouse.y) && (mouse.y <= (rect.position.y + rect.size.y)))
	{
		return true;
	}

	return false;
}
