#pragma once

#include <Windows.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <list>
#include <iterator>

using namespace std;

#define PI 3.141592

// 윈도우 창 크기 설정
#define WINDOW_X 1216
#define WINDOW_Y 839

#define WINDOW_RIGHT_BORDER 1200 // 1184
#define WINDOW_BOTTOM_BORDER 800 // 761

#define STAGE1_LEFT_BORDER 75
#define STAGE1_TOP_BORDER 25
#define STAGE1_RIGHT_BORDER 1075
#define STAGE1_BOTTOM_BORDER 725

#define WINDOW_X_CENTER (WINDOW_RIGHT_BORDER / 2)
#define WINDOW_Y_CENTER (WINDOW_BOTTOM_BORDER / 2)

#define FRAME_X 135
#define FRAME_Y 150

#define UP 0
#define LEFT 1
#define DOWN 2
#define RIGHT 3
#define NONE -1

#define TILE_NUMBER 20
#define EXPLOSION_CIRCLE_NUMBER 5

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CreateBullet(int playerX, int playerY, int dir);
void CreateSpecialBullet(int X, int Y);
void ResetGame();

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program Homework1 : Inversus";

static int g_BulletCount = 6;
static int g_Time = 0;
static int g_TimeEnemyCreate = 0;
static int g_TimeEnemyAnimation = 0;
static int g_Stage = 1;
static int g_PlayTime = 0;
static int g_RespawnTime = 0;
static int g_Score = 0;
static int g_ComboScore = 0;
static int g_ComboTime = 0;
static int g_KilledEnemyNumber = 0;
static int g_PrevKilledEnemyNumber = 0;
bool isInvincibleMode = false;

struct Point {
	int x;
	int y;
	Point() {}
	Point(int _x, int _y)
	{
		x = _x;
		y = _y;
	}
	Point& operator=(const Point& rhs)
	{
		if (this != &rhs)
		{
			x = rhs.x;
			y = rhs.y;
		}

		return *this;
	}
};

struct GradInfo {
	Point prev;
	Point current;

	GradInfo(Point pre, Point cur)
	{
		prev = pre;
		current = cur;
	}

	GradInfo& operator=(const GradInfo& rhs)
	{
		if (this != &rhs)
		{
			prev = rhs.prev;
			current = rhs.current;
		}

		return *this;
	}
};

struct Circle
{
	Point pos;
	int size = 20;
	int time = 0;
	bool isBigger = false;
	bool isVisible = false;
	COLORREF color;
};

class CObject
{
private:
	Point Position;
	int Size = 25;
	bool Explosable = true;
	bool isInvincible = false;
	HPEN hPen;
	HPEN hOldPen;
	HBRUSH hBrush;
	HBRUSH hOldBrush;

public:
	const Point getPosition() const { return Position; }
	const int getSize() const { return Size; }
	const bool getExplosable() const { return Explosable; }
	const bool getInvinsible() const { return isInvincible; }

public:
	void setPosition(int posX, int posY)
	{
		Position.x = posX;
		Position.y = posY;
	}
	void setExplosable(bool value) { Explosable = value; }
	void setInvincible(bool value) { isInvincible = value; }

public:
	void Init(int startX, int startY, bool isBlackTile)
	{
		Position.x = startX;
		Position.y = startY;
		Explosable = isBlackTile;
	}
	void Render(HDC hdc)
	{
		if (isInvincible)
		{
			hPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
			hBrush = CreateSolidBrush(RGB(255, 0, 0));
			hOldPen = (HPEN)SelectObject(hdc, hPen);
			hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
			Rectangle(hdc, Position.x - Size, Position.y - Size, Position.x + Size, Position.y + Size);
			DeleteObject(hPen);
			DeleteObject(hBrush);
			SelectObject(hdc, hOldPen);
			SelectObject(hdc, hOldBrush);

			return;
		}

		if (Explosable)
		{
			hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
			hBrush = CreateSolidBrush(RGB(0, 0, 0));
			hOldPen = (HPEN)SelectObject(hdc, hPen);
			hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
			Rectangle(hdc, Position.x - Size, Position.y - Size, Position.x + Size, Position.y + Size);
			DeleteObject(hPen);
			DeleteObject(hBrush);
			SelectObject(hdc, hOldPen);
			SelectObject(hdc, hOldBrush);
		}
		else
		{
			hPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
			hBrush = CreateSolidBrush(RGB(255, 255, 255));
			hOldPen = (HPEN)SelectObject(hdc, hPen);
			hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
			Rectangle(hdc, Position.x - Size, Position.y - Size, Position.x + Size, Position.y + Size);
			DeleteObject(hPen);
			DeleteObject(hBrush);
			SelectObject(hdc, hOldPen);
			SelectObject(hdc, hOldBrush);
		}
	}
	void Update()
	{
	}

public:
	CObject()
	{
	}
};

CObject objectGroup[TILE_NUMBER][TILE_NUMBER - 6];

class CBullet
{
private:
	Point Position;
	int Speed = 10;
	int Size = 5;
	int Radian = 0;
	int Angle = 0;

	int Direction = NONE;
	bool isVisible = true; // 탄창에서 보여져야 하는가?
	bool isActive = false; // 움직여야 하는가?
	bool isSpecial = false; // 특수 총알인가?

	HBRUSH hBrush;
	HBRUSH hOldBrush;

	HPEN hPen;
	HPEN hOldPen;

	vector<GradInfo> gradation;

public:
	const Point getPosition() const { return Position; }
	const int getSpeed() const { return Speed; }
	const int getSize() const { return Size; }
	const int getDirection() const { return Direction; }
	const bool getVisible() const { return isVisible; }
	const bool getActive() const { return isActive; }
	const bool getSpecial() const { return isSpecial; }
	const int getAngle() const { return Angle; }
	const int getRadian() const { return Radian; }

public:
	void setPosition(int posX, int posY)
	{
		Position.x = posX;
		Position.y = posY;
	}
	void setDirection(int dir) { Direction = dir; }
	void setVisible(bool value) { isVisible = value; }
	void setActive(bool value) { isActive = value; }
	void setSpecial(bool value) { isSpecial = value; }
	void VariateAngle(int value) { Angle += value; }
	void VariateRadian(int value) { Radian = value; }

	void MoveLeft() { Position.x -= Speed; }
	void MoveRight() { Position.x += Speed; }
	void MoveUp() { Position.y -= Speed; }
	void MoveDown() { Position.y += Speed; }

public:
	bool CheckCollision(CObject& _obj)
	{
		return false;
	}

public:
	void Init(int x, int y)
	{
		Position.x = x;
		Position.y = y;

		gradation.emplace_back(GradInfo(Point(Position.x, Position.y), Point(Position.x, Position.y)));
		gradation.emplace_back(GradInfo(Point(Position.x, Position.y), Point(Position.x, Position.y)));
		gradation.emplace_back(GradInfo(Point(Position.x, Position.y), Point(Position.x, Position.y)));
		gradation.emplace_back(GradInfo(Point(Position.x, Position.y), Point(Position.x, Position.y)));
		gradation.emplace_back(GradInfo(Point(Position.x, Position.y), Point(Position.x, Position.y)));
	}
	void Render(HDC hdc)
	{
		if (isActive)
		{
			if (!gradation.empty())
			{
				switch (Direction)
				{
				case UP:
				{
					for (int i = 0; i < 5; i++)
					{
						if (i == 0)
						{
							hBrush = CreateSolidBrush(RGB(0, 0, 0));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 1)
						{
							hBrush = CreateSolidBrush(RGB(50, 50, 50));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 2)
						{
							hBrush = CreateSolidBrush(RGB(100, 100, 100));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 3)
						{
							hBrush = CreateSolidBrush(RGB(150, 150, 150));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else
						{
							hBrush = CreateSolidBrush(RGB(200, 200, 200));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}

						hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
						hOldPen = (HPEN)SelectObject(hdc, hPen);
						Rectangle(hdc, Position.x - Size, Position.y - Size + (Size * 2) * i, Position.x + Size, Position.y + Size + (Size * 2) * i);
						SelectObject(hdc, hOldPen);
						SelectObject(hdc, hOldBrush);
						DeleteObject(hPen);
						DeleteObject(hBrush);
					}
				}
				break;
				case DOWN:
				{
					for (int i = 0; i < 5; i++)
					{
						if (i == 0)
						{
							hBrush = CreateSolidBrush(RGB(0, 0, 0));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 1)
						{
							hBrush = CreateSolidBrush(RGB(50, 50, 50));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 2)
						{
							hBrush = CreateSolidBrush(RGB(100, 100, 100));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 3)
						{
							hBrush = CreateSolidBrush(RGB(150, 150, 150));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else
						{
							hBrush = CreateSolidBrush(RGB(200, 200, 200));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}

						hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
						hOldPen = (HPEN)SelectObject(hdc, hPen);
						Rectangle(hdc, Position.x - Size, Position.y - Size - (Size * 2) * i, Position.x + Size, Position.y + Size - (Size * 2) * i);
						SelectObject(hdc, hOldPen);
						SelectObject(hdc, hOldBrush);
						DeleteObject(hPen);
						DeleteObject(hBrush);
					}
				}
				break;
				case LEFT:
				{
					for (int i = 0; i < 5; i++)
					{
						if (i == 0)
						{
							hBrush = CreateSolidBrush(RGB(0, 0, 0));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 1)
						{
							hBrush = CreateSolidBrush(RGB(50, 50, 50));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 2)
						{
							hBrush = CreateSolidBrush(RGB(100, 100, 100));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 3)
						{
							hBrush = CreateSolidBrush(RGB(150, 150, 150));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else
						{
							hBrush = CreateSolidBrush(RGB(200, 200, 200));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}

						hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
						hOldPen = (HPEN)SelectObject(hdc, hPen);
						Rectangle(hdc, Position.x - Size + (Size * 2) * i, Position.y - Size, Position.x + Size + (Size * 2) * i, Position.y + Size);
						SelectObject(hdc, hOldPen);
						SelectObject(hdc, hOldBrush);
						DeleteObject(hPen);
						DeleteObject(hBrush);
					}
				}
				break;
				case RIGHT:
				{
					for (int i = 0; i < 5; i++)
					{
						if (i == 0)
						{
							hBrush = CreateSolidBrush(RGB(0, 0, 0));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 1)
						{
							hBrush = CreateSolidBrush(RGB(50, 50, 50));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 2)
						{
							hBrush = CreateSolidBrush(RGB(100, 100, 100));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else if (i == 3)
						{
							hBrush = CreateSolidBrush(RGB(150, 150, 150));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}
						else
						{
							hBrush = CreateSolidBrush(RGB(200, 200, 200));
							hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
						}

						hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
						hOldPen = (HPEN)SelectObject(hdc, hPen);
						Rectangle(hdc, Position.x - Size - (Size * 2) * i, Position.y - Size, Position.x + Size - (Size * 2) * i, Position.y + Size);
						SelectObject(hdc, hOldPen);
						SelectObject(hdc, hOldBrush);
						DeleteObject(hPen);
						DeleteObject(hBrush);
					}
				}
				break;
				}
			}
		}

		if (isVisible)
		{
			if (isSpecial)
			{
				hBrush = CreateSolidBrush(RGB(0, 255, 255));
				hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
				Rectangle(hdc, Position.x - Size, Position.y - Size, Position.x + Size, Position.y + Size);
				DeleteObject(hBrush);
				SelectObject(hdc, hOldBrush);
			}
			else
			{
				hBrush = CreateSolidBrush(RGB(255, 255, 255));
				hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
				Ellipse(hdc, Position.x - Size, Position.y - Size, Position.x + Size, Position.y + Size);
				DeleteObject(hBrush);
				SelectObject(hdc, hOldBrush);
			}
		}
	}
	void Update()
	{
		if (isActive)
		{
			if (!gradation.empty())
			{
				for (int i = 0; i < 5; i++)
				{
					gradation[i].prev = gradation[i].current;
				}

				gradation[0].current = Position;

				for (int i = 0; i < 4; i++)
				{
					gradation[i + 1].current = gradation[i].prev;
				}
			}

			if (Position.y + Size > STAGE1_BOTTOM_BORDER)
			{
				isVisible = false;
				isActive = false;
			}
			if (Position.y - Size < STAGE1_TOP_BORDER)
			{
				isVisible = false;
				isActive = false;
			}
			if (Position.x + Size > STAGE1_RIGHT_BORDER)
			{
				isVisible = false;
				isActive = false;
			}
			if (Position.x - Size < STAGE1_LEFT_BORDER)
			{
				isVisible = false;
				isActive = false;
			}

			switch (Direction)
			{
			case UP:
			{
				MoveUp();
			}
			break;
			case DOWN:
			{
				MoveDown();
			}
			break;
			case LEFT:
			{
				MoveLeft();
			}
			break;
			case RIGHT:
			{
				MoveRight();
			}
			break;
			}
		}
	}

public:
	CBullet& operator=(const CBullet& rhs)
	{
		if (this != &rhs)
		{
			Position.x = rhs.Position.x;
			Position.y = rhs.Position.y;
			Speed = rhs.Speed;
			Size = rhs.Size;
			Direction = rhs.Direction;
			hBrush = rhs.hBrush;
			hOldBrush = rhs.hOldBrush;
			isVisible = rhs.isVisible;
			isActive = rhs.isActive;
			isSpecial = rhs.isSpecial;
			gradation = rhs.gradation;
		}

		return *this;
	}

public:
	CBullet() {}
	CBullet(int x, int y)
	{
		Position.x = x;
		Position.y = y;
	}
	CBullet(const CBullet& rhs)
	{
		Position.x = rhs.Position.x;
		Position.y = rhs.Position.y;
		Speed = rhs.Speed;
		Size = rhs.Size;
		Direction = rhs.Direction;
		hBrush = rhs.hBrush;
		hOldBrush = rhs.hOldBrush;
		isVisible = rhs.isVisible;
		isActive = rhs.isActive;
		isSpecial = rhs.isSpecial;
		gradation = rhs.gradation;
	}
};

vector<CBullet> magazine;

class CHero
{
private:
	Point Position;
	int Speed = 5;
	int Size = 20;
	int Direction;
	HBRUSH hBrush;
	HBRUSH hOldBrush;
	bool mustExplosive = false;
	bool mustRespawn = false;
	int Life = 3;
	int ExplosiveSize = 30;
	Circle effectCircle[EXPLOSION_CIRCLE_NUMBER];

public:
	const Point getPosition() const { return Position; }
	const int getSpeed() const { return Speed; }
	const int getSize() const { return Size; }
	const int getDirection() const { return Direction; }
	const bool getMustExplosive() const { return mustExplosive; }
	const int getLife() const { return Life; }
	const bool getMustRespawn() const { return mustRespawn; }
	const int getExplosiveSize() const { return ExplosiveSize; }

public:
	void setPosition(int posX, int posY)
	{
		Position.x = posX;
		Position.y = posY;
	}
	void setDirection(int dir) { Direction = dir; }
	void setMustExplosive(bool value) { mustExplosive = value; }
	void setLife(int life) { Life += life; }
	void setMustRespawn(bool value) { mustRespawn = value; }
	void setExplosiveSize(int size) { ExplosiveSize = size; }

	void MoveLeft() { Position.x -= Speed; }
	void MoveRight() { Position.x += Speed; }
	void MoveUp() { Position.y -= Speed; }
	void MoveDown() { Position.y += Speed; }

public:
	void Init()
	{
		Position.x = WINDOW_X_CENTER;
		Position.y = WINDOW_Y_CENTER;
		Direction = NONE;
	}
	void RenderExlosion(HDC hdc)
	{
		if (mustExplosive)
		{
			for (int i = 0; i < EXPLOSION_CIRCLE_NUMBER; i++)
			{
				if (effectCircle[i].isVisible)
				{
					hBrush = CreateSolidBrush(effectCircle[i].color);
					hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
					Ellipse(hdc, effectCircle[i].pos.x - effectCircle[i].size, effectCircle[i].pos.y - effectCircle[i].size, effectCircle[i].pos.x + effectCircle[i].size, effectCircle[i].pos.y + effectCircle[i].size);
					DeleteObject(hBrush);
					SelectObject(hdc, hOldBrush);
				}
			}
		}
	}
	void Render(HDC hdc)
	{
		if (Life > 0)
		{
			if (!mustExplosive)
			{
				hBrush = CreateSolidBrush(RGB(0, 0, 0));
				hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
				RoundRect(hdc, Position.x - Size, Position.y - Size, Position.x + Size, Position.y + Size, 10, 10);
				DeleteObject(hBrush);
				SelectObject(hdc, hOldBrush);
			}
		}
	}
	void UpdateExplosion()
	{
		if (mustExplosive)
		{
			for (int i = 0; i < EXPLOSION_CIRCLE_NUMBER; i++)
			{
				if (effectCircle[i].isVisible)
				{
					if (effectCircle[i].isBigger)
					{
						effectCircle[i].size += 2;
						if (effectCircle[i].size > 15)
							effectCircle[i].isBigger = false;
					}
					else
					{
						effectCircle[i].size -= 2;
						if (i < EXPLOSION_CIRCLE_NUMBER - 1)
						{
							effectCircle[i + 1].isVisible = true;
							effectCircle[i + 1].isBigger = true;
						}

						if (effectCircle[i].size < 1)
						{
							effectCircle[i].isVisible = false;
							if (i == EXPLOSION_CIRCLE_NUMBER - 1)
							{
								mustExplosive = false;
								mustRespawn = true;
								ResetGame();
							}
						}
					}
				}
			}
		}
	}
	void Update()
	{
		if ((GetAsyncKeyState('W') & 0x8000) || (GetAsyncKeyState('w') & 0x8000))
		{
			if (Position.y - Size > STAGE1_TOP_BORDER)
			{
				Position.y -= Speed;
			}
			else
				Position.y = STAGE1_TOP_BORDER + Size;
		}
		if ((GetAsyncKeyState('S') & 0x8000) || (GetAsyncKeyState('s') & 0x8000))
		{
			if (Position.y + Size < STAGE1_BOTTOM_BORDER)
			{
				Position.y += Speed;
			}
			else
				Position.y = STAGE1_BOTTOM_BORDER - Size;
		}
		if ((GetAsyncKeyState('A') & 0x8000) || (GetAsyncKeyState('a') & 0x8000))
		{
			if (Position.x - Size > STAGE1_LEFT_BORDER)
			{
				Position.x -= Speed;
			}
			else
				Position.x = STAGE1_LEFT_BORDER + Size;
		}
		if ((GetAsyncKeyState('D') & 0x8000) || (GetAsyncKeyState('d') & 0x8000))
		{
			if (Position.x + Size < STAGE1_RIGHT_BORDER)
			{
				Position.x += Speed;
			}
			else
				Position.x = STAGE1_RIGHT_BORDER - Size;
		}
	}

	void ShootBullet()
	{
		if (Life > 0)
		{
			if(!mustExplosive)
			{
				CreateBullet(Position.x, Position.y, Direction);
			}
		}
	}

	void ExplosionEffect(const int x, const int y, bool value)
	{
		for (int i = 0; i < EXPLOSION_CIRCLE_NUMBER; i++)
		{
			srand((unsigned int)time(NULL));
			effectCircle[i].size = rand() % 5 + 5;
			int r = rand() % 256;
			int g = rand() % 256;
			int b = rand() % 256;
			effectCircle[i].color = RGB(r, g, b);
		}

		effectCircle[0].pos.x = x - 15;
		effectCircle[0].pos.y = y - 15;
		effectCircle[1].pos.x = x + 15;
		effectCircle[1].pos.y = y + 15;
		effectCircle[2].pos.x = x - 15;
		effectCircle[2].pos.y = y + 15;
		effectCircle[3].pos.x = x + 15;
		effectCircle[3].pos.y = y - 15;
		effectCircle[4].pos.x = x;
		effectCircle[4].pos.y = y;

		effectCircle[0].isVisible = true;
		effectCircle[0].isBigger = true;

		mustExplosive = value;
	}

public:
	CHero()
	{
		Position.x = WINDOW_X_CENTER;
		Position.y = WINDOW_Y_CENTER;
		Direction = NONE;
	}
};

CHero hero;

list<CBullet> playerBulletList;
list<CBullet> specialBulletList;

bool CheckCollisionHeroNObject(CHero& _hero, CObject& _obj);
bool CheckCollisionBulletNObject(CBullet& _target, CObject& _obj);

void CreateBullet(int playerX, int playerY, int dir)
{
	CBullet Bullet = CBullet(playerX, playerY);
	Bullet.setDirection(dir);
	Bullet.setActive(true);
	Bullet.setSpecial(false);
	Bullet.Init(playerX, playerY);
	playerBulletList.emplace_back(Bullet);
}

void CreateSpecialBullet(int X, int Y)
{
	CBullet Bullet = CBullet(X, Y);
	Bullet.setDirection(NONE);
	Bullet.setVisible(true);
	Bullet.setSpecial(true);
	specialBulletList.emplace_back(Bullet);
}

class CEnemy
{
private:
	Point Position;
	int Speed = 1;
	int Size = 60;

	bool isVisible = true;
	bool isAnimation = true;
	bool mustExplosive = false;

	HBRUSH hBrush;
	HBRUSH hOldBrush;

	int i;
	int j;

	int ExplosiveSize = 25;
	Circle effectCircle[EXPLOSION_CIRCLE_NUMBER];

public:
	const Point getPosition() const { return Position; }
	const int getSpeed() const { return Speed; }
	const int getSize() const { return Size; }
	const int getExplosiveSize() const { return ExplosiveSize; }
	const bool getVisible() const { return isVisible; }
	const bool getAnimation() const { return isAnimation; }
	const bool getMustExplosive() const { return mustExplosive; }

public:
	void setSize(int size) { Size = size; }
	void setExplosiveSize(int size) { ExplosiveSize = size; }
	void setPosition(int posX, int posY)
	{
		Position.x = posX;
		Position.y = posY;
	}
	void setVisible(bool value) { isVisible = value; }
	void setAnimation(bool value) { isAnimation = value; }
	void setMustExplosive(bool value) { mustExplosive = value; }

public:
	void Init(int x, int y)
	{
		Position.x = x;
		Position.y = y;
	}
	void RenderExlosion(HDC hdc)
	{
		if (mustExplosive)
		{
			for (int i = 0; i < EXPLOSION_CIRCLE_NUMBER; i++)
			{
				if (effectCircle[i].isVisible)
				{
					hBrush = CreateSolidBrush(effectCircle[i].color);
					hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
					Ellipse(hdc, effectCircle[i].pos.x - effectCircle[i].size, effectCircle[i].pos.y - effectCircle[i].size, effectCircle[i].pos.x + effectCircle[i].size, effectCircle[i].pos.y + effectCircle[i].size);
					DeleteObject(hBrush);
					SelectObject(hdc, hOldBrush);
				}
			}
		}
	}
	void Render(HDC hdc)
	{
		if (isVisible)
		{
			SetROP2(hdc, R2_MASKPEN);
			hBrush = CreateHatchBrush(HS_BDIAGONAL, RGB(255, 0, 0));
			hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
			Rectangle(hdc, Position.x - 3 * ExplosiveSize, Position.y - 3 * ExplosiveSize, Position.x - ExplosiveSize, Position.y - ExplosiveSize); // 왼쪽 상단
			Rectangle(hdc, Position.x - ExplosiveSize, Position.y - 3 * ExplosiveSize, Position.x + ExplosiveSize, Position.y - ExplosiveSize); // 위쪽 상단
			Rectangle(hdc, Position.x + ExplosiveSize, Position.y - 3 * ExplosiveSize, Position.x + 3 * ExplosiveSize, Position.y - ExplosiveSize); // 오른쪽 상단

			Rectangle(hdc, Position.x - 3 * ExplosiveSize, Position.y - ExplosiveSize, Position.x - ExplosiveSize, Position.y + ExplosiveSize); // 왼쪽 중앙
			Rectangle(hdc, Position.x - ExplosiveSize, Position.y - ExplosiveSize, Position.x + ExplosiveSize, Position.y + ExplosiveSize); // 중앙
			Rectangle(hdc, Position.x + ExplosiveSize, Position.y - ExplosiveSize, Position.x + 3 * ExplosiveSize, Position.y + ExplosiveSize); // 오른쪽 중앙

			Rectangle(hdc, Position.x - 3 * ExplosiveSize, Position.y + ExplosiveSize, Position.x - ExplosiveSize, Position.y + 3 * ExplosiveSize); // 왼쪽 아래
			Rectangle(hdc, Position.x - ExplosiveSize, Position.y + ExplosiveSize, Position.x + ExplosiveSize, Position.y + 3 * ExplosiveSize); // 아래 중앙
			Rectangle(hdc, Position.x + ExplosiveSize, Position.y + ExplosiveSize, Position.x + 3 * ExplosiveSize, Position.y + 3 * ExplosiveSize); // 오른쪽 아래
			DeleteObject(hBrush);
			SelectObject(hdc, hOldBrush);

			SetROP2(hdc, R2_COPYPEN);
			hBrush = CreateSolidBrush(RGB(255, 0, 0));
			hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
			RoundRect(hdc, Position.x - Size, Position.y - Size, Position.x + Size, Position.y + Size, 10, 10);
			DeleteObject(hBrush);
			SelectObject(hdc, hOldBrush);
		}
	}
	void UpdateExplosion()
	{
		if (mustExplosive)
		{
			for (int i = 0; i < EXPLOSION_CIRCLE_NUMBER; i++)
			{
				if (effectCircle[i].isVisible)
				{
					if (effectCircle[i].isBigger)
					{
						effectCircle[i].size += 2;
						if (effectCircle[i].size > 15)
							effectCircle[i].isBigger = false;
					}
					else
					{
						effectCircle[i].size -= 2;
						if (i < EXPLOSION_CIRCLE_NUMBER - 1)
						{
							effectCircle[i + 1].isVisible = true;
							effectCircle[i + 1].isBigger = true;
						}

						if (effectCircle[i].size < 1)
						{
							effectCircle[i].isVisible = false;
							if (i == EXPLOSION_CIRCLE_NUMBER - 1)
							{
								mustExplosive = false;
								g_KilledEnemyNumber++;
								g_ComboScore++;
								g_ComboTime = 0;
								g_Score += 100;
							}
						}
					}
				}
			}
		}
	}
	void Update(int X, int Y)
	{
		if (isAnimation)
		{
			if (Size > 20)
				Size -= 1;
			else
				isAnimation = false;
		}

		if (isAnimation == false && isVisible == true)
		{
			setTileBlackAlongEnemyPath();

			if (Position.x < X)
			{
				Position.x += Speed;
			}
			else
			{
				Position.x -= Speed;
			}

			if (Position.y < Y)
			{
				Position.y += Speed;
			}
			else
			{
				Position.y -= Speed;
			}
		}
	}

public:
	bool CheckCollisionEnemyNObject(CObject& _obj)
	{
		if (Position.x - Size > _obj.getPosition().x + _obj.getSize())
		{
			return false;
		}
		if (Position.x + Size < _obj.getPosition().x - _obj.getSize())
		{
			return false;
		}
		if (Position.y - Size > _obj.getPosition().y + _obj.getSize())
		{
			return false;
		}
		if (Position.y + Size < _obj.getPosition().y - _obj.getSize())
		{
			return false;
		}

		return true;
	}

	void setTileBlackAlongEnemyPath()
	{
		for (int b = 0; b < TILE_NUMBER - 6; b++)
		{
			for (int a = 0; a < TILE_NUMBER; a++)
			{
				if (CheckCollisionEnemyNObject(objectGroup[a][b]))
				{
					objectGroup[a][b].setExplosable(true);
				}
			}
		}
	}
	void ExplosionEffect(const int x, const int y, bool value)
	{
		for (int i = 0; i < EXPLOSION_CIRCLE_NUMBER; i++)
		{
			srand((unsigned int)time(NULL));
			effectCircle[i].size = rand() % 5 + 5;
			int r = rand() % 256;
			int g = rand() % 256;
			int b = rand() % 256;
			effectCircle[i].color = RGB(r, g, b);
		}

		effectCircle[0].pos.x = x - 25;
		effectCircle[0].pos.y = y - 25;
		effectCircle[1].pos.x = x + 25;
		effectCircle[1].pos.y = y + 25;
		effectCircle[2].pos.x = x - 25;
		effectCircle[2].pos.y = y + 25;
		effectCircle[3].pos.x = x + 25;
		effectCircle[3].pos.y = y - 25;
		effectCircle[4].pos.x = x;
		effectCircle[4].pos.y = y;

		effectCircle[0].isVisible = true;
		effectCircle[0].isBigger = true;

		mustExplosive = value;
	}

public:
	CEnemy& operator=(const CEnemy& rhs)
	{
		if (this != &rhs)
		{
			Position.x = rhs.Position.x;
			Position.y = rhs.Position.y;
			Speed = rhs.Speed;
			Size = rhs.Size;
			hBrush = rhs.hBrush;
			hOldBrush = rhs.hOldBrush;
			isVisible = rhs.isVisible;
		}

		return *this;
	}

public:
	CEnemy() {}
	CEnemy(int x, int y)
	{
		Position.x = x;
		Position.y = y;
	}
	CEnemy(const CEnemy& rhs)
	{
		Position.x = rhs.Position.x;
		Position.y = rhs.Position.y;
		Speed = rhs.Speed;
		Size = rhs.Size;
		hBrush = rhs.hBrush;
		hOldBrush = rhs.hOldBrush;
		isVisible = rhs.isVisible;
	}
};

bool CheckCollisionBulletNEnemy(CBullet& _target, CEnemy& _obj);
bool CheckCollisionHeroNEnemy(CHero& _target, CEnemy& _obj);

list<CEnemy> enemyList;

void CreateEnemy()
{
	srand((unsigned int)time(NULL));
	int newX = (rand() % (STAGE1_RIGHT_BORDER - STAGE1_LEFT_BORDER)) + STAGE1_LEFT_BORDER;
	int newY = (rand() % (STAGE1_BOTTOM_BORDER - STAGE1_TOP_BORDER)) + STAGE1_TOP_BORDER;
	CEnemy enemy = CEnemy(newX, newY);
	enemy.setVisible(true);
	enemy.setAnimation(true);
	enemyList.emplace_back(enemy);
}

// 게임 전역 체크
bool CheckCollisionHeroNObject(CHero& _hero, CObject& _obj)
{
	if (!isInvincibleMode)
	{
		if (_hero.getPosition().x - _hero.getSize() > _obj.getPosition().x + _obj.getSize())
		{
			return false;
		}
		if (_hero.getPosition().x + _hero.getSize() < _obj.getPosition().x - _obj.getSize())
		{
			return false;
		}
		if (_hero.getPosition().y - _hero.getSize() > _obj.getPosition().y + _obj.getSize())
		{
			return false;
		}
		if (_hero.getPosition().y + _hero.getSize() < _obj.getPosition().y - _obj.getSize())
		{
			return false;
		}

		return true;
	}
	else
	{
		return false;
	}
}

bool CheckCollisionBulletNObject(CBullet& _target, CObject& _obj)
{
	if (_target.getVisible() && _obj.getExplosable())
	{
		if (_target.getPosition().x - _target.getSize() > _obj.getPosition().x + _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().x + _target.getSize() < _obj.getPosition().x - _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().y - _target.getSize() > _obj.getPosition().y + _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().y + _target.getSize() < _obj.getPosition().y - _obj.getSize())
		{
			return false;
		}

		return true;
	}

	return false;
}

bool CheckCollisionBulletNEnemy(CBullet& _target, CEnemy& _obj)
{
	if (_target.getVisible() && _obj.getVisible())
	{
		if (_target.getPosition().x - _target.getSize() > _obj.getPosition().x + _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().x + _target.getSize() < _obj.getPosition().x - _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().y - _target.getSize() > _obj.getPosition().y + _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().y + _target.getSize() < _obj.getPosition().y - _obj.getSize())
		{
			return false;
		}

		return true;
	}

	return false;
}

bool CheckCollisionHeroNEnemy(CHero& _target, CEnemy& _obj)
{
	if (!isInvincibleMode)
	{
		if (_obj.getVisible() == true && _target.getMustExplosive() == false)
		{
			if (_target.getPosition().x - _target.getSize() > _obj.getPosition().x + _obj.getSize())
			{
				return false;
			}
			if (_target.getPosition().x + _target.getSize() < _obj.getPosition().x - _obj.getSize())
			{
				return false;
			}
			if (_target.getPosition().y - _target.getSize() > _obj.getPosition().y + _obj.getSize())
			{
				return false;
			}
			if (_target.getPosition().y + _target.getSize() < _obj.getPosition().y - _obj.getSize())
			{
				return false;
			}

			return true;
		}

		return false;
	}
	else
	{
		return false;
	}
}

bool CheckBreakEnemyNTile(CEnemy& _target, CObject& _obj)
{
	if (_obj.getInvinsible())
	{
		return false;
	}
	else
	{
		if (_target.getPosition().x - 3 * _target.getExplosiveSize() > _obj.getPosition().x + _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().x + 3 * _target.getExplosiveSize() < _obj.getPosition().x - _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().y - 3 * _target.getExplosiveSize() > _obj.getPosition().y + _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().y + 3 * _target.getExplosiveSize() < _obj.getPosition().y - _obj.getSize())
		{
			return false;
		}

		return true;
	}
}

bool CheckBreakTileByRespawn(CHero& _hero, CObject& _obj)
{
	if (_obj.getInvinsible())
	{
		return false;
	}
	else
	{
		if (_hero.getPosition().x - 4 * _hero.getExplosiveSize() > _obj.getPosition().x + _obj.getSize())
		{
			return false;
		}
		if (_hero.getPosition().x + 4 * _hero.getExplosiveSize() < _obj.getPosition().x - _obj.getSize())
		{
			return false;
		}
		if (_hero.getPosition().y - 4 * _hero.getExplosiveSize() > _obj.getPosition().y + _obj.getSize())
		{
			return false;
		}
		if (_hero.getPosition().y + 4 * _hero.getExplosiveSize() < _obj.getPosition().y - _obj.getSize())
		{
			return false;
		}

		return true;
	}
}

bool CheckKillEnemyByRespawn(CHero& _hero, CEnemy& _obj)
{
	if (_hero.getPosition().x - 4 * _hero.getExplosiveSize() > _obj.getPosition().x + _obj.getSize())
	{
		return false;
	}
	if (_hero.getPosition().x + 4 * _hero.getExplosiveSize() < _obj.getPosition().x - _obj.getSize())
	{
		return false;
	}
	if (_hero.getPosition().y - 4 * _hero.getExplosiveSize() > _obj.getPosition().y + _obj.getSize())
	{
		return false;
	}
	if (_hero.getPosition().y + 4 * _hero.getExplosiveSize() < _obj.getPosition().y - _obj.getSize())
	{
		return false;
	}

	return true;
}

bool CheckCollisionBulletNHero(CBullet& _target, CHero& _hero)
{
	if (_target.getSpecial() == true && _hero.getMustExplosive() == false)
	{
		if (_target.getPosition().x - _target.getSize() > _hero.getPosition().x + _hero.getSize())
		{
			return false;
		}
		if (_target.getPosition().x + _target.getSize() < _hero.getPosition().x - _hero.getSize())
		{
			return false;
		}
		if (_target.getPosition().y - _target.getSize() > _hero.getPosition().y + _hero.getSize())
		{
			return false;
		}
		if (_target.getPosition().y + _target.getSize() < _hero.getPosition().y - _hero.getSize())
		{
			return false;
		}

		return true;
	}

	return false;
}

bool CheckCollisionEnemyNEnemy(CEnemy& _target, CEnemy& _obj)
{
	if (_target.getMustExplosive() && _obj.getVisible())
	{
		if (_target.getPosition().x - _target.getSize() > _obj.getPosition().x + _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().x + _target.getSize() < _obj.getPosition().x - _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().y - _target.getSize() > _obj.getPosition().y + _obj.getSize())
		{
			return false;
		}
		if (_target.getPosition().y + _target.getSize() < _obj.getPosition().y - _obj.getSize())
		{
			return false;
		}

		return true;
	}

	return false;
}

void ResetGame()
{
	g_BulletCount = 6;
	g_Time = 0;
	g_TimeEnemyCreate = 0;
	g_TimeEnemyAnimation = 0;
	g_PlayTime = 0;
	isInvincibleMode = false;
	g_ComboScore = 0;
	g_ComboTime = 0;
	g_KilledEnemyNumber = 0;
	g_PrevKilledEnemyNumber = 0;
	hero.setLife(-1);

	playerBulletList.clear();

	hero.Init();

	for (int i = 0; i < 6; i++)
	{
		magazine[i].setVisible(true);
	}
}

void ChangeStage()
{
	g_BulletCount = 6;
	g_Time = 0;
	g_TimeEnemyCreate = 0;
	g_TimeEnemyAnimation = 0;
	g_PlayTime = 0;
	isInvincibleMode = false;
	g_RespawnTime = 0;
	g_Score = 0;
	g_ComboScore = 0;
	g_ComboTime = 0;
	g_KilledEnemyNumber = 0;
	g_PrevKilledEnemyNumber = 0;

	playerBulletList.clear();
	specialBulletList.clear();
	enemyList.clear();

	switch (g_Stage)
	{
	case 1:
	{
		for (int j = 0; j < TILE_NUMBER - 6; j++)
		{
			for (int i = 0; i < TILE_NUMBER; i++)
			{
				objectGroup[i][j].setInvincible(false);
				if (j == 2 || j == (TILE_NUMBER - 6 - 1 - 2))
				{
					if (i >= 6 && i <= 13)
						objectGroup[i][j].setExplosable(false);
					else
						objectGroup[i][j].setExplosable(false);
				}
				else if (j == 3 || j == (TILE_NUMBER - 6 - 1 - 3))
				{
					if (i >= 5 && i <= 14)
						objectGroup[i][j].setExplosable(false);
					else
						objectGroup[i][j].setExplosable(false);
				}
				else if ((j >= 4 && j <= 9))
				{
					if (i >= 4 && i <= 15)
						objectGroup[i][j].setExplosable(false);
					else
						objectGroup[i][j].setExplosable(false);
				}
				else
				{
					objectGroup[i][j].setExplosable(false);
				}
			}
		}
	}
	break;
	case 2:
	{
		for (int j = 0; j < TILE_NUMBER - 6; j++)
		{
			for (int i = 0; i < TILE_NUMBER; i++)
			{
				objectGroup[i][j].setInvincible(false);
				if (j == 2 || j == (TILE_NUMBER - 6 - 1 - 2))
				{
					if (i >= 6 && i <= 13)
						objectGroup[i][j].setExplosable(false);
					else
						objectGroup[i][j].setExplosable(true);
				}
				else if (j == 3 || j == (TILE_NUMBER - 6 - 1 - 3))
				{
					if (i >= 5 && i <= 14)
						objectGroup[i][j].setExplosable(false);
					else
						objectGroup[i][j].setExplosable(true);
				}
				else if ((j >= 4 && j <= 9))
				{
					if (i >= 4 && i <= 15)
						objectGroup[i][j].setExplosable(false);
					else
						objectGroup[i][j].setExplosable(true);
				}
				else
				{
					objectGroup[i][j].setExplosable(true);
				}
			}
		}
	}
	break;
	case 3:
	{
		for (int j = 0; j < TILE_NUMBER - 6; j++)
		{
			for (int i = 0; i < TILE_NUMBER; i++)
			{
				if (j == 2 || j == (TILE_NUMBER - 6 - 1 - 2))
				{
					if (i >= 8 && i <= 11)
						objectGroup[i][j].setExplosable(false);
					else
						objectGroup[i][j].setExplosable(true);
				}
				else if (j == 3 || j == (TILE_NUMBER - 6 - 1 - 3))
				{
					if (i >= 5 && i <= 14)
						objectGroup[i][j].setExplosable(false);
					else
						objectGroup[i][j].setExplosable(true);
				}
				else if ((j >= 4 && j <= 9))
				{
					if (i >= 8 && i <= 11)
						objectGroup[i][j].setExplosable(false);
					else
						objectGroup[i][j].setExplosable(true);
				}
				else
				{
					objectGroup[i][j].setExplosable(true);
				}

				objectGroup[8][4].setExplosable(true);
				objectGroup[9][4].setExplosable(true);
				objectGroup[8][5].setExplosable(true);
				objectGroup[9][5].setExplosable(true);
				objectGroup[11][5].setExplosable(true);
				objectGroup[11][6].setExplosable(true);

				objectGroup[9][8].setExplosable(true);
				objectGroup[9][9].setExplosable(true);
				objectGroup[10][8].setExplosable(true);
				objectGroup[10][9].setExplosable(true);
				objectGroup[12][8].setExplosable(true);
				objectGroup[12][9].setExplosable(true);

				objectGroup[1][2].setInvincible(true);
				objectGroup[1][2].setExplosable(true);

				objectGroup[7][2].setInvincible(true);
				objectGroup[7][2].setExplosable(true);

				objectGroup[8][12].setInvincible(true);
				objectGroup[8][12].setExplosable(true);

				objectGroup[16][5].setInvincible(true);
				objectGroup[16][5].setExplosable(true);

				objectGroup[18][10].setInvincible(true);
				objectGroup[18][10].setExplosable(true);

				objectGroup[18][5].setInvincible(true);
				objectGroup[18][5].setExplosable(true);

				objectGroup[3][8].setInvincible(true);
				objectGroup[3][8].setExplosable(true);

				objectGroup[10][2].setInvincible(true);
				objectGroup[10][2].setExplosable(true);

				objectGroup[14][1].setInvincible(true);
				objectGroup[14][1].setExplosable(true);

				objectGroup[15][1].setInvincible(true);
				objectGroup[15][1].setExplosable(true);

				objectGroup[8][6].setInvincible(true);
				objectGroup[8][6].setExplosable(true);

				objectGroup[12][10].setInvincible(true);
				objectGroup[12][10].setExplosable(true);
			}
		}
	}
	break;
	}

	hero.Init();

	for (int i = 0; i < 6; i++)
	{
		magazine[i].setVisible(true);
	}
}