#include <Windows.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <stdlib.h>
#include "resource.h"

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

struct Point {
	int x;
	int y;

	Point& operator=(Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}

	bool operator==(Point& rhs)
	{
		if ((this->x == rhs.x) && (this->y == rhs.y))
		{
			return true;
		}
		else
			return false;
	}
};

Point getIndexFromPosition(Point position);

using namespace std;

#define WINDOW_X 1000
#define WINDOW_Y 1000

#define FRAME_X 135
#define FRAME_Y 150

enum KIND { NONE, LINE, CURVE };
enum DETAIL_KIND { GARO, SERO, CURVE1, CURVE2, CURVE3, CURVE4 };

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 5-5";

struct Block {
	int kind;
	int detailKind;
	HDC bitdc;

	// LINE
	HBITMAP hLINEGAROBitmap;
	HBITMAP hLINESEROBitmap;

	// CURVE
	HBITMAP hCURVE1Bitmap;
	HBITMAP hCURVE2Bitmap;
	HBITMAP hCURVE3Bitmap;
	HBITMAP hCURVE4Bitmap;

	Point position;
	Point realSize;
	RECT boundingBox;

	void Init(int x, int y, int size)
	{
		position.x = x;
		position.y = y;

		realSize.x = size;
		realSize.y = size;

		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;

		hLINEGAROBitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP1));
		hLINESEROBitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP4));

		hCURVE1Bitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP6));
		hCURVE2Bitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP3));
		hCURVE3Bitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP2));
		hCURVE4Bitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP5));
	}

	void Update()
	{
		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;
	}

	void Render(HDC _hdc, HWND _hWnd)
	{
		if (kind == LINE)
		{
			switch (detailKind)
			{
			case GARO:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hLINEGAROBitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			case SERO:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hLINESEROBitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			}
		}
		else if (kind == CURVE)
		{
			switch (detailKind)
			{
			case CURVE1:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hCURVE1Bitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			case CURVE2:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hCURVE2Bitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			case CURVE3:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hCURVE3Bitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			case CURVE4:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hCURVE4Bitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			}
		}
		else
		{
			Rectangle(_hdc, boundingBox.left, boundingBox.top, boundingBox.right, boundingBox.bottom);
		}
	}
};

Block TetrisPan[10][20];

struct Board {
	int kind;
	int detailKind;
	HDC bitdc;
	bool isMyOrder = false;

	// LINE
	HBITMAP hLINEGAROBitmap;
	HBITMAP hLINESEROBitmap;

	// CURVE
	HBITMAP hCURVE1Bitmap;
	HBITMAP hCURVE2Bitmap;
	HBITMAP hCURVE3Bitmap;
	HBITMAP hCURVE4Bitmap;

	Point position;
	Point realSize;
	RECT boundingBox;

	void Init(int x, int y, int size)
	{
		position.x = x;
		position.y = y;

		realSize.x = size;
		realSize.y = size;

		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;

		hLINEGAROBitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP1));
		hLINESEROBitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP4));

		hCURVE1Bitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP6));
		hCURVE2Bitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP3));
		hCURVE3Bitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP2));
		hCURVE4Bitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_BITMAP5));
	}

	void Update()
	{
		if (position.y == WINDOW_Y - 50)
		{
			if (isMyOrder)
			{
				isMyOrder = false;
				Point index = getIndexFromPosition(position);
				TetrisPan[index.x][index.y].kind = kind;
				TetrisPan[index.x][index.y].detailKind = detailKind;
			}
		}
		else if (boundingBox.bottom + realSize.y <= WINDOW_Y)
		{
			Point next;
			next.x = position.x; next.y = position.y + realSize.y;

			Point index = getIndexFromPosition(next);
			if (TetrisPan[index.x][index.y].kind != NONE)
			{
				if (isMyOrder)
				{
					isMyOrder = false;
					Point index = getIndexFromPosition(position);
					TetrisPan[index.x][index.y].kind = kind;
					TetrisPan[index.x][index.y].detailKind = detailKind;
				}
			}
			else
			{
				position.y += realSize.y;
			}
		}

		boundingBox.left = position.x;
		boundingBox.top = position.y;
		boundingBox.right = position.x + realSize.x;
		boundingBox.bottom = position.y + realSize.y;
	}

	void Move(int direction)
	{
		switch (direction)
		{
		case 0:
		{
			if (boundingBox.top - realSize.y >= 0)
			{
				Point next;
				next.x = position.x; next.y = position.y + realSize.y;

				Point index = getIndexFromPosition(next);
				if (TetrisPan[index.x][index.y].kind == NONE)
				{
					position.y -= realSize.y;
				}
			}
		}
		break;
		case 1:
		{
			if (boundingBox.bottom + realSize.y <= WINDOW_Y)
			{
				Point next;
				next.x = position.x; next.y = position.y + realSize.y;

				Point index = getIndexFromPosition(next);
			}
				position.y += realSize.y;
		}
		break;
		case 2:
		{
			if (boundingBox.left - realSize.x >= 0)
			{
				Point next;
				next.x = position.x - realSize.x; next.y = position.y;

				Point index = getIndexFromPosition(next);
				if (TetrisPan[index.x][index.y].kind == NONE)
				{
					position.x -= realSize.x;
				}
			}	
		}
		break;
		case 3:
		{
			if (boundingBox.right + realSize.x <= (WINDOW_X / 2))
			{
				Point next;
				next.x = position.x + realSize.x; next.y = position.y;

				Point index = getIndexFromPosition(next);
				if (TetrisPan[index.x][index.y].kind == NONE)
				{
					position.x += realSize.x;
				}
			}
		}
		break;
		}
	}

	void Render(HDC _hdc, HWND _hWnd)
	{
		if (kind == LINE)
		{
			switch (detailKind)
			{
			case GARO:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hLINEGAROBitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			case SERO:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hLINESEROBitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			}
		}
		else if(kind == CURVE)
		{
			switch (detailKind)
			{
			case CURVE1:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hCURVE1Bitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			case CURVE2:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hCURVE2Bitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			case CURVE3:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hCURVE3Bitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			case CURVE4:
			{
				bitdc = CreateCompatibleDC(_hdc);
				SelectObject(bitdc, hCURVE4Bitmap);
				StretchBlt(_hdc, position.x, position.y, realSize.x, realSize.y, bitdc, 0, 0, 100, 100, SRCCOPY);
				DeleteDC(bitdc);
			}
			break;
			}
		}
	}
};

Point startPoint;
Board TetrisBoard[100];
int CurrentIndex = 0;

Point getIndexFromPosition(Point position)
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			if (TetrisPan[i][j].position == position)
			{
				Point index;
				index.x = i;
				index.y = j;
				return index;
			}
		}
	}
}

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 0, 0,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static int startX, startY;
	static bool Drag;
	static int endX, endY;

	static HBITMAP hBitmap, hOldBitmap;

	static char str[256];

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);
		Drag = false;

		startPoint.x = 200;
		startPoint.y = 0;

		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				TetrisPan[i][j].Init(i * 50, j * 50, 50);
				TetrisPan[i][j].kind = NONE;
				TetrisPan[i][j].detailKind = -1;
			}
		}

		for (int i = 0; i < 9; i++)
		{
			TetrisPan[i][19].kind = LINE;
			TetrisPan[i][19].detailKind = GARO;
		}

		for (int i = 0; i < 100; i++)
		{
			TetrisBoard[i].Init(750, 150, 50);
			srand((unsigned int)time(NULL));
			if (i % 2 == 0)
				TetrisBoard[i].kind = LINE;
			else
				TetrisBoard[i].kind = CURVE;

			if (TetrisBoard[i].kind == 1)
			{
				srand((unsigned int)time(NULL));
				TetrisBoard[i].detailKind = rand() % 2;
			}
			else
			{
				srand((unsigned int)time(NULL));
				TetrisBoard[i].detailKind = rand() % 4 + 2;
			}
		}

		TetrisBoard[CurrentIndex].isMyOrder = true;
		TetrisBoard[CurrentIndex].position = startPoint;

		SetTimer(hWnd, 0, 250, NULL);
	}
	break;
	case WM_TIMER:
	{
		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				TetrisPan[i][j].Update();
			}
		}

		if (TetrisBoard[CurrentIndex].isMyOrder == false)
		{
			CurrentIndex++;
			TetrisBoard[CurrentIndex].isMyOrder = true;
			TetrisBoard[CurrentIndex].position = startPoint;
		}
		TetrisBoard[CurrentIndex].Update();

		for (int i = 0; i < 10; i++)
		{
			cout << TetrisPan[i][19].detailKind << " ";
		}
		cout << endl;
		// 한줄 체크 및 네모 검사

		for (int j = 0; j < 20; j++)
		{
			if ((TetrisPan[0][j].detailKind == GARO) &&
				(TetrisPan[1][j].detailKind == GARO) &&
				(TetrisPan[2][j].detailKind == GARO) &&
				(TetrisPan[3][j].detailKind == GARO) &&
				(TetrisPan[4][j].detailKind == GARO) &&
				(TetrisPan[5][j].detailKind == GARO) &&
				(TetrisPan[6][j].detailKind == GARO) &&
				(TetrisPan[7][j].detailKind == GARO) &&
				(TetrisPan[8][j].detailKind == GARO) &&
				(TetrisPan[9][j].detailKind == GARO)
				)
			{
				TetrisPan[0][j].kind = NONE;
				TetrisPan[1][j].kind = NONE;
				TetrisPan[2][j].kind = NONE;
				TetrisPan[3][j].kind = NONE;
				TetrisPan[4][j].kind = NONE;
				TetrisPan[5][j].kind = NONE;
				TetrisPan[6][j].kind = NONE;
				TetrisPan[7][j].kind = NONE;
				TetrisPan[8][j].kind = NONE;
				TetrisPan[9][j].kind = NONE;

				int k = j;

				// 위에껄 내려준다.
				for (int j = k; j > 1; j--)
				{
					for (int i = 0; i < 10; i++)
					{
						TetrisPan[i][j].kind = TetrisPan[i][j - 1].kind;
						TetrisPan[i][j].detailKind = TetrisPan[i][j - 1].detailKind;
					}
				}
			}
		}

		for (int j = 0; j < 19; j++)
		{
			for (int i = 0; i < 9; i++)
			{
				if ((TetrisPan[i][j].detailKind == CURVE1) &&
					(TetrisPan[i + 1][j].detailKind == CURVE2) &&
					(TetrisPan[i + 1][j + 1].detailKind == CURVE3) &&
					(TetrisPan[i][j + 1].detailKind == CURVE4))
				{
					TetrisPan[i][j].kind = NONE;
					TetrisPan[i + 1][j].kind = NONE;
					TetrisPan[i + 1][j + 1].kind = NONE;
					TetrisPan[i][j + 1].kind = NONE;

					int start = i;
					int end = i + 1;
					int top = j;
					int bottom = j + 1;

					for (int j = bottom; j > 2; j--)
					{
						TetrisPan[start][j].kind = TetrisPan[start][j - 2].kind;
						TetrisPan[start][j].detailKind = TetrisPan[start][j - 2].detailKind;

						TetrisPan[end][j].kind = TetrisPan[end][j - 2].kind;
						TetrisPan[end][j].detailKind = TetrisPan[end][j - 2].detailKind;
					}
				}
			}
		}


		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);

		Rectangle(memdc, 0, 0, WINDOW_X, WINDOW_Y);

		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				TetrisPan[i][j].Render(memdc, hWnd);
			}
		}

		if(CurrentIndex < 100 - 1)
			TetrisBoard[CurrentIndex + 1].Render(memdc, hWnd);

		if(TetrisBoard[CurrentIndex].isMyOrder)
			TetrisBoard[CurrentIndex].Render(memdc, hWnd);

		sprintf_s(str, "다음 나올 보드");
		TextOut(memdc, 725, 50, str, strlen(str));

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
	{
		switch (wParam)
		{
		case 'q': case 'Q':
		{
			PostQuitMessage(0);
		}
		break;
		}
	}
	break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_UP:
		{
		}
		break;
		case VK_DOWN:
		{
			/*if (TetrisBoard[CurrentIndex].isMyOrder)
				TetrisBoard[CurrentIndex].Move(1);*/
		}
		break;
		case VK_LEFT:
		{
			if (TetrisBoard[CurrentIndex].isMyOrder)
				TetrisBoard[CurrentIndex].Move(2);
		}
		break;
		case VK_RIGHT:
		{
			if (TetrisBoard[CurrentIndex].isMyOrder)
				TetrisBoard[CurrentIndex].Move(3);
		}
		break;
		case VK_SPACE:
		{
			if (TetrisBoard[CurrentIndex].kind == LINE)
			{
				if(TetrisBoard[CurrentIndex].detailKind == GARO)
					TetrisBoard[CurrentIndex].detailKind = SERO;
				else
					TetrisBoard[CurrentIndex].detailKind = GARO;
			}
			else if (TetrisBoard[CurrentIndex].kind == CURVE)
			{
				if (TetrisBoard[CurrentIndex].detailKind == 5)
				{
					TetrisBoard[CurrentIndex].detailKind = 2;
				}
				else
				{
					TetrisBoard[CurrentIndex].detailKind++;
				}
			}
		}
		break;
		}
	}
	break;
	case WM_RBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}