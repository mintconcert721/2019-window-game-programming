#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <crtdbg.h>
#include <iostream>

using namespace std;

#define MAX_OBJECTS_NUMBER 20

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 1000
#define WINDOW_Y 600

#define ELAPSED_TIME 1

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 3-2-1";

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);

typedef struct _Coordi {
	int x;
	int y;
	struct _Coordi() {}
	struct _Coordi(int _x, int _y)
	{
		x = _x;
		y = _y;
	}
	struct _Coordi& operator =(const struct _Coordi& rhs)
	{
		x = rhs.x;
		y = rhs.y;
		return *this;
	}
} Coordi;

typedef struct _Brick {
	Coordi m_Position; // 사각형의 중심좌표
	Coordi m_Half;
	int hp; // 체력
	RECT m_rect;

	struct _Brick() {}
	struct _Brick(Coordi Position, Coordi Half)
	{
		m_Position = Position;
		m_Half = Half;
		hp = 2;
		m_rect.left = m_Position.x - m_Half.x;
		m_rect.right = m_Position.x + m_Half.x;
		m_rect.top = m_Position.y - m_Half.y;
		m_rect.bottom = m_Position.y + m_Half.y;
	}
	void Update(HWND hWnd, int _speed)
	{
		m_Position.x += _speed;
		m_rect.left = m_Position.x - m_Half.x;
		m_rect.right = m_Position.x + m_Half.x;
		m_rect.top = m_Position.y - m_Half.y;
		m_rect.bottom = m_Position.y + m_Half.y;
		InvalidateRect(hWnd, NULL, TRUE);
	}
} Brick;

typedef struct _Ball {
	int m_PositionX; // 중심좌표 X
	int m_PositionY; // 중심좌표 Y
	int m_Radius; // 반지름
	RECT m_rect;
	int m_Speed; // 이동속도
	int m_Direction; // 방향

	struct _Ball()
	{
		m_rect.left = m_PositionX - m_Radius;
		m_rect.right = m_PositionX + m_Radius;
		m_rect.top = m_PositionY - m_Radius;
		m_rect.bottom = m_PositionY + m_Radius;
	}
} Ball;

class CProjectManager
{
private:
	HDC m_Hdc;
	Brick* m_BrickObjects[MAX_OBJECTS_NUMBER];
	Ball* m_Ball;

	double m_BrickHalfSizeX = 40.0;
	double m_BrickHalfSizeY = 20.0;

	int m_BrickSpeed = 5;

	bool m_IsGoLeft = true;
	bool m_IsGoRight = false;

	COLORREF BasicColor = RGB(255, 51, 204); // 분홍색
	COLORREF TouchedColor = RGB(0, 255, 255); // 하늘색

	HBRUSH m_BasicBrush;
	HBRUSH m_BasicOldBrush;
	HBRUSH m_TouchedBrush;
	HBRUSH m_TouchedOldBrush;

	float m_Time;

private:
	void ApplyForce(int x, int y)
	{
	}

	void CheckCollideBrickAndWall(Brick* _target)
	{
		int FrameX = 0;
		int FrameY = 0;
		int CaptionY = 0;

		FrameX = GetSystemMetrics(SM_CXFRAME);
		FrameY = GetSystemMetrics(SM_CYFRAME);
		CaptionY = GetSystemMetrics(SM_CYCAPTION);

		if (_target->m_rect.right + m_BrickSpeed > WINDOW_X - (FrameX << 1))
		{
			m_BrickSpeed = -5;
		}
		else if (_target->m_rect.left + m_BrickSpeed < 0)
		{
			m_BrickSpeed = 5;
		}
	}

	void CheckCollideRectAndBall()
	{
	}

	void CollectGarbage()
	{
	}

public:
	void Mouse(HWND _hWnd, WPARAM _wParam, LPARAM _lParam)
	{
	}

	void Keyboard(HWND _hWnd, WPARAM _wParam)
	{
	}

	void Draw(HWND _hWnd)
	{
		m_Hdc = GetDC(_hWnd);

		m_BasicBrush = CreateSolidBrush(BasicColor);
		m_BasicOldBrush = CreateSolidBrush(BasicColor);
		m_TouchedBrush = CreateSolidBrush(TouchedColor);
		m_TouchedOldBrush = CreateSolidBrush(TouchedColor);

		for (int i = 0; i < MAX_OBJECTS_NUMBER; i++) // 초기화
		{
			if (m_BrickObjects[i] != NULL)
			{
				CheckCollideBrickAndWall(m_BrickObjects[i]);

				switch (m_BrickObjects[i]->hp)
				{
				case 1:
					m_TouchedOldBrush = (HBRUSH)SelectObject(m_Hdc, m_TouchedBrush);
					Rectangle(m_Hdc, m_BrickObjects[i]->m_rect.left, m_BrickObjects[i]->m_rect.top, m_BrickObjects[i]->m_rect.right, m_BrickObjects[i]->m_rect.bottom);
					break;
				case 2:
					break;
				}
				m_BasicOldBrush = (HBRUSH)SelectObject(m_Hdc, m_BasicBrush);
				Rectangle(m_Hdc, m_BrickObjects[i]->m_rect.left, m_BrickObjects[i]->m_rect.top, m_BrickObjects[i]->m_rect.right, m_BrickObjects[i]->m_rect.bottom);
			}
		}

		SelectObject(m_Hdc, m_TouchedOldBrush);
		SelectObject(m_Hdc, m_BasicOldBrush);
		DeleteObject(m_TouchedBrush);
		DeleteObject(m_BasicBrush);

		ReleaseDC(_hWnd, m_Hdc);
	}

	void Update(HWND hWnd)
	{
		int FrameX = 0;
		int FrameY = 0;
		int CaptionY = 0;

		FrameX = GetSystemMetrics(SM_CXFRAME);
		FrameY = GetSystemMetrics(SM_CYFRAME);
		CaptionY = GetSystemMetrics(SM_CYCAPTION);

		for (int i = 0; i < MAX_OBJECTS_NUMBER; i++)
		{
			if (m_BrickObjects[i] != NULL)
			{
				m_BrickObjects[i]->Update(hWnd, m_BrickSpeed);
			}
		}
	}

	CProjectManager()
	{
		for (int i = 0; i < MAX_OBJECTS_NUMBER; i++) // 초기화
		{
			m_BrickObjects[i] = NULL;
			if (i < 10)
				m_BrickObjects[i] = new Brick(Coordi(m_BrickHalfSizeX + i * m_BrickHalfSizeX * 2, m_BrickHalfSizeY), Coordi(m_BrickHalfSizeX, m_BrickHalfSizeY));
			else
				m_BrickObjects[i] = new Brick(Coordi(m_BrickHalfSizeX + (i - 10) * m_BrickHalfSizeX * 2, m_BrickHalfSizeY + m_BrickHalfSizeY * 2), Coordi(m_BrickHalfSizeX, m_BrickHalfSizeY));
		}

		m_Ball = new Ball();
	}

	~CProjectManager()
	{
		for (int i = 0; i < MAX_OBJECTS_NUMBER; i++) // 초기화
			if (m_BrickObjects[i] != NULL)
				delete m_BrickObjects[i];

		if (m_Ball != NULL)	delete m_Ball;
	}
};

CProjectManager* g_Manager;
double g_time;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		g_Manager = new CProjectManager();
		SetTimer(hWnd, 1, ELAPSED_TIME, (TIMERPROC)TimerProc); // 1번 아이디의 타이머가 4초 마다 TimerProc 타이머 함수 실행
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		g_Manager->Draw(hWnd);
		EndPaint(hWnd, &ps);
		break;
	case WM_CHAR:
		break;
	case WM_KEYDOWN:
		break;
	case WM_DESTROY:
		delete g_Manager;
		KillTimer(hWnd, 1);
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime)
{
	switch (uMsg)
	{
	case WM_TIMER:
	{
		g_Manager->Update(hWnd);
	}
	break;
	}

	InvalidateRect(hWnd, NULL, TRUE);
}