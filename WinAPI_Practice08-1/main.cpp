#include <Windows.h>
#include <stdlib.h>
#include <iostream>
#include "resource.h"

using namespace std;

typedef struct _Point {
	int x;
	int y;
} Point;

HINSTANCE g_hInstance;
TCHAR lpstrFile[100] = "";
char str[256];
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 8-1 : 파일 입출력 기능이 있는 메모장 만들기";

FILE* fp;

char StringPad[10][100]; // 메모장 2차원 배열 선언

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	540
#define WINDOW_X 800
#define WINDOW_Y 600

OPENFILENAME OFN;
OPENFILENAME SFN;

char temp[100];
int StringPadLine;
int StringPadCount;
Point Cursor;
SIZE cursor_size;
bool CapsLock;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void FileSave(HWND _hWnd);
void FileLoad(HDC _hdc, HWND _hWnd);
char* GetFileName(char file_path[]);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc;
	PAINTSTRUCT ps;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		CreateCaret(hWnd, NULL, 2, 15);
		ShowCaret(hWnd);
		StringPadLine = 0;
		CapsLock = false;
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);

		GetTextExtentPoint(hdc, StringPad[StringPadLine], StringPadCount, &cursor_size);
		SetCaretPos(cursor_size.cx + Cursor.x, StringPadLine * 20);

		for (auto idx = 0; idx < 10; idx++)
			TextOut(hdc, Cursor.x, 20 * idx, StringPad[idx], strlen(StringPad[idx]));

		EndPaint(hWnd, &ps);
		break;
	case WM_CHAR:
		hdc = GetDC(hWnd);
		if (wParam == VK_BACK)
		{
			if (StringPadCount > 0)
			{
				StringPadCount--;
				StringPad[StringPadLine][StringPadCount] = NULL;

			}
			else if (StringPadCount == 0)
			{
				if (StringPadLine > 0)
				{
					StringPadLine--;
					StringPadCount = strlen(StringPad[StringPadLine]);
				}
			}
		}
		else if (wParam == VK_RETURN)
		{
			if (StringPadLine < 9)
			{
				StringPadCount = 0;
				StringPadLine++;
			}
		}
		else if (wParam == VK_TAB)
		{
			StringPad[StringPadLine][StringPadCount] = ' ';
			StringPad[StringPadLine][StringPadCount + 1] = ' ';
			StringPad[StringPadLine][StringPadCount + 2] = ' ';
			StringPad[StringPadLine][StringPadCount + 3] = ' ';
			StringPadCount += 4;
			if (StringPadCount > 99)
				StringPadCount = 99;
		}
		else if (wParam == VK_ESCAPE)
		{
			for (int line_index = 0; line_index < 10; line_index++)
				for (int count_index = 0; count_index < 100; count_index++)
					StringPad[line_index][count_index] = NULL;

			StringPadLine = 0;
			StringPadCount = 0;
		}
		else
		{
			if (CapsLock) if (wParam > 96 && wParam < 123) wParam -= 32;
			StringPad[StringPadLine][StringPadCount++] = wParam;
			StringPad[StringPadLine][StringPadCount] = '\0';
		}

		InvalidateRect(hWnd, NULL, TRUE);
		ReleaseDC(hWnd, hdc);
		break;
	case WM_KEYDOWN:
		hdc = GetDC(hWnd);

		if (wParam == VK_HOME) StringPadCount = 0;
		else if (wParam == VK_DELETE)
		{
			if (StringPadLine > 0)
			{
				for (int i = 0; i < 100; i++)
					StringPad[StringPadLine][i] = NULL;

				StringPadLine--;
				StringPadCount = 0;
			}
		}
		else if (wParam == VK_LEFT)
		{
			if (cursor_size.cx + Cursor.x > 0)
				StringPadCount--;
		}
		else if (wParam == VK_RIGHT)
		{
			if (StringPadCount < 100)
				StringPadCount++;
		}
		else if (wParam == VK_UP)
		{
			if (StringPadLine > 0)
			{
				StringPadLine--;
				StringPadCount = static_cast<int>(strlen(StringPad[StringPadLine]));
			}
		}
		else if (wParam == VK_DOWN)
		{
			if (StringPadLine < 10)
			{
				StringPadLine++;
				StringPadCount = static_cast<int>(strlen(StringPad[StringPadLine]));
			}
		}
		else if (wParam == VK_CAPITAL) CapsLock = (CapsLock == true) ? false : true;

		InvalidateRect(hWnd, NULL, TRUE);
		ReleaseDC(hWnd, hdc);
		break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case ID_FILE_SAVE:
			FileSave(hWnd);
			break;
		case ID_FILE_LOAD:
			FileLoad(hdc, hWnd);
			break;
		}
	}
	break;
	case WM_TIMER:
		break;
	case WM_DESTROY:
		HideCaret(hWnd);
		DestroyCaret();
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void FileSave(HWND _hWnd)
{
	memset(&SFN, 0, sizeof(OPENFILENAME));
	SFN.lStructSize = sizeof(OPENFILENAME);
	SFN.hwndOwner = _hWnd;
	SFN.lpstrFilter = TEXT("텍스트 파일(*.txt)\0*.txt\0모든 파일(*.*)\0*.*\0");
	SFN.lpstrFile = lpstrFile;
	SFN.nMaxFile = 256;
	SFN.lpstrInitialDir = ".";
	if (GetSaveFileName(&SFN) != 0)
	{
		strcpy_s(str, sizeof(str), (SFN.lpstrFile));
		char* name = GetFileName(str);

		fopen_s(&fp, name, "w");

		if (fp == NULL)
		{
			cout << "FILE OPEN ERROR!" << endl;
			exit(0);
		}

		fprintf_s(fp, "%d %d\n", StringPadLine, StringPadCount);

		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 100; j++)
			{
				fprintf_s(fp, "%c", StringPad[i][j]);
			}
		}

		fclose(fp);

		MessageBox(_hWnd, "파일을 저장하였습니다.", "저장하기", MB_OK);
	}
	else
	{
		MessageBox(_hWnd, "저장하기를 취소하셨습니다.", "실패", MB_OK);
	}

}

void FileLoad(HDC _hdc, HWND _hWnd)
{
	_hdc = GetDC(_hWnd);

	memset(&OFN, 0, sizeof(OPENFILENAME));
	OFN.lStructSize = sizeof(OPENFILENAME);
	OFN.hwndOwner = _hWnd;
	OFN.lpstrFilter = TEXT("텍스트 파일(*.txt)\0*.txt\0모든 파일(*.*)\0*.*\0");
	OFN.lpstrFile = lpstrFile;
	OFN.nMaxFile = 256;
	OFN.lpstrInitialDir = ".";
	if (GetOpenFileName(&OFN) != 0)
	{
		strcpy_s(str, sizeof(str), (OFN.lpstrFile));
		char* name = GetFileName(str);

		fopen_s(&fp, name, "r");

		if (fp == NULL)
		{
			cout << "FILE OPEN ERROR!" << endl;
			exit(0);
		}

		fscanf_s(fp, "%d %d\n", &StringPadLine, &StringPadCount);

		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 100; j++)
			{
				fscanf_s(fp, "%c", &StringPad[i][j]);
			}
		}

		fclose(fp);

		GetTextExtentPoint(_hdc, StringPad[StringPadLine], StringPadCount, &cursor_size);
		SetCaretPos(cursor_size.cx + Cursor.x, StringPadLine * 20);

		InvalidateRect(_hWnd, NULL, TRUE);
		ReleaseDC(_hWnd, _hdc);
	}
	else
	{
		MessageBox(_hWnd, "불러오기를 취소하셨습니다.", "실패", MB_OK);
	}
}

char* GetFileName(char file_path[])
{
	char* FileName = nullptr;
	while (*file_path)
	{
		if (*file_path == '\\' && (file_path + 1) != NULL)
		{
			FileName = file_path + 1;
		}

		file_path++;
	}
	return FileName;
}