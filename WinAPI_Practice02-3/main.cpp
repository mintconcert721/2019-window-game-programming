#include <Windows.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 2-3";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	540
#define WINDOW_X 800
#define WINDOW_Y 600

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

typedef struct _Point {
	int x;
	int y;
} Point;

class CProjectManager
{
private:
public:
	CProjectManager()
	{
	}
};

CProjectManager g_Manager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;

	static char StringPad[10][100]; // 메모장 2차원 배열 선언
	static char temp[100];
	static int StringPadLine;
	static int StringPadCount;
	static Point Cursor;
	static SIZE size;
	static bool CapsLock;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		CreateCaret(hWnd, NULL, 2, 15);
		ShowCaret(hWnd);
		StringPadLine = 0;
		CapsLock = false;
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);

		GetTextExtentPoint(hdc, StringPad[StringPadLine], StringPadCount, &size);
		SetCaretPos(size.cx + Cursor.x, StringPadLine * 20);

		for (auto idx = 0; idx < 10; idx++)
			TextOut(hdc, Cursor.x, 20 * idx, StringPad[idx], strlen(StringPad[idx]));

		EndPaint(hWnd, &ps);
		break;
	case WM_CHAR:
		hdc = GetDC(hWnd);
		if (wParam == VK_BACK)
		{
			if (StringPadCount > 0)
			{
				StringPadCount--;
				StringPad[StringPadLine][StringPadCount] = NULL;
				
			}
			else if(StringPadCount == 0)
			{
				if (StringPadLine > 0)
				{
					StringPadLine--;
					StringPadCount = strlen(StringPad[StringPadLine]);
				}
			}
		}
		else if (wParam == VK_RETURN)
		{
			if (StringPadLine < 9)
			{
				StringPadCount = 0;
				StringPadLine++;
			}
		}
		else if (wParam == VK_TAB)
		{
			StringPad[StringPadLine][StringPadCount] = ' ';
			StringPad[StringPadLine][StringPadCount + 1] = ' ';
			StringPad[StringPadLine][StringPadCount + 2] = ' ';
			StringPad[StringPadLine][StringPadCount + 3] = ' ';
			StringPadCount += 4;
			if (StringPadCount > 99)
				StringPadCount = 99;
		}
		else if (wParam == VK_ESCAPE)
		{
			for (int line_index = 0; line_index < 10; line_index++)
				for (int count_index = 0; count_index < 100; count_index++)
					StringPad[line_index][count_index] = NULL;

			StringPadLine = 0;
			StringPadCount = 0;
		}
		else
		{
			if (CapsLock) if (wParam > 96 && wParam < 123) wParam -= 32;
			StringPad[StringPadLine][StringPadCount++] = wParam;
			StringPad[StringPadLine][StringPadCount] = '\0';
		}

		InvalidateRect(hWnd, NULL, TRUE);
		ReleaseDC(hWnd, hdc);
		break;
	case WM_KEYDOWN:
		hdc = GetDC(hWnd);

		if (wParam == VK_HOME) StringPadCount = 0;
		else if (wParam == VK_DELETE)
		{
			if (StringPadLine > 0)
			{
				for (int i = 0; i < 100; i++)
					StringPad[StringPadLine][i] = NULL;

				StringPadLine--;
				StringPadCount = 0;
			}
		}
		else if (wParam == VK_LEFT)
		{
			if (size.cx + Cursor.x > 0)
				StringPadCount--;
		}
		else if (wParam == VK_RIGHT)
		{
			if (StringPadCount < 100)
				StringPadCount++;
		}
		else if (wParam == VK_UP)
		{
			if (StringPadLine > 0)
			{
				StringPadLine--;
				StringPadCount = static_cast<int>(strlen(StringPad[StringPadLine]));
			}
		}
		else if (wParam == VK_DOWN)
		{
			if (StringPadLine < 10)
			{
				StringPadLine++;
				StringPadCount = static_cast<int>(strlen(StringPad[StringPadLine]));
			}
		}
		else if (wParam == VK_CAPITAL) CapsLock = (CapsLock == true) ? false : true;

		InvalidateRect(hWnd, NULL, TRUE);
		ReleaseDC(hWnd, hdc);
		break;
	case WM_TIMER:
		break;
	case WM_DESTROY:
		HideCaret(hWnd);
		DestroyCaret();
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}