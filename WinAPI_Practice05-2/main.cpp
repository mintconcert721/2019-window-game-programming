#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include "resource.h"

//#ifdef UNICODE
//#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
//#else
//#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
//#endif

template<typename T>
void Swap(T& left, T& right)
{
	T temp = left;
	left = right;
	right = temp;
}

using namespace std;

#define WINDOW_X 1200
#define WINDOW_Y 600

#define PAINTPAN_X 600
#define WIDTH WINDOW_X / 2

#define DIV_1 1
#define DIV_2 2
#define DIV_3 3
#define DIV_4 4
#define DIV_5 5

int GameMode = DIV_3;
bool isFirst = true;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 5-2";

typedef struct _Point {
	int x;
	int y;

	_Point& operator=(_Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}
} Point;

class CBmpComponent
{
private:
	Point m_Position;
	Point m_ImagePosition;
	int m_Size;
	bool m_IsSelected = false;
	bool m_IsCornerSelected = false;

public:
	void SetIsCornerSelected(bool value)
	{
		m_IsCornerSelected = value;
	}

	const bool GetIsCornerSelected()
	{
		return m_IsCornerSelected;
	}

	void SetIsSelected(bool value)
	{
		m_IsSelected = value;
	}

	const bool GetIsSelected()
	{
		return m_IsSelected;
	}

	void SetImagePosition(int x, int y)
	{
		m_ImagePosition.x = x;
		m_ImagePosition.y = y;
	}

	const Point GetImagePosition() const
	{
		return m_ImagePosition;
	}

	void SetPosition(int x, int y)
	{
		m_Position.x = x;
		m_Position.y = y;
	}

	const Point GetPosition() const
	{
		return m_Position;
	}

	void SetSize(int size)
	{
		m_Size = size;
	}

	const int GetSize() const
	{
		return m_Size;
	}

	CBmpComponent& operator=(const CBmpComponent& rhs)
	{
		if (this != &rhs)
		{
			m_ImagePosition.x = rhs.m_ImagePosition.x;
			m_ImagePosition.y = rhs.m_ImagePosition.y;
		}

		return *this;
	}

public:
	CBmpComponent()
	{
	}

	CBmpComponent(const CBmpComponent& rhs)
	{
		m_Position.x = rhs.m_Position.x;
		m_Position.y = rhs.m_Position.y;

		m_ImagePosition.x = rhs.m_ImagePosition.x;
		m_ImagePosition.y = rhs.m_ImagePosition.y;

		m_Size = rhs.m_Size;
		m_IsSelected = rhs.m_IsSelected;
	}
};

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
bool IsPointInRect(CBmpComponent target, int x, int y);
bool IsPointInCorner(CBmpComponent target, int x, int y);

CBmpComponent Source[DIV_3][DIV_3];

CBmpComponent Dest1[DIV_1][DIV_1];
CBmpComponent Dest2[DIV_2][DIV_2];
CBmpComponent Dest3[DIV_3][DIV_3];
CBmpComponent Dest4[DIV_4][DIV_4];
CBmpComponent Dest5[DIV_5][DIV_5];

Point g_savePosition;
Point g_saveImagePosition;

class CPrjojectManager
{
private:
	HPEN m_hPen;
	HPEN m_hOldPen;

	Point m_SquaretLength;
	
public:
	void SetSquareLength(int x, int y) 
	{
		m_SquaretLength.x = x;
		m_SquaretLength.y = y;
	}

	const Point GetSquareLength()
	{
		return m_SquaretLength;
	}

	void Draw(HDC hdc, HDC memdc, HWND hWnd)
	{
		for (auto i = 0; i < DIV_3; i++)
		{
			for (auto j = 0; j < DIV_3; j++)
			{
				// cout << Source[i][j].GetIsSelected() << " ";
			}
			// cout << endl;
		}

		for (auto i = 0; i < DIV_3; i++)
		{
			for (auto j = 0; j < DIV_3; j++)
			{
				BitBlt(hdc, Source[i][j].GetPosition().x, Source[i][j].GetPosition().y, Source[i][j].GetSize(), Source[i][j].GetSize(),
					memdc, Source[i][j].GetImagePosition().x, Source[i][j].GetImagePosition().y, SRCCOPY);
			}
		}

		m_hPen = CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
		m_hOldPen = CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
		SelectObject(hdc, m_hOldPen);

		MoveToEx(hdc, 0, 200, NULL);
		LineTo(hdc, 600, 200);

		MoveToEx(hdc, 0, 400, NULL);
		LineTo(hdc, 600, 400);

		MoveToEx(hdc, 200, 0, NULL);
		LineTo(hdc, 200, 600);

		MoveToEx(hdc, 400, 0, NULL);
		LineTo(hdc, 400, 600);

		DeleteObject(m_hPen);

		m_hPen = CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
		m_hOldPen = CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
		SelectObject(hdc, m_hOldPen);

		switch (GameMode)
		{
		case DIV_1:
		{
			Rectangle(hdc, PAINTPAN_X, 0, PAINTPAN_X + WIDTH, WINDOW_Y);
		}
		break;
		case DIV_2:
		{
			for (auto i = 0; i < DIV_2; i++)
			{
				for (auto j = 0; j < DIV_2; j++)
				{
					Rectangle(hdc, PAINTPAN_X + i * (PAINTPAN_X / DIV_2),  j * (WINDOW_Y / DIV_2), PAINTPAN_X + (i + 1) * (PAINTPAN_X / DIV_2), (j + 1) * (WINDOW_Y / DIV_2));
				}
			}
		}
		break;
		case DIV_3:
		{
			for (auto i = 0; i < DIV_3; i++)
			{
				for (auto j = 0; j < DIV_3; j++)
				{
					Rectangle(hdc, PAINTPAN_X + i * (PAINTPAN_X / DIV_3), j * (WINDOW_Y / DIV_3), PAINTPAN_X + (i + 1) * (PAINTPAN_X / DIV_3), (j + 1) * (WINDOW_Y / DIV_3));
				}
			}
		}
		break;
		case DIV_4:
		{
			for (auto i = 0; i < DIV_4; i++)
			{
				for (auto j = 0; j < DIV_4; j++)
				{
					Rectangle(hdc, PAINTPAN_X + i * (PAINTPAN_X / DIV_4), j * (WINDOW_Y / DIV_4), PAINTPAN_X + (i + 1) * (PAINTPAN_X / DIV_4), (j + 1) * (WINDOW_Y / DIV_4));
				}
			}
		}
		break;
		case DIV_5:
		{
			for (auto i = 0; i < DIV_5; i++)
			{
				for (auto j = 0; j < DIV_5; j++)
				{
					Rectangle(hdc, PAINTPAN_X + i * (PAINTPAN_X / DIV_5), j * (WINDOW_Y / DIV_5), PAINTPAN_X + (i + 1) * (PAINTPAN_X / DIV_5), (j + 1) * (WINDOW_Y / DIV_5));
				}
			}
		}
		break;
		}

		DeleteObject(m_hPen);
		
		SetStretchBltMode(hdc, COLORONCOLOR);
		if (isFirst== false)
			StretchBlt(hdc, g_savePosition.x, g_savePosition.y, m_SquaretLength.x, m_SquaretLength.y, memdc, g_saveImagePosition.x, g_saveImagePosition.y, WIDTH / DIV_3, WIDTH / DIV_3, SRCCOPY);
	}

public:
	CPrjojectManager()
	{
		m_SquaretLength.x = (WINDOW_X / 2) / DIV_3;
		m_SquaretLength.y= (WINDOW_Y) / DIV_3;

		// TODO : 3X3 퍼즐 초기화
		for (auto i = 0; i < DIV_3; i++)
		{
			for (auto j = 0; j < DIV_3; j++)
			{
				Source[i][j].SetPosition(j * ((WINDOW_X / 2) / DIV_3), i * ((WINDOW_Y) / DIV_3));
				Source[i][j].SetImagePosition(j * ((WINDOW_X / 2) / DIV_3), i * ((WINDOW_Y) / DIV_3));
				Source[i][j].SetSize((WINDOW_X / 2) / DIV_3);
			}
		}

		Dest1[0][0].SetPosition(PAINTPAN_X, 0);
		Dest1[0][0].SetSize(PAINTPAN_X);

		for (auto i = 0; i < DIV_2; i++)
		{
			for (auto j = 0; j < DIV_2; j++)
			{
				Dest2[i][j].SetPosition(PAINTPAN_X + j * (PAINTPAN_X / DIV_2), i * (WINDOW_Y / DIV_2));
				Dest2[i][j].SetSize(PAINTPAN_X / DIV_2);
			}
		}

		for (auto i = 0; i < DIV_3; i++)
		{
			for (auto j = 0; j < DIV_3; j++)
			{
				Dest3[i][j].SetPosition(PAINTPAN_X + j * (PAINTPAN_X / DIV_3), i * (WINDOW_Y / DIV_3));
				Dest3[i][j].SetSize(PAINTPAN_X / DIV_3);
			}
		}

		for (auto i = 0; i < DIV_4; i++)
		{
			for (auto j = 0; j < DIV_4; j++)
			{
				Dest4[i][j].SetPosition(PAINTPAN_X + j * (PAINTPAN_X / DIV_4), i * (WINDOW_Y / DIV_4));
				Dest4[i][j].SetSize(PAINTPAN_X / DIV_4);
			}
		}

		for (auto i = 0; i < DIV_5; i++)
		{
			for (auto j = 0; j < DIV_5; j++)
			{
				Dest5[i][j].SetPosition(PAINTPAN_X + j * (PAINTPAN_X / DIV_5), i * (WINDOW_Y / DIV_5));
				Dest5[i][j].SetSize(PAINTPAN_X / DIV_5);
			}
		}
	}
};

CPrjojectManager g_ProjectManager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, true);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	PAINTSTRUCT ps;

	static int mouseX = 0;
	static int mouseY = 0;

	static HBITMAP hBitmap;

	static int tempMode;
	static bool RMouseButtonSelection = false;
	static bool LMouseButtonSelection = false;
	static bool LMouseButtonCornerSelection = false;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		hBitmap = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_MABINOGI));
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);
		memdc = CreateCompatibleDC(hdc);
		SelectObject(memdc, hBitmap);

		g_ProjectManager.Draw(hdc, memdc, hWnd);

		DeleteDC(memdc);
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
	{
		switch (wParam)
		{
		case '1':
			GameMode = DIV_1;
			g_ProjectManager.SetSquareLength(WIDTH / DIV_1, WIDTH / DIV_1);
			break;
		case '2':
			GameMode = DIV_2;
			g_ProjectManager.SetSquareLength(WIDTH / DIV_2, WIDTH / DIV_2);
			break;
		case '3':
			GameMode = DIV_3;
			g_ProjectManager.SetSquareLength(WIDTH / DIV_3, WIDTH / DIV_3);
			break;
		case '4':
			GameMode = DIV_4;
			g_ProjectManager.SetSquareLength(WIDTH / DIV_4, WIDTH / DIV_4);
			break;
		case '5':
			GameMode = DIV_5;
			g_ProjectManager.SetSquareLength((WIDTH / DIV_5), (WIDTH / DIV_5));
			break;
		}

		InvalidateRgn(hWnd, NULL, true);
		// cout << GameMode << endl;
	}
	break;
	case WM_KEYDOWN:
		break;
	case WM_RBUTTONDOWN:
	{
		switch (GameMode)
		{
		case DIV_1:
		{
			for (auto i = 0; i < DIV_1; i++)
			{
				for (auto j = 0; j < DIV_1; j++)
				{
					if (IsPointInRect(Dest1[i][j], mouseX, mouseY))
					{
						Dest1[i][j].SetIsSelected(true);
					}
				}
			}
		}
		break;
		case DIV_2:
		{
			for (auto i = 0; i < DIV_2; i++)
			{
				for (auto j = 0; j < DIV_2; j++)
				{
					if (IsPointInRect(Dest2[i][j], mouseX, mouseY))
					{
						Dest2[i][j].SetIsSelected(true);
					}
				}
			}
		}
		break;
		case DIV_3:
		{
			for (auto i = 0; i < DIV_3; i++)
			{
				for (auto j = 0; j < DIV_3; j++)
				{
					if (IsPointInRect(Dest3[i][j], mouseX, mouseY))
					{
						Dest3[i][j].SetIsSelected(true);
					}
				}
			}
		}
		break;
		case DIV_4:
		{
			for (auto i = 0; i < DIV_4; i++)
			{
				for (auto j = 0; j < DIV_4; j++)
				{
					if (IsPointInRect(Dest4[i][j], mouseX, mouseY))
					{
						Dest4[i][j].SetIsSelected(true);
					}
				}
			}
		}
		break;
		case DIV_5:
		{
			for (auto i = 0; i < DIV_5; i++)
			{
				for (auto j = 0; j < DIV_5; j++)
				{
					if (IsPointInRect(Dest5[i][j], mouseX, mouseY))
					{
						Dest5[i][j].SetIsSelected(true);
					}
				}
			}
		}
		break;
		}

		RMouseButtonSelection = true;

		InvalidateRgn(hWnd, NULL, true);
	}
	break;
	case WM_RBUTTONUP:
	{
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);

		switch (GameMode)
		{
		case DIV_1:
		{
			for (auto i = 0; i < DIV_1; i++)
			{
				for (auto j = 0; j < DIV_1; j++)
				{
					if (Dest1[i][j].GetIsSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest1[i][j].GetSize();
						int dest_j = mouseY / Dest1[i][j].GetSize();

						g_savePosition.x = Dest1[dest_j][dest_i].GetPosition().x;
						g_savePosition.y = Dest1[dest_j][dest_i].GetPosition().y;

						// cout << dest_i << ", " << dest_j << endl;
						Dest1[i][j].SetIsSelected(false);
					}
				}
			}
		}
		break;
		case DIV_2:
		{
			for (auto i = 0; i < DIV_2; i++)
			{
				for (auto j = 0; j < DIV_2; j++)
				{
					if (Dest2[i][j].GetIsSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest2[i][j].GetSize();
						int dest_j = mouseY / Dest2[i][j].GetSize();

						g_savePosition.x = Dest2[dest_j][dest_i].GetPosition().x;
						g_savePosition.y = Dest2[dest_j][dest_i].GetPosition().y;

						// cout << dest_i << ", " << dest_j << endl;
						Dest2[i][j].SetIsSelected(false);
					}
				}
			}
		}
		break;
		case DIV_3:
		{
			for (auto i = 0; i < DIV_3; i++)
			{
				for (auto j = 0; j < DIV_3; j++)
				{
					if (Dest3[i][j].GetIsSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest3[i][j].GetSize();
						int dest_j = mouseY / Dest3[i][j].GetSize();

						g_savePosition.x = Dest3[dest_j][dest_i].GetPosition().x;
						g_savePosition.y = Dest3[dest_j][dest_i].GetPosition().y;

						// cout << dest_i << ", " << dest_j << endl;
						Dest3[i][j].SetIsSelected(false);
					}
				}
			}
		}
		break;
		case DIV_4:
		{
			for (auto i = 0; i < DIV_4; i++)
			{
				for (auto j = 0; j < DIV_4; j++)
				{
					if (Dest4[i][j].GetIsSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest4[i][j].GetSize();
						int dest_j = mouseY / Dest4[i][j].GetSize();

						g_savePosition.x = Dest4[dest_j][dest_i].GetPosition().x;
						g_savePosition.y = Dest4[dest_j][dest_i].GetPosition().y;

						// cout << dest_i << ", " << dest_j << endl;
						Dest4[i][j].SetIsSelected(false);
					}
				}
			}
		}
		break;
		case DIV_5:
		{
			for (auto i = 0; i < DIV_5; i++)
			{
				for (auto j = 0; j < DIV_5; j++)
				{
					if (Dest5[i][j].GetIsSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest5[i][j].GetSize();
						int dest_j = mouseY / Dest5[i][j].GetSize();

						g_savePosition.x = Dest5[dest_j][dest_i].GetPosition().x;
						g_savePosition.y = Dest5[dest_j][dest_i].GetPosition().y;

						// cout << dest_i << ", " << dest_j << endl;
						Dest5[i][j].SetIsSelected(false);
					}
				}
			}
		}
		break;
		}

		RMouseButtonSelection = false;

		InvalidateRgn(hWnd, NULL, true);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);

		if (LMouseButtonCornerSelection == false)
		{
			for (auto i = 0; i < DIV_3; i++)
			{
				for (auto j = 0; j < DIV_3; j++)
				{
					if (IsPointInRect(Source[i][j], mouseX, mouseY))
					{
						Source[i][j].SetIsSelected(true);

						g_saveImagePosition.x = Source[i][j].GetImagePosition().x;
						g_saveImagePosition.y = Source[i][j].GetImagePosition().y;

						LMouseButtonSelection = true;
					}
				}
			}
		}
		
		if (LMouseButtonSelection == false)
		{
			switch (GameMode)
			{
			case DIV_1:
			{
				for (auto i = 0; i < DIV_1; i++)
				{
					for (auto j = 0; j < DIV_1; j++)
					{
						if (IsPointInCorner(Dest1[i][j], mouseX, mouseY))
						{
							Dest1[i][j].SetIsCornerSelected(true);
							LMouseButtonCornerSelection = true;
						}
					}
				}
			}
			break;
			case DIV_2:
			{
				for (auto i = 0; i < DIV_2; i++)
				{
					for (auto j = 0; j < DIV_2; j++)
					{
						if (IsPointInCorner(Dest2[i][j], mouseX, mouseY))
						{
							Dest2[i][j].SetIsCornerSelected(true);
							LMouseButtonCornerSelection = true;
						}
					}
				}
			}
			break;
			case DIV_3:
			{
				for (auto i = 0; i < DIV_3; i++)
				{
					for (auto j = 0; j < DIV_3; j++)
					{
						if (IsPointInCorner(Dest3[i][j], mouseX, mouseY))
						{
							Dest3[i][j].SetIsCornerSelected(true);
							LMouseButtonCornerSelection = true;
						}
					}
				}
			}
			break;
			case DIV_4:
			{
				for (auto i = 0; i < DIV_4; i++)
				{
					for (auto j = 0; j < DIV_4; j++)
					{
						if (IsPointInCorner(Dest4[i][j], mouseX, mouseY))
						{
							Dest4[i][j].SetIsCornerSelected(true);
							LMouseButtonCornerSelection = true;
						}
					}
				}
			}
			break;
			case DIV_5:
			{
				for (auto i = 0; i < DIV_5; i++)
				{
					for (auto j = 0; j < DIV_5; j++)
					{
						if (IsPointInCorner(Dest5[i][j], mouseX, mouseY))
						{
							Dest5[i][j].SetIsCornerSelected(true);
							LMouseButtonCornerSelection = true;
						}
					}
				}
			}
			break;
			}
		}
		
		InvalidateRgn(hWnd, NULL, true);
	}
	break;
	case WM_LBUTTONUP:
	{
		isFirst = false;

		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);

		switch (GameMode)
		{
		case DIV_1:
		{
			for (auto i = 0; i < DIV_1; i++)
			{
				for (auto j = 0; j < DIV_1; j++)
				{
					if (Source[i][j].GetIsSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest1[i][j].GetSize();
						int dest_j = mouseY / Dest1[i][j].GetSize();

						// cout << dest_i << ", " << dest_j << endl;

						g_savePosition.x = Dest1[dest_j][dest_i].GetPosition().x;
						g_savePosition.y = Dest1[dest_j][dest_i].GetPosition().y;

						// cout << g_savePosition.x << ", " << g_savePosition.y << endl;

						Source[i][j].SetIsSelected(false);
						LMouseButtonSelection = false;
					}
					else if (Dest1[i][j].GetIsCornerSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest1[i][j].GetSize();
						int dest_j = mouseY / Dest1[i][j].GetSize();

						g_ProjectManager.SetSquareLength(Dest1[dest_j][dest_i].GetPosition().x + Dest1[dest_j][dest_i].GetSize() - Dest1[i][j].GetPosition().x,
							Dest1[dest_j][dest_i].GetPosition().y + Dest1[dest_j][dest_i].GetSize() - Dest1[i][j].GetPosition().y);

						Dest1[i][j].SetIsCornerSelected(false);
						LMouseButtonCornerSelection = false;
					}
				}
			}
		}
		break;
		case DIV_2:
		{
			for (auto i = 0; i < DIV_2; i++)
			{
				for (auto j = 0; j < DIV_2; j++)
				{
					if (Source[i][j].GetIsSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest2[i][j].GetSize();
						int dest_j = mouseY / Dest2[i][j].GetSize();

						// cout << dest_i << ", " << dest_j << endl;

						g_savePosition.x = Dest2[dest_j][dest_i].GetPosition().x;
						g_savePosition.y = Dest2[dest_j][dest_i].GetPosition().y;

						// cout << g_savePosition.x << ", " << g_savePosition.y << endl;

						Source[i][j].SetIsSelected(false);
						LMouseButtonSelection = false;
					}
					else if (Dest2[i][j].GetIsCornerSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest2[i][j].GetSize();
						int dest_j = mouseY / Dest2[i][j].GetSize();

						g_ProjectManager.SetSquareLength(Dest2[dest_j][dest_i].GetPosition().x + Dest2[dest_j][dest_i].GetSize() - Dest2[i][j].GetPosition().x,
							Dest2[dest_j][dest_i].GetPosition().y + Dest2[dest_j][dest_i].GetSize() - Dest2[i][j].GetPosition().y);

						Dest2[i][j].SetIsCornerSelected(false);
						LMouseButtonCornerSelection = false;
					}
				}
			}
		}
		break;
		case DIV_3:
		{
			for (auto i = 0; i < DIV_3; i++)
			{
				for (auto j = 0; j < DIV_3; j++)
				{
					if (Source[i][j].GetIsSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest3[i][j].GetSize();
						int dest_j = mouseY / Dest3[i][j].GetSize();

						// cout << dest_i << ", " << dest_j << endl;

						g_savePosition.x = Dest3[dest_j][dest_i].GetPosition().x;
						g_savePosition.y = Dest3[dest_j][dest_i].GetPosition().y;

						// cout << g_savePosition.x << ", " << g_savePosition.y << endl;

						Source[i][j].SetIsSelected(false);
						LMouseButtonSelection = false;
					}
					else if (Dest3[i][j].GetIsCornerSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest3[i][j].GetSize();
						int dest_j = mouseY / Dest3[i][j].GetSize();

						g_ProjectManager.SetSquareLength(Dest3[dest_j][dest_i].GetPosition().x + Dest3[dest_j][dest_i].GetSize() - Dest3[i][j].GetPosition().x,
							Dest3[dest_j][dest_i].GetPosition().y + Dest3[dest_j][dest_i].GetSize() - Dest3[i][j].GetPosition().y);

						Dest3[i][j].SetIsCornerSelected(false);
						LMouseButtonCornerSelection = false;
					}
				}
			}
		}
		break;
		case DIV_4:
		{
			for (auto i = 0; i < DIV_4; i++)
			{
				for (auto j = 0; j < DIV_4; j++)
				{
					if (Source[i][j].GetIsSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest4[i][j].GetSize();
						int dest_j = mouseY / Dest4[i][j].GetSize();

						// cout << dest_i << ", " << dest_j << endl;

						g_savePosition.x = Dest4[dest_j][dest_i].GetPosition().x;
						g_savePosition.y = Dest4[dest_j][dest_i].GetPosition().y;

						// cout << g_savePosition.x << ", " << g_savePosition.y << endl;

						Source[i][j].SetIsSelected(false);
						LMouseButtonSelection = false;
					}
					else if (Dest4[i][j].GetIsCornerSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest4[i][j].GetSize();
						int dest_j = mouseY / Dest4[i][j].GetSize();

						g_ProjectManager.SetSquareLength(Dest4[dest_j][dest_i].GetPosition().x + Dest4[dest_j][dest_i].GetSize() - Dest4[i][j].GetPosition().x,
							Dest4[dest_j][dest_i].GetPosition().y + Dest4[dest_j][dest_i].GetSize() - Dest4[i][j].GetPosition().y);

						Dest4[i][j].SetIsCornerSelected(false);
						LMouseButtonCornerSelection = false;
					}
				}
			}
		}
		break;
		case DIV_5:
		{
			for (auto i = 0; i < DIV_5; i++)
			{
				for (auto j = 0; j < DIV_5; j++)
				{
					if (Source[i][j].GetIsSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest5[i][j].GetSize();
						int dest_j = mouseY / Dest5[i][j].GetSize();

						// cout << dest_i << ", " << dest_j << endl;

						g_savePosition.x = Dest5[dest_j][dest_i].GetPosition().x;
						g_savePosition.y = Dest5[dest_j][dest_i].GetPosition().y;

						// cout << g_savePosition.x << ", " << g_savePosition.y << endl;

						Source[i][j].SetIsSelected(false);
						LMouseButtonSelection = false;
					}
					else if (Dest5[i][j].GetIsCornerSelected())
					{
						int dest_i = (mouseX - PAINTPAN_X) / Dest5[i][j].GetSize();
						int dest_j = mouseY / Dest5[i][j].GetSize();

						g_ProjectManager.SetSquareLength(Dest5[dest_j][dest_i].GetPosition().x + Dest5[dest_j][dest_i].GetSize() - Dest5[i][j].GetPosition().x,
							Dest5[dest_j][dest_i].GetPosition().y + Dest5[dest_j][dest_i].GetSize() - Dest5[i][j].GetPosition().y);

						Dest5[i][j].SetIsCornerSelected(false);
						LMouseButtonCornerSelection = false;
					}
				}
			}
		}
		break;
		}

		InvalidateRgn(hWnd, NULL, true);
	}
	break;
	case WM_MOUSEMOVE:
	{
		hdc = GetDC(hWnd);
		if (RMouseButtonSelection)
		{
			mouseX = LOWORD(lParam);
			mouseY = HIWORD(lParam);

			g_savePosition.x = mouseX;
			g_savePosition.y = mouseY;

			InvalidateRgn(hWnd, NULL, false);
		}
		else if (LMouseButtonSelection)
		{
			mouseX = LOWORD(lParam);
			mouseY = HIWORD(lParam);

			g_savePosition.x = mouseX;
			g_savePosition.y = mouseY;

			InvalidateRgn(hWnd, NULL, false);
		}
		else if (LMouseButtonCornerSelection)
		{
			mouseX = LOWORD(lParam);
			mouseY = HIWORD(lParam);

			switch (GameMode)
			{
			case DIV_1:
			{
				for (auto i = 0; i < DIV_1; i++)
				{
					for (auto j = 0; j < DIV_1; j++)
					{
						if (Dest1[i][j].GetIsCornerSelected())
						{
							g_ProjectManager.SetSquareLength(mouseX - Dest1[i][j].GetPosition().x, mouseY - Dest1[i][j].GetPosition().y);
						}
					}
				}
			}
			break;
			case DIV_2:
			{
				for (auto i = 0; i < DIV_2; i++)
				{
					for (auto j = 0; j < DIV_2; j++)
					{
						if (Dest2[i][j].GetIsCornerSelected())
						{
							g_ProjectManager.SetSquareLength(mouseX - Dest2[i][j].GetPosition().x, mouseY - Dest2[i][j].GetPosition().y);
						}
					}
				}
			}
			break;
			case DIV_3:
			{
				for (auto i = 0; i < DIV_3; i++)
				{
					for (auto j = 0; j < DIV_3; j++)
					{
						if (Dest3[i][j].GetIsCornerSelected())
						{
							g_ProjectManager.SetSquareLength(mouseX - Dest3[i][j].GetPosition().x, mouseY - Dest3[i][j].GetPosition().y);
						}
					}
				}
			}
			break;
			case DIV_4:
			{
				for (auto i = 0; i < DIV_4; i++)
				{
					for (auto j = 0; j < DIV_4; j++)
					{
						if (Dest4[i][j].GetIsCornerSelected())
						{
							g_ProjectManager.SetSquareLength(mouseX - Dest4[i][j].GetPosition().x, mouseY - Dest4[i][j].GetPosition().y);
						}
					}
				}
			}
			break;
			case DIV_5:
			{
				for (auto i = 0; i < DIV_5; i++)
				{
					for (auto j = 0; j < DIV_5; j++)
					{
						if (Dest5[i][j].GetIsCornerSelected())
						{
							g_ProjectManager.SetSquareLength(mouseX - Dest5[i][j].GetPosition().x, mouseY - Dest5[i][j].GetPosition().y);
						}
					}
				}
			}
			break;
			}

			InvalidateRgn(hWnd, NULL, false);
		}
		ReleaseDC(hWnd, hdc);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

bool IsPointInRect(CBmpComponent target, int x, int y)
{
	if ((target.GetPosition().x <= x && x <= target.GetPosition().x + target.GetSize()) &&
		(target.GetPosition().y <= y && y <= target.GetPosition().y + target.GetSize()))
	{
		return true;
	}
	else
		return false;
}

bool IsPointInCorner(CBmpComponent target, int x, int y)
{
	if ((target.GetPosition().x <= x && x <= (target.GetPosition().x + 10)) && (target.GetPosition().y <= y && y <= (target.GetPosition().y + 10)))
	{
		cout << "코너 클릭" << endl;
		return true;
	}
	if ((target.GetPosition().x + target.GetSize() - 10 <= x && x <= (target.GetPosition().x + target.GetSize())) && (target.GetPosition().y <= y && y <= (target.GetPosition().y + 10)))
	{
		cout << "코너 클릭" << endl;
		return true;
	}
	if ((target.GetPosition().x <= x && x <= (target.GetPosition().x)) && (target.GetPosition().y + target.GetSize() - 10 <= y && y <= (target.GetPosition().y + target.GetSize())))
	{
		cout << "코너 클릭" << endl;
		return true;
	}
	if ((target.GetPosition().x + target.GetSize() - 10 <= x && x <= (target.GetPosition().x + target.GetSize())) &&
		(target.GetPosition().y + target.GetSize() - 10 <= y && y <= (target.GetPosition().y + target.GetSize())))
	{
		cout << "코너 클릭" << endl;
		return true;
	}

	return false;
}