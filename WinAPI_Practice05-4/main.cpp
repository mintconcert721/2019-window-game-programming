#include <Windows.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <atlimage.h>

#pragma comment(lib, "msimg32.lib")

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

typedef struct _Point {
	int x;
	int y;

	_Point& operator=(_Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}
} Point;

template<typename T>
void Swap(T& left, T& right)
{
	T temp = left;
	left = right;
	right = temp;
}

using namespace std;

#define WINDOW_X 1280
#define WINDOW_Y 720

#define FRAME_X 135
#define FRAME_Y 150

#define MOVE 0
#define CLICKED 1

#define UP 0
#define LEFT 1
#define DOWN 2
#define RIGHT 3

#define ORIGIN 0
#define BIGGER 1
#define SMALLER 2

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 5-4";

int g_Mode = MOVE;
int g_VisibleIndex = 1;
int g_ControlIndex = 0;
Point g_Position;
Point g_Scale;

class CCharacter
{
private:
	bool isVisible;
	bool isLClicked = false;
	bool isRClicked = false;
	bool isJump = false;

	CImage m_SpriteImage[2];
	Point m_nSprite[2];

	int m_nSpriteEntireCount = 4;
	int m_nSpriteCurrentIndex = 0;

	Point m_CurrentPosition;
	Point m_Scale;

	int m_Speed = 10;
	int m_Direction;

	int m_ScaleMode = 0;
	bool isGoToOrigin = false;
	bool isFirst = true;

	int dangerHeight = 0;

public:
	bool IsPointInRect(int x, int y)
	{
		int left = m_CurrentPosition.x - m_Scale.x / 2;
		int right = m_CurrentPosition.x + m_Scale.x / 2;
		int up = m_CurrentPosition.y - m_Scale.y / 2;
		int down = m_CurrentPosition.y + m_Scale.y / 2;

		if ((left <= x && x <= right) && (up <= y && y <= down))
		{
			return true;
		}
		else
			return false;
	}

	void SetIsLClicked(bool value) { isLClicked = value; }
	const int GetIsLClicked() const { return isLClicked; }

	void SetIsRClicked(bool value) { isRClicked = value; }
	const int GetIsRClicked() const { return isRClicked; }

	void SetScaleMode(int mode) { m_ScaleMode = mode; }
	const int GetScaleMode() const { return m_ScaleMode; }

	void SetDirection(int direction) { m_Direction = direction; }
	const int GetDirection() const { return m_Direction; }

	void SetPosition(int x, int y)
	{
		m_CurrentPosition.x = x;
		m_CurrentPosition.y = y;
	}

	void SetScale(int x, int y)
	{
		m_Scale.x = x;
		m_Scale.y = y;
	}

	const Point GetPosition() const { return m_CurrentPosition; }
	const Point GetScale() const { return m_Scale; }

	void SetIsVisible(bool value) { isVisible = value; }
	const bool GetIsVisible() { return isVisible; }

	void SetSpeed(int speed) { m_Speed = speed; }
	const int GetSpeed() const { return m_Speed; }

	void SetJump(bool value) { isJump = value; }
	
	void Move()
	{
		// cout << m_CurrentPosition.x << ", " << m_CurrentPosition.y << endl;
		switch (m_Direction)
		{
		case UP:
		{
			if (m_CurrentPosition.y - m_Scale.y / 2 > 0)
			{
				m_CurrentPosition.y -= m_Speed;
			}
			else
			{
				if (m_Speed != 10 && isRClicked == true)
				{
					m_Speed = 10;
					isRClicked = false;
				}

				if (isLClicked)
				{
					isLClicked = false;
					isFirst = true;
				}

				m_Direction = DOWN;
			}
		}
		break;
		case LEFT:
		{
			if (m_CurrentPosition.x - m_Scale.x / 2 > 0)
			{
				m_CurrentPosition.x -= m_Speed;
			}
			else
			{
				if (m_Speed != 10 && isRClicked == true)
				{
					m_Speed = 10;
					isRClicked = false;
				}
				
				if (isLClicked)
				{
					isLClicked = false;
					isFirst = true;
				}

				m_Direction = RIGHT;
			}
		}
		break;
		case DOWN:
		{
			if (m_CurrentPosition.y + m_Scale.y / 2 < WINDOW_Y)
			{
				m_CurrentPosition.y += m_Speed;
			}
			else
			{
				if (m_Speed != 10 && isRClicked == true)
				{
					m_Speed = 10;
					isRClicked = false;
				}

				if (isLClicked)
				{
					isLClicked = false;
					isFirst = true;
				}

				m_Direction = UP;
			}
		}
		break;
		case RIGHT:
		{
			if (m_CurrentPosition.x + m_Scale.x / 2 < WINDOW_X)
			{
				m_CurrentPosition.x += m_Speed;
			}
			else
			{
				if (m_Speed != 10 && isRClicked == true)
				{
					m_Speed = 10;
					isRClicked = false;
				}


				if (isLClicked)
				{
					isLClicked = false;
					isFirst = true;
				}

				m_Direction = LEFT;
			}
		}
		break;
		}
	}

	void Update()
	{
		(++m_nSpriteCurrentIndex) %= m_nSpriteEntireCount;
		
		Move();

		switch (m_ScaleMode)
		{
		case ORIGIN:
			break;
		case BIGGER:
		{
			if (isGoToOrigin)
			{
				m_Scale.x -= 10;
				m_Scale.y -= 10;

				if (m_Scale.x <= 200 && m_Scale.y <= 200)
				{
					m_ScaleMode = ORIGIN;
					isGoToOrigin = false;
				}
			}
			else
			{
				m_Scale.x += 10;
				m_Scale.y += 10;

				if (m_Scale.x >= 300 && m_Scale.y >= 300)
					isGoToOrigin = true;
			}
		}
		break;
		case SMALLER:
		{
			if (isGoToOrigin)
			{
				m_Scale.x += 10;
				m_Scale.y += 10;

				if (m_Scale.x >= 200 && m_Scale.y >= 200)
				{
					m_ScaleMode = ORIGIN;
					isGoToOrigin = false;
				}
			}
			else
			{
				m_Scale.x -= 10;
				m_Scale.y -= 10;

				if (m_Scale.x <= 100 && m_Scale.y <= 100)
					isGoToOrigin = true;
			}
		}
		break;
		}

		if (isJump)
		{
			m_CurrentPosition.y -= 5;
			dangerHeight += 5;
			if (dangerHeight >= 50)
			{
				isJump = false;
			}
		}
		else
		{
			if (dangerHeight >= 5)
			{
				m_CurrentPosition.y += 5;
				dangerHeight -= 5;
			}
		}

		if (isLClicked && isFirst)
		{
			switch (m_Direction)
			{
			case UP:
			{
				srand((unsigned int)time(NULL));
				int randarray[3] = { LEFT, DOWN, RIGHT };
				int index = rand() % 3;
				m_Direction = randarray[index];
				isFirst = false;
			}
			break;
			case DOWN:
			{
				srand((unsigned int)time(NULL));
				int randarray[3] = { LEFT, UP, RIGHT };
				int index = rand() % 3;
				m_Direction = randarray[index];
				isFirst = false;
			}
			break;
			case LEFT:
			{
				srand((unsigned int)time(NULL));
				int randarray[3] = { UP, DOWN, RIGHT };
				int index = rand() % 3;
				m_Direction = randarray[index];
				isFirst = false;
			}
			break;
			case RIGHT:
			{
				srand((unsigned int)time(NULL));
				int randarray[3] = { UP, DOWN, LEFT };
				int index = rand() % 3;
				m_Direction = randarray[index];
				isFirst = false;
			}
			break;
			}
		}
	}

	void DrawSpriteImage(HDC _hdc, size_t _index)
	{
		int SpriteWidth = m_SpriteImage[_index].GetWidth() / m_nSprite[_index].x;
		int SpriteHeight = m_SpriteImage[_index].GetHeight() / m_nSprite[_index].y;

		int xCoord = m_nSpriteCurrentIndex % SpriteWidth;
		int yCoord = m_nSpriteCurrentIndex / SpriteWidth;

		m_SpriteImage[_index].Draw(_hdc, m_CurrentPosition.x - m_Scale.x / 2, m_CurrentPosition.y - m_Scale.y / 2, m_Scale.x, m_Scale.y,
			xCoord * m_nSprite[_index].x, yCoord * m_nSprite[_index].y, m_nSprite[_index].x, m_nSprite[_index].y);
	}

public:
	CCharacter()
	{
		isLClicked = false;
		isRClicked = false;
		isJump = false;

		m_SpriteImage[MOVE].Load(TEXT("./Sprite/mushmom_move.png"));
		m_SpriteImage[CLICKED].Load(TEXT("./Sprite/mushmom_hit.png"));

		m_nSprite[MOVE].x = 135; // 읽을 너비
		m_nSprite[MOVE].y = 150; // 읽을 높이

		m_nSprite[CLICKED].x = 143; // 읽을 너비
		m_nSprite[CLICKED].y = 150; // 읽을 높이

		m_nSpriteEntireCount = 4;
		m_nSpriteCurrentIndex = 0;

		m_Scale.x = 150;
		m_Scale.y = 150;

		m_CurrentPosition.x = WINDOW_X / 2;
		m_CurrentPosition.y = WINDOW_Y / 2;

		m_Direction = RIGHT;
	}
};

CCharacter g_Character[5];
CImage g_Background;

// 함수 선언
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static int startX, startY;
	static bool Drag;
	static int endX, endY;

	static HBITMAP hBitmap, hOldBitmap;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);
		Drag = false;

		g_Position.x = WINDOW_X / 2;
		g_Position.y = WINDOW_Y / 2;

		g_Scale.x = 150;
		g_Scale.y = 150;

		g_Character[0].SetIsVisible(true);
		g_Character[0].SetPosition(g_Position.x, g_Position.y);
		g_Character[0].SetScale(g_Scale.x, g_Scale.y);

		g_Background.Load(TEXT("./Sprite/background.png"));

		SetTimer(hWnd, 0, 150, NULL);
	}
	break;
	case WM_TIMER:
	{
		for (int i = 0; i < 5; i++)
		{
			if (g_Character[i].GetIsVisible())
			{
				g_Character[i].Update();
			}
		}
		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);

		g_Background.Draw(memdc, 0, 0, WINDOW_X, WINDOW_Y);

		// TODO
		for (int i = 0; i < 5; i++)
		{
			if (g_Character[i].GetIsVisible())
			{
				if (g_Character[i].GetIsLClicked())
				{
					g_Character[i].DrawSpriteImage(memdc, CLICKED);
				}
				else
				{
					g_Character[i].DrawSpriteImage(memdc, MOVE);
				}
				// cout << g_Character[i].GetScaleMode() << endl;
			}
		}

		/*Rectangle(memdc, g_Character[0].GetPosition().x - g_Character[0].GetScale().x / 2, g_Character[0].GetPosition().y - g_Character[0].GetScale().y / 2,
			g_Character[0].GetPosition().x + g_Character[0].GetScale().x / 2, g_Character[0].GetPosition().y + g_Character[0].GetScale().y / 2);*/

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
	{
		switch (wParam)
		{
		case 'j': case 'J':
		{
			for (int i = 0; i < 5; i++)
			{
				if (g_Character[i].GetIsVisible())
					g_Character[i].SetJump(true);
			}
		}
		break;
		case 'e': case 'E':
		{
			for (int i = 0; i < 5; i++)
			{
				if (g_Character[i].GetIsVisible())
					g_Character[i].SetScaleMode(BIGGER);
			}
		}
		break;
		case 's': case 'S':
		{
			for (int i = 0; i < 5; i++)
			{
				if (g_Character[i].GetIsVisible())
					g_Character[i].SetScaleMode(SMALLER);
			}
		}
		break;
		case 't': case 'T':
		{
			srand((unsigned int)time(NULL));

			int dir = rand() % 4;
			int x = rand() % 800 + 200;
			int y = rand() % 200 + 200;

			g_Character[g_VisibleIndex].SetDirection(dir);
			g_Character[g_VisibleIndex].SetIsVisible(true);
			g_Character[g_VisibleIndex].SetPosition(x, y);
			g_Character[g_VisibleIndex].SetScale(g_Scale.x, g_Scale.y);
			g_VisibleIndex++;
		}
		break;
		}
	}
	break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_UP:
		{
			for (int i = 0; i < 5; i++)
			{
				if(g_Character[i].GetIsVisible())
					g_Character[i].SetDirection(UP);
			}
		}
		break;
		case VK_DOWN:
		{
			for (int i = 0; i < 5; i++)
			{
				if (g_Character[i].GetIsVisible())
					g_Character[i].SetDirection(DOWN);
			}
		}
		break;
		case VK_LEFT:
		{
			for (int i = 0; i < 5; i++)
			{
				if (g_Character[i].GetIsVisible())
					g_Character[i].SetDirection(LEFT);
			}
		}
		break;
		case VK_RIGHT:
		{
			for (int i = 0; i < 5; i++)
			{
				if (g_Character[i].GetIsVisible())
					g_Character[i].SetDirection(RIGHT);
			}
		}
		break;
		}
	}
	break;
	case WM_RBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);

		for (int i = 0; i < 5; i++)
		{
			if (g_Character[i].GetIsVisible())
			{
				if (g_Character[i].IsPointInRect(mousex, mousey))
				{
					g_Character[i].SetSpeed(50);
					g_Character[i].SetIsRClicked(true);
				}
			}
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);

		for (int i = 0; i < 5; i++)
		{
			if (g_Character[i].GetIsVisible())
			{
				if (g_Character[i].IsPointInRect(mousex, mousey))
				{
					g_Character[i].SetIsLClicked(true);
				}
			}
		}
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void DrawSprite(HDC _hdc, Point startCoordinate = Point{ 0, 0 })
{

}