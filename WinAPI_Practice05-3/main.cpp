#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include "resource.h"

#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

template<typename T>
void Swap(T& left, T& right)
{
	T temp = left;
	left = right;
	right = temp;
}

using namespace std;

#define WINDOW_X 1280
#define WINDOW_Y 720

#define ORIGINAL 1.0
#define ENLARGE_2 1.2
#define ENLARGE_4 1.4

int Mode = 0;
int BiggerValue = 0;
double Multiple = 0.0;
bool isFullScreen = false;
bool isCopy = false;
bool isPaste = false;
bool isReversed = false;
bool isFirst = true;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 5-3";

typedef struct _Point {
	int x;
	int y;

	_Point& operator=(_Point& rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return *this;
	}
} Point;

typedef struct _MagnifyingGlass{
	Point position;
	RECT rect;
	int width;
	int height;
} MagnifyingGlass;

MagnifyingGlass g_MyGlass;
MagnifyingGlass g_MyCopyRect;
MagnifyingGlass g_MySource;

Point g_OriginPosition;
Point g_ContinuedPosition;

class CBmpComponent
{
private:
	Point m_Position;
	Point m_ImagePosition;
	int m_Size;
	bool m_IsSelected = false;
	bool m_IsCornerSelected = false;

public:
	void SetIsCornerSelected(bool value)
	{
		m_IsCornerSelected = value;
	}

	const bool GetIsCornerSelected()
	{
		return m_IsCornerSelected;
	}

	void SetIsSelected(bool value)
	{
		m_IsSelected = value;
	}

	const bool GetIsSelected()
	{
		return m_IsSelected;
	}

	void SetImagePosition(int x, int y)
	{
		m_ImagePosition.x = x;
		m_ImagePosition.y = y;
	}

	const Point GetImagePosition() const
	{
		return m_ImagePosition;
	}

	void SetPosition(int x, int y)
	{
		m_Position.x = x;
		m_Position.y = y;
	}

	const Point GetPosition() const
	{
		return m_Position;
	}

	void SetSize(int size)
	{
		m_Size = size;
	}

	const int GetSize() const
	{
		return m_Size;
	}

	CBmpComponent& operator=(const CBmpComponent& rhs)
	{
		if (this != &rhs)
		{
			m_ImagePosition.x = rhs.m_ImagePosition.x;
			m_ImagePosition.y = rhs.m_ImagePosition.y;
		}

		return *this;
	}

public:
	CBmpComponent()
	{
	}

	CBmpComponent(const CBmpComponent& rhs)
	{
		m_Position.x = rhs.m_Position.x;
		m_Position.y = rhs.m_Position.y;

		m_ImagePosition.x = rhs.m_ImagePosition.x;
		m_ImagePosition.y = rhs.m_ImagePosition.y;

		m_Size = rhs.m_Size;
		m_IsSelected = rhs.m_IsSelected;
	}
};

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
bool IsPointInRect(CBmpComponent target, int x, int y);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 50, 50,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, memdc;
	PAINTSTRUCT ps;

	static int startX, startY;
	static bool Drag;
	static int endX, endY;

	static HBITMAP hBitmap;
	static HPEN hPen, hOldPen;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		g_MyGlass.rect = RECT{ 0, 0, 0, 0 };
		g_MySource.rect = RECT{ 0, 0, WINDOW_X, WINDOW_Y };
		hBitmap = (HBITMAP)LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_MAPLESTORY));
		hPen = CreatePen(PS_SOLID, 3, RGB(0, 0, 0));
		Drag = false;
		break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);
		memdc = CreateCompatibleDC(hdc);
		SelectObject(memdc, hBitmap);

		BitBlt(hdc, 0, 0, WINDOW_X, WINDOW_Y, memdc, 0, 0, SRCCOPY);

		Rectangle(hdc, g_MyGlass.rect.left, g_MyGlass.rect.top, g_MyGlass.rect.right, g_MyGlass.rect.bottom);

		// 돋보기 메인 (1배, 1.2배, 1.4배)
		if (isFirst == false)
		{
			hOldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, g_MyGlass.rect.left, g_MyGlass.rect.top, g_MyGlass.rect.right + BiggerValue, g_MyGlass.rect.bottom + BiggerValue);
			SelectObject(hdc, hOldPen);
		}

		if (isReversed)
		{
			SetStretchBltMode(hdc, DSTINVERT);
			StretchBlt(hdc, g_MyGlass.rect.left, g_MyGlass.rect.top, g_MyGlass.rect.right - g_MyGlass.rect.left + BiggerValue, g_MyGlass.rect.bottom - g_MyGlass.rect.top + BiggerValue,
				memdc, 
				g_MySource.rect.left + (g_MySource.rect.right - g_MySource.rect.left) / Multiple,
				g_MySource.rect.top + (g_MySource.rect.bottom - g_MySource.rect.top) / Multiple,
				-(g_MySource.rect.right - g_MySource.rect.left) / Multiple, 
				-(g_MySource.rect.bottom - g_MySource.rect.top) / Multiple,
				SRCCOPY);
		}
		else
		{
			StretchBlt(hdc, g_MyGlass.rect.left, g_MyGlass.rect.top, g_MyGlass.rect.right - g_MyGlass.rect.left + BiggerValue, g_MyGlass.rect.bottom - g_MyGlass.rect.top + BiggerValue,
				memdc,
				g_MySource.rect.left,
				g_MySource.rect.top,
				(g_MySource.rect.right - g_MySource.rect.left) / Multiple,
				(g_MySource.rect.bottom - g_MySource.rect.top) / Multiple,
				SRCCOPY);
		}

		// 복사 대상이 있다면
		if (isPaste)
		{
			StretchBlt(hdc, g_MyCopyRect.position.x, g_MyCopyRect.position.y, g_MyCopyRect.width + BiggerValue, g_MyCopyRect.height + BiggerValue,
				memdc, g_MyCopyRect.rect.left, g_MyCopyRect.rect.top, g_MyCopyRect.width / Multiple, g_MyCopyRect.height / Multiple, SRCCOPY);
		}

		if (isFullScreen)
		{
			StretchBlt(hdc, 0, 0, WINDOW_X, WINDOW_Y,
				memdc, g_MySource.rect.left, g_MySource.rect.top, (g_MySource.rect.right - g_MySource.rect.left) / Multiple, (g_MySource.rect.bottom - g_MySource.rect.top) / Multiple, SRCCOPY);
		}

		if (isReversed)
		{

		}

		DeleteDC(memdc);
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
	{
		switch (wParam)
		{
		case '0':
			Multiple = ORIGINAL;
			break;
		case '1':
			Multiple = ENLARGE_2;
			break;
		case '2':
			Multiple = ENLARGE_4;
			break;
		case 'c': case 'C':
		{
			g_MyCopyRect.rect.left = g_MyGlass.rect.left;
			g_MyCopyRect.rect.top = g_MyGlass.rect.top;
			g_MyCopyRect.width = g_MyGlass.rect.right - g_MyGlass.rect.left;
			g_MyCopyRect.height = g_MyGlass.rect.bottom - g_MyGlass.rect.top;
			isCopy = true;
		}
		break;
		case 'p': case 'P':
		{
			isPaste = true;
		}
		break;
		case 'r': case 'R':
		{
			Multiple = ORIGINAL;
			isCopy = false;
			isPaste = false;
			isFullScreen = false;
			isReversed = false;
			BiggerValue = 0;
			isFirst = false;
			g_MyGlass.rect = { 0, 0, 0, 0 };
		}
		break;
		case 'x': case 'X':
		{
			if (isFullScreen)
				isFullScreen = false;
			else
				isFullScreen = true;
		}
		break;
		case 'm': // 사각형 작게
		{
			if (BiggerValue > 0)
				BiggerValue -= 10;
		}
		break;
		case 'M': // 사각형 크게
		{
			if (BiggerValue < 100)
				BiggerValue += 10;
		}
		break;
		case 'h': case 'H': // 반전
		{
			if (isReversed)
				isReversed = false;
			else
				isReversed = true;
		}
		break;
		case 'w': case 'W': // 위
			g_MySource.rect.top -= 1;
			g_MySource.rect.bottom -= 1;
			break;
		case 's': case 'S': // 아래
			g_MySource.rect.top += 1;
			g_MySource.rect.bottom += 1;
			break;
		case 'a': case 'A': // 왼쪽
			g_MySource.rect.left -= 1;
			g_MySource.rect.right -= 1;
			break;
		case 'd': case 'D': // 오른쪽
			g_MySource.rect.left += 1;
			g_MySource.rect.right += 1;
			break;
		}

		InvalidateRgn(hWnd, NULL, true);
	}
	break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_UP:
			g_MyGlass.rect.top -= 5;
			g_MyGlass.rect.bottom -= 5;
			g_MySource.rect.top -= 5;
			g_MySource.rect.bottom -= 5;
			break;
		case VK_DOWN:
			g_MyGlass.rect.top += 5;
			g_MyGlass.rect.bottom += 5;
			g_MySource.rect.top += 5;
			g_MySource.rect.bottom += 5;
			break;
		case VK_LEFT:
			g_MyGlass.rect.left -= 5;
			g_MyGlass.rect.right -= 5;
			g_MySource.rect.left -= 5;
			g_MySource.rect.right -= 5;
			break;
		case VK_RIGHT:
			g_MyGlass.rect.left += 5;
			g_MyGlass.rect.right += 5;
			g_MySource.rect.left += 5;
			g_MySource.rect.right += 5;
			break;
		}
		InvalidateRgn(hWnd, NULL, true);
	}
	break;
	case WM_RBUTTONDOWN:
	{
		if (isCopy)
		{
			g_MyCopyRect.position.x = LOWORD(lParam);
			g_MyCopyRect.position.y = HIWORD(lParam); // 시작 좌표

			isCopy = false;
		}
	}
	break;
	case WM_RBUTTONUP:
	{
		// InvalidateRgn(hWnd, NULL, true);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		Drag = true;
		g_MyGlass.rect.left = LOWORD(lParam);
		g_MyGlass.rect.top = HIWORD(lParam); // 시작 좌표
		g_MySource.rect.left = LOWORD(lParam);
		g_MySource.rect.top = HIWORD(lParam); // 시작 좌표
	}
	break;
	case WM_LBUTTONUP:
	{
		g_MyGlass.rect.right = LOWORD(lParam);
		g_MyGlass.rect.bottom = HIWORD(lParam);
		g_MySource.rect.right = LOWORD(lParam);
		g_MySource.rect.bottom = HIWORD(lParam); // 시작 좌표
		Drag = false;
		InvalidateRgn(hWnd, NULL, true);
	}
	break;
	case WM_MOUSEMOVE:
	{
		if (Drag)
		{
			// hdc = GetDC(hWnd);
			g_MyGlass.rect.right = LOWORD(lParam);
			g_MyGlass.rect.bottom = HIWORD(lParam);
			g_MySource.rect.right = LOWORD(lParam);
			g_MySource.rect.bottom = HIWORD(lParam); // 시작 좌표
			// Rectangle(hdc, g_MyGlass.rect.left, g_MyGlass.rect.top, g_MyGlass.rect.right, g_MyGlass.rect.bottom);
			// ReleaseDC(hWnd, hdc);
			InvalidateRgn(hWnd, NULL, false);
		}
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

bool IsPointInRect(CBmpComponent target, int x, int y)
{
	if ((target.GetPosition().x <= x && x <= target.GetPosition().x + target.GetSize()) &&
		(target.GetPosition().y <= y && y <= target.GetPosition().y + target.GetSize()))
	{
		return true;
	}
	else
		return false;
}