#include <Windows.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 2-2";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	540
#define WINDOW_X 800
#define WINDOW_Y 600

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

typedef struct _Point {
	size_t x;
	size_t y;
	_Point(size_t _x, size_t _y) : x(_x), y(_y) {}
} Point;

class CProjectManager
{
private:
	size_t m_Dan;
	Point m_PrintPosition;

public:
	void SetOption(static char* _str)
	{
		char tempX[10];
		size_t idx_X = 0;

		while (*_str != ' ')
		{
			tempX[idx_X++] = *_str;
			_str++;
		}
		m_PrintPosition.x = atoi(tempX);
		_str++;

		char tempY[10];
		size_t idx_Y = 0;

		while (*_str != ' ')
		{
			tempY[idx_Y++] = *_str;
			_str++;
		}
		m_PrintPosition.y = atoi(tempY);
		_str++;

		char tempDan[10];
		size_t idx_Dan = 0;

		while (*_str != '\0')
		{
			tempDan[idx_Dan++] = *_str;
			_str++;
		}
		m_Dan = atoi(tempDan);
	}

	const Point GetPosition() const { return m_PrintPosition; }
	const size_t GetDan() const { return m_Dan; }

	CProjectManager() : m_Dan(0), m_PrintPosition(0, 0)// 생성자
	{
	}
};

CProjectManager g_Manager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	static char str[256];
	static int count;

	static char buffer[256];
	static bool IsInput = false;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		count = 0;
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		TextOut(hdc, 0, 0, str, count);

		if (IsInput)
		{
			for (int idx = 1; idx < 10; idx++)
			{
				sprintf_s(buffer, "%d X %d = %d", g_Manager.GetDan(), idx, g_Manager.GetDan()*idx);
				TextOut(hdc, g_Manager.GetPosition().x, g_Manager.GetPosition().y + 20 * idx, buffer, strlen(buffer));
			}
		}

		EndPaint(hWnd, &ps);
		break;
	case WM_CHAR:
		hdc = GetDC(hWnd);
		if (wParam == VK_SPACE) str[count++] = ' ';
		else if ((wParam == VK_BACK) && (count > 0)) count--;
		else if (wParam == VK_RETURN)
		{
			IsInput = true;
			g_Manager.SetOption(str);
			if (g_Manager.GetDan() == 0) exit(0);
			count = 0;
		}
		else
		{
			if ((wParam != VK_BACK) && (wParam != VK_RETURN))
			{
				str[count++] = (TCHAR)wParam;
				str[count] = '\0';
			}
		}
		InvalidateRect(hWnd, NULL, TRUE);
		ReleaseDC(hWnd, hdc);
		break;
	case WM_TIMER:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}