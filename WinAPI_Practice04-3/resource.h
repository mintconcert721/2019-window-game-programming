//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// WinAPI_Practice04-3.rc에서 사용되고 있습니다.
//
#define ID_MENU                         101
#define ID_SHAPE_40001                  40001
#define ID_SHAPE_40002                  40002
#define ID_SHAPE_40003                  40003
#define ID_COLOR_40004                  40004
#define ID_COLOR_40005                  40005
#define ID_COLOR_40006                  40006
#define ID_COLOR_40007                  40007
#define ID_COLOR_40008                  40008
#define ID_COLOR_40009                  40009
#define ID_GRID_40010                   40010
#define ID_GRID_40011                   40011
#define ID_GRID_OFF                     40012
#define ID_GRID_ON                      40013
#define ID_COLOR_01                     40014
#define ID_COLOR_02                     40015
#define ID_COLOR_03                     40016
#define ID_COLOR_04                     40017
#define ID_COLOR_05                     40018
#define ID_COLOR_06                     40019
#define ID_SHAPE_CIRCLE                 40020
#define ID_SHAPE_RECT                   40021
#define ID_SHAPE_FREEDRAW               40022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40023
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
