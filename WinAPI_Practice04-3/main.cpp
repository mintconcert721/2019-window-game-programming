#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include "resource.h"

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 4-3";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 1000
#define WINDOW_Y 1000

#define SECOND 1000
#define BOARD_NUM 100
#define RADIUS 50
#define FPS 60

#define CIRCLE 1
#define RECTANGLE 2
#define FREEDRAW 3

COLORREF color1 = RGB(255, 0, 0);
COLORREF color2 = RGB(0, 255, 0);
COLORREF color3 = RGB(0, 0, 255);
COLORREF color4 = RGB(255, 0, 255); // 분홍
COLORREF color5 = RGB(255, 255, 0); // 노랑
COLORREF color6 = RGB(0, 255, 255); // 하늘

bool g_IsGrid = true;
int g_Shape_Kind = CIRCLE;
COLORREF g_Color_Info = color1;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);

typedef struct _Shape {
	RECT rect;
	int kind;
	COLORREF color;
} Shape;

typedef struct _FreeDrawShape {
	RECT rect;
	int kind;
	bool isDrawing = false;
	COLORREF color;
} FreeDrawShape;

Shape g_ShapeArray[50];
FreeDrawShape g_FreeDrawArray[BOARD_NUM][BOARD_NUM];

int g_ArrayIndex = 0;
int g_FreeDrawIndex = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.lpszMenuName = MAKEINTRESOURCE(ID_MENU);
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc;
	PAINTSTRUCT ps;

	static bool selection = false;

	int mouseX = 0;
	int mouseY = 0;

	static int startX, startY, oldX, oldY;
	static int endX, endY;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		selection = false;

		for (auto i = 0; i < BOARD_NUM; i++)
		{
			for (auto j = 0; j < BOARD_NUM; j++)
			{
				g_FreeDrawArray[i][j].rect.top = i * 10;
				g_FreeDrawArray[i][j].rect.left = j * 10;
				g_FreeDrawArray[i][j].rect.bottom = g_FreeDrawArray[i][j].rect.top + 10;
				g_FreeDrawArray[i][j].rect.right = g_FreeDrawArray[i][j].rect.left + 10;
			}
		}
	}
		break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		////////////////////////////////////////////////////// TODO //////////////////////////////////////////////////////

		// TODO : 모눈종이 ON/OFF
		if (g_IsGrid)
		{
			for (auto i = 0; i < BOARD_NUM; i++)
			{
				for (auto j = 0; j < BOARD_NUM; j++)
				{
					HPEN m_Hpen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
					HPEN m_Oldpen = (HPEN)SelectObject(hdc, m_Hpen);
					HBRUSH hBrush = CreateSolidBrush(RGB(0, 0, 0));
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBrush);
					Rectangle(hdc, i * 10, j * 10, (i + 1) * 10, (j + 1) * 10);
					SelectObject(hdc, oldBrush);
					DeleteObject(hBrush);
					SelectObject(hdc, m_Oldpen);
					DeleteObject(m_Hpen);
				}
			}			
		}

		SetROP2(hdc, R2_MERGEPEN);

		// TODO : 도형 그리기
		for (auto i = 0; i < g_ArrayIndex + 1; i++)
		{
			HBRUSH hBrush = CreateSolidBrush(g_ShapeArray[i].color);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBrush);
			switch (g_ShapeArray[i].kind)
			{
			case CIRCLE:
				Ellipse(hdc, g_ShapeArray[i].rect.left, g_ShapeArray[i].rect.top, g_ShapeArray[i].rect.right, g_ShapeArray[i].rect.bottom);
				break;
			case RECTANGLE:
				Rectangle(hdc, g_ShapeArray[i].rect.left, g_ShapeArray[i].rect.top, g_ShapeArray[i].rect.right, g_ShapeArray[i].rect.bottom);
				break;
			}
			SelectObject(hdc, oldBrush);
			DeleteObject(hBrush);
		}

		// TODO : 도형 그리기
		for (auto i = 0; i < BOARD_NUM; i++)
		{
			for (auto j = 0; j < BOARD_NUM; j++)
			{
				if (g_FreeDrawArray[i][j].isDrawing)
				{
					HBRUSH hBrush = CreateSolidBrush(g_FreeDrawArray[i][j].color);
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBrush);
					Rectangle(hdc, g_FreeDrawArray[i][j].rect.top, g_FreeDrawArray[i][j].rect.left, g_FreeDrawArray[i][j].rect.bottom, g_FreeDrawArray[i][j].rect.right);
					SelectObject(hdc, oldBrush);
					DeleteObject(hBrush);
				}
			}
		}

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
		break;
	case WM_KEYDOWN:
		break;
	case WM_LBUTTONDOWN:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);

		{
			selection = true;
			g_ShapeArray[g_ArrayIndex].rect.left = mouseX;
			g_ShapeArray[g_ArrayIndex].rect.top = mouseY;
			g_ShapeArray[g_ArrayIndex].color = g_Color_Info;
			g_ShapeArray[g_ArrayIndex].kind = g_Shape_Kind;
		}
		break;
	case WM_LBUTTONUP:
		InvalidateRect(hWnd, NULL, TRUE);

		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);

		{
			selection = false;

			g_ShapeArray[g_ArrayIndex].rect.right = mouseX;
			g_ShapeArray[g_ArrayIndex].rect.bottom = mouseY;
			
			g_ArrayIndex++;
		}
		break;
	case WM_MOUSEMOVE:
	{
		if (selection == true)
		{
			hdc = GetDC(hWnd);

			mouseX = LOWORD(lParam);
			mouseY = HIWORD(lParam);

			switch (g_Shape_Kind)
			{
			case CIRCLE:
				{
					Ellipse(hdc, g_ShapeArray[g_ArrayIndex].rect.left, g_ShapeArray[g_ArrayIndex].rect.top, g_ShapeArray[g_ArrayIndex].rect.right, g_ShapeArray[g_ArrayIndex].rect.bottom);
					g_ShapeArray[g_ArrayIndex].rect.right = mouseX;
					g_ShapeArray[g_ArrayIndex].rect.bottom = mouseY;
					Ellipse(hdc, g_ShapeArray[g_ArrayIndex].rect.left, g_ShapeArray[g_ArrayIndex].rect.top, g_ShapeArray[g_ArrayIndex].rect.right, g_ShapeArray[g_ArrayIndex].rect.bottom);
				}
				break;
			case RECTANGLE:
				{
					Rectangle(hdc, g_ShapeArray[g_ArrayIndex].rect.left, g_ShapeArray[g_ArrayIndex].rect.top, g_ShapeArray[g_ArrayIndex].rect.right, g_ShapeArray[g_ArrayIndex].rect.bottom);
					g_ShapeArray[g_ArrayIndex].rect.right = mouseX;
					g_ShapeArray[g_ArrayIndex].rect.bottom = mouseY;
					Rectangle(hdc, g_ShapeArray[g_ArrayIndex].rect.left, g_ShapeArray[g_ArrayIndex].rect.top, g_ShapeArray[g_ArrayIndex].rect.right, g_ShapeArray[g_ArrayIndex].rect.bottom);
				}
				break;
			case FREEDRAW:
				{
					int a = mouseX / 10;
					int b = mouseY / 10;

					if (g_FreeDrawArray[a][b].isDrawing == false)
					{
						g_FreeDrawArray[a][b].color = g_Color_Info;
						g_FreeDrawArray[a][b].isDrawing = true;
					}
				}
				break;
			}

			ReleaseDC(hWnd, hdc);
		}
		InvalidateRect(hWnd, NULL, TRUE);
	}
		break;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case ID_GRID_ON:
			g_IsGrid = true;
			break;
		case ID_GRID_OFF:
			g_IsGrid = false;
			break;
		case ID_COLOR_01:
			g_Color_Info = color1;
			break;
		case ID_COLOR_02:
			g_Color_Info = color2;
			break;
		case ID_COLOR_03:
			g_Color_Info = color3;
			break;
		case ID_COLOR_04:
			g_Color_Info = color4;
			break;
		case ID_COLOR_05:
			g_Color_Info = color5;
			break;
		case ID_COLOR_06:
			g_Color_Info = color6;
			break;
		case ID_SHAPE_CIRCLE:
			g_Shape_Kind = CIRCLE;
			break;
		case ID_SHAPE_RECT:
			g_Shape_Kind = RECTANGLE;
			break;
		case ID_SHAPE_FREEDRAW:
			g_Shape_Kind = FREEDRAW;
			break;
		}
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}