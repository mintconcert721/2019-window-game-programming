#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <iostream>

using namespace std;

HINSTANCE g_hInstance;
LPCTSTR lpszClass = "Window Class Name";
LPCTSTR lpszWindowName = "Window Program 2-5";

#define SCREEN_X 1920
#define SCREEN_Y 1080
#define SCREEN_CENTER_X 960
#define SCREEN_CENTER_Y	510
#define WINDOW_X 800
#define WINDOW_Y 800

#define KIND_LINE 1
#define KIND_CIRCLE 2
#define KIND_TRIANGLE 3
#define KIND_RECTANGLE 4

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

typedef struct _Coordi {
	int x = 0;
	int y = 0;
} Coordi;

class CProjectManager
{
private:
	HDC m_Hdc;
	HPEN m_Pen;
	HPEN m_OldPen;
	HBRUSH m_Brush;
	HBRUSH m_OldBrush;
	COLORREF m_PenColor;
	COLORREF m_FigureColor;

	int m_Thickness;
	int m_FigureKind;

	int m_Left;
	int m_Top;
	int m_Right;
	int m_Bottom;

public:
	void MoveFigure(HWND _hwnd, WPARAM _wParam)
	{
		m_Hdc = GetDC(_hwnd);
		switch (_wParam)
		{
		case VK_UP:
			if (m_Top > 0)
			{
				m_Top -= 10;
				m_Bottom -= 10;
			}
			break;
		case VK_DOWN:
			if (m_Bottom < WINDOW_Y)
			{
				m_Top += 10;
				m_Bottom += 10;
			}
			break;
		case VK_LEFT:
			if (m_Left > 0)
			{
				m_Left -= 10;
				m_Right -= 10;
			}
			break;
		case VK_RIGHT:
			if (m_Right < WINDOW_X)
			{
				m_Left += 10;
				m_Right += 10;
			}
			break;
		}
		ReleaseDC(_hwnd, m_Hdc);
		InvalidateRect(_hwnd, NULL, TRUE);
	}

	void SetThickness(int _width)
	{
		if(m_Thickness < 30)
			m_Thickness += _width;
	}

	void SetThinness(int _width)
	{
		if(m_Thickness > 0)
			m_Thickness -= _width;
	}

	void SetFigure(static char* _str)
	{
		char _FigureKind[10];
		size_t idx_FigureKind = 0;

		while (*_str != ' ')
		{
			_FigureKind[idx_FigureKind++] = *_str;
			_str++;
		}
		m_FigureKind = atoi(_FigureKind);
		_str++;

		char _Left[10];
		size_t idx_Left = 0;

		while (*_str != ' ')
		{
			_Left[idx_Left++] = *_str;
			_str++;
		}
		m_Left = atoi(_Left);
		_str++;

		char _Top[10];
		size_t idx_Top = 0;

		while (*_str != ' ')
		{
			_Top[idx_Top++] = *_str;
			_str++;
		}
		m_Top = atoi(_Top);
		_str++;

		char _Right[10];
		size_t idx_Right = 0;

		while (*_str != ' ')
		{
			_Right[idx_Right++] = *_str;
			_str++;
		}
		m_Right = atoi(_Right);
		_str++;

		char _Bottom[10];
		size_t idx_Bottom = 0;

		while (*_str != ' ')
		{
			_Bottom[idx_Bottom++] = *_str;
			_str++;
		}
		m_Bottom = atoi(_Bottom);
		_str++;

		char _Thickness[10];
		size_t idx_Thickness = 0;

		while (*_str != '\0')
		{
			_Thickness[idx_Thickness++] = *_str;
			_str++;
		}
		m_Thickness = atoi(_Thickness);
	}

	void DrawPaint(HWND _hwnd)
	{
		srand(unsigned int(time(NULL)));
		int r, g, b;

		r = rand() % 255;
		g = rand() % 255;
		b = rand() % 255;

		m_PenColor = RGB(r, g, b);

		r = rand() % 255;
		g = rand() % 255;
		b = rand() % 255;

		m_FigureColor = RGB(r, g, b);

		m_Pen = CreatePen(PS_SOLID, m_Thickness, m_PenColor);
		m_OldPen = CreatePen(PS_SOLID, m_Thickness, m_PenColor);

		m_Brush = CreateSolidBrush(m_FigureColor);
		m_OldBrush = CreateSolidBrush(m_FigureColor);

		m_Hdc = GetDC(_hwnd);

		m_OldPen = (HPEN)SelectObject(m_Hdc, m_Pen);
		m_OldBrush = (HBRUSH)SelectObject(m_Hdc, m_Brush);

		POINT point[3] = {{m_Left, m_Bottom}, {m_Right, m_Bottom}, {(m_Left + m_Right) / 2, m_Top}};
		
		switch (m_FigureKind)
		{
		case KIND_LINE:
			MoveToEx(m_Hdc, m_Left, m_Top, NULL);
			LineTo(m_Hdc, m_Right, m_Bottom);
			break;
		case KIND_CIRCLE:
			Ellipse(m_Hdc, m_Left, m_Top, m_Right, m_Bottom);
			break;
		case KIND_TRIANGLE:
			Polygon(m_Hdc, point, 3);
			break;
		case KIND_RECTANGLE:
			Rectangle(m_Hdc, m_Left, m_Top, m_Right, m_Bottom);
			break;
		}

		SelectObject(m_Hdc, m_OldPen);
		SelectObject(m_Hdc, m_OldBrush);
		DeleteObject(m_Pen);
		DeleteObject(m_Brush);

		ReleaseDC(_hwnd, m_Hdc);
	}

	const int GetFigureKind() const { return m_FigureKind; }

	CProjectManager() : m_PenColor(RGB(0, 0, 0)), m_FigureColor(RGB(0, 0, 0)), m_Thickness(1), m_FigureKind(0), m_Left(0), m_Top(0), m_Right(0), m_Bottom(0)
	{
	}
};

CProjectManager g_Manager;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	int FrameX = 0;
	int FrameY = 0;
	int CaptionY = 0;

	FrameX = GetSystemMetrics(SM_CXFRAME);
	FrameY = GetSystemMetrics(SM_CYFRAME);
	CaptionY = GetSystemMetrics(SM_CYCAPTION);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW
		, SCREEN_CENTER_X - WINDOW_X / 2, SCREEN_CENTER_Y - WINDOW_Y / 2, WINDOW_X + (FrameX << 1), WINDOW_Y + (FrameY << 1) + CaptionY, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;

	static char str[256];
	static int count;
	static SIZE size;
	static int cursor_x = 20;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
		CreateCaret(hWnd, NULL, 2, 15);
		ShowCaret(hWnd);
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		Rectangle(hdc, 10, 750, 210, 780);
		g_Manager.DrawPaint(hWnd);
		GetTextExtentPoint(hdc, str, count, &size);
		SetCaretPos(size.cx + cursor_x, 755);
		TextOut(hdc, cursor_x, 755, str, count);
		if(g_Manager.GetFigureKind() == 0) TextOut(hdc, 250, 250, "도형이 없습니다. 도형을 생성해주세요!", strlen("도형이 없습니다. 도형을 생성해주세요!"));
		EndPaint(hWnd, &ps);
		break;
	case WM_CHAR:
		hdc = GetDC(hWnd);
		if (wParam == '=') g_Manager.SetThickness(1);
		else if (wParam == '-') g_Manager.SetThinness(1);
		else if (wParam == VK_SPACE) str[count++] = ' ';
		else if ((wParam == VK_BACK) && (count > 0)) count--;
		else if (wParam == VK_RETURN)
		{
			g_Manager.SetFigure(str);
			count = 0;
		}
		else
		{
			if ((wParam != VK_BACK) && (wParam != VK_RETURN))
			{
				str[count++] = (TCHAR)wParam;
				str[count] = '\0';
			}
		}
		InvalidateRect(hWnd, NULL, TRUE);
		ReleaseDC(hWnd, hdc);
		break;
	case WM_KEYDOWN:
		g_Manager.MoveFigure(hWnd, wParam);
		break;
	case WM_TIMER:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}